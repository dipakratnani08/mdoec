        
        <script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/jquery-3.4.1.min.js')) }}"></script>
		<script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/main.js')) }}"></script>
		<script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/bootstrap.min.js')) }}"></script>
		<script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/jquery.counterup.min.js')) }}"></script>
		<script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/jquery.fancybox.pack.js')) }}"></script>
		<script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/jquery.magnific-popup.min.js')) }}"></script>
		<script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/owl.carousel.min.js')) }}"></script>
	   	<script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/slick.min.js')) }}"></script>
		<script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/typed.min.js')) }}"></script>
		<script type="text/javascript" src="{{ url (asset('/public/sitetheme/js/waypoints.min.js')) }}"></script>
<!--End of Tawk.to Script-->
        @yield('AdditionalVendorScriptsInclude')