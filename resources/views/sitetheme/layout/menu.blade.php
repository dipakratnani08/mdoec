<!-- header area start -->
<header id="header" class="transparent-header">
    <div class="topheader top_header_light hidemobile">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                      <div class="address-icon">We can change your life! Call us today <a href="tel:+91 1142271060">+91 1142271060</a>,<a href="tel:+91 8527661060">+91 8527661060</a></div> </div>
                <!-- <div class="col-lg-5 text-right">
                    <div class="custom-page-top">
                        <a href="#">Login</a><a href="#">Register</a></div>

                    <div class="social_top_header">
                        <a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a><a href="#"><i class="fab fa-pinterest-p" aria-hidden="true"></i></a><a href="#"><i class="fab fa-behance" aria-hidden="true"></i></a><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a>                   
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <!-- #navigation start -->
    <nav class="navbar navbar-default navbar-expand-md navbar-light" id="navigation" data-offset-top="1">
        <!-- .container -->
        <div class="container">
            <!-- Logo and Menu -->
            <div class="navbar-header">
                <div class="navbar-brand"><a href="{{ url('/')}}"><img src="{{ url (asset('/public/sitetheme/images/main_logo.png')) }}" alt="Logo"/></a></div>
                <!-- site logo -->
            </div>
            <!-- Menu Toogle -->
                <div class="burger-icon">
                    <div class="bar1"></div>
                    <div class="bar2"></div>
                    <div class="bar3"></div>
                </div>
            <div class="collapse navbar-collapse " id="navbarCollapse">
                <ul class="nav navbar-nav ml-auto">
                    <!-- Menu Link -->
                    <li class="{{ Request::is('/') ? 'active' : ''}}"><a href="{{ url('/')}}">Home</a></li>
                    <li class="{{ Request::is('aboutus') ? 'active' : ''}}"><a href="{{ url('/aboutus')}}">About Us</a></li>
                    <li class="{{ Request::is('study-in-australia') ? 'active' : ''}} {{ Request::is('study-in-canada') ? 'active' : ''}} {{ Request::is('study-in-uk') ? 'active' : ''}} {{ Request::is('study-in-ireland') ? 'active' : ''}} {{ Request::is('study-in-newzealand') ? 'active' : ''}} subnav">
                        <a href="javascript:void(0);">Study Destination</a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('study-in-australia') ? 'active' : ''}}"><a href="{{ url('/study-in-australia')}}" class="nav-link ">Study In Australia</a></li>
                            <li class="{{ Request::is('study-in-canada') ? 'active' : ''}}"><a href="{{ url('/study-in-canada')}}" class="nav-link ">Study In Canada</a></li>
                            <li class="{{ Request::is('study-in-uk') ? 'active' : ''}}"><a href="{{ url('/study-in-uk')}}" class="nav-link ">Study In UK</a></li>
                            <li class="{{ Request::is('study-in-ireland') ? 'active' : ''}}"><a href="{{ url('/study-in-ireland')}}" class="nav-link ">Study In Ireland</a></li>
                            <li class="{{ Request::is('study-in-newzealand') ? 'active' : ''}}"><a href="{{ url('/study-in-newzealand')}}" class="nav-link ">Study In New Zealand</a></li>
                        </ul>
                    </li>
                    <li class="{{ Request::is('career-counselling') ? 'active' : ''}} {{ Request::is('application-assistance') ? 'active' : ''}} {{ Request::is('visa-application-assistance') ? 'active' : ''}} {{ Request::is('accommodation') ? 'active' : ''}} {{ Request::is('pre-departure-support') ? 'active' : ''}} subnav">
                        <a href="javascript:void(0);">Our Services</a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('career-counselling') ? 'active' : ''}}"><a href="{{ url('/career-counselling')}}">Career Counselling</a></li>
                            <li class="{{ Request::is('application-assistance') ? 'active' : ''}}"><a href="{{ url('/application-assistance')}}" class="nav-link ">Application Assistance</a></li>
                            <li class="{{ Request::is('visa-application-assistance') ? 'active' : ''}}"><a href="{{ url('/visa-application-assistance')}}" class="nav-link ">Visa Application Assistance</a></li>
                            <li class="{{ Request::is('accommodation') ? 'active' : ''}}"><a href="{{ url('/accommodation')}}" class="nav-link ">Accommodation</a></li>
                            <li class="{{ Request::is('pre-departure-support') ? 'active' : ''}}"><a href="{{ url('/pre-departure-support')}}" class="nav-link ">Pre-Departure Support</a></li>
                            <li class="{{ Request::is('travel') ? 'active' : ''}}"><a href="{{ url('/travel')}}" class="nav-link ">Travel</a></li>
                            <li class="{{ Request::is('forex') ? 'active' : ''}}"><a href="{{ url('/forex')}}" class="nav-link ">Forex</a></li>
                        </ul>
                    </li>
                    <li class="{{ Request::is('career-counselling') ? 'active' : ''}} {{ Request::is('application-assistance') ? 'active' : ''}} {{ Request::is('visa-application-assistance') ? 'active' : ''}} {{ Request::is('accommodation') ? 'active' : ''}} {{ Request::is('pre-departure-support') ? 'active' : ''}} subnav">
                        <a href="javascript:void(0);">Popular Courses</a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('architecture') ? 'active' : ''}}"><a href="{{ url('/architecture')}}">Architecture & Interior Design</a></li>
                            <li class="{{ Request::is('art') ? 'active' : ''}}"><a href="{{ url('/art')}}" class="nav-link ">Art & Humanities</a></li>
                            <li class="{{ Request::is('business-commerce') ? 'active' : ''}}"><a href="{{ url('/business-commerce')}}" class="nav-link ">Business Commerce & Management</a></li>
                            <li class="{{ Request::is('computing') ? 'active' : ''}}"><a href="{{ url('/computing')}}" class="nav-link ">Computing & IT</a></li>
                            <li class="{{ Request::is('engineering') ? 'active' : ''}}"><a href="{{ url('/engineering')}}" class="nav-link ">Engineering</a></li>
                            <li class="{{ Request::is('hospitality') ? 'active' : ''}}"><a href="{{ url('/hospitality')}}" class="nav-link ">Hospitality and Tourism</a></li>
                            <li class="{{ Request::is('health') ? 'active' : ''}}"><a href="{{ url('/health')}}" class="nav-link ">Health & Medicine</a></li>
                            <li class="{{ Request::is('law') ? 'active' : ''}}"><a href="{{ url('/law')}}" class="nav-link ">Law</a></li>
                        </ul>
                    </li>
                    <li class="{{ Request::is('aboutus') ? 'contact' : ''}}"><a href="{{ url('/contact')}}">Contact</a></li>
                </ul>
                <div class="header-cta">
                    <a href="{{ url('/appointment')}}" class="btn btn-1c">Enqiry</a>
                </div>
            </div>
            <!-- Menu Toogle end -->
        </div>
        <!-- .container end -->
    </nav>
    <!-- #navigation end -->
</header>
<!-- end header area -->



<!-- <section id="header" class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div id="logo" class="logo float-left">
							<a href="{{ url('/')}}" title="Sahastrar Dham">
								<img src="{{ url (asset('/public/sitetheme/images/logo.jpg')) }}">
							</a>
						</div>
						<div class="nav-wrap">
							<div class="btn-menu">
						        <span></span>
						    </div>
							<div id="mainnav" class="mainnav">
								<ul class="menu">
									<li class="{{ Request::is('/') ? 'active' : ''}} has-menu-mega">
										<a href="{{ url('/')}}" title="">
											Home
										</a>
									</li>
									<li class="{{ Request::is('aboutproject') ? 'active' : ''}} {{ Request::is('paymentsuccess/*') ? 'active' : ''}} {{ Request::is('paymentfail/*') ? 'active' : ''}} has-submenu">
										<a href="{{ url('/aboutproject')}}" title="">
											About Project
										</a>
									</li>
                                                                        <li class="{{ Request::is('healthcentre') ? 'active' : ''}} {{ Request::is('roomdetails') ? 'active' : ''}} {{ Request::is('roombooking') ? 'active' : ''}} {{ Request::is('roombookingsuccess/*') ? 'active' : ''}} {{ Request::is('roombookingfail/*') ? 'active' : ''}} {{ Request::is('dormitorydetails') ? 'active' : ''}} {{ Request::is('dormitorybooking') ? 'active' : ''}} has-submenu">
										<a href="{{ url('/healthcentre')}}" title="">
											Health Centre
										</a>
									</li>
									<li class="{{ Request::is('school') ? 'active' : ''}} has-submenu">
										<a href="{{ url('/school')}}" title="">
											School
										</a>
									</li>
									
									<li class="{{ Request::is('meditationhall') ? 'active' : ''}}  has-submenu">
										<a href="{{ url('/meditationhall')}}" title="">
											Meditation Hall
										</a>
									</li>
                                                                        <li class="{{ Request::is('events') ? 'active' : ''}} {{ Request::is('eventdetails/*') ? 'active' : ''}} {{ Request::is('eventregsuccess/*') ? 'active' : ''}} {{ Request::is('eventregfail/*') ? 'active' : ''}} has-submenu">
										<a href="{{ url('/events')}}" title="">
											Events
										</a>
									</li>
                                                                        <li class="{{ Request::is('contact') ? 'active' : ''}} has-submenu">
										<a href="{{ url('/contact')}}" title="">
											Contact Us
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->