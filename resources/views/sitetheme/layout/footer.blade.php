<footer id="footer">
    <div class="footer-top">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-sm-6"><!-- footer widget -->
                    <div class="f-widget-title">
                        <h4>Multi-Destination Overseas Education Consultants Pvt. Ltd.</h4>
                    </div>
                    <div class="sigle-address">
                        <div class="address-icon">
                            <i class="fas fa-home"></i>
                        </div>
                        <p>1101, 11th floor Chiranjiv tower, <br>
                            Nehru Place, New Delhi -110019
                        </p>
                    </div>
                    <div class="sigle-address">
                        <div class="address-icon">
                            <i class="far fa-envelope-open"></i>
                        </div>
                        <p>info@mdoec.in</p>
                    </div>
                    <div class="sigle-address">
                        <div class="address-icon">
                            <i class="fas fa-headphones"></i>
                        </div>
                        <p>+91 8527661060</p>
                    </div>
                </div><!-- footer widget -->
                <div class="col-xl-3 offset-xl-1 col-lg-2 col-sm-6"><!-- footer widget -->
                    <div class="f-widget-title">
                        <h4>Study Destination</h4>
                    </div>
                    <div class="f-widget-link">
                        <ul>
                            <!-- Menu Link -->
                            <li><a href="{{ url('/study-in-australia')}}">Study In Australia</a></li>
                            <li><a href="{{ url('/study-in-canada')}}">Study In Canada</a></li>
                            <li><a href="{{ url('/study-in-uk')}}">Study In UK</a></li>
                            <li><a href="{{ url('/study-in-ireland')}}">Study In Ireland</a></li>
                            <li><a href="{{ url('/study-in-newzealand')}}">Study In New Zealand</a></li>
                        </ul>
                    </div>
                </div><!-- footer widget -->
                <div class="col-xl-3 offset-xl-1 col-lg-3 col-sm-6"><!-- footer widget -->
                    <div class="f-widget-title">
                        <h4>Quick Link</h4>
                    </div>
                    <div class="f-widget-link">
                        <ul>
                            <!-- Menu Link -->
                            <li><a href="{{ url('/aboutus')}}">About Us</a></li>
                            <li><a href="{{ url('/')}}">Home</a></li>
                            <li><a href="{{ url('/accommodation')}}">Accommodation</a></li>
                            <li><a href="{{ url('/contact')}}">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <!-- footer widget -->
                <!-- <div class="col-xl-3 col-lg-3 col-sm-6">
                    <div class="f-widget-title">
                        <h4><a target="_blank" href="https://www.theidioms.com/in-touch/">Keep in touch</a></h4>
                    </div>

                    <div class="footer-newsletter sigle-address subscribe-form-pt">
                        <p>Subscribe to our newsletter!
                            Stay always in touch!</p>
                        <form class="themeioan-form-newsletter form" action="#">
                            <div class="newslleter-call">
                                <input class="input-text required-field" type="text" placeholder="Your email"
                                       title="Your email"/>
                                <div class="footer-submit">
                                    <input class="newsletter-submit" type="submit" value="Send"/>
                                </div>
                            </div>
                        </form>
                        <span>* Don't worry, we don't spam.</span>
                    </div>

                </div> -->
                <!-- footer widget -->
            </div>
            <!-- to top -->
            <div class="cd-top"><i class="fas fa-level-up-alt"></i></div>
        </div>
    </div>
    <!-- .container end -->

    <!-- #footer bottom start -->
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="copyright">
                    <p>©2020 <?php if (date("Y") != 2020) { echo '-'.date("Y");} ?> MDOEC. All Rights Reserved. 
                        <!-- Thanks to <a href="https://www.theidioms.com" title="Idioms" target="_blank">Idioms</a> -->
                    </p>
                </div>
            </div>
            <!-- <div class="col-sm-6">

                <div class="text-right icon-round-white footer-social mt-25 mb-25">
                    <a href="#" title="Facebook"><i class="fab fa-facebook"></i></a>
                    <a href="#" title="Twitter"><i class="fab fa-twitter"></i></a>
                    <a href="#" title="Instagram"><i class="fab fa-instagram"></i></a>
                    <a href="#" title="Google+"><i class="fab fa-google-plus"></i></a>
                </div>
            </div> -->
        </div>

        <!-- to top -->
        <div class="cd-top">Top</div>

    </div>
    <!-- #footer bottom end -->
</footer>