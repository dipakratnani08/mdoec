<head>
	<!-- Basic Page Needs -->
	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<title>MDOEC @yield('htmlheader_title', 'Your title here')</title>
        <meta name="google-site-verification" content="aYjCreCufbuhC3G__RiZY1j5-hH7Hk8cB3tjxaVaWAo" />
	<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="description" content="Karka - Education Services School Template"/>
    <meta name="keywords" content="Landing Page, Services, Learning"/>
    <meta name="author" content="Ioan Drozd"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141138540-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-141138540-1');
</script>

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Boostrap style -->
	<link rel="stylesheet" type="text/css" href="{{ url (asset('/public/sitetheme/css/bootstrap.min.css')) }}">

	<!-- Theme style -->
	<link rel="stylesheet" type="text/css" href="{{ url (asset('/public/sitetheme/css/fontawesome.min.css')) }}">

	<!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ url (asset('/public/sitetheme/css/jquery.fancybox.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ url (asset('/public/sitetheme/css/magnific-popup.css')) }}">

	<!-- Theme style -->
	<link rel="stylesheet" type="text/css" href="{{ url (asset('/public/sitetheme/css/main.css')) }}">

	<!-- Reponsive -->
	<link rel="stylesheet" type="text/css" href="{{ url (asset('/public/sitetheme/css/owl.carousel.min.css')) }}">

	<!-- Colors -->
	<link rel="stylesheet" type="text/css" href="{{ url (asset('/public/sitetheme/css/owl.theme.default.min.css')) }}">
	<link rel="stylesheet" type="text/css" href="{{ url (asset('/public/sitetheme/css/slick.css')) }}">

	<!-- Favicon -->

<link rel="icon" href="{{ url (asset('/public/sitetheme/images/logo.jpg')) }}">
<link href="{{ url (asset('/public/sitetheme/images/favicon_site.png')) }}" rel="shortcut icon" type="image/x-icon">
    @yield('AdditionalVendorCssInclude')
</head>