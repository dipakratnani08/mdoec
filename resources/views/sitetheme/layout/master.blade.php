<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@include('sitetheme.layout.htmlheader')

<body>

	<div class="preloader">
		<div class="dizzy-gillespie"></div>
	</div>

	<!-- <div class="boxed"> -->
    @include('sitetheme.layout.menu')
    @yield('main-content')

    @include('sitetheme.layout.footer')
        <!-- </div> -->
@include('sitetheme.layout.scripts')

</body>
</html>