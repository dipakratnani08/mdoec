@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Study In Canada
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Study In Canada</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Study In Canada</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Why Study in Canada? </h2>
                            <div class="bar"></div>
                            <p>The combination of high quality education at affordable tuition fees and post-study work and immigration options makes this cold country one of the top picks for students aiming international education. Hosting nearly half a million international students, Canada has experienced a huge increase in demand from the Indian Subcontinent.</p>
                             <br>


                            <h5>Quality of Education</h5>
                            <!-- <div class="bar"></div> -->
                            <p>The quality of education and living standards in the Canadian cities are ranked among the highest in the world. Canada’s focus on high academic excellence ensures you’ll be getting the best quality education which would help you accomplish your future endeavors and benefit your career in the long run.</p>
                            <br>


                            <h5>Affordable Education</h5>
                            <!-- <div class="bar"></div> -->
                            <p>The expenditure of living and the tuition fees is much less for international students studying in Canada when compared to those in US or UK. Along with them is the scope of getting part-time job which is better in Canadian cities and hence makes the living even more affordable.</p>
                            <br>

                            <h5>Diversity</h5>
                            <!-- <div class="bar"></div> -->
                            <p>For an international student studying in Canada, one can expect a multicultural environment with a friendly bunch of people. With the diverse representation of the cultures from across the world in Canada, it’s not very difficult to find the ethnic foods and recreational activities related to the specific cultures.</p>
                            <br>

                            <h5>Immigration possibilities</h5>
                            <!-- <div class="bar"></div> -->
                            <p>International graduates from the Canadian universities can work upto 3 years post completion of their education under the Post-Graduate Work Permit Program. This would also help the students to apply for a Permanent Residency (PR) in the future.</p>
                            <br>

                            <h5>Variety of research opportunities</h5>
                            <!-- <div class="bar"></div> -->
                            <p>Major Canadian Universities are research based university where extensive focus of is done on research based work so the student would have ample of opportunities to learn while trying your hands at the research work of your specialization.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
