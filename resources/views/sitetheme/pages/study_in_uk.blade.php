@extends('sitetheme.layout.master')

@section('htmlheader_title')
| About
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Study In UK</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Study In UK</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Study In UK</h2>
                            <div class="bar"></div>
                            <p>The United Kingdom, being the ideal destination for millions of international students for decades has an expertise in a wide range of areas like Engineering, Sciences, Arts & Humanities, Business Management, Law & Finance.
                            </p>


                            <h5>Why UK? </h5>
                            <!-- <div class="bar"></div> -->
                            <p>A degree from a university of UK is respected world wide and considered to be top class quality education. The UK’s universities have a reputation of fostering great minds across multiple disciplines and even has 2nd highest number of Noble Price winners in the world.</p>
                            <p>Studying in UK has its own benefits ranging from cultural and social benefits to financial advantages as the courses are shorter but more intense to help you get an early kick start in your careers. These universities even have a range of programs designed to provide discounts to the students and even enable students to access cheap public transport and study essentials.</p>
                            <p>There are numerous opportunities offered to international students from the UK universities to avail scholarships, grants, financial aids and bursaries.</p>
                            <p>A wide variety of work opportunities as part time and full-time jobs helps cope up with the finance management.</p>
                            <br>


                            <h5>Education System</h5>
                            <!-- <div class="bar"></div> -->
                            <p>Within the United Kingdom, responsibility for education is delegated to each of the four jurisdictions: England, Northern Ireland, Scotland and Wales. There are differences between the four countries in the educational systems in place, the qualifications offered, and how these are regulated. The differences are most pronounced in general and secondary education. As a result, there are several different qualifications and credit frameworks.</p>
                            <br>


                            <h5>Qualifications Framework</h5>
                            <!-- <div class="bar"></div> -->
                            <p>There are two parallel frameworks for higher education qualifications of UK degree-awarding bodies, one that applies to Scotland and one that operates in the rest of the UK.</p>
                            <p>These are:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>The framework for Higher Education Qualifications of degree Awarding Bodies in England, Wales and Northern Ireland (FHEQ).</li>
                                <li><i class="fas fa-check-circle"></i>The framework for qualification of higher education institution in Scotland (FQHEIS).</li>
                            </ul> <br>
                            <p>These frameworks are the principal national reference points for academic standards in UK higher education. There are 160 universities and colleges in the UK that are permitted to award a wide variety of degrees to suit most educational aspirations.</p>
                            <br>

                            <h5>Fees and scholarship</h5>
                            <!-- <div class="bar"></div> -->
                            <p>For undergraduates, the fees range from £10,000 a year to £18,000 a year depending upon the course and institution. For post graduate students, the fees vary according to the type of the course they choose.</p>
                            <p>Scholarships, grants, bursaries, fellowships, financial awards, loans – there are many financial support options for international students who wish to study in a UK course. Demand for scholarships is always greater than supply; to maximise your chances, you should apply as early as you can.</p>
                            <p>A great starting point for students to research potential support options is Study UK. Students should also review the particular websites for the schools and universities they are interested in. The right time to look for financial support is well in advance of the start of the course. It can be very hard to find funding, particularly midway through the academic year.</p>
                            <br>

                            <h5>Living Expenses</h5>
                            <!-- <div class="bar"></div> -->
                            <p>Life abroad is very different from what is offered in our native land. Every student needs to lead a healthy and happy life in the UK, so keeping a check on your daily expenses always help you lead a better financial led life.</p>
                            <p>The generally suggested expense of living:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>In London is £1300 (apprx)</li>
                                <li><i class="fas fa-check-circle"></i>Outside of London is £1000 (apprx)</li>
                            </ul> <br>
                            <br>

                            <h5>After Graduation</h5>
                            <!-- <div class="bar"></div> -->
                            <p>Many international students want to stay in the UK after they graduate and put the skills they have learnt into practice. Students may be able to extend their stay if they meet the requirements for the schemes that the UK Government operates.</p>
                            <p>The three most common visas available to international students are the Tier 1 (Graduate Entrepreneur), Tier 2 (General) and Tier 5 (Temporary Worker).</p>
                            <br>


                            <h5>Visa Requirements</h5>
                            <!-- <div class="bar"></div> -->
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Schedule a counselling session with our expert to guide you through the selection of the course and the university of your choice.</li>
                                <li><i class="fas fa-check-circle"></i>Make a structured deadline chart for the university to fulfill the application requirement.</li>

                                <li><i class="fas fa-check-circle"></i>Submitting the application well within the time limit to ensure a fair chance at scholarship.</li>
                                <li><i class="fas fa-check-circle"></i>Crafting an impressive SOP with the help of our team to ensure highest chances of selection at your preferred university.</li>
                                <li><i class="fas fa-check-circle"></i>Thorough preparation for the interview rounds as per university requirements and rigorous training with our trained officials to tackle the interview properly.</li>
                                <li><i class="fas fa-check-circle"></i>Receiving and accepting of the offer letter from the university.</li>
                                <li><i class="fas fa-check-circle"></i>Preparation of the required bank documents and deposit the fees within the mentioned time limits to confirm admission.</li>
                                <li><i class="fas fa-check-circle"></i>Schedule counselling sessions with our experts for visa interviews.</li>
                                <li><i class="fas fa-check-circle"></i>Applying for Tier 4 (General) student visa with proper completion of the formalities.</li>
                                <li><i class="fas fa-check-circle"></i>Attend the Pre-Study workshop to thoroughly prepare you to study in UK!</li>
                            </ul>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->
</main>
@endsection
