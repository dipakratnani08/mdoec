@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Engineering
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Engineering</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Engineering</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Engineering</h2>
                            <div class="bar"></div>
                            <p>
                                Are you fascinated about knowing the functioning of the everything? If yes, Engineering is the field for you.
                            </p>
                            <p>
                                A qualification in engineering is your key to unlock an exciting career. Engineers work across vast range of fields and plays a vital role in building the world we live in starting from the medical equipment to the industrial establishment of factories.
                            </p>
                            <p>
                                Engineering gives you an edge over the other courses and change your perspective about how you look at the world and helps you get a kick start in life.
                            </p>
                            <br>
                            <p>
                                Few of the Engineering fields to explore:
                            </p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Electrical Engineering</li>
                                <li><i class="fas fa-check-circle"></i>Aerospace Engineering</li>
                                <li><i class="fas fa-check-circle"></i>Civil Engineering</li>
                                <li><i class="fas fa-check-circle"></i>Mechanical Engineering</li>
                                <li><i class="fas fa-check-circle"></i>Computer Engineering</li>
                                <li><i class="fas fa-check-circle"></i>Biomedical Engineering</li>
                                <li><i class="fas fa-check-circle"></i>Geological Engineering</li>
                                <li><i class="fas fa-check-circle"></i>Chemical and Process Engineering</li>
                                <li><i class="fas fa-check-circle"></i>Marine Engineering</li>
                            </ul>
                            <p>
                                Everyone has problems, Engineers have solutions!
                            </p>
                            <br>
                            <p>
                                Irrespective of the field you choose to work in, you would require to build a practical approach to find the solutions to the world’s biggest problems. 
                            </p>

                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Build futuristic robots for human assistance by using your mechanical and software engineering skills</li>

                                <li><i class="fas fa-check-circle"></i>Construct major infrastructure projects to help the community run smoothly</li>

                                <li><i class="fas fa-check-circle"></i>Lead the way by changing the approach</li>

                                <li><i class="fas fa-check-circle"></i>Efficient usage of the chemicals and controlling the pollution created by the industries can help lead to a better environment and hence justifying your chemical engineering skills.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
