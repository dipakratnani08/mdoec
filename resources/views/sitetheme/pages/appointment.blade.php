@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Contact
@endsection
@section ('AdditionalVendorCssInclude')
<style>
    label.error {
    color: #ff0000;
}

.alert.alert-info {
    background-color: #00cae3;
    color: #fff;
}
.alert.alert-success {
    background-color: #55b559;
    color: #fff;
}
.alert.alert-danger {
    background-color: #f55145;
    color: #fff;
}
.alert.alert-warning {
    background-color: #ff9e0f;
    color: #fff;
}
.alert {
    border: 0;
    border-radius: 0;
    padding: 20px 15px;
    line-height: 20px;
    position: relative;
}
.alert .alert-icon {
    display: block;
    float: left;
    margin-right: 1.071rem;
}
.close:not(:disabled):not(.disabled) {
    cursor: pointer;
}
.alert .close {
    color: #fff;
    text-shadow: none;
    opacity: .9;
}
button.close {
    padding: 0;
    background-color: transparent;
    border: 0;
    -webkit-appearance: none;
}
.alert b {
    font-weight: 500;
    text-transform: uppercase;
    font-size: 12px;
}
</style>
<link href="{{ url (asset('/public/sitetheme/datetimepicker/bootstrap-datetimepicker.css')) }}" rel="stylesheet">

@endsection
@section('AdditionalVendorScriptsInclude')
<!-- <script src="{{ url (asset('/public/sitetheme/datetimepicker/moment-with-locales.js')) }}"></script> -->
<script src="{{ url (asset('/public/sitetheme/javascript/jquery-validate.js')) }}"></script>
<script src="{{ url (asset('/public/sitetheme/datetimepicker/bootstrap-datetimepicker.js')) }}"></script>
<script type="text/javascript">
    $("#contactform").validate({
        rules:{
            first_name: {
                required: true
            },
            mobile_no: {
                required: true,
                number: true
            },
            email: {
                required: true,
                email: true
            },
            destination: {
                required: true
            },
            intake: {
                required: true
            },
            appointment_datetime: {
                required: true
            },
            city: {
                required: true
            }
        },
        messages:{
            first_name: {
                required: 'Please enter your first name'
            },
            mobile_no: {
                required: 'Please enter your mobile number'
            },
            email: {
                required: 'Please enter your email'
            },
            destination: {
                required: 'Please select your preferred destination'
            },
            intake: {
                required: 'Please select your preferred intake'
            },
            appointment_datetime: {
                required: 'Please select your appointment date-time'
            },
            city: {
                required: 'Please enter your city'
            }
        }
    });
    
    $(document).ready(function() {
                /*BEgin : Hide  message*/
                $('.alert-success,.alert-danger').delay(5000).fadeOut('fast');
                /*End : Hide  message*/
});


    $(function () {
             $('#appointment_datetime').datetimepicker();
         });
</script>
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Enquiry</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Enquiry</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- contact area start -->
    <div id="contact" class="wrap-bg">
        <div class="container">
            @if(session('success_message'))    
                <div class="alert alert-success"><a class="close" aria-hidden="true" data-dismiss="alert" href="javascript:void(0);"><i class="fa fa-close"></i></a>{{session('success_message')}}</div>
                @endif
                @if(session('error_message'))    
                <div class="alert alert-danger"><a class="close" aria-hidden="true" data-dismiss="alert" href="javascript:void(0);"><i class="fa fa-close"></i></a>{{session('error_message')}}</div>
                @endif
            <div class="row justify-content-center text-center">
                <div class="col-lg-8">
                    <div class="section-title with-p">
                        <h2>Book your appointment</h2>
                        <div class="bar"></div>
                        <p>We are waiting for you on our office in New Delhi, book your appointment via the form below your idea. We are here to answer any question.
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-lg-6">
                    {!! Form::open(array('url' => '/saveappintment','id' => 'contactform', 'files' => true)) !!}
                        <!-- Change Placeholder and Title -->
                        <div>
                            <input class="input-text required-field" type="text" name="first_name" id="first_name"
                                   placeholder="First Name" autocomplete="off" />
                        </div>
                        <div>
                            <input class="input-text" type="text" name="last_name" id="last_name"
                                   placeholder="Last Name" autocomplete="off"/>
                        </div>
                        <div>
                            <input class="input-text required-field" type="text" name="mobile_no" id="mobile_no"
                                   placeholder="Mobile Number" autocomplete="off"/>
                        </div>
                        <div>
                            <input class="input-text required-field email-field" type="email" name="email"
                                   id="email" placeholder="Email" autocomplete="off"/>
                        </div>
                        <div>
                            <select class="form-control" name="destination" id="destination">
                                <option value="">Preferred Destination</option>
                                <option value="Australia">Australia</option>
                                <option value="Canada">Canada</option>
                                <option value="UK">UK</option>
                                <option value="Ireland">Ireland</option>
                                <option value="New Zealand">New Zealand</option>
                            </select>
                        </div>
                        <br>
                        <div>
                            <select class="form-control required-field" name="intake" id="intake">
                                <option value="">Preferred Intake</option>
                                <option value="Jan-Mar">Jan-Mar</option>
                                <option value="Apr-Jun">Apr-Jun</option>
                                <option value="Jul-Sept">Jul-Sept</option>
                                <option value="Oct-Dec">Oct-Dec</option>
                            </select>
                        </div>
                        <div>
                            <input class="input-text" type="text" name="appointment_datetime" id="appointment_datetime" placeholder="Appointment Date-Time" readonly/>
                        </div>
                        <div>
                            <input class="input-text required-field" type="text" name="city" id="city"
                                   placeholder="City" autocomplete="off"/>
                        </div>
                        <input class="color-two button" type="submit" value="Book Now"/>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="dreidbgleft">
                        <img src="{{ url (asset('/public/sitetheme/images/content/landing/contact.jpg')) }}" alt="Buy this Course">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- contact area end -->
</main>
@endsection
