@extends('sitetheme.layout.master')

@section('htmlheader_title')
| About
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Forex</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Forex</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Forex</h2>
                            <div class="bar"></div>
                            <p>The value of currency keeps on fluctuating on a daily basis and that has a immense effect on the financial management of the student too while travelling abroad. At MDOEC, we provide guidance at Forex, we provide guidance at Demand Draft, International Debit Card, Travelling Cheques, Wire Transfers and exchange of currency. We have an association with the local currency exchange providing companies and provide assistance to the students to get the best possible deal at the cheapest price.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->
</main>
@endsection
