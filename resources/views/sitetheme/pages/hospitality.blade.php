@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Hospitality and Tourism
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Hospitality and Tourism</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Hospitality and Tourism</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Hospitality and Tourism</h2>
                            <div class="bar"></div>
                            <p>
                                This industry is all about the best experiences for the customers be it food, accommodation or complementary services like massage etc.
                            </p>
                            <p>
                                You will get the best facility to practice your skills hands-on and develop yourself as a true candidate of the industry at the end of this course by learning from the experts of this field.
                            </p>
                            <p>
                                Your happiness lies in the happiness of others.
                            </p>
                            <p>
                                With a qualification from a leading school, you will build an incredible employability prospects and gain the practical experience you require to build a successful career.
                            </p>
                            <br>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Your love for food and experimenting with unique flavour combinations would be fruitful for this industry</li>
                                <li><i class="fas fa-check-circle"></i>Learning Hospitality management and you could find yourself running a luxury hotel in an exotic destination</li>
                                <li><i class="fas fa-check-circle"></i>Become an essential part of the retail business and drive sales across the board.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
