@extends('sitetheme.layout.master')

@section('htmlheader_title')
| About
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Accommodation</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Accommodation</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->
        <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Accommodation</h2>
                            <div class="bar"></div>
                            <p>One of the most worrying topics after finalisation of your plan to study abroad is your accommodation facilities. It’s all about choosing the right type of house you would want to step in to give you the feeling of your Abode for the duration of your education in this new country. 
                            </p>
                            <p>Below given are a few types of accommodation facility types enlisted:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Studio Apartment: High rise apartment booked by individuals and usually non sharing type</li>
                                <li><i class="fas fa-check-circle"></i>Shared Accommodations: Space shared by 3-5 people with common kitchen and washroom</li>
                                <li><i class="fas fa-check-circle"></i>University Accommodation: Hostel accommodation available inside university premises.</li>
                                <li><i class="fas fa-check-circle"></i>Private accommodations: Local house of residents given on rental basis on contracts.</li>
                                <li><i class="fas fa-check-circle"></i>Homestays: Shared accommodations with local residents of the country who rent a spare room.</li>
                                <li><i class="fas fa-check-circle"></i>Other types: Various other options are Hotels, Motels, Air BnB which are usually short duration accommodation opting until you don’t find a permanent one.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->
</main>
@endsection
