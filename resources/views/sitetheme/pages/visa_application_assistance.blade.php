@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Visa Application Assistance
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Visa Application Assistance</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Visa Application Assistance</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Visa Application Assistance</h2>
                            <div class="bar"></div>
                            <p>Applying for the visa seems to be a tedious process, but don’t worry, we’ve got you covered. Our team of experts to guide you through the details of the application process for the visa.
                            </p>

                            <br>

                            <h5>When to apply? </h5>
                            <p>Visa application should begin right you have received the confirmation of your enrollment in your chosen school or university.</p>
                            <p>It’s always advised to apply as early as possible, regardless of the time for the course commencement, as visa processing takes time.</p>
                            <br>


                            <h5>How to apply? </h5>
                            <p>There are several steps involved in completion of your student visa application. The order of the steps and how to complete the application is a subjective issue which varies on the basis of the chosen study destination.</p>


                            <p>Generally required document for all the Visas:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Proof of enrolment in a recognized institution</li>
                                <li><i class="fas fa-check-circle"></i>Proof of your capacity to cover the cost of air fares, tuitions fee and living cost for the duration of the stay</li>
                                <li><i class="fas fa-check-circle"></i>Passport valid for at least 6 months beyond the completion of you study period.</li>
                                <li><i class="fas fa-check-circle"></i>Evidence of English proficiency would be required along with a medical exam and/or police checks.</li>
                            </ul> <br>


                            <h5>Where do we come in?</h5>
                            <!-- <div class="bar"></div> -->
                            <p>Our counsellor would guide you with the list of required documents to be provided. All documents need to be scanned and certified and certified translation services may be required which would be provided by our expert.</p>
                            <p>As the visa requirements can change quickly, we ensure you have the access to the official websites and can cross verify to get the latest updates regarding the application.</p>
                            <p>We will provide you with the latest updates on the visa requirements. For a hassle-free experience of getting the visas, we even provide with translation and couriering.</p><br>


                            <h5>Requirements for UK Visa</h5>
                            <div class="bar"></div>
                            <p>Everything you need to know about UK Visa</p>
                            <p>The visa you need will depend on your age and the type of study you want to do. An overview of study visa types and application requirements is included below, but for more information you should visit the <b><a href="https://www.gov.uk/browse/visas-immigration/work-visas" target="_blank" style="color:blue;">Gov.UK website</a></b></p>
                            <br>
                            <p>To be eligible for a 4 tier Student Visa, you must:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Have an unconditional offer of a plane on a course with a licensed tier 4 sponsor</li>
                                <li><i class="fas fa-check-circle"></i>Be able to speak read write and understand English</li>
                                <li><i class="fas fa-check-circle"></i>Have enough money to support yourself and pay for your course</li>
                            </ul><br>

                            <h5>Apply for Visa Online</h5>
                            <!-- <div class="bar"></div> -->
                            <p>You must apply for your student visa application online using the British Government’s official <b><a href="https://www.gov.uk/" target="_blank" style="color:blue;">Gov.UK</a></b> website (unless you are applying from North Korea, in which case you can use a hardcopy form).</p>
                            <p>You can apply up to three months before your intended date of travel to the UK. Your application date is the date when your application fee is paid</p><br>

                            <p>When applying for tier 4 Visa, you would generally need:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>A current passport or other valid travel documentation. </li>
                                <li><i class="fas fa-check-circle"></i>Evidence of funds to provide your living expenses for the duration of your course</li>
                                <li><i class="fas fa-check-circle"></i>Confirmation of Acceptance for Studies (CAS) reference number and documents used to obtain CAS</li>
                                <li><i class="fas fa-check-circle"></i>Passport sized coloured photograph</li>
                                <li><i class="fas fa-check-circle"></i>Tuberculosis screening (if required)</li>
                                <li><i class="fas fa-check-circle"></i>Assessment documentation</li>
                                <li><i class="fas fa-check-circle"></i>Academic Technology Approval Scheme (ATAS) clearance certificate.</li>
                            </ul><br>


                            <p>Additional documents may also be required if you are:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Not a citizen of the country you are applying from.</li>
                                <li><i class="fas fa-check-circle"></i>Under 18 years of age</li>
                                <li><i class="fas fa-check-circle"></i>Have family (Dependents)</li>
                            </ul><br>

                            <p>If any of your supporting documents is not in English, you will need to have it translated. Each translated document must be dated, and include the translator's name and signature and confirmation that it is an accurate translation of the original document.</p><br>

                            <h5>English Language requirement</h5>
                            <!-- <div class="bar"></div> -->
                            <p>You must provide evidence of your knowledge of the English language when you apply for your visa. This usually means passing a secure English language test.</p>
                            <p>UK Visas and Immigration (UKVI) have minimum English language requirements for reading, writing, listening and speaking according to your level of study. Your education institution (sponsor) may have different English language requirements.</p>
                            <p>There is a chance you may be interviewed by a UKVI officer as part of the visa application process. If you cannot hold a simple conversation without an interpreter, you may be refused entry to the UK, regardless of your English language results.</p>
                            <br>





                            <h5>Requirements for Australia Visa</h5>
                            <div class="bar"></div>
                            <p>We can explain the visa process for you to make it a little easier. Our counsellors have been through the process many times before with students like you, so a few pointers from someone with experience may save you many hours and worries.</p>
                            <p>The counsellor can walk through the student visa application process and the relevant information on the Australian government’s website. They can help you prepare the necessary documents for your submission and help you prepare for your visa interview.</p>
                            <p>You are required to lodge your application for a visa at least 12 weeks before the orientation date at your institution.</p>
                            <br>
                            <p>Document required for the Visa application:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Visa application form (which you education counsellor will help you complete)</li>
                                <li><i class="fas fa-check-circle"></i>Visa application fee</li>
                                <li><i class="fas fa-check-circle"></i>Four passport sized photographs</li>
                                <li><i class="fas fa-check-circle"></i>Valid passport</li>
                                <li><i class="fas fa-check-circle"></i>Electronic confirmation enrolment</li>
                                <li><i class="fas fa-check-circle"></i>IELTS result (Your counsellor can book you place in an English test)</li>
                                <li><i class="fas fa-check-circle"></i>Academic and work experience documents</li>
                                <li><i class="fas fa-check-circle"></i>Statement of purpose</li>
                                <li><i class="fas fa-check-circle"></i>Evidence of financial ability (tuition fees, living expenses, expenses for dependents, return airfare)</li>
                            </ul>
                            <p><em style="color: red;display: inline-block;font-style: normal;margin-left: 2px;vertical-align: middle; font-size: 20pt;">*</em> Detailed list of documents would be given by the counsellor pertaining to the visa requirements. Your application will be allocated a unique Transaction Reference Number (TRN) by the Australian Government. You can find out about the status of your <b><a href="http://www.homeaffairs.gov.au/" target="_blank" style="color:blue;">application online</a></b>.</p>
                            <br>




                            <h5>Requirements for Canadian Visa</h5>
                            <div class="bar"></div>
                            <p>Often referred to as the Study permit, the student visa for the Canadian universities is a vital part before you leave the country for your studies.</p>
                            <p>As the study permit conditions can change without prior information so its always advised to get the update information from Immigration and Refugees and Citizenship Canada (IRCC)<b><a href="https://www.canada.ca/en/immigration-refugees-citizenship.html" target="_blank" style="color:blue;"> https://www.canada.ca/en/immigration-refugees-citizenship.html</a></b></p>
                            <p>Any help required in finding the required information regarding IRCC, our counsellor would guide you through the same step-by-step and would also help you find a n authorised immigration representative for a hassle free application experience.
                            </p>
                            <br>



                            <h5>Requirements for New Zealand Visa</h5>
                            <div class="bar"></div>
                            <p>The visa you need will depend on the type of course you choose, and how long you want to stay in New Zealand. While studying in New Zealand, it’s important to maintain your Fee-Paying student status. This status relates to the purpose, or reason why you came to New Zealand.</p>

                            <p>Documentation</p>
                            <p>To apply for a visa, you must have the following documentation ready:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>A valid passport which must be valis for at least three months after your period of stay in New Zealand</li>
                                <li><i class="fas fa-check-circle"></i>-    A letter of acceptance from a New Zealand education provider states the minimum course duration, total tuition fee and whether the tuition fee is in domestic and foreign currency. The course must be approved by the New Zealand Qualifications Authority.</li>
                            </ul>
                            <p>Additional documents required :</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Academic preparation documents such as transcripts, diplomas, degrees or certificates.</li>
                                <li><i class="fas fa-check-circle"></i>Current application form- Student Visa application form</li>
                                <li><i class="fas fa-check-circle"></i>Correct Visa application fee</li>
                                <li><i class="fas fa-check-circle"></i>Tuition fee receipt showing payment to date.</li>
                                <li><i class="fas fa-check-circle"></i>Evidence that you have sufficient funds to cover your living expenses throughout the periods of your stay. You will need to show you have NZ $15,000 for a full yeah of study or NZ $ 1,250/month</li>
                            </ul>
                            <br>
                            <p>This may include:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Bank statements.</li>
                                <li><i class="fas fa-check-circle"></i>Financial undertaking by a sponsor to cover accommodation and living costs.</li>
                                <li><i class="fas fa-check-circle"></i>Scholarship programs.</li>
                                <li><i class="fas fa-check-circle"></i>Evidence that you are leaving New Zealand after the completion of your course. This can be in the form of flight tickets; however, it is optional.</li>
                                <li><i class="fas fa-check-circle"></i>A police certificate if you are aged 17 years or over and plan to study for more than 24 months. A police certificate is a document which is used as evidence of good character.</li>
                                <li><i class="fas fa-check-circle"></i>An X-ray certificate - you will need to have a chest x-ray if you are staying in New Zealand for more than six months, or if you’re a citizen of a country with a relatively high incidence of tuberculosis, or if you’ve spent more than three months in the last five years in a country with a relatively high incidence of tuberculosis.</li>
                                <li><i class="fas fa-check-circle"></i>Full medical examination</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
