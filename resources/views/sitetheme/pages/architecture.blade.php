@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Architecture & Interior Design
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Architecture & Interior Design</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Architecture & Interior Design</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Architecture & Interior Design</h2>
                            <div class="bar"></div>
                            <p>
                                Passionate about designing the building you plan to live in, this might be the perfect course for you.
                            </p>
                            <p>
                                The architectural industry is a highly-regarded profession and these top-notch international institutions will equip you with the required skills to be amongst the leading architectural industry.
                            </p>
                            <p>
                                You learn the perspective of looking at a structure as a combination of design and dimensions and grow an aesthetic point of view to look at any structure built. Prior to the completion of your course you would be provided with an opportunity to work on an actual project as an intern to put your theoretical learning into practice.
                            </p>
                            <br>
                            <center><h4>
                                Design buildings to build a society. 
                            </h4></center>
                            <p>
                                The design field is rapidly changing and one can expand their scope of working on a huge range of projects in various cities and sites across the world.
                            </p><br>

                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Design home that people love to live in.</li>

                                <li><i class="fas fa-check-circle"></i>Establish your name in the architectural industry by building Marvelous Monuments to Stunning Skyscrapers, Museums to Apartments, Malls to Parks.</li>

                                <li><i class="fas fa-check-circle"></i>Craft an affordable living space for the low-income group of the society.</li>

                                <li><i class="fas fa-check-circle"></i>You might end up working on the construction of major building of the city to building airports, stadiums, hospitals, monuments etc.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
