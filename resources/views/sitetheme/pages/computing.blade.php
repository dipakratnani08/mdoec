@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Computing & IT
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Computing & IT</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Computing & IT</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Computing & IT</h2>
                            <div class="bar"></div>
                            <p>
                                Wish to bring the next revolution in the IT industry?
                            </p>
                            <p>
                                The key factor of expansion of our global outreach is the upstanding IT and software support. This is relied upon for almost all the task which we need to carry out in our professional as well as personal world.
                            </p><br>
                            <p>
                                Various IT fields to choose from:
                            </p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Software Design and Development</li>
                                <li><i class="fas fa-check-circle"></i>Computer Systems & Networks</li>
                                <li><i class="fas fa-check-circle"></i>User Experience Design</li>
                                <li><i class="fas fa-check-circle"></i>Web Development</li>
                                <li><i class="fas fa-check-circle"></i>Game Development</li>
                                <li><i class="fas fa-check-circle"></i>IT security Analyst</li>
                                <li><i class="fas fa-check-circle"></i>Artificial Intelligence</li>
                            </ul>
                            <br>
                            <p>
                                If you love to fondle with the computer systems and the     new technology excites you, IT might be the field for you.
                            </p>
                            <p>
                                IT field is highly demanding in the fast-growing world and IT professionals are often rewarded well so building a successful career is assured in IT. As our reliance on the systems is growing, the demand for efficient qualified IT professionals to compensate and fulfill their requirement.
                            </p>

                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Developing a game and writing codes to invent application for user ease to change the way we use the internet.</li>

                                <li><i class="fas fa-check-circle"></i>Solve complex computer problems of people and help them use their machines to its best abilities.</li>

                                <li><i class="fas fa-check-circle"></i>Becoming an IT Analyst to show companies the real potential of efficient usage of the data to transform their business and.</li>

                                <li><i class="fas fa-check-circle"></i>Work with clients to develop websites that help them reach the online platforms and target audience.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
