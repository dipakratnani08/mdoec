@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Study In Ireland
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Study In Ireland</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Study In Ireland</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <p>Willing to study at a university in the midst of scenic green country side associated with great history? Then, Ireland is your dream destination.</p>
                            <p>The universities having one of the world’s best education system, friendly and open-minded people and major cities being easily navigable makes this study destination ne of the best option for students.</p>
                             <br>

                            <h2>Why Ireland? </h2>
                            <div class="bar"></div>
                            <p>Ireland, a small island nation on the westernmost corner of Europe has one of the best education systems of the world. The educational institutes connected globally and the Irish graduates have an access to opportunities in many fields in their careers ahead.</p>
                             <br>


                            <h5>Education</h5>
                            <!-- <div class="bar"></div> -->
                            <p>The education offered at the Irish universities is among the world’s best. Seven of the top-level universities being stationed in Ireland, this is a study hub for students willing to pursue quality education.</p>
                            <br>


                            <h5>Research opportunity</h5>
                            <!-- <div class="bar"></div> -->
                            <p>Irish universities being in the top 1% of the research universities of the world and the homme of major the and pharma companies, Ireland offers the international students unmatched research opportunities to drive their innovative streak and see them skyrocket in their careers ahead.</p>
                            <br>

                            <h5>Career</h5>
                            <!-- <div class="bar"></div> -->
                            <p>The graduates of the Irish universities are exemplified by their ability to not just tackle the problems of today but to sought the solution for tomorrow in their chosen fields.</p>
                            <br>

                            <h5>Cost of living</h5>
                            <!-- <div class="bar"></div> -->
                            <p>Approximate cost of living in Ireland is about €7,000 to €12,000 per year which includes the rent, electricity, food, books, laundry, medicine, as well as social expenses and travel, but excludes the tuition fees.</p>

                            <b><p>The important documents required for the application process:</p></b>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i> Academic Transcripts: Mark Sheets of Standard X, XII, and the bachelor’s degree (if applicable).</li>
                                <li><i class="fas fa-check-circle"></i> Internet-based TOEFL or IELTS scores</li>
                                <li><i class="fas fa-check-circle"></i> If you have work experience then two letters of recommendation (LOR) from the employer/manager whom you have a good rapport with can comment on your professional abilities</li>

                                <li><i class="fas fa-check-circle"></i> Statement of Purpose (SOP)</li>
                                <li><i class="fas fa-check-circle"></i> Academic Resume</li>
                                <li><i class="fas fa-check-circle"></i> Portfolio (in case of students applying for art and design courses or architecture programs)</li>
                                <li><i class="fas fa-check-circle"></i> Others (Certificates/achievements at the state and national level and extracurricular activities)</li>
                                <li><i class="fas fa-check-circle"></i> Proof of funds</li>
                                <li><i class="fas fa-check-circle"></i> Health insurance</li>
                                <li><i class="fas fa-check-circle"></i> A copy of your passport and photocopies of these documents should be translated in English and certified by the educational institution or by notary.</li>
                            </ul> <br>

                            <b><p>Key point to remember:</p></b>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i> Schedule a counselling session with our expert to guide you through the selection of the course and the university of your choice.</li>
                                <li><i class="fas fa-check-circle"></i> Make a structured deadline chart for the university to fulfill the application requirement.</li>
                                <li><i class="fas fa-check-circle"></i> Submitting the application well within the time limit to ensure a fair chance at scholarship.</li>
                                <li><i class="fas fa-check-circle"></i> Crafting an impressive SOP with the help of our team to ensure highest chances of selection at your preferred university.</li>
                                <li><i class="fas fa-check-circle"></i> Thorough preparation for the interview rounds as per university requirements and rigorous training with our trained officials to tackle the interview properly.</li>
                                <li><i class="fas fa-check-circle"></i> Receiving and accepting the offer letter from the university.</li>
                                <li><i class="fas fa-check-circle"></i> Preparation of the required bank documents and deposit the fees within the mentioned time limits to confirm admission.</li>
                                <li><i class="fas fa-check-circle"></i> Schedule counselling sessions with our experts for visa interviews.</li>
                                <li><i class="fas fa-check-circle"></i> Applying for the student visa with proper completion of the formalities.</li>
                                <li><i class="fas fa-check-circle"></i> Attend the Pre-Study workshop to thoroughly prepare you to study in Ireland!</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
