@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Home
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
     <!-- Slider Start -->
    <div class="owl-navigation owl-carousel owl-theme">
        <!-- slider item 1 -->
        <div class="item">
            <div class="image-overly-dark-opacity header-content slider-bg3" role="banner">
                <div class="container opac">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 header-area">
                            <div class="header-area-inner header-text"> <!-- single content header -->
                                <div><span class="subtitle ">Find your <span class="base-color">Mentor</span></span></div>
                                <h1 class="title">We provide
                                    <span id="typed-strings">
                            <span>Coaching</span>
                            <span>Training</span>
                        </span>
                                    <span id="typed" class="underline-large"></span>
                                </h1>
                                <div class="btn-section">
                                    <a href="#" class="color-two btn-custom">Get in Touch <i class="fas fa-arrow-right"></i></a>
                                    <div class="video-relative">
                                        <a href="https://www.youtube.com/watch?v=TKnufs85hXk" class="video-btn popup-video ">
                                            <i class="fas fa-play"></i>
                                            <span class="ripple orangebg"></span>
                                            <span class="ripple orangebg"></span>
                                            <span class="ripple orangebg"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end slider item 3 -->
        <!-- slider item 2 -->
        <div class="item">
            <div class="header-content image-overly-dark-opacity slider-bg10" role="banner">
                <div class="container opac">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 header-area">
                            <div class="header-area-inner header-text"> <!-- single content header -->
                                <span class="welcome">Welcome to MDOEC Consulting</span>
                                <div><span class="subtitle">Change <span class="base-color">Your Life</span></span></div>
                                <h1 class="title">We teach you Online</h1>
                                <div class="btn-section">
                                    <a href="#" class="color-one btn-custom">Get in Touch <i class="fas fa-arrow-right"></i></a>
                                    <div class="video-relative">
                                        <a href="https://www.youtube.com/watch?v=TKnufs85hXk" class="video-btn popup-video ">
                                            <i class="fas fa-play"></i>
                                            <span class="ripple orangebg"></span>
                                            <span class="ripple orangebg"></span>
                                            <span class="ripple orangebg"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end slider item 1 -->
        <!-- slider item 3 -->
        <div class="item">
            <div class="position-center image-overly-dark-opacity header-content slider-bg1" role="banner">
                <div class="container opac">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 header-area">
                            <div class="header-area-inner header-text"> <!-- single content header -->
                                <div><span class="subtitle ">Change <span class="base-color">Your Life</span></span></div>
                                <h1 class="title">We provide Consulting</h1>
                                <div class="btn-section">
                                    <a href="#" class="color-two btn-custom">Get in Touch <i class="fas fa-arrow-right"></i></a>
                                    <div class="video-relative">
                                        <a href="https://www.youtube.com/watch?v=TKnufs85hXk" class="video-btn popup-video ">
                                            <i class="fas fa-play"></i>
                                            <span class="ripple orangebg"></span>
                                            <span class="ripple orangebg"></span>
                                            <span class="ripple orangebg"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end slider item 2 -->
    </div>
    <!-- Slider End-->


    <!-- services two area start -->
    <div id="services-two" class="wrap-bg wrap-bg-light text-left">
        <!-- .container -->
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="section-title section-text-left">
                        <div>
                            <h5>Welcome To</h5>
                            <h2>MDOEC</h2>
                            <div class="bar"></div>
                            <p>We are the key link you were missing to put your wheels of your dreams in motion. We at MDOEC having a team of experts in the field of overseas education are here you to guide you through ever step of the way to make your plans of studying abroad, a reality.</p>

                            <p>Our aim is to focus on the requirements enlisted by the students to accomplish their goal of receiving a world class education and we ensure to deliver the best service in our might to help them make their journeys a walk in the park. When it comes to Visas, our team scrutinize every aspect of the application keeping in mind their achievements, qualifications to make it the best version according to the pre-set norms of the concerned authorities.</p>

                            <p>Having tie-ups with major leading universities across globe to give a wide variety of options to choose your study destination like Australia, Canada, UK, New Zealand & Ireland helps the students get a broader perspective while narrowing down their options.</p>
                            <div class="mt-25 mb-50">
                                <a href="#" class="color-two btn-custom ">Start Today <i class="fas fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="min-h-350px services_image services_bg1">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="min-h-350px services_image services_bg2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services two area end -->



    <!-- services area start -->
    <!-- <div id="services" class="wrap-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="section-title section-text-left text-left">
                        <div>
                            <h5>Welcome To</h5>
                            <h2>MDOEC</h2>
                            <div class="bar"></div>
                            <p>We are the key link you were missing to put your wheels of your dreams in motion. We at MDOEC having a team of experts in the field of overseas education are here you to guide you through ever step of the way to make your plans of studying abroad, a reality.</p>

                            <p>Our aim is to focus on the requirements enlisted by the students to accomplish their goal of receiving a world class education and we ensure to deliver the best service in our might to help them make their journeys a walk in the park. When it comes to Visas, our team scrutinize every aspect of the application keeping in mind their achievements, qualifications to make it the best version according to the pre-set norms of the concerned authorities.</p>

                            <p>Having tie-ups with major leading universities across globe to give a wide variety of options to choose your study destination like Australia, Canada, UK, New Zealand & Ireland helps the students get a broader perspective while narrowing down their options.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="dreidbgleft">
                        <img src="{{ url (asset('/public/sitetheme/images/content/landing/about.jpg')) }}" alt="Buy this Course">
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- services area end -->

    <!-- why-us area start -->
    <div id="why-us-white">
        <div class="why-us-container why-us-left-bg5">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-5 col-xl-6 col-lg-6">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-7 col-xl-6 col-lg-6 text-left">
                        <div class="white-box-large">
                            <div class="section-title">
                                <div>
                                    <h3>Why MDOEC?</h3>
                                    <div class="bar"></div>
                                </div>
                            </div>
                            <p>We, at MDOEC, work hand in hand with the students to give them out-and-out details pertaining to their requirements by providing them with regular counselling sessions, helping them in course selection, completing their application process for the desired institution in the country of their choice and assisting them in fulfilling their visa formalities to make their journey hassle free and smooth. The opportunity of direct interaction with a university representative to get proper guidance when it comes to the institution assures a different level of satisfaction to the students and gives better clarity at fulfilling their dreams.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- why-us area end -->
</main>
@endsection
