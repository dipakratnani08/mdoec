@extends('sitetheme.layout.master')

@section('htmlheader_title')
| About
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>About Company</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">About</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->
    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h3>About Us</h3>
                            <div class="bar"></div>
                            <p>I would like to take this opportunity to introduce our organization Multi-Destination Overseas Education Consultants, an International student recruitment organization based in New Delhi India. We have started our operations in Year 2020 with our first office in the capital city of India- New Delhi and gradually expanding its wings in and around the country.</p>
                            <p>MDOEC focuses on Client satisfaction from day one and keeping the best interest at heart we are compliant with the needs and the wants of the students choosing to study overseas with us keeping in check the service quality we offer. When it comes to visas, a thorough analysis of the client’s profile is carried out, taken into consideration  their qualifications and achievements and a complete verification with the criteria set by the educational authorities, thus ensuring a hassle-free & successful visa application process for the student.</p>


                            <h4>MISSION AND VISION</h4>
                            <div class="bar"></div>
                            <p>We as an organization aim towards betterment of ourselves  as we grow and provide finest possible services that cater to the requirements of the students eyeing education abroad whilst maintaining highest possible quality standards. Besides education, MDOEC aims to offer a wide variety of services, catering to almost all international student needs and requirements for their smooth transition in to the new world like Accommodation assistance, On-Arrival assistance and other Value-added Services.</p>
                            <p>With a strong reference base and a team of leading industry experts, our primary focus remains on student recruitment. Therefore, establishing intimate relationship with education providers and living up to the recruitment standards is one of the principal goals of MDOEC. We believe that it is only by establishing and strengthening relationships harmoniously, our team can do justice to our goal of helping the High-Quality aspirational students to attain a Quality education.</p>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="dreidbgleft">
                        <img src="{{ url (asset('/public/sitetheme/images/content/landing/about.jpg')) }}" alt="Buy this Course">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="section-title section-text-left text-left">
                        <div>

                            <h4>VALUES</h4>
                            <div class="bar"></div>
                            <p>We at MDOEC are committed to quality over quantity and hence, conduct business operations in the most ethical manner possible. Every client is given advice that will assist him/her to the best of their interest.</p>
                            <p>It is our clients who would help us attain the success and therefore, we continuously strive to better ourselves to live up to and better the standards we have created for ourselves.</p>
                            <br>
                            <h5>Our Motto and Mission</h5>
                            <div class="bar"></div>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i> Ethical counselling.</li>
                                <li><i class="fas fa-check-circle"></i> To provide trustworthy education consultancy services to qualified international students with genuine interest in pursuing a global education.</li>
                            </ul><br>

                            <h5>Our Office Infrastructure</h5>
                            <div class="bar"></div>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i> Centrally located and easily accessible by public transport</li>
                                <li><i class="fas fa-check-circle"></i> World class infrastructure</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->
</main>
@endsection
