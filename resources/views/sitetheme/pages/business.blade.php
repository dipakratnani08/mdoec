@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Business Commerce & Management
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Business Commerce & Management</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Business Commerce & Management</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Business Commerce & Management</h2>
                            <div class="bar"></div>
                            <p>
                                Be the leader you wish to follow
                            </p>
                            <p>
                                Qualification in business and management can take you places without any upper limit. You would graduate with job ready skills and gain practical experience you would require to take on ground-breaking roles with some of the    world’s most exciting companies or become a leading entrepreneur of tomorrow.
                            </p>
                            <p>
                                Few of the skills you’ll learn here are effective team management, financial order, handle supply and demand chain etc.
                            </p><br>
                            <p>
                                Some subjects of your choice you could explore:
                            </p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Business</li>
                                <li><i class="fas fa-check-circle"></i>Commerce</li>
                                <li><i class="fas fa-check-circle"></i>Accounting</li>
                                <li><i class="fas fa-check-circle"></i>Economic</li>
                                <li><i class="fas fa-check-circle"></i>Marketing</li>
                                <li><i class="fas fa-check-circle"></i>Human resources</li>
                                <li><i class="fas fa-check-circle"></i>Logistics</li>
                            </ul>
                            <br>
                            <p>
                                Polishing your skills and establishing strong connections across industry takes you long way in life.
                            </p>
                            <p>
                                Leading a career in business you will find yourself work everywhere. These programs are designed to give you qualifications required to succeed with a backing of the innovative internships and placement offers give you the best shot at your career success.
                            </p>
                            <br>

                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Working at a major organization, leading teams and creating unique business solutions.</li>

                                <li><i class="fas fa-check-circle"></i>Be your own boss and own an empire in your chosen field.</li>

                                <li><i class="fas fa-check-circle"></i>Develop your creative and marketing skills to help companies achieve their goals and reach out to their target audience in the most unique manner.</li>

                                <li><i class="fas fa-check-circle"></i>Working across industries to build your career as no two careers in business are the same.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
