@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Law
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Law</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Law</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Law</h2>
                            <div class="bar"></div>
                            <p>
                                The one who dances on the edge with the judicial system.
                            </p>
                            <p>
                                To be a successful lawyer, you need to have a good in-depth knowledge about the law and getting an international education in law would open doors for you wherever you go.
                            </p>
                            <p>
                                Law programs generally teach the knowledge and skills to practice law professionally, but often combined with a general outline of subjects to provide rounded education pertaining to the law.
                            </p>
                            <br>
                            <p>
                                Some subjects which can be studied in this field:
                            </p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Law and practical legal training</li>
                                <li><i class="fas fa-check-circle"></i>Politics</li>
                                <li><i class="fas fa-check-circle"></i>Journalism</li>
                                <li><i class="fas fa-check-circle"></i>Business</li>
                                <li><i class="fas fa-check-circle"></i>Economics</li>
                                <li><i class="fas fa-check-circle"></i>Crime & justice</li>
                            </ul>
                            <br>

                            <p>
                                One gets an all-round qualification which helps you sought the problem with an ‘out-of-the-box’ approach.
                            </p><br>


                            <p>
                                Law teaches you skills which can be implemented across many fields.
                            </p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Become a solicitor and work in a major firm</li>
                                <li><i class="fas fa-check-circle"></i>Blend law and social work and help resolve conflict as a professional mediator.</li>
                                <li><i class="fas fa-check-circle"></i>Work as an in-house council for companies and protect their interest</li>
                                <li><i class="fas fa-check-circle"></i>Write essential legislation and liaise with politicians and govt to develop laws that benefit society.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
