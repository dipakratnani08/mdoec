@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Contact
@endsection
@section ('AdditionalVendorCssInclude')
<style>
    label.error {
    color: #ff0000;
}

.alert.alert-info {
    background-color: #00cae3;
    color: #fff;
}
.alert.alert-success {
    background-color: #55b559;
    color: #fff;
}
.alert.alert-danger {
    background-color: #f55145;
    color: #fff;
}
.alert.alert-warning {
    background-color: #ff9e0f;
    color: #fff;
}
.alert {
    border: 0;
    border-radius: 0;
    padding: 20px 15px;
    line-height: 20px;
    position: relative;
}
.alert .alert-icon {
    display: block;
    float: left;
    margin-right: 1.071rem;
}
.close:not(:disabled):not(.disabled) {
    cursor: pointer;
}
.alert .close {
    color: #fff;
    text-shadow: none;
    opacity: .9;
}
button.close {
    padding: 0;
    background-color: transparent;
    border: 0;
    -webkit-appearance: none;
}
.alert b {
    font-weight: 500;
    text-transform: uppercase;
    font-size: 12px;
}
</style>

@endsection
@section('AdditionalVendorScriptsInclude')
<script src="{{ url (asset('/public/sitetheme/javascript/jquery-validate.js')) }}"></script>
<script type="text/javascript">
    $("#contactform").validate({
        rules:{
            first_name: {
                required: true
            },
            mobile_no: {
                required: true,
                number: true
            },
            email: {
                required: true,
                email: true
            },
            destination: {
                required: true
            },
            intake: {
                required: true
            },
            city: {
                required: true
            }
        },
        messages:{
            first_name: {
                required: 'Please enter your first name'
            },
            mobile_no: {
                required: 'Please enter your mobile number'
            },
            email: {
                required: 'Please enter your email'
            },
            destination: {
                required: 'Please select your preferred destination'
            },
            intake: {
                required: 'Please select your preferred intake'
            },
            city: {
                required: 'Please enter your city'
            }
        }
    });
    
    $(document).ready(function() {
                /*BEgin : Hide  message*/
                $('.alert-success,.alert-danger').delay(5000).fadeOut('fast');
                /*End : Hide  message*/
});
</script>
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Contact</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Contact</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->


    <!-- contact area start -->
    <div id="features" class="wrap-bg wrap-bg-light">
        <!-- .container -->
        <div class="container">
                @if(session('success_message'))    
                <div class="alert alert-success"><a class="close" aria-hidden="true" data-dismiss="alert" href="javascript:void(0);"><i class="fa fa-close"></i></a>{{session('success_message')}}</div>
                @endif
                @if(session('error_message'))    
                <div class="alert alert-danger"><a class="close" aria-hidden="true" data-dismiss="alert" href="javascript:void(0);"><i class="fa fa-close"></i></a>{{session('error_message')}}</div>
                @endif
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <!-- 1 -->
                    <div class="single-features-light"><!-- single features -->
                        <div class="move">
                            <!-- uses solid style -->
                            <i class="secondary-color fas fa-building fa-3x"></i>
                            <h4><a href="#">MDOEC HQ</a></h4>
                            <p>1101, 11th floor Chiranjiv tower, Nehru Place, New Delhi -110019</p>
                            <!-- <div class="feature_link">
                                <a href="#"><i class="fas fa-arrow-right"></i></a>
                            </div> -->
                        </div>
                    </div><!-- end single features -->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <!-- 2 -->
                    <div class="single-features-light"><!-- single features -->
                        <div class="move">
                            <i class="secondary-color fas fa-envelope fa-3x"></i>
                            <h4><a href="#">Mail Us On</a></h4>
                            <p>info@mdoec.in</p>
                            <br>
                            <!-- <div class="feature_link">
                                <a href="#"><i class="fas fa-arrow-right"></i></a>
                            </div> -->
                        </div>
                    </div><!-- end single features -->
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 ">
                    <!-- 3 -->
                    <div class="single-features-light"><!-- single features -->
                        <div class="move">
                            <i class="secondary-color fas fa-mobile fa-3x"></i>
                            <h4><a href="#">Call Us On</a></h4>
                            <p>+91 1142271060, +91 8527661060</p>
                            <br>
                            <!-- <div class="feature_link">
                                <a href="#"><i class="fas fa-arrow-right"></i></a>
                            </div> -->
                        </div>
                    </div><!-- end single features -->
                </div>
            </div>
            <!-- .row end -->
        </div>
        <!-- .container end -->
    </div>
    <!-- contact area end -->

    <!-- contact area start -->
    <div id="contact" class="wrap-bg">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-lg-8">
                    <div class="section-title with-p">
                        <h2>We’re Here To Help You</h2>
                        <div class="bar"></div>
                        <p>We are waiting for you on our office in New Delhi, contact us via the contact form below your idea. We are here to answer any question.
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="dreidbgleft">
                        <img src="{{ url (asset('/public/sitetheme/images/content/landing/contact.jpg')) }}" alt="Buy this Course">
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    {!! Form::open(array('url' => '/savecontactus','id' => 'contactform', 'files' => true)) !!}
                        <!-- Change Placeholder and Title -->
                        <div>
                            <input class="input-text required-field" type="text" name="first_name" id="first_name"
                                   placeholder="First Name" autocomplete="off" />
                        </div>
                        <div>
                            <input class="input-text" type="text" name="last_name" id="last_name"
                                   placeholder="Last Name" autocomplete="off"/>
                        </div>
                        <div>
                            <input class="input-text required-field" type="text" name="mobile_no" id="mobile_no"
                                   placeholder="Mobile Number" autocomplete="off"/>
                        </div>
                        <div>
                            <input class="input-text required-field email-field" type="email" name="email"
                                   id="email" placeholder="Email" autocomplete="off"/>
                        </div>
                        <div>
                            <select class="form-control" name="destination" id="destination">
                                <option value="">Preferred Destination</option>
                                <option value="Australia">Australia</option>
                                <option value="Canada">Canada</option>
                                <option value="UK">UK</option>
                                <option value="Ireland">Ireland</option>
                                <option value="New Zealand">New Zealand</option>
                            </select>
                        </div>
                        <br>
                        <div>
                            <select class="form-control required-field" name="intake" id="intake">
                                <option value="">Preferred Intake</option>
                                <option value="Jan-Mar">Jan-Mar</option>
                                <option value="Apr-Jun">Apr-Jun</option>
                                <option value="Jul-Sept">Jul-Sept</option>
                                <option value="Oct-Dec">Oct-Dec</option>
                            </select>
                        </div>
                        <div>
                            <input class="input-text required-field" type="text" name="city" id="city"
                                   placeholder="City" autocomplete="off"/>
                        </div>
                        <input class="color-two button" type="submit" value="Send Message"/>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- contact area end -->
</main>
@endsection
