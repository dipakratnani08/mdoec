@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Pre-Departure Support
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Pre-Departure Support</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Pre-Departure Support</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->


    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Pre-Departure Support</h2>
                            <div class="bar"></div>
                            <p>A new life in a new city is very exciting but equally challenging and that’s why we have a fully dedicated team who looks after the queries of all the students regarding the whereabout of the country they are planning to spend their next few years. 
                            </p>
                            <p>Pre-Departure orientation Sessions would help the student get a better insight towards the internal functioning of the place they have chosen to study.
                            </p>

                            <br>


                            <p>Important topics like:</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Insights in the local culture</li>
                                <li><i class="fas fa-check-circle"></i>Student life of the new country</li>
                                <li><i class="fas fa-check-circle"></i>Support and wellbeing facilities</li>
                                <li><i class="fas fa-check-circle"></i>Banking and foreign exchange</li>
                                <li><i class="fas fa-check-circle"></i>Part-time jobs and internship opportunities during the academic session</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->


</main>
@endsection
