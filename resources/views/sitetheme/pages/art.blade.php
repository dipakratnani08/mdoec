@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Art & Humanities
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Art & Humanities</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Art & Humanities</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Art & Humanities</h2>
                            <div class="bar"></div>
                            <p>
                                Learning about the social sciences is a great way to lead your life with a solid foundation about the dynamics of the society.
                            </p>
                            <p>
                                If you are interested in knowing the functioning of our world, pursuing a course in this field will give you a skill set which would help you understand your place in it whilst preparing for employment for it.
                            </p>
                            <p>
                                Learning about History throughout the ages, our language system and how they developed, communication across this world and the various techniques in which our community interacts are some of the things taught in these courses.
                            </p>
                            <p>
                                Few course opportunities you could explore:
                            </p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>International Studies</li>
                                <li><i class="fas fa-check-circle"></i>Media & Journalism</li>
                                <li><i class="fas fa-check-circle"></i>History</li>
                                <li><i class="fas fa-check-circle"></i>Arts</li>
                                <li><i class="fas fa-check-circle"></i>Sociology</li>
                                <li><i class="fas fa-check-circle"></i>Philosophy</li>
                                <li><i class="fas fa-check-circle"></i>Languages</li>
                                <li><i class="fas fa-check-circle"></i>Politics</li>
                                <li><i class="fas fa-check-circle"></i>Music</li>
                            </ul>
                            <br>
                            <center><h4>
                                Learning about the heart of the society
                            </h4></center>
                            <p>
                                These subjects give you a scope of freedom to project your brain on to the society through art, communication, critical thinking. By learning these subjects, you would develop skills that will prepare you for life in the ‘Real World’.
                            </p><br>

                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Learn about history & culture and know your place in the world and society with history, politics and international studies.</li>

                                <li><i class="fas fa-check-circle"></i>Develop your designing skills to create inspiring artworks or melodious music if you choose to become an artist or musician.</li>

                                <li><i class="fas fa-check-circle"></i>Break the stereotype of TRP inflicted journalism and serve the world with real-world news to give the quality journalism.</li>

                                <li><i class="fas fa-check-circle"></i>You could solve real-world problems by working as psychologist and improving the well-being of the society.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
