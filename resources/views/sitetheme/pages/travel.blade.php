@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Travel
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Travel</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Travel</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->


    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Travel</h2>
                            <div class="bar"></div>
                            <p>Travelling is often associated with excitement, but when you are travelling on a one-way ticket, away from your loved ones, at times it invokes anxiety too. At MDOEC, we understand the position of the student as well as their parents and hence make sure to make both their journey a smooth and comfortable affair. Please be assured, when talking about travelling, it’s not just the air tickets from native land to the foreign land, it is travel from your current home to your new home, i.e. the transportation from the airport to your place of residency.
                            </p>
                            <p>We at MDOEC leave no stone unturned to make the student’s journey to their native land to the new home of their choice as hassle free as possible. Time to time information about the travel tips and assistance with the airline tickets are few of the services we provide to help the student transition better.
                            </p>
                            <p>MDOEC takes the responsibility of booking flight tickets well in advance to get preferred travel dates & routes at an affordable price. Our assistance extends for the students to even identify the best airlines for heavy baggage schemes and fetch the best available offers for student travel as offered by many airlines.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->
</main>
@endsection
