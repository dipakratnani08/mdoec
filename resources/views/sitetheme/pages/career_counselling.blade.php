@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Career Counselling
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Career Counselling</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Career Counselling</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->


    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Career Counselling</h2>
                            <div class="bar"></div>
                            <p>When we have more options to choose from, it often becomes difficult to choose the one best for you, but don’t you worry, our team of experts would conduct a career counselling session and talk you through the whole process to help you find the best suitable course for you to pursue your masters of your choice. 
                            </p>
                            <p>The major steps involved in narrowing down our targeted course and university is defining your study goals and our experts, many of whom have themselves been international students would very easily be able to answer all your queries regarding the shortlisting of the university/course.
                            </p>
                            <p>After finalising the course and the university of your choice comes the vital part of filling the application at the university of your choice and our experts would be assisting you all along to help you fill the application in the best possible manner.
                            </p>
                            <p>We would send your application to the universities and help get a session fixed with one of the representatives of the university who would assess the application and determine your eligibility for the course.
                            </p>
                            <p>Further, a Response would be received from the university/institution. Acceptance of the offer letter and payments details could be discussed thereafter.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->
</main>
@endsection
