@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Study In Australia
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Study In Australia</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Study In Australia</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->
    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Why Study in Australia? </h2>
                            <div class="bar"></div>
                            <p>Eyeing further education in your career, planning to bag a world-class degree, Australia has a wide variety of opportunities. Whether you plan on enhancing your technical skills by pursuing your masters in engineering, or improve your language scope by choosing an Language course or opt for humanities program or polish your skills by pursuing MBA, Australia is difficult to beat the quality of education being offered whilst the standard of living and support for international students.</p>
                            <p>Let’s talk facts,</p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i> Education abroad is mainly chosen on the merits of the Post-work opportunities offered by the universities and the Australian universities offers outstanding work opportunities post completion of the degree.</li>
                                <li><i class="fas fa-check-circle"></i> The next biggest concern of every aspiring student eyeing the education abroad is the living expenses during the course and Australia being an English speaking country makes it easier to get part time jobs to meet out their expenses for rent, groceries and other bills too.</li>
                                <li><i class="fas fa-check-circle"></i> Another area of concern for the students aiming education overseas is the scholarships offered at the good universities and as the Australian education sector is on the rise, the scholarships offered at these universities is class apart and eases the burden of the tuition fees without compromising the quality of the education offered.</li>
                            </ul> <br>


                            <h5>Quality of life</h5>
                            <!-- <div class="bar"></div> -->
                            <p>The 2nd best country in the world only after Norway, Australia has class apart infrastructure, dynamic range of public transport, world-leading healthcare services, numerous student services and relatively affordable lifestyle which makes it a paradise to study abroad. </p>
                            <p>The capital cities of this Island Continent like Melbourne Sydney, Canberra, Adelaide and Brisbane are ranked in the top 30 best cities for students in terms of cost of living, employment scopes and student community.</p>

                            <p>On top of it all, the Australian government is very supportive to the international student and thus offer many incentivized jobs to those looking to study in their country, including some $200 million worth scholarships, post work visa in various fields, numerous research opportunities and even a chance to gain permanent residency status after completion of your academic course.</p><br>


                            <h5>Key points to fulfill your dream of studying Australia:</h5>
                            <!-- <div class="bar"></div> -->
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i> Schedule a counselling session with our expert to guide you through the selection of the course and the university of your choice.</li>
                                <li><i class="fas fa-check-circle"></i>	Make a structured deadline chart for the university to fulfill the application requirement.</li>
                                <li><i class="fas fa-check-circle"></i> Submitting the application well within the time limit to ensure a fair chance at scholarship.</li>

                                <li><i class="fas fa-check-circle"></i> Crafting an impressive SOP with the help of our team to ensure highest chances of selection at your preferred university.</li>
                                <li><i class="fas fa-check-circle"></i> Thorough preparation for the interview rounds as per university requirements and rigorous training with our trained officials to tackle the interview properly.</li>
                                <li><i class="fas fa-check-circle"></i> Receival (en. Aus) and acceptance of the offer letter from the university.</li>
                                <li><i class="fas fa-check-circle"></i> Preparation of the required bank documents and deposit the fees within the mentioned time limits to confirm admission.</li>
                                <li><i class="fas fa-check-circle"></i> Schedule counselling sessions with our experts for visa interviews.</li>
                                <li><i class="fas fa-check-circle"></i> Applying for the student visa with proper completion of the formalities.</li>
                                <li><i class="fas fa-check-circle"></i> Attend the Pre-Study workshop to thoroughly prepare you to study in Australia!</li>
                            </ul><br>


                            <h5>Facilities at the university:</h5>
                            <!-- <div class="bar"></div> -->
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>	Major Australian universities being research based universities gives them a nick over the other universities around the world in terms of state-of-the-art laboratories, equipment, ever-helping faculties at the university campus, the humongous libraries and most advanced technologies helping the student in whatever they require assistance makes the experience of learning at these universities truly World-Class.</li>
                                <li><i class="fas fa-check-circle"></i> The educators at the Australian universities believe in delivering Practical and hands-on knowledge which would help them shape up their careers in the future and make them confident graduates with the appropriate skill set as required by their employers. The diversity of the class of each course pertaining to the students admitted by the universities gives their fellow mates a better grip at learning things from many different perspectives which in turn prepares them for the longer run to tackle problems in their professional lives ahead.</li>

                                <li><i class="fas fa-check-circle"></i> The multiplicity of the faculty hired at these universities from across the globe makes it easier for the student to always step out of their comfort zone to understand the learning from the professor and hence makes them better at handling the worldly problems posed at them during as well as after their completion of their course.</li>
                                <li><i class="fas fa-check-circle"></i> Thorough preparation for the interview rounds as per university requirements and rigorous training with our trained officials to tackle the interview properly.</li>
                                <li><i class="fas fa-check-circle"></i> Receival (en. Aus) and acceptance of the offer letter from the university.</li>
                                <li><i class="fas fa-check-circle"></i> Preparation of the required bank documents and deposit the fees within the mentioned time limits to confirm admission.</li>
                                <li><i class="fas fa-check-circle"></i> Schedule counselling sessions with our experts for visa interviews.</li>
                                <li><i class="fas fa-check-circle"></i> Applying for the student visa with proper completion of the formalities.</li>
                                <li><i class="fas fa-check-circle"></i> Attend the Pre-Study workshop to thoroughly prepare you to study in Australia!</li>
                            </ul>

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
