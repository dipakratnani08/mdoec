@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Study In New Zealand
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Study In New Zealand</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Study In New Zealand</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <p>Are you aiming to have a world class educational prospect amidst a beautiful environment to study around? New Zealand is your answer!</p>
                            <p>While choosing New Zealand one can always be confident in picking any institution and that would surely meet the highest educational standards.</p>
                             <br>

                            <h2>Why New Zealand? </h2>
                            <div class="bar"></div>
                            <p>New Zealand is an amazing study destination for the international students. Offering World-Class Education, all of its institutions abide by the global standards which are routinely tested and monitored by the government to ensure persistence of the quality of education being offered at their institutes. Their institutes offer a progressive, responsive Educational system combining with their traditional learning methods fused with the innovation and technology makes the globally recognized institute of this country worth the time and effort.</p>
                             <br>


                            <h5>Post-Work Facilities</h5>
                            <!-- <div class="bar"></div> -->
                            <p>New Zealand offers great deal when it comes to working during and after the completion of their academics. The advantage of getting an option of working full time during the holidays and an opportunity to get easy work permit for 6 months post completion of their course.</p>
                            <br>


                            <h5>Education System</h5>
                            <!-- <div class="bar"></div> -->
                            <p>The education system of New Zealand is enormously diverse and offers a wide variety of options to the students aspiring to study form their universities. Influenced by wide scope of employment and increasingly mobile networks of teaching staff and good quality researchers.</p>
                            <p>According to New Zealand’s education system, students can progress through a variety of flexible pathways in the system which is backed by a numerous institutions and qualifications at all levels which are governed to ensure a good quality education and a meaningful qualification for the students.</p>
                            <br>

                            <h5>Teaching Style</h5>
                            <!-- <div class="bar"></div> -->
                            <p>New Zealand offers a very supportive environment for its international students. The number of students per classrooms is often smaller than other western countries, allowing for more personalised attention. As teaching methods are constantly developing, you will experience a wide range of teaching techniques and environments. The support for international students goes even further than the classroom, with the New Zealand Government being the first in the world to create a code of practice that outlines a standard of care for international students both in the classroom and outside of it.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
