@extends('sitetheme.layout.master')

@section('htmlheader_title')
| Health & Medicine
@endsection
@section ('AdditionalVendorCssInclude')

@endsection
@section('AdditionalVendorScriptsInclude')
@endsection

@section('main-content')
<main>
    <!-- breadcrumb banner content area start -->
    <div class="lernen_banner large bg-about">
        <div class="container">
            <div class="row">
                <div class="lernen_banner_title">
                    <h1>Health & Medicine</h1>
                    <div class="lernen_breadcrumb">
                        <div class="breadcrumbs">
                                    <span class="first-item">
                                    <a href="{{ url('/')}}">Homepage</a></span>
                            <span class="separator">&gt;</span>
                            <span class="last-item">Health & Medicine</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end breadcrumb banner content area start -->

    <!-- services area start -->
    <div id="services" class="wrap-bg">
        <!-- .container -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class=" section-text-left text-left">
                        <div>
                            <h2>Health & Medicine</h2>
                            <div class="bar"></div>
                            <p>
                                Mend the people to mend the society.
                            </p>
                            <p>
                                If you’d like to give it back to the community and equip yourself with the skills of knowing the in and out of the functioning of human body, this is your chosen field. The field of health & medicine is demanding but rewarding at the same time. This profession is always in demand and with your polished skill-set you can fulfill the role of caregivers to the community. If you have a very strong inclination towards learning science, you should opt for medicine and if you just wish to give back to the community through caretaking, you should opt for health.
                            </p>
                            <br>
                            <p>
                                Some of the subjects you can study under medicine are: 
                            </p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Surgery</li>
                                <li><i class="fas fa-check-circle"></i>Anesthetics</li>
                                <li><i class="fas fa-check-circle"></i>Pediatrics</li>
                                <li><i class="fas fa-check-circle"></i>Oncology</li>
                                <li><i class="fas fa-check-circle"></i>Emergency Medicine</li>
                                <li><i class="fas fa-check-circle"></i>Community Health</li>
                                <li><i class="fas fa-check-circle"></i>Forensic Pathology</li>
                                <li><i class="fas fa-check-circle"></i>Intensive care Medicine</li>
                                <li><i class="fas fa-check-circle"></i>General practice</li>
                                <li><i class="fas fa-check-circle"></i>Obstetrics</li>
                                <li><i class="fas fa-check-circle"></i>Psychiatry</li>
                                <li><i class="fas fa-check-circle"></i>Radiology</li>
                                <li><i class="fas fa-check-circle"></i>Rural and remote practice</li>
                                <li><i class="fas fa-check-circle"></i>Medical Administration</li>
                            </ul>
                            <br>


                            <p>
                                Some of the subjects you can study under health are:
                            </p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Medical science</li>
                                <li><i class="fas fa-check-circle"></i>Occupational Therapy</li>
                                <li><i class="fas fa-check-circle"></i>Social work</li>
                                <li><i class="fas fa-check-circle"></i>Public Healthcare</li>
                                <li><i class="fas fa-check-circle"></i>Physiotherapy</li>
                                <li><i class="fas fa-check-circle"></i>Optometry</li>
                                <li><i class="fas fa-check-circle"></i>Nursing</li>
                                <li><i class="fas fa-check-circle"></i>Nutrition</li>
                                <li><i class="fas fa-check-circle"></i>Pathology</li>
                            </ul>
                            <br>


                            <p>
                                Medicine is considered to be the noblest of profession.
                            </p>
                            <p>
                                Medicine is a highly sought-after qualification, with varied career options demanding long hours of work but with equally rewarding returns. This field offers wide range of placements at various location, both urban and rural.
                            </p>
                            <ul class="themeioan_ul_icon">
                                <li><i class="fas fa-check-circle"></i>Become a world-renowned surgeon performing delicate operations.</li>
                                <li><i class="fas fa-check-circle"></i>Be at the cutting-edge of research into lifesaving drugs and equipment as a researcher.</li>
                                <li><i class="fas fa-check-circle"></i>Become a social worker or psychologist and promote good health in your community.</li>
                                <li><i class="fas fa-check-circle"></i>Take up nursing and care for those in need in hospitals, community health clinics or individual practices.</li>
                            </ul>
                            <br>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- services area end -->

</main>
@endsection
