@extends('auth.auth')

@section('htmlheader_title')
    MDOEC
@endsection

@section('content')
<body class="login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="javascript:void(0);">
                                 <img src="{{ url (asset('/public/sitetheme/images/main_logo.png')) }}" alt="bkms" />
                               </a>
        </div><!-- /.login-logo -->
    @if(session('message'))
        <div class="alert alert-success alert-dismissible"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>{{session('message')}}</div>  
        @endif
         
        <?php if (isset($error_message)) { ?>
        <div class="col-md-12 col-sm-12 col-xs-12 text-center"><h4 class="h4-verify messageinfo">
        <?= $error_message; ?>
        </h4></div>
    <?php } ?>
					
				

</div><!-- /.login-box -->
</body>
@endsection


