@extends('layouts/master')

@section('RenderHeader')
    <style>
      #fa-spinner {
        color: #e7e7e7;
        font-size: 30px;
      }
    </style>
@endsection

@section('RenderBody')
  <div id="loading" ng-app="loadingApp">
    <div ui-view></div>
    <div style="width: 30px; margin: auto; padding-top: 30px;">
      <span id="fa-spinner" class="glyphicon glyphicon-repeat fa-spin"></span>
    </div>
  </div>
@endsection

@section('AdditionalVendorScriptsInclude')
    <!-- Application Scripts -->
    <script src="/js/www/loginload/app.js"></script>
    <script src="/js/www/loginload/authController.js"></script>
@endsection
