@extends('auth.auth')

@section('htmlheader_title')
    MDOEC
@endsection

<!--Begin: Add CSS For This Page-->
@section ('AdditionalVendorCssInclude')
<style type="text/css">
    #reset_password_form label.error {
    color: #ff0000;
}
    </style>
@endsection
<!--End: Add CSS For This Page-->

@section('content')
<body class="login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="javascript:void(0);">
                                 <img src="{{ url (asset('/public/sitetheme/images/main_logo.png')) }}" alt="mdoec" />
                               </a>
        </div><!-- /.login-logo -->

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if(session('message'))
        <div class="alert alert-success alert-dismissible"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>{{session('message')}}</div>  
        @endif
    <div class="login-box-body">
    <p class="login-box-msg">Change Password</p>
    <form action="{{ url('/password/changepassword') }}" method="post" name="reset_password_form" id="reset_password_form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="email" value="{{$email}}">
        <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="New Password">
        </div>
        <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <input type="password" class="form-control" name="confirmedpassword" id="confirmedpassword" placeholder="Confirm Password">
        </div>
        <div class="row">
            <div class="col-xs-4 pull-right">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
            </div><!-- /.col -->
        </div>
    </form>

</div><!-- /.login-box-body -->

</div><!-- /.login-box -->

    @include('partials.scripts')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
        
        $(function($){
            $(function(){
                /*Begin : Form vAlidation*/
                    $('#reset_password_form').validate({
                    rules: {
                        newpassword: {
                            required: true
                        },
                        confirmedpassword: {
                            equalTo: "#newpassword",
                            required: true
                        },
                    },
                    messages: {
                        newpassword: {
                            required: "New Password is required."
                        },
                        confirmedpassword: {
                            equalTo: "Confirm password does not match the new password.",
                            required: "Confirm Password is required."
                        }
                    },
                });
                    /*End : Form vAlidation*/
            });
      })
    </script>
</body>

@endsection


