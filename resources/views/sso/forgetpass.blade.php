@extends('auth.auth')

@section('htmlheader_title')
    BK Management System
@endsection
<!--Begin: Add CSS For This Page-->
@section ('AdditionalVendorCssInclude')
<style type="text/css">
    #login_form label.error {
    color: #ff0000;
}
    </style>
@endsection
<!--End: Add CSS For This Page-->
@section('content')

<body class="login-page">
    <div class="login-box">
        <div class="login-logo">
                    <a href="javascript:void(0);">
                                 <img src="{{ url (asset('/public/sitetheme/images/main_logo.png')) }}" alt="bkms" />
                               </a>
        </div>
        <!-- /.login-logo -->

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(session('message'))
             <div class="alert alert-success alert-dismissible"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>{{session('message')}}</div>  
          @endif
          @if(session('errormessage'))
             <div class="alert alert-danger alert-dismissible"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>{{session('errormessage')}}</div>  
          @endif
        <div class="login-box-body">
            <p class="login-box-msg">Reset Password</p>
            <form action="{{ url('/password/forgotpassword') }}" method="post" name="login_form" id="login_form">
                <div class="form-group has-feedback">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    <input type="text" class="form-control" placeholder="User Id" name="user_id" value="{{ old('user_id') }}" id="user_id"/>
                </div>
                <div class="form-group has-feedback">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}"/>
                </div>

                
                <div class="col-xs-12 col-sm-12 col-lg-12 text-right">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Send Password Reset Link</button>
                        </div>
            </form>

            <a href="{{ url('/ssologin') }}">Log in</a><br>

        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->

    @include('partials.scripts')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
        
        $(function($){
            $(function(){
                /*Begin : Form vAlidation*/
                    $("#login_form").validate({ 
                        rules: {
                            email : {required: true,email:true},
                            user_id : {required: true},
                        },
                        messages: { 
                            email : {required: "Please Enter Email."},
                            user_id : {required: "Please Enter User Id."},
                       }
                    });
                    /*End : Form Validation*/
            });
      }); 
    </script>
</body>

@endsection
