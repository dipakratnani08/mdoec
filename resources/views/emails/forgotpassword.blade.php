
<!DOCTYPE html>
<html>
    <head>
        <title>MDOEC</title>

        <link href="fonts.googleapis.com/css?family=Helvetica Neue:100" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="cdnjs.cloudflare.com/ajax/libs/jquery-ui-bootstrap/0.5pre/assets/js/jquery-1.9.0.min.js"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <style>

            .table_class{
                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif !important;
            }

        </style>
    </head>
    <body>

        <table width="100%" border="0" class="table_class" cellpadding="0" cellspacing="0" align="center" class="full" object="drag-module" bgcolor="#2a2a2a" c-style="bgColor" style="background-color: #364150 !important;">
            <tbody><tr mc:repeatable="">
                    <td bgcolor="#2a2a2a" c-style="bgColor" align="center" style="background-color: #364150 !important;">
                        <div mc:hideable="">

                            <!-- Mobile Wrapper -->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#2a2a2a" c-style="bgColor" style="background-color: #364150 !important;">
                                <tbody><tr>
                                        <td width="100%" align="center">


                                            <div class="sortable_inner ui-sortable">
                                                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small" bgcolor="#2a2a2a" c-style="bgColor" style="background-color: #ECEEF1 !important;">
                                                    <tbody><tr>
                                                            <td align="center" width="352" valign="middle" bgcolor="#2a2a2a" c-style="bgColor" style="background-color: #364150 !important;">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse: collapse; background-color: #364150 !important;" class="fullCenter" bgcolor="#2a2a2a" c-style="bgColor">
                                                                    <tbody><tr>
                                                                            <td width="100%" height="30"></td>
                                                                        </tr>
                                                                    </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>
                                                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small" bgcolor="#2a2a2a" c-style="bgColor" style="background-color:#364150 !important;">
                                                    <tbody><tr>
                                                            <td align="center" width="352" valign="middle">

                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                    <tbody><tr>
                                                                            <td width="100%" height="50"></td>
                                                                        </tr>
                                                                    </tbody></table>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>

                                                <table width="352" border="0" style="margin-bottom: 10px;" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                                                    <tbody><tr>
                                                            <td align="center" width="352" valign="middle">

                                                                <!-- Header Text --> 
                                                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                <tbody><tr>
                                                                        <td width="100%" align="center" style="text-align: center;"><span object="image-editable"><img editable="true" src="{{ url (asset('/public/sitetheme/images/main_logo.png')) }}" width="" alt="" border="0" mc:edit="10"></span></td>
                                                                    </tr>
                                                                </tbody></table>
                                        </td>
                                    </tr>
                                </tbody></table>

                        </div>
                    </td>
                </tr>
            </tbody></table>

        <table width="392" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
            <tbody><tr>
                    <td align="center" width="20" valign="middle" bgcolor="#2a2a2a" c-style="bgColor" style="background-color: #364150 !important;"></td>
                    <td align="center" width="352" valign="middle">

                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="full" style="border-top-right-radius: 5px; border-top-left-radius: 5px;">
                            <tbody><tr>
                                    <td align="center" width="352" valign="middle" bgcolor="#ECEEF1" style="-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px;">

                                        <div class="sortable_inner ui-sortable">

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140" style="border-top-right-radius: 5px; border-top-left-radius: 5px; background-color: #ECEEF1 !important;" c-style="redBG" object="drag-module-small">
                                                <tbody><tr>
                                                        <td align="center" width="352" valign="middle">

                                                            <table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                <tbody><tr>
                                                                        <td width="100%" height="50"></td>
                                                                    </tr>
                                                                </tbody></table>							
                                                        </td>
                                                    </tr>
                                                </tbody></table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140" c-style="redBG" object="drag-module-small" style="background-color: #ECEEF1 !important;">
                                                <tbody><tr>
                                                        <td align="center" width="352" valign="middle" class="image77">
<table width="300" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                    <tbody><tr>
                                                                            <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 22px;line-height: 32px; font-weight: 100;" t-style="whiteTextTop" class="fullCenter" mc:edit="9" object="text-editable">
                                                                    <multiline><!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]--> Hello {{ $user }},
<!--                                                                            {{ $user }},[if !mso]><!</span><![endif] -->

                                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgbold', Helvetica; font-weight: normal;"><!--<![endif]--><!--[if !mso]><!--></span><!--<![endif]--></multiline>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>						
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140" c-style="redBG" object="drag-module-small" style="background-color: #ECEEF1 !important;">
                                                <tbody><tr>
                                                        <td align="center" width="352" valign="middle">

                                                            <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">

                                                                <tbody><tr>
                                                                        <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 48px; color: #ffffff; line-height: 44px; font-weight: bold;" t-style="whiteTextBody" class="fullCenter" mc:edit="11" object="text-editable">
                                                                            <!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgbold', Helvetica; font-weight: normal;"><!--<![endif]--><singleline><!---msg --></singleline><!--[if !mso]><!--></span><!--<![endif]-->
                                                                        </td>
                                                                    </tr>
                                                                </tbody></table>							
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140" c-style="redBG" object="drag-module-small" style="background-color: #ECEEF1 !important;">
                                                <tbody><tr>
                                                        <td align="center" width="352" valign="middle">

                                                            <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">

                                                                <tbody><tr>
                                                                        <td width="100%" height="30"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>							
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140" c-style="redBG" object="drag-module-small" style="background-color: #ECEEF1 !important;">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" width="352" valign="middle">

                                                            <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                            <tbody>
                                                <tr>
                                                    <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px;" t-style="whiteTextBody" class="fullCenter" mc:edit="14">
                                                        <span object="text-editable"><multiline><!--[if !mso]><!--><span style="text-align: center;font-family: 'proxima_nova_rgregular', Helvetica;font-size: 14px;line-height: 24px;color:black;font-weight: normal;"><!--<![endif]--> We have received a request to reset your password. <!--[if !mso]><!--></span><!--<![endif]--></multiline></span> 
                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_novasemibold', Helvetica; font-weight: normal;"><!--<![endif]--><a href="#" object="link-editable" style="color: #ffffff;" t-style="whiteTextBody"></a><!--[if !mso]><!--></span><!--<![endif]-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px;" t-style="whiteTextBody" class="fullCenter" mc:edit="14">
                                                        <span object="text-editable"><multiline><!--[if !mso]><!--><span style="text-align: center;font-family: 'proxima_nova_rgregular', Helvetica;font-size: 14px;line-height: 24px;color:black;font-weight: normal;"><!--<![endif]--> Please click on the Reset Password button below to reset your password.<!--[if !mso]><!--></span><!--<![endif]--></multiline></span> 

                                                    </td>
                                                </tr>
                                            </tbody></table>						
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140" c-style="redBG" object="drag-module-small" style="background-color: #ECEEF1 !important;">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" width="352" valign="middle">

                                                            <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                                <!----------------- Button Center ----------------->
                                                                <tbody><tr>
                                                                        <td align="center">
                                                                            <table border="0" cellpadding="0" cellspacing="0" align="center"> 
                                                                                <tbody><tr> 
                                                                                        <td align="center" height="45" c-style="buttonBG" bgcolor="#ffffff" style="border-radius: 5px; padding-left: 30px; padding-right: 30px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; color: #364150 !important;background-color: #ECEEF1 !important
;" t-style="buttonText" mc:edit="13">
                                                                                <multiline><!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgbold', Helvetica; font-weight: normal;"><!--<![endif]-->

                                                                                        <a href="<?= $url; ?>/password/changepass?key=<?= $key; ?>&email=<?= $email; ?>" style="color: #fff;background-color: #337ab7;border-color: #2e6da4;background-color: #428bca;margin:0;font:inherit;font-family:inherit;display:inline-block;padding:6px 12px;margin-bottom:0;font-size:14px;font-weight:400;line-height:1.42857143;text-align:center;white-space:nowrap;vertical-align:middle;text-decoration: none;width: auto;margin-bottom: 25px;margin-top:25px;border-radius: 5px;"> Reset Password Button</a>
                                                                                        <a href="" style="color: #2a2a2a; font-size:15px; text-decoration: none; line-height:34px; width:100%;" t-style="buttonText" object="link-editable"></a>
                                                                                       
                                                                                        <!--[if !mso]><!--></span><!--<![endif]--></multiline>
                                                                        </td> 
                                                                    </tr> 
                                                                </tbody>
                                                            </table> 
                                                        </td>
                                                    </tr>
                                                    <!----------------- End Button Center ----------------->
                                                </tbody>
                                             </table>							
                                    </td>
                                </tr>
                            </tbody></table>

                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140" c-style="redBG" object="drag-module-small" style="background-color: #ECEEF1 !important;">
                            <tbody><tr>
                                    <td align="center" width="352" valign="middle">

                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                            <tbody>
                                                <tr>
                                                    <td valign="middle" width="100%" style="text-align: center; font-family: 'proxima_nova_rgregular', Helvetica;color: black; font-size: 14px; line-height: 24px;" t-style="whiteTextBody" class="fullCenter" mc:edit="14">
                                                        <span object="text-editable"><multiline><!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal; color:black;"><!--<![endif]--> If you did not request your password to be reset, then please ignore this email.<!--[if !mso]><!--></span><!--<![endif]--></multiline></span> 
                                                        <!--[if !mso]><!--><span style="font-family: 'proxima_novasemibold', Helvetica; font-weight: normal;"><!--<![endif]--><a href="#" object="link-editable" style="color: #ffffff;" t-style="whiteTextBody"></a><!--[if !mso]><!--></span><!--<![endif]-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; line-height: 24px;" t-style="whiteTextBody" class="fullCenter" mc:edit="14">
                                                        <br><span object="text-editable"><multiline><!--[if !mso]><!--><span style="color: black;font-family: 'proxima_nova_rgregular', Helvetica; font-weight: bold; color:black;"><!--<![endif]--> Thank you!</span><!--<![endif]--></multiline></span> 

                                                    </td>
                                                </tr>
                                            </tbody></table>							
                                    </td>
                                </tr>
                            </tbody></table>

                        <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#e85140" c-style="redBG" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; background-color: #ECEEF1 !important;" object="drag-module-small">
                            <tbody><tr>
                                    <td align="center" width="352" valign="middle">

                                        <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                            <tbody><tr>
                                                    <td width="100%" height="50"></td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        </div>

                    </td>
                </tr>
            </tbody></table>

    </td>
<td align="center" width="20" valign="middle" bgcolor="#2a2a2a" c-style="bgColor" style="background-color: #364150 !important;"></td>
</tr>
</tbody></table>

<!-- Mobile Wrapper -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" bgcolor="#2a2a2a" c-style="bgColor" style="background-color: #364150 !important;">
    <tbody><tr>
            <td width="100%" align="center" bgcolor="#2a2a2a" c-style="bgColor" style="background-color: #364150 !important;">

                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                    <tbody><tr>
                            <td align="center" width="352" valign="middle">

                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                    <tbody><tr>
                                            <td width="100%" height="30" style="color:#ffffff;"><?= date('Y'); ?>  © BAPS Swaminarayan Sanstha </td>
                                        </tr>
                                    </tbody></table>

                            </td>
                        </tr>
                    </tbody></table>

                <table width="352" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" object="drag-module-small">
                    <tbody><tr>
                            <td align="center" width="352" valign="middle">

                                <table width="265" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                    <tbody><tr>
                                            <td width="100%" height="50"></td>
                                        </tr>
                                        <tr>
                                            <td width="100%" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
                                        </tr>
                                    </tbody></table>

                            </td>
                        </tr>
                    </tbody>
                </table>


            </td>
        </tr>
    </tbody>
</table>

</div>
</td>
</tr>
</tbody>
</table>
</body>
</html>
