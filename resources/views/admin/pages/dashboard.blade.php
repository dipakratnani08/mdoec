@extends('admin/layouts/master')

@section ('AdditionalVendorCssInclude')
<style type="text/css">
    .errorlist, .errorlist th, .errorlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }

    .errorlist th, .errorlist td {
        padding: 5px;
        text-align: left;
    }
    .successlist, .successlist th, .successlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }
    .data-table-wrapper .dataTables_wrapper .ucenter
    {
        padding-right: 30px !important;
    }
    .successlist th, .successlist td {
        padding: 5px;
        text-align: left;
    }
    .table_titlecase
    {
        text-transform: capitalize !important;
    }

    .table_titlecase_record
    {
        text-transform: capitalize !important;
    }
    
    @media only screen and (max-width:767px){
        .sidebar-mini , .wrapper
        {
            height: auto !important;
            min-height: 100% !important;
        }
    }
    
    @media only screen and (min-width:768px){
        .sidebar-mini , .wrapper
        {
            height: 100% !important;
        }
    }
    .btn-primary{
        margin-right: 15px;
    }
    
/*    .sidebar-mini , .wrapper
        {
            height: auto;
    min-height: 100%;
            height: 100% !important;
            min-height: 100% !important;
        }*/
</style>
<link href="{{ asset('public/admin_theme/plugins/datepicker/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('AdditionalVendorScriptsInclude')
<!--Input Mask js-->
<script src="{{ asset('public/sitetheme/javascript/jquery.inputmask.bundle.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/fastclick/fastclick.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/admin_theme/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function () {


    $("#activity_log").on("change", ".action", function () {
                var curr_val = $(this).val();
                var appointment_id = $(this).attr("appointment_id");
                if (curr_val != "") {
                    if (curr_val == "Attempted") {
                        url = "<?= action('AdminController@getUpdateappointmentattend') ?>/" + btoa(appointment_id);
                        window.location.href = url;
                    }
                    if (curr_val == "Contacted") {
                        url = "<?= action('AdminController@getUpdateappointmentcontact') ?>/" + btoa(appointment_id);
                        window.location.href = url;
                    }
                    if (curr_val == "New Opportunity") {
                        url = "<?= action('AdminController@getUpdateappointmentnew') ?>/" + btoa(appointment_id);
                        window.location.href = url;
                    }
                    if (curr_val == "Additional Contact") {
                        url = "<?= action('AdminController@getUpdateappointmentadditional') ?>/" + btoa(appointment_id);
                        window.location.href = url;
                    }
                    if (curr_val == "Disqualified") {
                        url = "<?= action('AdminController@getUpdateappointmentdis') ?>/" + btoa(appointment_id);
                        window.location.href = url;
                    }
                    if (curr_val == "Not Attempted") {
                        url = "<?= action('AdminController@getUpdateappointmentnotattempted') ?>/" + btoa(appointment_id);
                        window.location.href = url;
                    }
                    
                }
            });
     
     var activity_log = $('#activity_log').dataTable({
                "bLengthChange": true,
                "processing": false,
                "responsive": true,
                "bSort":false,
                "bStateSave": false, //for load back page where edit action was applied
                dom: 'Bfrltip',
                buttons: [
                    {
                        extend: 'excel',
                        title: 'Activity Log',
                        text: 'Export To Excel',
                        className:'exportbutton',
                        exportOptions: {
                            columns: ':visible:not(.not-export-col)'
                        },
                    }
                ],
                "lengthMenu": [[50, 100, -1], [50, 100, "All"]],
                "serverSide": true, //for load back page where edit action was applied
                "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Appointment Record(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Record found.", "sLengthMenu": "Show _MENU_"},
                "ajax": {
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    'type': 'POST',
                    'url': '<?= action('AdminController@postFetchactivityloglist') ?>',
                    'data': function (param) {
                        param.year = $("#year").val();
                        param.month = $("#month").val();
                        param.activity_fromdate = $("#activity_fromdate").val();
                        param.activity_todate = $("#activity_todate").val();
                        param.activity_type = $("#activity_type").val();
                        param.search_id = $("#search_id").val();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //    console.log('ERRORS: ' + textStatus);
                    },

                },
                "bFilter": false,
                "language": {
                    "emptyTable": "No Record found.",
                    "search": ""
                },
                "columns": [
                    {"data": "id", name: 'al.id', "bsortable": false, "class": "usenum"},
                    {"data": "first_name", name: 'al.first_name', "class": "table_titlecase_record"},
                    {"data": "email", name: 'al.email'},
                    {"data": "mobile_no", name: 'al.mobile_no', "class": "table_titlecase_record"},
                    {"data": "destination", name: 'al.destination', "class": "table_titlecase_record"},
                    {"data": "intake", name: 'al.intake', "class": "table_titlecase_record"},
                    {"data": "appointment_datetime", name: 'al.appointment_datetime', "class": "table_titlecase_record"},
                    {"data": "created_date", name: 'al.created_date', "class": "ucenter table_titlecase_record"},
                    {"data": "status", name: 'status', "class": "ucenter table_titlecase_record"},
                ],
    });
        
         
        $("#activity_fromdate,#activity_todate").inputmask();
 
        var FromEndDate = '';
        var startDate = '';
        $('#activity_fromdate').datepicker({
            format: "mm/dd/yyyy",
            endDate: '+0d',
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            $('#activity_todate').datepicker('setStartDate', startDate);
        });

        $('#activity_todate').datepicker({
            format: "mm/dd/yyyy",
            endDate: '+0d',
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#activity_fromdate').datepicker('setEndDate', FromEndDate);
        });

/*Begin : Fetch Filter Result*/
        $(document).on('click', '#search_submit', function (e) {
                activity_log.api().draw();
                return false;
        });
            /*End: Filter Result*/
 });
 //End: Load Activity Log Table Record In Datatable

</script>
@endsection
@section('RenderBody')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>


<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    @if(session('message'))
    <div class="alert alert-success alert-dismissible"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>{{session('message')}}</div>
    @endif
    @if(session('error_message'))
    <div class="alert alert-danger alert-dismissible"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>{{session('error_message')}}</div>
    @endif

<div class="box box-info">
        <div class="box-header">
            <h3 class="box-title"><b>Search Criteria:</b> Enter any combination of none or more search criteria to narrow down your results</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
        </div>
        <div class="box-body padding-left-25 padding-right-25">
            <form class="form-horizontal">
                <div class="form-group">
                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <label for="inputEmail3" class="control-label">Activity From Date</label>
                        <input type="text" name="activity_fromdate" id="activity_fromdate" class="form-control filteroption" placeholder="mm/dd/yyyy" data-inputmask="'alias': 'mm/dd/yyyy'">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <label for="inputEmail3" class="control-label">Activity To Date</label>
                        <input type="text" name="activity_todate" id="activity_todate" class="form-control filteroption" placeholder="mm/dd/yyyy" data-inputmask="'alias': 'mm/dd/yyyy'">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <label for="inputEmail3" class="control-label">Activity Type</label>
                        <select class="form-control filteroption" name="activity_type" id="activity_type">
                            <option value="">Activity Type</option>
                            <option value="user">User</option>
                            <option value="events">Events</option>
                            <option value="reporting">Reporting</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <label for="inputEmail3" class="control-label">Search Id</label>
                        <input type="text" name="search_id" id="search_id" class="form-control filteroption" placeholder="Search Id">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12 col-sm-12 text-right">
                        <input type="submit" class="btn btn-primary updateinfo" value="Search" id="search_submit">
                        <a href="{!! URL::to('admin/home') !!}" class="btn reset-btn btn-danger">Reset</a>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box-body -->
    </div>
    <!--End : Start Filter-->
    
    <!--Begin : Start Activity Log Table-->
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Appointment Record</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                  <table id="activity_log" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>First Name</th>
                    <th>Email</th>
                    <th>Contact No</th>
                    <th>Destination</th>
                    <th>Intake</th>
                    <th>Appointment Date-Time</th>
                    <th>Created Date</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
    </div>
    <!--End : Start Activity Log Table-->
</section>
<!-- /.content -->
@endsection