@extends('admin/layouts/master')
@section('htmlheader_title')
Edit Profile
@endsection
@section ('AdditionalVendorCssInclude')
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('cropper/css/cropper.min.css') }}">
<style type="text/css">
    .errorlist, .errorlist th, .errorlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }
    .valerror
    {
        color:red;
    }
    label.error {
        color: #ff0000;
    }
    .errorlist th, .errorlist td {
        padding: 5px;
        text-align: left;
    }
    .successlist, .successlist th, .successlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }

    .successlist th, .successlist td {
        padding: 5px;
        text-align: left;
    }

    .btn-success{
        margin-right: 15px;
    }


    .emergencyborder
    {
        border-top: none !important;
    }

    .margin-left-15
    {
        margin-left: 15px !important;
    }

    .margin-right-15
    {
        margin-right: 15px !important;
    }

    .padding-left-25
    {
        padding-left: 25px !important;
    }

    .padding-right-25
    {
        padding-right: 25px !important;
    }

    .margin-bottom-15
    {
        margin-bottom: 15px !important;
    }
    .mandatory-field,label.error,.valerror
    {
        color: red !important;
    }
</style>
@endsection
@section('AdditionalVendorScriptsInclude')
<script src="{{ asset('public/admin_theme/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
/*Begin : Form Validation*/
$("#edituser_form").validate({
    rules: {
        name: {required: true},
        lastname: {required: true},
        email: {required: {
                required: true
            }, email: true},
        password: {minlength: 6, maxlength: 15},
        cpassword: {minlength: 6, maxlength: 15, equalTo: "#password"},
    },
    messages: {
        name: {required: "Please First Name."},
        email: {required: "Please Enter Email."},
        password: {required: " Please Enter Password",
        },
        cpassword: {required: "Please Enter Confirm Password",
            equalTo: "Password & Confirm Password Must be Same"},
    }
});
/*End : Form Validation*/
</script>
@endsection

@section('RenderBody')
<!-- Main content -->
<section class="content-header">
    <h1>
        Edit Profile
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/admin/home') }}"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Edit Profile</li>
    </ol>
</section>

@if (count($errors) > 0)
@foreach ($errors->all() as $error)
<label class="col-sm-3"></label>
<label class="valerror col-sm-9" >{{ $error }} </label><br/>
@endforeach
@endif
<section class="content">
    <div class="row">
        @if(session('message'))
        <div class="alert alert-success alert-dismissible margin-left-15 margin-right-15"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>{{session('message')}}</div>  
        @endif
        @if(session('error_message'))
        <div class="alert alert-danger alert-dismissible margin-left-15 margin-right-15"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>{{session('error_message')}}</div>  
        @endif
        {!! Form::open(array('url' => 'admin/updateuser' , 'class' => 'form-horizontal','id' => 'edituser_form', 'files' => true)) !!}

        <div class=" margin-left-15 margin-bottom-15">

            <input type="submit" class=" btn btn-success updateinfo" value="Save" />

            <a href="{{ url('/admin/home') }}" class=" cancel_btn btn btn-danger reset-btn text-right btn-info">Cancel</a>
        </div>
        <!-- /.col -->
        <div class="col-xs-12 col-sm-12 col-lg-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#contact" data-toggle="tab">Profile</a></li>
                </ul>
                <div class="tab-content">
                    <!-- Begin :  Contact Information Section-->
                    <div class="active tab-pane" id="contact">
                        <div class="form-group">
                            <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Name<em class="mandatory-field">*</em></label>
                            <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                <input type="text" class="form-control capitalizedata" placeholder="Name" value="{{ $user->name or ''}}" name="name" maxlength="30" id="name">
                            </div>
                            <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Email<em class="mandatory-field">*</em></label>
                            <div class="col-xs-12 col-sm-9 col-lg-4">
                                <input type="text" class="form-control useremailaddress" placeholder="Email" value="{{ $user->email or ''}}" name="email" maxlength="50" id="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Password</label>
                            <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                <input class="form-control" type="password" placeholder="Password" name="password" id="password" value="">
                            </div>
                            <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Confirm Password</label>
                            <div class="col-xs-12 col-sm-9 col-lg-4">
                                <input class="form-control" type="password" placeholder="Confirm Password" name="cpassword" id="cpassword" value="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        <!-- /.row -->
</section>
@endsection
