<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

@include('admin.partials.htmlheader')
<body class="skin-blue sidebar-mini">
    <?php if(Auth::check()){ ?>
<div class="wrapper">

    @include('admin.partials.mainheader')

    @include('admin.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
<!--        <section class="content">-->
            <!-- Your Page Content Here -->
            @yield('RenderBody')
<!--        </section>-->
        <!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('admin.partials.footer')

</div><!-- ./wrapper -->
    <?php } ?>
@include('admin.partials.scripts')


</body>
</html>