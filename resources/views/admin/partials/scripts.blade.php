<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset('public/admin_theme/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('public/admin_theme/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/admin_theme/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/admin_theme/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/admin_theme/js/app.min.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function() {
                /*BEgin : Hide delete account success message*/
                $('.deleteaccountmessage').delay(5000).fadeOut('fast');
                /*End : Hide delete account success message*/
                /*Hide Success -error message after page load*/
                $('.alert-dismissible').delay(5000).fadeOut('slow');
});</script>
@yield('AdditionalVendorScriptsInclude')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->