<!--<style>
    .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>.treeview-menu
    {
        width: 225px !important;
    }
    .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>a>span:not(.pull-right), .sidebar-mini.sidebar-collapse .sidebar-menu>li:hover>.treeview-menu
    {
        width: 225px !important;
    }
    
</style>-->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('public/admin_theme/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>@if(Auth::check()){{ Auth::user()->name }}@endif</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="{{ Request::is('admin/home') ? 'active menu-open' : ''}}">
                <a href="{{ url('admin/home') }}"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
