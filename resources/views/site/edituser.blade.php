@extends('layouts/master')

@section ('AdditionalVendorCssInclude')
<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('cropper/css/cropper.min.css') }}">
<style type="text/css">
    .errorlist, .errorlist th, .errorlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }
    .valerror
    {
        color:red;
    }
    label.error {
        color: #ff0000;
    }

    .form-control.uplod-buttons {
        bottom: 0;
        cursor: pointer;
        height: 100%;
        left: 0;
        margin: 0 auto;
        opacity: 0;
        position: absolute;
        right: 0;
        top: 0;
        width: 180px;
    }

    .errorlist th, .errorlist td {
        padding: 5px;
        text-align: left;
    }
    .successlist, .successlist th, .successlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }

    .successlist th, .successlist td {
        padding: 5px;
        text-align: left;
    }

    .btn-success{
        margin-right: 15px;
    }
    .profile-user-img {
        margin: 0 auto;
        width: 100px;
        /*    padding: 3px;*/
        border: 4px solid #d2d6de;
    }
    .dpandlocation {float: left !important;padding: 40px 40px 0 40px !important;text-align: center !important;width: 100% !important;}
    .dpandlocation.dp_information {float: left !important;padding: 0 0 40px 0 !important;text-align: center !important;width: 100% !important;}

    .emergencyborder
    {
        border-top: none !important;
    }

    .margin-left-15
    {
        margin-left: 15px !important;
    }

    .margin-right-15
    {
        margin-right: 15px !important;
    }

    .margin-top-15
    {
        margin-top: 15px !important;
    }

    .padding-left-25
    {
        padding-left: 25px !important;
    }

    .padding-right-25
    {
        padding-right: 25px !important;
    }

    .margin-bottom-15
    {
        margin-bottom: 15px !important;
    }
    .imagediv
    {
        margin-top: 49px !important;
    }
    /*.action-buttons {
        
        bottom: -10px;
       float: right;
       padding: 0px 0;
        right: -10px;
        text-align: center;
        top: -10px;
        z-index: 1;
    }
    
    .action-buttons span {
        float:initial;
        width: 100%;
        text-align: center;
        margin-bottom: 15px;
    }
    
    .action-buttons span:first-of-type
    {
            margin-top: -23px;
            margin-right: 15px;
    }*/
/*    .action-buttons
    {
        float: left;
    }*/
    .action-buttons ,.actionfield
    {
        width:55px;
    }
    textarea {
        resize: none;
    }

    .profile-dp {
        display: inline-block;
        overflow: hidden;

    }

    .margin-leftright-5
    {
        margin-left:5px !important;
        margin-right:5px !important;
    }
    .buttons-excel
    {
        background: #00c0ef !important;
        border-color: #00acd6 !important;
        color: #fff !important;
        font-size: 12px !important;
        font-weight: 400 !important;
    }
    .buttons-colvis,.buttons-columnVisibility
    {
        background: #00c0ef !important;
        border-color: #00acd6 !important;
        color: #fff !important;
        font-size: 12px !important;
        font-weight: 400 !important;
    }

    .buttons-columnVisibility
    {
        background: #00a65a !important;
        border-color: #008d4c !important;
        color: #fff !important;
        font-size: 12px !important;
        font-weight: 400 !important;
    }
    div.dt-buttons
    {
        width: 100% !important;
    }
    .adduser-icon .btn-success
    {
        margin-right: 5px !important;
    }

    .subboxdiv {
            margin-left: 0px !important;
            margin-right: 0px !important; 
        }
    @media (max-width:768px)
    {
        .userbottommargin { margin-bottom: 15px !important;}
        .subboxdiv {
/*            margin-left: -10px !important;
            margin-right: -10px !important; */
margin-left: 0px !important;
            margin-right: 0px !important; 
        }
    }
    /*    @media (max-width:486px)
        {
            .action-buttons {
                width:55px !important;
            }
        }*/
    @media (min-width:768px) and (max-width:1200px)
    {
        .userbottommargin { margin-bottom: 15px !important;}

    }
    @media (min-width:1200px)
    {
        .socailinfotext { text-align: left !important;}
    }
    .table_titlecase_record {
    text-transform: capitalize !important;
}
</style>
<!--Begin : Add css For Add Button in datatables like export to xlsx-->
<link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!--Begin : Css For Datatable Responsive-->
<link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />
<!--End : Css For Datatable Responsive-->
<!--Begin : Add css For Add Button in datatables like export to xlsx-->
@endsection

<!--@section('PageTitle')
My Profile
@endsection-->

@section('AdditionalVendorScriptsInclude')
<script src="{{ asset('cropper/js/cropper.min.js') }}"></script>
<script src="{{ asset('bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('global/js/SetCase.js') }}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.GOOGLE_MAP_API_KEY') ?>&libraries=places"></script>
<script type="text/javascript" src="{{ asset('global/js/jquery.geocomplete.js') }}"></script>
<!--Begin : Add Js For Add Button in datatables like export to xlsx-->
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
<!--Begin : Js For Datatable Responsive-->
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<!--End : Js For Datatable Responsive-->
<!--End : Add Js For Add Button in datatables like export to xlsx-->
<script type="text/javascript">
/*Begin : Convert Case*/
//    $(".capitalizedata").Setcase({
//        caseValue: 'pascal',
//    });
/*End : Convert text case*/
/*Begin : Create Validation Method*/

/*Begin : Coockie function for store value in coockie*/
//    function setCookie(cname,cvalue,exdays) {
//    var d = new Date();
//    d.setTime(d.getTime() + (exdays*24*60*60*1000));
//    var expires = "expires=" + d.toGMTString();
//    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
//}
//
//function getCookie(cname) {
//    var name = cname + "=";
//    var decodedCookie = decodeURIComponent(document.cookie);
//    var ca = decodedCookie.split(';');
//    for(var i = 0; i < ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0) == ' ') {
//            c = c.substring(1);
//        }
//        if (c.indexOf(name) == 0) {
//            return c.substring(name.length, c.length);
//        }
//    }
//    return "";
//}
//
//function checkCookie() {
//    var user=getCookie("checkactionschild");
//    if (user != "") {
////        alert("Welcome again " + user);
//    } else {
//       setCookie("checkactionschild", 1, 30);
//    }
//}
/*End : Coockie function for store value in coockie*/
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
$.validator.addMethod('positiveNumber',
        function (value) {
            return Number(value) > 0;
        }, 'Please Enter Positive Number.');
/*Begin : Create Method Mofr Phone Number Validation*/
$.validator.addMethod("usPhoneFormat", function (value, element) {
    return this.optional(element) || /^\(\d{3}\)\d{3}\-\d{4}?$/.test(value);
}, "Enter a valid phone number.");
/*End: Create Method Mofr Phone Number Validation*/
/*Begin : Create Validation Method For check user age is below 18 then parent email require*/
function check_age() {
    var dob = $("#dob").val();
    var personal_wing_name = $("#personal_wing_name").val();
    if((personal_wing_name != "") && (personal_wing_name == "Bal" || personal_wing_name == "BAL" || personal_wing_name == "Balak" || personal_wing_name == "BALAK" || personal_wing_name == "Balika" || personal_wing_name == "BALIKA" ))
    {
        var is_bal = 1; /* If user primary wing is kishore/kishori*/
    }
    else
    {
        var is_bal = 2;
    }
    if (dob != "") {
        arr = dob.split("/");
        var month = arr[0];
        var day = arr[1];
        var year = arr[2];
//        var age = 23;
        var age = 18;
        var mydate = new Date();
        mydate.setFullYear(year, month - 1, day);
        var currdate = new Date();
        currdate.setFullYear(currdate.getFullYear() - age);

        if (currdate < mydate) {
            return true;
        }
        else if(currdate > mydate && is_bal == 1) {
            return true;
        }
        else {
            return false;
        }
        //return currdate < mydate;
    } else {
        return true;
    }
}
/*End : Create Validation Method For check user age is below 18 then parent email require*/
/*Begin : Create Validation Method For Display parent's password field*/
function check_parentspassword_show() {
    var dob = $("#dob").val();
    var parent_password_class = $("#parent_password_class").val();
    var personal_wing_name = $("#personal_wing_name").val();
    if((personal_wing_name != "") && (personal_wing_name == "Bal" || personal_wing_name == "BAL" || personal_wing_name == "Balak" || personal_wing_name == "BALAK" || personal_wing_name == "Balika" || personal_wing_name == "BALIKA" ))
    {
        var is_bal = 1; /* If user primary wing is kishore/kishori*/
    }
    else
    {
        var is_bal = 2;
    }
    
    if (dob != "") {
        arr = dob.split("/");
        var month = arr[0];
        var day = arr[1];
        var year = arr[2];
//        var age = 23;
        var age = 18;
        var mydate = new Date();
        mydate.setFullYear(year, month - 1, day);
        var currdate = new Date();
        currdate.setFullYear(currdate.getFullYear() - age);

        if (currdate < mydate) {
            if(parent_password_class == 1)
            {
                var element = document.getElementById("parents_password_div");
                element.classList.remove("hide"); 
            }  
        }
        else if(currdate > mydate && is_bal == 1) {
            if(parent_password_class == 1)
            {
                var element = document.getElementById("parents_password_div");
                element.classList.remove("hide");
            }
             
        }
        else {
            if(parent_password_class == 1)
            {
                var element = document.getElementById("parents_password_div");
               element.classList.add("hide");
            }
            
        }
    } else {
         if(parent_password_class == 1)
         {
             var element = document.getElementById("parents_password_div");
            element.classList.remove("hide"); 
         }
        
    }
}
/*End : Create Validation Method For Display parent's password field*/
/*Begin : Create Validation Method For check user age is below 18 then own email not require*/
function check_myage() {
    var dob = $("#dob").val();
    var personal_wing_name = $("#personal_wing_name").val();
    if((personal_wing_name != "") && (personal_wing_name == "Kishore" || personal_wing_name == "KISHORE" || personal_wing_name == "Kishori" || personal_wing_name == "KISHORI" ))
    {
        var is_kishore = 1; /* If user personal wing is kishore/kishori*/
    }
    else
    {
        var is_kishore = 2;
    }
    
    if((personal_wing_name != "") && (personal_wing_name == "Bal" || personal_wing_name == "BAL" || personal_wing_name == "Balak" || personal_wing_name == "BALAK" || personal_wing_name == "Balika" || personal_wing_name == "BALIKA" ))
    {
        var is_bal = 1; /* If user personal wing is Bal/balika*/
    }
    else
    {
        var is_bal = 2; /* If user personal wing is not Bal/balika*/
    }
    
    if (dob != "") {
        arr = dob.split("/");
        var month = arr[0];
        var day = arr[1];
        var year = arr[2];
//        var age = 23;
        var age = 18;
        var mydate = new Date();
        mydate.setFullYear(year, month - 1, day);
        var currdate = new Date();
        currdate.setFullYear(currdate.getFullYear() - age);
        
        if (currdate > mydate && is_bal == 1) {
            return false;
        }
        else if (currdate > mydate && is_bal == 2) {
            
            return true;
        }
        else if(currdate < mydate && is_kishore == 1) {
            return true;
        }
        else {
            return false;
        }
    } else {
        return true;
    }
}

function check_age_for_mother() {
    var dob = $("#dob").val();
    var motheremail = $.trim($("#motheremail").val());
    var fatheremail = $.trim($("#fatheremail").val());
    var personal_wing_name = $("#personal_wing_name").val();
    if((personal_wing_name != "") && (personal_wing_name == "Kishore" || personal_wing_name == "KISHORE" || personal_wing_name == "Kishori" || personal_wing_name == "KISHORI" ))
    {
        var is_kishore = 1; /* If user personal wing is kishore/kishori*/
    }
    else
    {
        var is_kishore = 2;
    }
    
    if((personal_wing_name != "") && (personal_wing_name == "Bal" || personal_wing_name == "BAL" || personal_wing_name == "Balak" || personal_wing_name == "BALAK" || personal_wing_name == "Balika" || personal_wing_name == "BALIKA" ))
    {
        var is_bal = 1; /* If user personal wing is Bal/balika*/
    }
    else
    {
        var is_bal = 2; /* If user personal wing is not Bal/balika*/
    }
    
    if (dob != "") {
        arr = dob.split("/");
        var month = arr[0];
        var day = arr[1];
        var year = arr[2];
//        var age = 23;
        var age = 18;
        var mydate = new Date();
        mydate.setFullYear(year, month - 1, day);
        var currdate = new Date();
        currdate.setFullYear(currdate.getFullYear() - age);

        if (currdate > mydate && is_bal == 1) {
            if(fatheremail != "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else if (currdate > mydate && is_bal == 2) {
            return false;
        }
        else if(currdate < mydate) {
            if(fatheremail != "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
//        else if (currdate < mydate && is_bal == 1) {
//            if(fatheremail != "")
//            {
//                return false;
//            }
//            else
//            {
//                return true;
//            }
//        }
//        else if(currdate < mydate && is_kishore == 1) {
//            if(fatheremail != "")
//            {
//                return false;
//            }
//            else
//            {
//                return true;
//            }
//        }
        else {
            return false;
        }
    } else {
        if(fatheremail != "")
            {
                return false;
            }
            else
            {
                return true;
            }
    }
}


function check_age_for_father() {
    var dob = $("#dob").val();
    var motheremail = $.trim($("#motheremail").val());
    var fatheremail = $.trim($("#fatheremail").val());
    var personal_wing_name = $("#personal_wing_name").val();
    if((personal_wing_name != "") && (personal_wing_name == "Kishore" || personal_wing_name == "KISHORE" || personal_wing_name == "Kishori" || personal_wing_name == "KISHORI" ))
    {
        var is_kishore = 1; /* If user personal wing is kishore/kishori*/
    }
    else
    {
        var is_kishore = 2;
    }
    
    if((personal_wing_name != "") && (personal_wing_name == "Bal" || personal_wing_name == "BAL" || personal_wing_name == "Balak" || personal_wing_name == "BALAK" || personal_wing_name == "Balika" || personal_wing_name == "BALIKA" ))
    {
        var is_bal = 1; /* If user personal wing is Bal/balika*/
    }
    else
    {
        var is_bal = 2; /* If user personal wing is not Bal/balika*/
    }
    
    if (dob != "") {
        arr = dob.split("/");
        var month = arr[0];
        var day = arr[1];
        var year = arr[2];
//        var age = 23;
        var age = 18;
        var mydate = new Date();
        mydate.setFullYear(year, month - 1, day);
        var currdate = new Date();
        currdate.setFullYear(currdate.getFullYear() - age);
        
        if (currdate > mydate && is_bal == 1) {
            if(motheremail != "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else if (currdate > mydate && is_bal == 2) {
            return false;
        }
        else if(currdate < mydate) {
            if(motheremail != "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
//        else if (currdate < mydate && is_bal == 1) {
//            if(motheremail != "")
//            {
//                return false;
//            }
//            else
//            {
//                return true;
//            }
//        }
//        else if(currdate < mydate && is_kishore == 1) {
//            if(motheremail != "")
//            {
//                return false;
//            }
//            else
//            {
//                return true;
//            }
//        }
        else {
            return false;
        }
    } else {
        
        if(motheremail != "")
            {
                return false;
            }
            else
            {
                return true;
            }
    }
}
/*End : Create Validation Method For check user age is below 18 then own email not require*/
//Begin : Check For User is admin or not validation
function checkuser() {
    var checkusertype = $("#is_admin").val();
    if (checkusertype == 2) { //If user is not admin then no need to do validation
        return false;
    } else {
        return true;
    }
}
//End : Check For user
//Begin : used to check valid facebook url
$.validator.addMethod("valid_facebook_url", function ($url) {
    if ($url != "")
    {
        return /^(http|https)\:\/\/www.facebook.com\/[a-z0-9_]+$/i.test($url);
    } else
    {
        return true;
    }

}, "Enter Valid Facebook Account Url");
//End : used to check valid facebook url


//Begin : To check valid url of twitter
$.validator.addMethod("valid_twitter_url", function ($url) {
    if ($url != "")
    {
        return /^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i.test($url);
    } else
    {
        return true;
    }

}, "Enter Valid Twitter Account Url");
//End : used to check valid twitter url

//Begin : To check valid url of instagram
$.validator.addMethod("valid_instagram_url", function ($url) {
    if ($url)
    {
        return /^(http|https)\:\/\/www.instagram.com\/[a-z0-9_]+$/i.test($url);
    } else
    {
        return true;
    }

}, "Enter Valid Instagram Account Url");
//End : used to check valid url of instagram

//Begin : To check valid url of linkedin
$.validator.addMethod("valid_linkedin_url", function ($url) {
    if ($url != "")
    {
        return /((https?:\/\/)((www|\w\w)\.)?linkedin\.com\/)((([\w]{2,3})?)|([^\/]+\/(([\w|\d-&#?=])+\/?){1,}))$/gm.test($url);
//        return /((https?:\/\/)((www|\w\w)\.)?linkedin\.com\/)((([\w]{2,3})?)|([^\/]+\/(([\w|\d-&#?=])+\/?){1,}))[a-z0-9_]+$/gm.test($url); //For nowt allow last -forward slash
    } else
    {
        return true;
    }

}, "Enter Valid Linkedin Account Url");
//End : used to check valid url of linkedin

//Begin : To check valid url of linkedin
$.validator.addMethod("valid_snapchat_url", function ($url) {
    if ($url != "")
    {
        return /^(http|https)\:\/\/www.snapchat.com\/add\/[a-z0-9_]+$/i.test($url);
    } else
    {
        return true;
    }

}, "Enter Valid Snapchat Account Url");
//End : used to check valid url of linkedin
//Begin : Validation for Mandal 
function chekcusertype_profile()
{

    var user_id = <?= $user->id; ?>;
    var login_user = <?= Auth::id(); ?>;
    if (login_user == user_id)
    {
        return false;
    } else
    {
        return true;
    }
}
//End : Validation for Mandal 
(function ($) {
    $(function () {
        /*Begin : Form Validation*/
        $("#edituser_form").validate({
            rules: {
                firstname: {required: true},
                //middlename: {required: true},
                lastname: {required: true},
                email: {required: {
                        depends: check_myage
                    }, email: true},
                password: {minlength: 6, maxlength: 15},
                cpassword: {minlength: 6, maxlength: 15, equalTo: "#pwd"},
                parents_password: {minlength: 6, maxlength: 15},
                cparents_password: {minlength: 6, maxlength: 15, equalTo: "#parents_password"},
                dob: {required: true},
                gender: {required: true},
                region_id: {required: true},
                center_id: {required: true},
//                tshirtsize: {required: true},
//                yearenteredsatsang: {required: true},
//                address1: {required: true},
                //apartment: {required: true},
//                city: {required: true},
//                state: {required: true},
//                zip: {required: true},
//                country: {required: true},
                //primarysevawing : {required: true},
                phonehome: {usPhoneFormat: true},
                phonecell: {usPhoneFormat: true},
                schoolyear: {
                    //required: { depends: check_age },
                    required: true,
                },
//                schoolcollegename: {
//                    required: true
//                },
//                schoolcollegename: {
//                    required: {
//                        depends: check_age
//                    },
//                },
//                majorexpected: {
//                    required: {
//                        depends: check_age
//                    },
//                },
//                careerinterest: {
//                    required: {
//                        depends: check_age
//                    },
//                },
                /*fathername: {
                 required: {
                 depends: check_age
                 },
                 },*/
                fathercellphone: {
                    usPhoneFormat: true
                            /*required: {
                             depends: check_age
                             },
                             number: {
                             depends: check_age
                             },
                             positiveNumber: {
                             depends: check_age
                             }*/
                },
                fatheremail: {
                    required: {
                        depends: check_age_for_father
                    },
                    email: true,
//                    checkfatheremailexistorno: true
                },
                /* mothername: {
                 required: {
                 depends: check_age
                 },
                 },*/
                mothercellphone: {
                    usPhoneFormat: true
                            /*required: {
                             depends: check_age
                             },
                             number: {
                             depends: check_age
                             },
                             positiveNumber: {
                             depends: check_age
                             }*/
                },
                motheremail: {
                    email: true,
                    required: {
                        depends: check_age_for_mother
                    },
//                    checkmotheremailexistorno: true
//                    email: {
//                        depends: check_age
//                    }
                },
//                emergencycontact1name: {required: true},
                emergencycontact1priphone: {
//                    required: true, 
                    usPhoneFormat: true/*number: true, positiveNumber: true*/},
                emergencycontact1secphone: {
                    //number: true
                    usPhoneFormat: true
                },
                emergencycontact1email: {
//                    required: true,
                    email: true
                },
//                emergencycontact2name: {required: true},
                emergencycontact2priphone: {
//                    required: true,
                    usPhoneFormat: true /*number: true, positiveNumber: true*/},
                emergencycontact2secphone: {
                    //number: true
                    usPhoneFormat: true
                },
                emergencycontact2email: {
//                    required: true,
                    email: true},
//                allergies: {required: true},
//                medicationstaken: {required: true},
//                medicalconditions: {required: true},
//                spclmedicalinstructions: {required: true},
                "role_type[]": {
                    required: {
                        depends: checkuser
                    },
                },
                facebook_id: {valid_facebook_url: true},
                twitter_id: {valid_twitter_url: true},
                instagram_id: {valid_instagram_url: true},
                linkedin_id: {valid_linkedin_url: true},
                snapchat_id: {valid_snapchat_url: true},
            },
            messages: {
                firstname: {required: "Please Enter First Name."},
                //middlename: {required: "Please Enter Middle Name."},
                lastname: {required: "Please Enter Last Name."},
                email: {required: "Please Enter Email."},
                password: {required: " Please Enter Password",
                },
                cpassword: {required: "Please Enter Confirm Password",
                    equalTo: "Password & Confirm Password Must be Same"},
                cparents_password: {required: "Please Enter Parent's Confirm Password",
                    equalTo: "Parent's Password & Parent's Confirm Password Must be Same"},
                photo: {
                    accept: "Please Enter jpg,png or jpeg file.",
                },
                dob: {required: "Please Enter Date Of Birth."},
                gender: {required: "Please Select Gender."},
                region_id: {required: "Please Select Region."},
                center_id: {required: "Please Select Center."},
                tshirtsize: {required: "Please Select T-Shirt Size."},
                yearenteredsatsang: {required: "Please Enter Year Entered Satsang."},
                address1: {required: "Please Enter Address."},
                //address2: {required: "Please Enter Address 2."},
                //apartment: {required: "Please Enter Apartment."},
                city: {required: "Please Enter City."},
                state: {required: "Please Select State."},
                zip: {required: "Please Enter Zip/Postal Code."},
                country: {required: "Please Select Country."},
                phonehome: {require_from_group: "Please enter at least either Home Phone or Cell Phone."},
                phonecell: {require_from_group: "Please enter at least either Home Phone or Cell Phone."},
                fax: {required: "Please Enter Fax."},
                //primarysevawing: {required: "Please Select Primary Seva Wing."},
                primaryseva: {required: "Please Select Primary Seva."},
                secondarysevawing: {required: "Please Select Secondary Seva Wing."},
                secondaryseva: {required: "Please Select Secondary Seva."},
                schoolyear: {required: "Please Select School Year."},
                schoolcollegename: {required: "Please Enter School College Name."},
                majorexpected: {required: "Please Enter Major/Expected Major."},
                careerinterest: {required: "Please Enter Career Interest."},
                fathername: {required: "Please Enter Father Name."},
                fathercellphone: {required: "Please Enter Father Cell Phone."},
                fatheremail: {required: "Please Enter Father's Email."},
                mothername: {required: "Please Enter Mother Name."},
                mothercellphone: {required: "Please Enter Mother Cell Phone."},
                motheremail: {required: "Please Enter Mother's Email."},
                emergencycontact1name: {required: "Please Enter Emergency Contact1 Name."},
                emergencycontact1priphone: {required: "Please Enter Emergency Contact1 Primary Phone ."},
                emergencycontact1secphone: {required: "Please Enter Emergency Contact1 Secondary Phone ."},
                emergencycontact1email: {required: "Please Enter Emergency Contact1 Email ."},
                emergencycontact2name: {required: "Please Enter Emergency Contact2 Name."},
                emergencycontact2priphone: {required: "Please Enter Emergency Contact2 Primary Phone."},
                emergencycontact2secphone: {required: "Please Enter Emergency Contact2 Secondary Phone."},
                emergencycontact2email: {required: "Please Enter Emergency Contact2 Email."},
                allergies: {required: "Please Enter Allergies."},
                medicationstaken: {required: "Please Enter Medications Taken."},
                medicalconditions: {required: "Please Enter Medications Conditions."},
                spclmedicalinstructions: {required: "Please Enter Special Medical Instructions."},
                facebook_id: {valid_facebook_url: "Enter Valid Facebook Account Url."},
                twitter_id: {valid_twitter_url: "Enter Valid Twitter Account Url."},
                instagram_id: {valid_instagram_url: "Enter Valid Instagram Account Url."},
                linkedin_id: {valid_linkedin_url: "Enter Valid Linkedin Account Url."},
                snapchat_id: {valid_snapchat_url: "Enter Valid Snapchat Account Url."},
                notes: {required: "Please Enter Notes."},
                'first_name[]': 'Please enter first name',
                'last_name[]': 'Please enter last name',
                'dob_child[]': {
                    required: 'Please select date of birth',
                    date: 'Please select valid date'
                },
                'role_type[]': {
                    required: 'Please select role type'
                },
                'gender_child[]': 'Please select gender',

            }
        });
        /*End : Form Validation*/
        $(document).on("focusin", "#dob_child,#edit_dob_child", function (event) {
            $(this).prop('readonly', true);
        });
        $(document).on("focusout", "#dob_child,#edit_dob_child", function (event) {
            $(this).prop('readonly', false);
        });

        /*Begin : Show Subgoup Option Om Page load*/
        var names_arr = ['Bal','bal','BAL','Balak','BALAK','balak','Balika','BALIKA','balika'];
                    
                   var checkgroup =  names_arr.indexOf($("#personal_wing_name").val());
                   if(checkgroup > -1)
                   {
                       $(".subgroup_div").removeClass('hide');
                   }
                   else
                   {
                       $(".subgroup_div").addClass('hide');
                   }

        var show_cols;
        var statesave;
        var show_action;
        if ($("#is_action").val() == 1)
        {
            show_cols = [0, 1, 2, 3, 4];
            statesave = false;
            show_action = true;
//                checkCookie();
//                var currentcoockiename = getCookie("checkactionschild");
//                if (currentcoockiename == 1)
//                {
//                    /*Begin : Reset And Detsroy Datatable for state save*/
//                    setCookie("checkactionschild", 2, 30);
//                    var newcoockiename = getCookie("checkactionschild");
//                    if(newcoockiename == 2)
//                    {
//                        var tableuser = $('#children_list').DataTable();
//                        tableuser.state.clear();
//                        tableuser.destroy();
//                    }
//                    /*End : Reset And Detsroy Datatable for state save*/
//                    
//                }

        } else
        {
            show_cols = [0, 1, 2, 3];
            statesave = true;
            show_action = false;
            /*Begin : Reset And Detsroy Datatable for state save*/
//                var tableuser = $('#children_list').DataTable();
//                tableuser.state.clear();
//                tableuser.destroy();
            /*End : Reset And Detsroy Datatable for state save*/
        }

        /*Begin : Datatables for Childern List*/
        var children_list = $('#children_list').dataTable({
            "bLengthChange": false,
            "bpageLength": 10,
            "responsive": true,
            "processing": false,
//            "stateSave": true,
            "bStateSave": false, //for load back page where edit action was applied
//            "bStateSave": true,
            "serverSide": true,
            "bAutoWidth": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    title: 'Children Information',
                    text: 'Export To Excel',
                    className: 'exportbutton',
                    exportOptions: {
                        columns: ':visible:not(.not-export-col)'
                    }
                },
                {
//                        extend: 'colvis',
                    extend: 'columnsToggle',
                    columns: show_cols,
                },
            ],
            "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Children(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Record found."},
            "ajax": {
                'type': 'POST',
                'url': '<?= action('AdminuserController@postUserchildernlistajax') ?>',
                'data': function (param) {
                    param.user_id = "{{$user->id}}";
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //    console.log('ERRORS: ' + textStatus);
                },
            },
            "bFilter": false,
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                var oSettings = children_list.fnSettings();
                $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);

                return nRow;
            },
            "language": {
                "emptyTable": "No Record found.",
                "search": ""
            },
            "columns": [
                {"data": "id", "bsortable": false, "class": "usenum"},
                {"data": "first_name", "class": "ucenter"},
                {"data": "last_name", "class": "ucenter"},
                {"data": "gender", "class": "ucenter"},
                {"data": "action", "class": "table_titlecase_record", "searchable": false, "bsortable": false, "orderable": false},
            ],
            aoColumnDefs: [
                {targets: -1, visible: show_action}
            ]
        });
        /*End : Datatable For Childern list*/

        $("#addchild_form").validate({
            rules: {
                first_name: {required: true},
                last_name: {required: true},
                gender_child: {required: true},
                dob_child: {required: true},
//                allergies_medicalinfo: {required: true},
            },
            messages: {
                first_name: {required: "Please Enter First Name."},
                last_name: {required: "Please Enter Last Name."},
                gender_child: {required: "Please Select Gender."},
                dob_child: {required: "Please Select date of birth."},
                allergies_medicalinfo: {required: "Please Enter Allergies & Medical Information."},
            },
            submitHandler: function (form) {
                var child_id = $('#child_id').val();
                var data = $(form).serialize();
                var url = (child_id.trim() != '') ? '<?= action('AdminuserController@postEdituserchild') ?>' : '<?= action('AdminuserController@postInsertuserchild') ?>';

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: "json",
                    success: function (result) {
                        $('#myModal').modal('hide');
                        $('#editchild').modal('hide');
                        if (result == false) {
                            toastr.error("Problem while saving data, Please try again.", 'Error');
                            //$('#addchild_form').trigger("reset");
                        } else
                        {
                            $('#child_id').val().trim() != '' ? toastr.success("<?= Config::get('constants.RECORD_UPDATE_SUCCES'); ?>", 'Success') : toastr.success("<?= Config::get('constants.RECORD_INSERT_SUCCESS'); ?>", 'Success');
                            children_list.fnDraw(false);
                        }

                        //window.location.reload();
                    },
                    error: function (result) {
                        //console.log(result);
                    }
                });
                return false;
            }
        });



        $("#editchild_form").validate({
            rules: {
                first_name: {required: true},
                last_name: {required: true},
                gender_child: {required: true},
                dob_child: {required: true},
//                allergies_medicalinfo: {required: true},
            },
            messages: {
                first_name: {required: "Please Enter First Name."},
                last_name: {required: "Please Enter Last Name."},
                gender_child: {required: "Please Select Gender."},
                dob_child: {required: "Please Select date of birth."},
                allergies_medicalinfo: {required: "Please Enter Allergies & Medical Information."},
            },
            submitHandler: function (form) {
                var child_id = $('#child_id').val();
                var data = $(form).serialize();
                var url = (child_id.trim() != '') ? '<?= action('AdminuserController@postEdituserchild') ?>' : '<?= action('AdminuserController@postInsertuserchild') ?>';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: "json",
                    success: function (result) {
                        $('#myModal').modal('hide');
                        $('#editchild').modal('hide');
                        if (result == false) {
                            toastr.error("Problem while saving data, Please try again.", 'Error');
                            //$('#addchild_form').trigger("reset");
                        } else
                        {
                            $('#child_id').val().trim() != '' ? toastr.success("<?= Config::get('constants.RECORD_UPDATE_SUCCES'); ?>", 'Success') : toastr.success("<?= Config::get('constants.RECORD_INSERT_SUCCESS'); ?>", 'Success');
                            children_list.fnDraw(false);
                        }

                        //window.location.reload();
                    },
                    error: function (result) {
                        //console.log(result);
                    }
                });
                return false;
            }
        });
    });
    $(document).ready(function () {

        $('#phonehome,#phonecell,#fathercellphone,#mothercellphone,#emergencycontact1priphone,#emergencycontact1secphone,#emergencycontact2priphone,#emergencycontact2secphone').inputmask();
        /*Begin : To select content on focus*/
        $("input").focus(function () {
            $(this).select();
        });
        $("textarea").focus(function () {
            $(this).select();
        });
        /*End : To select content on focus*/

        $('#dob').datepicker({
            format: "mm/dd/yyyy",
            startView: "years",
            endDate: '+0d',
            autoclose: true
        });
        $("#dob").inputmask();
        $('#dob_child,#edit_dob_child').datepicker({
            format: "mm/dd/yyyy",
            startView: "years",
            endDate: '+0d',
            autoclose: true
        });

        $('#yearenteredsatsang').datepicker({
            format: " yyyy",
            viewMode: "years",
            endDate: '+0y',
            minViewMode: "years",
            autoclose: true
        });
<?php
if (isset($children) && !empty($children)) {
    foreach ($children as $child1) {
        ?>
                $('#exists_dp_<?= $child1->id; ?>').datepicker({
                    format: "mm/dd/yyyy"
                });
        <?php
    }
}
?>


        /*Begin : User Image Cropping By Dipak Ratnani*/
        //var original_profile_pic = "";
        var $image;
        $('#profile_file').on('change', function () {
            var input = $(this)[0];
            var file = input.files[0];
            // access image size here 
            var extension = input.files[0].name.split('.').pop().toLowerCase();
            //select image type   
            var match = ["jpeg", "png", "jpg"];
            if (!((extension == match[0]) || (extension == match[1]) || (extension == match[2]))) {

                //alert("<?= Config::get('constants.IMAGE_VALIDATION_ERROR') ?>");

                toastr.error("<?= Config::get('constants.IMAGE_VALIDATION_ERROR') ?>", 'Error');
                return false;
            }

            var original_profile_pic = $('#original_profile_pic').val();



            $('.profile-dp').css('height', '100px');
            $('.profile-dp').css('width', '100px');
            $('.profile-dp').css('border-radius', '50%');


            $('.profile-user-img').attr('src', original_profile_pic);

            var reader = new FileReader();
            var image = new Image();
            reader.onload = function (e)
            {
                $('.profile-dp').css('border-radius', '50%');
                $('.profile-dp').css('border', '4px solid #d2d6de');
                image.src = e.target.result;

                image.onload = function ()
                {
                    // access image size here 

                    if (this.width < 100 && this.height < 100)
                    {
                        $('#profile_file').val('');
                        //alert("<?= Config::get('constants.IMAGE_SMALL_ERROR') ?>");

                        toastr.error("<?= Config::get('constants.IMAGE_SMALL_ERROR') ?>", 'Error');
                    } else
                    {
                        $(".modal_profile_pic").remove();
                        $(".cropper-container").remove();

                        $('#div_profile_pic').append('<img id="modal_profile_pic" src="' + e.target.result + '" class="cropper_logo" />');

                        img = document.getElementById('modal_profile_pic');
                        var width = img.clientWidth;
                        var height = img.clientHeight;

                        $('#myModal_profile').modal('show');

                        $image = $(".cropper_logo").cropper({
                            aspectRatio: 1 / 1,
                            responsive: false,
                            //cropBoxResizable: false,
                            strict: true,
                            preview: ".profile-dp",
                            minCropBoxWidth: 100,
                            minCropBoxHeight: 100,
                            crop: function (data) {
                                $("#profile_x1").val(Math.round(data.x));
                                $("#profile_y1").val(Math.round(data.y));
                                $("#profile_h").val(Math.round(data.height));
                                $("#profile_w").val(Math.round(data.width));
                                $(".modal_profile_pic").on("dragend.cropper", function ()
                                {
                                    var height_temp = $("#profile_h").val();
                                    if (height_temp < 100)
                                    {
                                        $(".modal_profile_pic").cropper("setData", {width: 200, height: 200});
                                    }
                                });
                            }
                        });
                    }
                };
            };
            if (file) {
                reader.readAsDataURL(file);
            }
        });

        // Hide the Profile Picture modal.
        $('#save_profile_pic').click(function () {

            $('.profile-dp > img').css('height', '95px');
            $('.profile-dp > img').css('width', '95px');
//             $('.profile-dp > img').css("margin", "-4px 0 0 -10px");
            $('.profile-dp > img').css("margin", "0px -10px -13px -7px");

            $('#myModal_profile').modal('hide');

            var fd = new FormData();
            fd.append("photo", document.getElementById('profile_file').files[0]);
            fd.append("user_id", "{{ $user->id or ''}}");
            fd.append("profile_x1", document.getElementById('profile_x1').value);
            fd.append("profile_y1", document.getElementById('profile_y1').value);
            fd.append("profile_w", document.getElementById('profile_w').value);
            fd.append("profile_h", document.getElementById('profile_h').value);

            $.ajax({
                url: '<?= action('AdminuserController@postUpdateprofileimage') ?>',
                type: 'post',
                data: fd,
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data, textStatus, jqXHR)
                { //location.reload();
                    if (data.success == true)
                    {
                        //If image successfully changed
                        $('#original_profile_pic').val(data.filename);
                        var user_id = <?= $user->id; ?>;
                        var login_user = <?= Auth::id(); ?>;
                        if (user_id == login_user)
                        {
                            original_profile_pic = data.filename;
                            $('.user-ico > img').attr('src', data.filename);
                            $('.profile-dp > img').attr('src', data.filename);

                        }

                        toastr.success("<?= Config::get('constants.PROFILE_IMAGE_UPDATE_SUCCESS') ?>", 'Success');

                    } else
                    {

                        toastr.error("<?= Config::get('constants.PROFILE_IMAGE_UPDATE_FAIL') ?>", 'Error');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $('.overlay').hide();
                    $('.loader-walk').hide();
                }
            });
        });

        // Close Profile Pic Modal Cropper
        $(".closeModal").click(function () {
            //console.log(original_profile_pic);
            $('#profile_file').val('');
            $(".profile-dp > img").remove();
            $('#myModal_profile').modal('hide');

            $("#modal_profile_pic").remove();

            $(".profile-dp").append('<img src="' + $('#original_profile_pic').val() + '" class="jcrop-preview .original-profile-pic img-responsive" alt="Preview Profile Picture"/>');
        });
        /*End : User Image Cropping*/
        $(".closerelocatemodal").click(function () {
            $("#relocate_user_form").trigger('reset');
            $("#relocate_user_modal").hide();
            
        });
        /*Begin : Get Center asp er region change*/
//        $("#primarysevawing,#region_id").change(function () {
        $("#region_id").change(function () {
            $region_id = $('#region_id').val();
            $primarysevawing = $('#primarysevawing').val() == "" ? 0 : $('#primarysevawing').val();
            //if primarysevawing is blank then load all centers from region
            $("#center_id").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postRegion_based_center_ajax') ?>',
                data: {"region_id": $region_id},
                dataType: "json",
                success: function (result) {
                    $("#center_id").prop("disabled", false);
                    html = "<option value=''>Select Center</option>";
                    for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
                        }
//                    if (result.center_count == 1)
//                    {
//                        html += "<option value='" + result.centers.id + "'>" + result.centers.CenterName + "</option>";
//                    } else
//                    {
//                        for (i = 0; i < result.centers.length; i++) {
//                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
//                        }
//                    }
                    $("#center_id").html(html);

                },
                error: function (result) {
                    //console.log(result);
                }
            });
//                if($primarysevawing == 0){
//                    $.ajax({
//                        headers: {
//                            'X-CSRF-TOKEN': '{{ csrf_token() }}',
//                        },
//                        type: 'POST',
//                        url: '<//?= action('AdminuserController@postRegion_based_center_ajax') ?>',
//                        data: {"region_id": $region_id},
//                        dataType: "json",
//                        success: function (result) {
//                            html = "<option value=''>Select Center</option>";
//                            for (i = 0; i < result.centers.length; i++) {
//                                html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
//                            }
//                            $("#center_id").html(html);
//
//                        },
//                    });
//                } else {
//                    $.ajax({
//                        headers: {
//                            'X-CSRF-TOKEN': '{{ csrf_token() }}',
//                        },
//                        type: 'POST',
//                        url: '<//?= action('AdminuserController@postFetchcenterlist') ?>',
//                        data:{"region_id":$region_id,"primarysevawing":$primarysevawing},
//                        dataType: "json",
//                        success: function (result) {
//                            html = "<option value=''>Select Center</option>";
//                            for (i = 0; i < result.centers.length; i++) {
//                                html += "<option value='" + result.centers[i].center_id + "'>" + result.centers[i].CenterName + "</option>";
//                            }
//                            $("#center_id").html(html);
//                        },
//                    });
//                }
            /*Begin : Uncheck Netowrk Individual*/
            $("#networking_target").iCheck('uncheck');
            /*End : Uncheck Netowrk Individual*/
        });

        //set location value selected if server side validation error occur
        var region_id = "{{old(@region_id)}}";
        var region_id_db = "<?php
if (count($user) > 0) {
    echo $user->region_id;
}
?>";

        var primarysevawing = "{{old(@primarysevawing)}}";
        var primarysevawing_db = "<?php
if (count($user) > 0) {
    echo $user->PrimarySevaWing;
}
?>";

//        if ((region_id != "") && (primarysevawing != "")) {
        if (region_id != "") {
            $region_id = region_id;
            $primarysevawing = primarysevawing;

            var center = "{{old(@center_id)}}";
            $("#center_id").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postRegion_based_center_ajax') ?>',
                data: {"region_id": $region_id},
                dataType: "json",
                success: function (result) {
                    $("#center_id").prop("disabled", false);
                    html = "<option value=''>Select Center</option>";
                    for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'";
                            if (result.centers[i].id == center) {
                                html += " selected";
                            }
                            html += ">" + result.centers[i].CenterName + "</option>";
                        }
//                    if (result.center_count == 1)
//                    {
//                        html += "<option value='" + result.centers.id + "'";
//                        if (result.centers.id == center) {
//                            html += " selected";
//                        }
//                        html += ">" + result.centers.CenterName + "</option>";
//                    } else
//                    {
//                        for (i = 0; i < result.centers.length; i++) {
//                            html += "<option value='" + result.centers[i].id + "'";
//                            if (result.centers[i].id == center) {
//                                html += " selected";
//                            }
//                            html += ">" + result.centers[i].CenterName + "</option>";
//                        }
//                    }
                    $("#center_id").html(html);

                },
                error: function (result) {
                    //console.log(result);
                }

            });
//            $.ajax({
//                headers: {
//                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
//                },
//                type: 'POST',
//                url: '<//?= action('AdminuserController@postFetchcenterlist') ?>',
//                data:{"region_id":$region_id,"primarysevawing":$primarysevawing},
//                dataType: "json",
//                success: function (result) {
//                    html = "<option value=''>Select Center</option>";
//                    for (i = 0; i < result.centers.length; i++) {
//                        html += "<option value='" + result.centers[i].center_id + "'";
//                        if (result.centers[i].center_id == center) {
//                            html += " selected";
//                        }
//                        html += ">" + result.centers[i].CenterName + "</option>";
//                    }
//                    $("#center_id").html(html);
//                },
//            });
//        } else if (region_id_db != "" && region_id_db != 0 && primarysevawing_db != "" && primarysevawing_db !=0) {
        } else if (region_id_db != "" && region_id_db != 0) {
            $region_id = region_id_db;
            $primarysevawing = primarysevawing_db;

            var center = "<?php
if (count($user) > 0) {
    echo $user->center_id;
}
?>";
            $("#center_id").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postRegion_based_center_ajax') ?>',
                data: {"region_id": $region_id},
                dataType: "json",
                success: function (result) {
                    $("#center_id").prop("disabled", false);
                    html = "<option value=''>Select Center</option>";
                    for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'";
                            if (result.centers[i].id == center) {
                                html += " selected";
                            }
                            html += ">" + result.centers[i].CenterName + "</option>";
                        }
//                    if (result.center_count == 1)
//                    {
//                        html += "<option value='" + result.centers.id + "'";
//                        if (result.centers.id == center) {
//                            html += " selected";
//                        }
//                        html += ">" + result.centers.CenterName + "</option>";
//                    } else
//                    {
//                        for (i = 0; i < result.centers.length; i++) {
//                            html += "<option value='" + result.centers[i].id + "'";
//                            if (result.centers[i].id == center) {
//                                html += " selected";
//                            }
//                            html += ">" + result.centers[i].CenterName + "</option>";
//                        }
//                    }
                    $("#center_id").html(html);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //    console.log('ERRORS: ' + textStatus);
                },
            });
//            $.ajax({
//                headers: {
//                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
//                },
//                type: 'POST',
//                url: '<//?= action('AdminuserController@postFetchcenterlist') ?>',
//                data:{"region_id":$region_id,"primarysevawing":$primarysevawing},
//                dataType: "json",
//                success: function (result) {
//                    html = "<option value=''>Select Center</option>";
//                    for (i = 0; i < result.centers.length; i++) {
//                        html += "<option value='" + result.centers[i].center_id + "'";
//                        if (result.centers[i].center_id == center) {
//                            html += " selected";
//                        }
//                        html += ">" + result.centers[i].CenterName + "</option>";
//                    }
//                    $("#center_id").html(html);
//                },
//            });
        }
        /*End : Get Center list*/

        /*Begin : Display Parent's Password field as per criteria on page load*/
        check_parentspassword_show();
        /*End : Display Parent's Password field as per criteria on page load*/
        
        /*Begin : Get state as per country change*/
        $("#country").change(function () {
            $country_id = $(this).val();
            $("#state").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchstatename') ?>',
                data: {"country_id": $country_id},
                dataType: "json",
                success: function (result) {
                    $("#state").prop("disabled", false);
                    html = "<option value=''>Select State</option>";
                    if (result.state_count == 1)
                    {
                        html += "<option value='" + result.state.id + "'>" + result.state.name + "</option>";
                    } else
                    {
                        for (i = 0; i < result.state.length; i++) {
                            html += "<option value='" + result.state[i].id + "'>" + result.state[i].name + "</option>";
                        }
                    }
                    $("#state").html(html);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //    console.log('ERRORS: ' + textStatus);
                },
            });
            $("#zip").val("");
        });

        var country_id = "{{old(@country)}}";
        var country_id_db = "<?php
if (count($user) > 0) {
    echo $user->Country;
}
?>";

        if (country_id != "") {
            $country_id = country_id;
            var state = "{{old(@state)}}";
            $("#state").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchstatename') ?>',
                data: {"country_id": $country_id},
                dataType: "json",
                success: function (result) {
                    $("#state").prop("disabled", false);
                    html = "<option value=''>Select State</option>";

                    if (result.state_count == 1)
                    {
                        html += "<option value='" + result.state.id + "'";
                        if (result.state.id == state) {
                            html += " selected";
                        }
                        html += ">" + result.state.name + "</option>";
                    } else
                    {
                        for (i = 0; i < result.state.length; i++) {
                            html += "<option value='" + result.state[i].id + "'";
                            if (result.state[i].id == state) {
                                html += " selected";
                            }
                            html += ">" + result.state[i].name + "</option>";
                        }
                    }
                    $("#state").html(html);
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        } else if (country_id_db != "" && country_id_db != 0) {
            $country_id = country_id_db;
            var state = "<?php
if (count($user) > 0) {
    echo $user->State;
}
?>";
            $("#state").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchstatename') ?>',
                data: {"country_id": $country_id},
                dataType: "json",
                success: function (result) {
                    $("#state").prop("disabled", false);
                    html = "<option value=''>Select State</option>";

                    if (result.state_count == 1)
                    {
                        html += "<option value='" + result.state.id + "'";
                        if (result.state.id == state) {
                            html += " selected";
                        }
                        html += ">" + result.state.name + "</option>";
                    } else
                    {
                        for (i = 0; i < result.state.length; i++) {
                            html += "<option value='" + result.state[i].id + "'";
                            if (result.state[i].id == state) {
                                html += " selected";
                            }
                            html += ">" + result.state[i].name + "</option>";
                        }
                    }
                    $("#state").html(html);
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        }
        /*End : Get region as per country change*/
        //Begin :Dynamic Wing list as per gender
        $("#gender").change(function () {
            $gender = $(this).val();
            $("#primarysevawing,#secondarysevawing").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchuserwings') ?>',
                data: {"gender": $gender},
                dataType: "json",
                success: function (result) {
                    $("#primarysevawing,#secondarysevawing").prop("disabled", false);
                    html = "<option value='0'>Select Wing</option>";
                    for (i = 0; i < result.wings.length; i++) {
                        html += "<option value='" + result.wings[i].id + "'>" + result.wings[i].wingName + "</option>";
                    }
                    $("#primarysevawing,#secondarysevawing").html(html);

                    $('#primaryseva').html("<option value=''>Select Primary Seva</option>");
                    $('#secondaryseva').html("<option value=''>Select Secondary Seva</option>");
                },
                error: function (result) {
                    //console.log(result);
                }
            });

            /*Begin : Fetch group name from gender DOB and school year*/
            var school_year = $("#schoolyear").val();
            var dob = $("#dob").val();
            gender = $(this).val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchwinggroupseva') ?>',
                data: {"schoolyear": school_year, "gender": gender, "dob": dob},
                dataType: "json",
                success: function (result) {

                    $("#hidden_personal_wing_id").val(result.wing_data.wing_id);
                    $("#personal_wing_name").val(result.wing_data.wing_name);
                    var names_arr = ['Bal','bal','BAL','Balak','BALAK','balak','Balika','BALIKA','balika'];
                    
                   var checkgroup =  names_arr.indexOf(result.wing_data.wing_name);
                   if(checkgroup > -1)
                   {
                       $(".subgroup_div").removeClass('hide');
                   }
                   else
                   {
                       $(".subgroup_div").addClass('hide');
                   }
                    if (result.group_data.group_name == "")
                    {
                        $("#group_name").val("");
                    } else {
                        $("#group_name").val(result.group_data.group_name);
                    }
                    $("#group_id").val(result.group_data.group_id);
                    check_parentspassword_show();
                    
                    /*Begin: Check personal wing name is kishore / kishori*/
                    var kishore_array = ['Kishore','kishore','KISHORE','Kishori','kishori','KISHORI','Bal','bal','BAL','Balak','BALAK','balak','Balika','BALIKA','balika'];
                    var existing_personal_wing_name = $("#existing_personal_wing_name").val() != "" ? $("#existing_personal_wing_name").val() : "";
                    var checkgroupkishore_array =  kishore_array.indexOf(result.wing_data.wing_name);
                   if(checkgroupkishore_array > -1)
                   {
                       $(".networking_individual_label").removeClass('hide');
                       if(existing_personal_wing_name != "")
                       {
                           if(existing_personal_wing_name != result.wing_data.wing_name)
                           {
                               //if both wing are not same thne uncheck it
                               $("#networking_target").iCheck('uncheck');
                           }
                       }
                   }
                   else
                   {
                       
                       $(".networking_individual_label").addClass('hide');
                   }
                    /*End: Check personal wing name is kishore / kishori*/
                    
                     $center_id = $('#center_id').val();
                        $personal_wing_name = $('#personal_wing_name').val() == "" ? 0 : $('#personal_wing_name').val();
                        $personal_group_name = $('#group_name').val() == "" ? 0 : $('#group_name').val();
                        //if primarysevawing is blank then load all centers from region
                        $("#subgroup").prop("disabled", true);
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}',
                            },
                            type: 'POST',
                            url: '<?= action('AdminuserController@postFetchsubgroup') ?>',
                            data: {"center_id": $center_id , "personal_wing_name": $personal_wing_name, "group_name": $personal_group_name},
                            dataType: "json",
                            success: function (result) {
                                $("#subgroup").prop("disabled", false);
                                html = "<option value=''>Select Sub Group</option>";

                                for (i = 0; i < result.subgroups_count; i++) {
                                        html += "<option value='" + result.subgroups[i] + "'>" + result.subgroups[i] + "</option>";
                                    }
                                $("#subgroup").html(html);
                            },
                            error: function (result) {
                                //console.log(result);
                            }
                        });
                },
                error: function (result) {
                    //console.log(result);
                    
                     $center_id = $('#center_id').val();
                        $personal_wing_name = $('#personal_wing_name').val() == "" ? 0 : $('#personal_wing_name').val();
                        $personal_group_name = $('#group_name').val() == "" ? 0 : $('#group_name').val();
                        //if primarysevawing is blank then load all centers from region
                        $("#subgroup").prop("disabled", true);
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}',
                            },
                            type: 'POST',
                            url: '<?= action('AdminuserController@postFetchsubgroup') ?>',
                            data: {"center_id": $center_id , "personal_wing_name": $personal_wing_name, "group_name": $personal_group_name},
                            dataType: "json",
                            success: function (result) {
                                $("#subgroup").prop("disabled", false);
                                html = "<option value=''>Select Sub Group</option>";

                                for (i = 0; i < result.subgroups_count; i++) {
                                        html += "<option value='" + result.subgroups[i] + "'>" + result.subgroups[i] + "</option>";
                                    }
                                $("#subgroup").html(html);
                            },
                            error: function (result) {
                                //console.log(result);
                            }
                        });
                }
            });
            /*END : Fetch group name from gender DOB and school year*/
            
            
            
        });
        
        $("#dob").change(function () {
            /*Begin : Fetch group name from gender DOB and school year*/
            var school_year = $("#schoolyear").val();
            var dob = $(this).val();
            var gender = $("#gender").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchwinggroupseva') ?>',
                data: {"schoolyear": school_year, "gender": gender, "dob": dob},
                dataType: "json",
                success: function (result) {

                    $("#hidden_personal_wing_id").val(result.wing_data.wing_id);
                    $("#personal_wing_name").val(result.wing_data.wing_name);
                    
                    var names_arr = ['Bal','bal','BAL','Balak','BALAK','balak','Balika','BALIKA','balika'];
                    
                   var checkgroup =  names_arr.indexOf(result.wing_data.wing_name);
                   if(checkgroup > -1)
                   {
                       $(".subgroup_div").removeClass('hide');
                   }
                   else
                   {
                       $(".subgroup_div").addClass('hide');
                   }
                   
                   var kishore_array = ['Kishore','kishore','KISHORE','Kishori','kishori','KISHORI','Bal','bal','BAL','Balak','BALAK','balak','Balika','BALIKA','balika'];
                   var checkgroupkishore_array =  kishore_array.indexOf(result.wing_data.wing_name);
                   var existing_personal_wing_name = $("#existing_personal_wing_name").val() != "" ? $("#existing_personal_wing_name").val() : "";
                   if(checkgroupkishore_array > -1)
                   {
                       $(".networking_individual_label").removeClass('hide');
                       if(existing_personal_wing_name != "")
                       {
                           if(existing_personal_wing_name != result.wing_data.wing_name)
                           {
                               //if both wing are not same thne uncheck it
                               $("#networking_target").iCheck('uncheck');
                           }
                       }
                   }
                   else
                   {
                       $(".networking_individual_label").addClass('hide');
                   }
                    /*End: Check personal wing name is kishore / kishori*/

                    if (result.group_data.group_name == "")
                    {
                        $("#group_name").val("");
                    } else {
                        $("#group_name").val(result.group_data.group_name);
                    }
                    $("#group_id").val(result.group_data.group_id);
                    check_parentspassword_show();
                    
                    
                    $center_id = $('#center_id').val();
                        $personal_wing_name = $('#personal_wing_name').val() == "" ? 0 : $('#personal_wing_name').val();
                        $personal_group_name = $('#group_name').val() == "" ? 0 : $('#group_name').val();
                        //if primarysevawing is blank then load all centers from region
                        $("#subgroup").prop("disabled", true);
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}',
                            },
                            type: 'POST',
                            url: '<?= action('AdminuserController@postFetchsubgroup') ?>',
                            data: {"center_id": $center_id , "personal_wing_name": $personal_wing_name , "group_name": $personal_group_name},
                            dataType: "json",
                            success: function (result) {
                                $("#subgroup").prop("disabled", false);
                                html = "<option value=''>Select Sub Group</option>";

                                for (i = 0; i < result.subgroups_count; i++) {
                                        html += "<option value='" + result.subgroups[i] + "'>" + result.subgroups[i] + "</option>";
                                    }
                                $("#subgroup").html(html);
                            },
                            error: function (result) {
                                //console.log(result);
                            }
                        });
                },
                error: function (result) {
                    //console.log(result);
                    
                    $center_id = $('#center_id').val();
                        $personal_wing_name = $('#personal_wing_name').val() == "" ? 0 : $('#personal_wing_name').val();
                        $personal_group_name = $('#group_name').val() == "" ? 0 : $('#group_name').val();
                        //if primarysevawing is blank then load all centers from region
                        $("#subgroup").prop("disabled", true);
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}',
                            },
                            type: 'POST',
                            url: '<?= action('AdminuserController@postFetchsubgroup') ?>',
                            data: {"center_id": $center_id , "personal_wing_name": $personal_wing_name, "group_name": $personal_group_name},
                            dataType: "json",
                            success: function (result) {
                                $("#subgroup").prop("disabled", false);
                                html = "<option value=''>Select Sub Group</option>";

                                for (i = 0; i < result.subgroups_count; i++) {
                                        html += "<option value='" + result.subgroups[i] + "'>" + result.subgroups[i] + "</option>";
                                    }
                                $("#subgroup").html(html);
                            },
                            error: function (result) {
                                //console.log(result);
                            }
                        });
                }
            });
            /*END : Fetch group name from gender DOB and school year*/
        });
        //set location value selected if server side validation error occur
        var gender = "{{old(@gender)}}";
        var gender_db = "<?php
if (count($user) > 0) {
    echo $user->Gender;
}
?>";

        if (region_id != "") {
            $gender = gender;
            var primarysevawing = "{{old(@primarysevawing)}}";
            var secondarysevawing = "{{old(@secondarysevawing)}}";
            $("#primarysevawing,#secondarysevawing").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchuserwings') ?>',
                data: {"gender": $gender},
                dataType: "json",
                success: function (result) {
                    $("#primarysevawing,#secondarysevawing").prop("disabled", false);
                    html = "<option value='0'>Select Wing</option>";
                    htmlsecondary = "<option value='0'>Select Wing</option>";
                    for (i = 0; i < result.wings.length; i++) {
                        html += "<option value='" + result.wings[i].id + "'";
                        if (result.wings[i].id == primarysevawing) {
                            html += " selected";
                        }
                        html += ">" + result.wings[i].wingName + "</option>";

                        //Changes in secondary sewa wings
                        htmlsecondary += "<option value='" + result.wings[i].id + "'";
                        if (result.wings[i].id == secondarysevawing) {
                            htmlsecondary += " selected";
                        }
                        htmlsecondary += ">" + result.wings[i].wingName + "</option>";
                    }

//                                    if(result.wings_count == 1)
//                                    {
//                                        html += "<option value='" + result.wings.id + "'";
//                                        if (result.wings.id == primarysevawing) {
//                                            html += " selected";
//                                        }
//                                        html += ">" + result.wings.wingName + "</option>";
//
//                                        //Changes in secondary sewa wings
//                                        htmlsecondary += "<option value='" + result.wings.id + "'";
//                                        if (result.wings.id == secondarysevawing) {
//                                            htmlsecondary += " selected";
//                                        }
//                                        htmlsecondary += ">" + result.wings.wingName + "</option>";
//                                    }
//                                    else
//                                    {
//                                       for (i = 0; i < result.wings.length; i++) {
//                                                html += "<option value='" + result.wings[i].id + "'";
//                                                if (result.wings[i].id == primarysevawing) {
//                                                    html += " selected";
//                                                }
//                                                html += ">" + result.wings[i].wingName + "</option>";
//                        
//                                                //Changes in secondary sewa wings
//                                                htmlsecondary += "<option value='" + result.wings[i].id + "'";
//                                                if (result.wings[i].id == secondarysevawing) {
//                                                    htmlsecondary += " selected";
//                                                }
//                                                htmlsecondary += ">" + result.wings[i].wingName + "</option>";
//                                            }
//                                    }
                    $("#primarysevawing").html(html);
                    $("#secondarysevawing").html(htmlsecondary);
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        }
//                else if(region_id_db != "" && region_id_db != 0){
//                     $region_id = region_id_db;
//                    var primarysevawing = "<?php
if (count($user) > 0) {
    echo $user->PrimarySevaWing;
}
?>";
//                    var secondarysevawing = "<?php
if (count($user) > 0) {
    echo $user->SecondarySevaWing;
}
?>";
        //End : Dynamic Wing list as per gender
        /*Begin : Drodp down for primary seva wing*/
        $("#primarysevawing").change(function () {
            $primarywingid = $(this).val();
            var user_role_type = "{{$user_role_type}}";
            var user_id = <?= $user->id; ?>;
            var login_user = <?= Auth::id(); ?>;
            $("#primaryseva").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchsevafromwingaslevel') ?>',
                data: {"wingid": $primarywingid},
                dataType: "json",
                success: function (result) {
                    $("#primaryseva").prop("disabled", false);
                    html = "<option value=''>Select Primary Seva</option>";
                    for (i = 0; i < result.seva.length; i++)
                    {
                        html += "<option value='" + result.seva[i].id + "'>" + result.seva[i].sevaName + "</option>";
                    }
//                    if(result.seva_count == 1)
//                    {
//                                    html += "<option value='"+result.seva.id+"'>"+result.seva.sevaName+"</option>";
//                    }
//                        else
//                    {
//                                     for(i=0; i<result.seva.length; i++)
//                                    {
//                                        html += "<option value='"+result.seva[i].id+"'>"+result.seva[i].sevaName+"</option>";
//                                    }
//                    }
                    
                    
                    if(user_role_type != 4 )
                    {
                        @permission('update.user.database')
                        if(user_id == login_user)
                        {
                           $("#primaryseva").prop("disabled", true); 
                        }
                        else
                        {
                            $("#primaryseva").html(html);
                        }
                        @else
                        $("#primaryseva").prop("disabled", true); 
                        @endpermission
                         
                    }
                    else
                    {
                        $("#primaryseva").html(html);
                    }
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        });

        var primarywingid = "{{old(@primarysevawing)}}";
        var primarywingid_db = "<?php
if (count($user) > 0) {
    echo $user->PrimarySevaWing;
}
?>";

        if (primarywingid != "" && primarywingid != 0) {
            $primarywingid = primarywingid;
            var primaryseva = "{{old(@primaryseva)}}";
            $("#primaryseva").prop("disabled", true);
            
            var user_role_type = "{{$user_role_type}}";
            var user_id = <?= $user->id; ?>;
            var login_user = <?= Auth::id(); ?>;
          
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchsevafromwingaslevel') ?>}',
                data: {"wingid": $primarywingid},
                dataType: "json",
                success: function (result) {
                    $("#primaryseva").prop("disabled", false);
                    html = "<option value=''>Select Primary Seva</option>";
                    for (i = 0; i < result.seva.length; i++) {
                        html += "<option value='" + result.seva[i].id + "'";
                        if (result.seva[i].id == primaryseva) {
                            html += " selected";
                        }
                        html += ">" + result.seva[i].sevaName + "</option>";
                    }
//                    if(result.seva_count == 1)
//                                {
//                                    html += "<option value='"+result.seva.id+"'";
//                                         if(result.seva.id == primaryseva){ 
//                                             html +=" selected";
//                                         }       
//                                         html+=">"+result.seva.sevaName+"</option>";
//                                }
//                                else
//                                {
//                                    for (i = 0; i < result.seva.length; i++) {
//                                        html += "<option value='" + result.seva[i].id + "'";
//                                        if (result.seva[i].id == primaryseva) {
//                                            html += " selected";
//                                        }
//                                        html += ">" + result.seva[i].sevaName + "</option>";
//                                    }
//                                }
                    $("#primaryseva").html(html);
                    if(user_role_type != 4 )
                    {
                         
                         @permission('update.user.database')
                        if(user_id == login_user)
                        {
                           $("#primaryseva").prop("disabled", true);
                        }
                        @else
                        $("#primaryseva").prop("disabled", true);
                        @endpermission
                    }    
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        } else if (primarywingid_db != "" && primarywingid_db != 0) {
            $primarywingid = primarywingid_db;
            var primaryseva = "<?php
if (count($user) > 0) {
    echo $user->PrimarySeva;
}
?>";
                var user_role_type = "{{$user_role_type}}";
            var user_id = <?= $user->id; ?>;
            var login_user = <?= Auth::id(); ?>;
            $("#primaryseva").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchsevafromwingaslevel') ?>',
                data: {"wingid": $primarywingid},
                dataType: "json",
                success: function (result) {
                    $("#primaryseva").prop("disabled", false);
                    html = "<option value=''>Select Primary Seva</option>";
                    for (i = 0; i < result.seva.length; i++) {
                        html += "<option value='" + result.seva[i].id + "'";
                        if (result.seva[i].id == primaryseva) {
                            html += " selected";
                        }
                        html += ">" + result.seva[i].sevaName + "</option>";
                    }


//                    if(result.seva_count == 1)
//                                {
//                                    html += "<option value='"+result.seva.id+"'";
//                                         if(result.seva.id == primaryseva){ 
//                                             html +=" selected";
//                                         }       
//                                         html+=">"+result.seva.sevaName+"</option>";
//                                }
//                                else
//                                {
//                                    for (i = 0; i < result.seva.length; i++) {
//                                        html += "<option value='" + result.seva[i].id + "'";
//                                        if (result.seva[i].id == primaryseva) {
//                                            html += " selected";
//                                        }
//                                        html += ">" + result.seva[i].sevaName + "</option>";
//                                    }
//                                }
                    $("#primaryseva").html(html);
                    
                    if(user_role_type != 4)
                    {
                         @permission('update.user.database')
                        if(user_id == login_user)
                        {
                           $("#primaryseva").prop("disabled", true);
                        }
                        @else
                        $("#primaryseva").prop("disabled", true);
                        @endpermission
                    }
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        }
        /*End : Drodp down for primary seva wing*/

        /*Begin : Fetch group name from wing and school year*/
        $("#schoolyear").change(function () {
            school_year = $(this).val();
            dob = $("#dob").val();
            gender = $("#gender").val();

            //NOTE : for right now group only assign from AGE
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchwinggroupseva') ?>',
                data: {"schoolyear": school_year, "gender": gender, "dob": dob},
                dataType: "json",
                success: function (result) {
                    html = "<option value=''>Select Primary Seva</option>";
                    for (i = 0; i < result.seva.length; i++) {
                        html += "<option value='" + result.seva[i].id + "'>" + result.seva[i].sevaName + "</option>";
                    }
//                    $("#primaryseva").html(html);
                    if (result.wing_data.wing_id == 0) {
                        html = "<option value=" + result.wing_data.wing_id + " selected>Select Wing</option>";
                    } else {
                        html = "<option value=" + result.wing_data.wing_id + " selected>" + result.wing_data.wing_name + "</option>";
                    }
//                    $("#primarysevawing").html(html);
                    $("#hidden_primarysevawing").val(result.wing_data.wing_id);

                    $("#hidden_personal_wing_id").val(result.wing_data.wing_id);
                    $("#personal_wing_name").val(result.wing_data.wing_name);
                    
                    var names_arr = ['Bal','bal','BAL','Balak','BALAK','balak','Balika','BALIKA','balika'];
                    
                   var checkgroup =  names_arr.indexOf(result.wing_data.wing_name);
                   if(checkgroup > -1)
                   {
                       $(".subgroup_div").removeClass('hide');
                   }
                   else
                   {
                       $(".subgroup_div").addClass('hide');
                   }
                   
                   
                   var kishore_array = ['Kishore','kishore','KISHORE','Kishori','kishori','KISHORI','Bal','bal','BAL','Balak','BALAK','balak','Balika','BALIKA','balika'];
                   var existing_personal_wing_name = $("#existing_personal_wing_name").val() != "" ? $("#existing_personal_wing_name").val() : "";
                   var checkgroupkishore_array =  kishore_array.indexOf(result.wing_data.wing_name);
                   if(checkgroupkishore_array > -1)
                   {
                       $(".networking_individual_label").removeClass('hide');
                       if(existing_personal_wing_name != "")
                       {
                           if(existing_personal_wing_name != result.wing_data.wing_name)
                           {
                               //if both wing are not same thne uncheck it
                               $("#networking_target").iCheck('uncheck');
                           }
                       }
                   }
                   else
                   {
                       $(".networking_individual_label").addClass('hide');
                   }
                    /*End: Check personal wing name is kishore / kishori*/
                    if (result.group_data.group_name == "") {
                        $("#group_name").val("");
                    } else {
                        $("#group_name").val(result.group_data.group_name);
                    }
                    $("#group_id").val(result.group_data.group_id);
                    
                    check_parentspassword_show();
                    
                    $center_id = $('#center_id').val();
                        $personal_wing_name = $('#personal_wing_name').val() == "" ? 0 : $('#personal_wing_name').val();
                        $personal_group_name = $('#group_name').val() == "" ? 0 : $('#group_name').val();
                        //if primarysevawing is blank then load all centers from region
                        $("#subgroup").prop("disabled", true);
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}',
                            },
                            type: 'POST',
                            url: '<?= action('AdminuserController@postFetchsubgroup') ?>',
                            data: {"center_id": $center_id , "personal_wing_name": $personal_wing_name, "group_name": $personal_group_name},
                            dataType: "json",
                            success: function (result) {
                                $("#subgroup").prop("disabled", false);
                                html = "<option value=''>Select Sub Group</option>";

                                for (i = 0; i < result.subgroups_count; i++) {
                                        html += "<option value='" + result.subgroups[i] + "'>" + result.subgroups[i] + "</option>";
                                    }
                                $("#subgroup").html(html);
                            },
                            error: function (result) {
                                //console.log(result);
                            }
                        });
                },
                error: function (result) {
                $center_id = $('#center_id').val();
                        $personal_wing_name = $('#personal_wing_name').val() == "" ? 0 : $('#personal_wing_name').val();
                        $personal_group_name = $('#group_name').val() == "" ? 0 : $('#group_name').val();
                        //if primarysevawing is blank then load all centers from region
                        $("#subgroup").prop("disabled", true);
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}',
                            },
                            type: 'POST',
                            url: '<?= action('AdminuserController@postFetchsubgroup') ?>',
                            data: {"center_id": $center_id , "personal_wing_name": $personal_wing_name, "group_name": $personal_group_name},
                            dataType: "json",
                            success: function (result) {
                                $("#subgroup").prop("disabled", false);
                                html = "<option value=''>Select Sub Group</option>";

                                for (i = 0; i < result.subgroups_count; i++) {
                                        html += "<option value='" + result.subgroups[i] + "'>" + result.subgroups[i] + "</option>";
                                    }
                                $("#subgroup").html(html);
                            },
                            error: function (result) {
                                //console.log(result);
                            }
                        });
                }
            });
        });
        /*End : Drodp down for primary seva wing*/
        /*Begin : Drodp down for secondary seva wing*/

        $("#secondarysevawing").change(function () {
        
        var user_role_type = "{{$user_role_type}}";
            var user_id = <?= $user->id; ?>;
            var login_user = <?= Auth::id(); ?>;
            $secondarywingid = $(this).val();
            $("#secondaryseva").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchsevafromwingaslevel') ?>',
                data: {"wingid": $secondarywingid},
                dataType: "json",
                success: function (result) {
                    $("#secondaryseva").prop("disabled", false);
                    html = "<option value=''>Select Secondary Seva</option>";

                    for (i = 0; i < result.seva.length; i++) {
                        html += "<option value='" + result.seva[i].id + "'>" + result.seva[i].sevaName + "</option>";
                    }
//                    if(result.seva_count == 1)
//                    {
//                        html += "<option value='" + result.seva.id + "'>" + result.seva.sevaName + "</option>";
//                    }
//                    else
//                    {
//                        for (i = 0; i < result.seva.length; i++) {
//                            html += "<option value='" + result.seva[i].id + "'>" + result.seva[i].sevaName + "</option>";
//                        }
//                    }
                    
                    
                    if(user_role_type != 4)
                    {
                         @permission('update.user.database')
                        if(user_id == login_user)
                        {
                           $("#secondaryseva").prop("disabled", true);
                        }
                        else
                        {
                            $("#secondaryseva").html(html);
                        }
                        @else
                        $("#secondaryseva").prop("disabled", true);
                        @endpermission
                    }
                    else
                    {
                        $("#secondaryseva").html(html);
                    }
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        });

        var secondarywingid = "{{old(@secondarysevawing)}}";
        var secondarywingid_db = "<?php
if (count($user) > 0) {
    echo $user->SecondarySevaWing;
}
?>";

        if (secondarywingid != "") {
            $secondarywingid = secondarywingid;
            var secondaryseva = "{{old(@secondaryseva)}}";
            $("#secondaryseva").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchsevafromwingaslevel') ?>',
                data: {"wingid": $secondarywingid},
                dataType: "json",
                success: function (result) {
                    $("#secondaryseva").prop("disabled", false);
                    html = "<option value=''>Select Secondary Seva</option>";

                    if (result.seva_count == 1)
                    {
                        html += "<option value='" + result.seva.id + "'";
                        if (result.seva.id == secondaryseva) {
                            html += " selected";
                        }
                        html += ">" + result.seva.sevaName + "</option>";
                    } else
                    {
                        for (i = 0; i < result.seva.length; i++) {
                            html += "<option value='" + result.seva[i].id + "'";
                            if (result.seva[i].id == secondaryseva) {
                                html += " selected";
                            }
                            html += ">" + result.seva[i].sevaName + "</option>";
                        }
                    }
                    $("#secondaryseva").html(html);
                    
                    if(user_role_type != 4)
                    {
                         @permission('update.user.database')
                        if(user_id == login_user)
                        {
                           $("#secondaryseva").prop("disabled", true);
                        }
                        @else
                        $("#secondaryseva").prop("disabled", true);
                        @endpermission
                    }
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        } else if (secondarywingid_db != "" && secondarywingid_db != 0) {
            $secondarywingid = secondarywingid_db;
            var secondaryseva = "<?php
if (count($user) > 0) {
    echo $user->SecondarySeva;
}
?>";
                
                var user_role_type = "{{$user_role_type}}";
            var user_id = <?= $user->id; ?>;
            var login_user = <?= Auth::id(); ?>;
            
            
            $("#secondaryseva").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchsevafromwingaslevel') ?>',
                data: {"wingid": $secondarywingid},
                dataType: "json",
                success: function (result) {
                    $("#secondaryseva").prop("disabled", false);
                    html = "<option value=''>Select Secondary Seva</option>";
                    for (i = 0; i < result.seva.length; i++) {
                        html += "<option value='" + result.seva[i].id + "'";
                        if (result.seva[i].id == secondaryseva) {
                            html += " selected";
                        }
                        html += ">" + result.seva[i].sevaName + "</option>";
                    }


//                    if(result.seva_count == 1)
//                    {
//                            html += "<option value='" + result.seva.id + "'";
//                            if (result.seva.id == secondaryseva) {
//                                html += " selected";
//                            }
//                            html += ">" + result.seva.sevaName + "</option>";
//                    }
//                    else
//                    {
//                            for (i = 0; i < result.seva.length; i++) {
//                                html += "<option value='" + result.seva[i].id + "'";
//                                if (result.seva[i].id == secondaryseva) {
//                                    html += " selected";
//                                }
//                                html += ">" + result.seva[i].sevaName + "</option>";
//                            }
//                    }
                    $("#secondaryseva").html(html);
                    
                    if(user_role_type != 4)
                    {
                         
                         @permission('update.user.database')
                        if(user_id == login_user)
                        {
                           $("#secondaryseva").prop("disabled", true);
                        }
                        @else
                        $("#secondaryseva").prop("disabled", true);
                        @endpermission
                    }
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        }
        /*End : Drodp down for secondary seva wing*/
        //Begin : Fetch Center Name On region change for relaocate usser(applied in modal popup)
        $("#new_region").change(function () {
            $region_id = $(this).val();
            $current_region_id = $("#current_region_id").val();
            $primarysevawing = $("#primarysevawing").val() != "" ? $("#primarysevawing").val() : 0;
            $("#new_center").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postRegionbasedcenterajaxignorecurrent') ?>',
                data: {"region_id": $region_id, "current_center_id": $current_region_id, "primarysevawing": $primarysevawing},
                dataType: "json",
                success: function (result) {
                    $("#new_center").prop("disabled", false);
                    html = "<option value=''>Select Center</option>";
                    if (result.center_count == 1)
                    {
                        //html += "<option value='" + result.centers.id + "'>" + result.centers.CenterName + "</option>";
                        for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
                        }
                    } else
                    {
                        for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
                        }
                    }

                    $("#new_center").html(html);
                },
                error: function (result) {
                    //console.log(result);
                }
            });
        });
        //Begin : Fetch Center Name On region change for relaocate usser(applied in modal popup)

        //Begin: To allow Only Numberic value from 0-9
        $("#phonehome,#phonecell,#emergencycontact1priphone,#emergencycontact1secphone,#emergencycontact2priphone,#emergencycontact2secphone,#fathercellphone,#mothercellphone").keypress(function (event) {
            var key = event.which;
//            if (!(key >= 48 && key <= 57))
//                event.preventDefault();

            var regex = new RegExp("^[0-9]+$");
            var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (regex.test(str)) {
                return true;
            } else
            {
                if ($("zip").val() != "")
                {
                    if (event.which == 13)
                    {
                        return true;
                    }
                }
            }
            event.preventDefault();
            return false;
        });
        //End: To allow Only Numberic value from 0-9
        /*Begin : Don't Allow Space In Social Media Input*/
        $("#facebook_id,#twitter_id,#snapchat_id,#instagram_id,#linkedin_id").keydown(function (e) {
            if (e.keyCode == 32) {
                return false; // return false to prevent space from being added
            }
        });
        /*End : Don't Allow Space In Social Media Input*/
        $("#zip").keypress(function (event) {

            if ($("#country option:selected").text() == "Canada" || $("#country option:selected").text() == "canada")
            {
//                        
                var regex = new RegExp("^[a-zA-Z0-9]+$");
                var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (regex.test(str)) {
                    return true;
                } else
                {
                    if ($("zip").val() != "")
                    {
                        if (event.which == 13)
                        {
                            return true;
                        }
                    }
                }
                event.preventDefault();
                return false;
            } else
            {

                var regex = new RegExp("^[0-9]+$");
                var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (regex.test(str)) {
                    return true;
                } else
                {
                    if ($("zip").val() != "")
                    {
                        if (event.which == 13)
                        {
                            return true;
                        }
                    }
                }
                event.preventDefault();
                return false;
            }

        });

        // Begin :  Set Sizze for bootstrap select picker for Spouse id
        $('.selectpicker').selectpicker({
            // style: 'btn-info',
            size: 10
        });
        // End :  Set Sizze for bootstrap select picker for Spouse id

        //Begin : For Open Modal For Relocate User
        $('.openrelocateuser').click(function (event) {
            $("#relocate_user_modal").modal('show');

        });
        //End : For Open Modal For Relocate User
        
        /*Begin : Formating  Empty Table Of Campus Sabha & Karyakar Goshthi Sabha In Sabha Attendace Module*/
        $('.campus_sabha').click(function (event) {
            if($("#campus_sabha_list tbody tr td:first").hasClass("dataTables_empty"))
            {
                $("#campus_sabha_list tbody tr td:first").attr("colspan",5);
            }
        });
        
        $('.karyakar_goshthi').click(function (event) {
            if($("#karyakar_goshthi_list tbody tr td:first").hasClass("dataTables_empty"))
            {
                $("#karyakar_goshthi_list tbody tr td:first").attr("colspan",4);
            }
        });
        /*End : Formating  Empty Table Of Campus Sabha & Karyakar Goshthi Sabha In Sabha Attendace Module*/

        //Begin:For Relocate user with form validation
        $('#save_relocate_user').click(function (event) {

            //Form validation for Relocate User

            $('#relocate_user_form').validate(
                    {
                        rules: {
                            new_region: {
                                required: true
                            },
                            new_center: {
                                required: true
                            }
                        },
                        messages:
                                {
                                    new_region: {
                                        required: "Please select new region.",
                                    },
                                    new_center: {
                                        required: "Please select new center.",
                                    }
                                },
                        submitHandler: function (form)
                        {
                            event.preventDefault();
                            var data_String = $("#relocate_user_form").serialize();
                            //Ajax call for submit new bank account
                            $.post('<?php echo action('AdminuserController@postRelocateuser'); ?>', data_String, function (data)
                            {
                                var result = data.status;
                                if (result == true)
                                {
                                    $('#relocate_user_modal').modal('hide');
                                    //alert(data.message);
                                    toastr.success(data.message, 'Success');
                                    $('#relocate_user_modal').trigger("reset");
                                    location.reload();
//                                    window.location="<//?php echo action('AdminuserController@getUserlist'); ?>";
                                } else
                                {
                                    $('#relocate_user_modal').modal('hide');
                                    toastr.error(data.message, 'Error');
                                    $('#relocate_user_modal').trigger("reset");
                                    location.reload();
                                }
                            });

                        }
                    });
        });
        //End:For Relocate User with form validation
        $("#address1").geocomplete();
        //$("#address2").geocomplete();
        /*Begin : Fetch City , State ,Country Info Using Address*/
        $('#address1').keyup(function () {
            $('#is_address_changed').val('2');
            var is_mannual_click = $("#is_mannual_click").val();
            if ($("#address1").val() != "" && is_mannual_click == 1) //if input is available in address field and address is not mannual selection then call this ajax
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: '<?= action('AdminuserController@postFetchaddressinfo') ?>',
                    data: {"address": $("#address1").val()},
                    //dataType: "json",
                    success: function (result) {
                        if (result != "false")
                        {
                            $("#is_verified_address").val('1');
                            $("#verified_city").val(result.city);
                            $("#verified_country").val(result.country_id);
                            $("#verified_state").val(result.state_id);
                            $("#verified_zip").val(result.zipcode);
                            $(".countrylist,.citystateziplist").addClass('hide');
                            $(".mannual_address_option").html('');
                        } else
                        {
                            $("#is_verified_address").val('-1');
                            $(".mannual_address_option").html('<a href="javascript:void(0);" class="apply_mannual_address">Manually Enter Address</a>');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //    console.log('ERRORS: ' + textStatus);
                    },
                });
            }
        });
        /*End : Fetch City , State ,Country Info Using Address*/
        /*Begin : Display Mannual Address*/;
        $(document).on('click', "a.apply_mannual_address", function () {
            $("#is_verified_address").val('2');
            $("#address1").val('');
            $("#is_mannual_click").val('2');
            $(".countrylist,.citystateziplist").removeClass('hide');
            $(".mannual_address_option").html('<label class="mannualinfo">Manually Enter Address</label><br><a href="javascript:void(0);" class="dynamic_address">Enter Verified Address</a>');
        });
        /*End : Display Mannual Address*/
        /*Begin : Display Dynmic / Default Address*/;
        $(document).on('click', "a.dynamic_address", function () {
            $("#is_verified_address").val('');
            $("#address1").val('');
            $("#is_mannual_click").val('1');
            $(".countrylist,.citystateziplist").addClass('hide');
            $(".mannual_address_option").html('');
        });
        /*End : Display Dynmic / Default Address*/
        /*Begin : Address Mannual Validation On Save Button Click*/

        $(document).on('click', ".updateinfo", function () {

            var is_verified_address = $("#is_verified_address").val();
            if (is_verified_address == -1)
            {
                //alert("Address is not verified.Please click on Mannual Enter Address for enter your address"); return false;

                toastr.info("Address is not verified.Please click on Mannual Enter Address for enter your address", 'Info');
                return false;
            }
            var user_personal_wing_name = $("#personal_wing_name").val();
            
            var useremail = $("#email").val();
                var fatheremail = $("#fatheremail").val();
                var motheremail = $("#motheremail").val();
                if (useremail != "")
                {
                    if (fatheremail != "")
                    {
                        if (fatheremail == useremail)
                        {
                            toastr.info("User's email address should not the same as the father's or mother's email address", 'Info');
                            return false;
                        }
                    }

                    if (motheremail != "")
                    {
                        if (motheremail == useremail)
                        {
                            toastr.info("User's email address should not the same as the father's or mother's email address", 'Info');
                            return false;
                        }
                    }
                }



        });
        /* End : Address Mannual Validation On SAve Click*/
        /*Begin : Set Only First Character in capitalize*/
        $('#allergies_medicalinfo').keyup(function () {
            var yourtext = $(this).val();
            var allergies_medicalinfo = yourtext.substr(0, 1).toUpperCase() + yourtext.substr(1);
            $("#allergies_medicalinfo").val(allergies_medicalinfo);
        });
        /*End: Set Only First Character in capitalize*/
        /*Begin : For Delete Record Show Modal and manage action*/
        $(document).on('click', '.deleterecord', function (e) {
            $("#modal_deleterecord").modal('show');
            var passpath = $(this).attr('data-pathaccess');
            if (passpath != "")
            {
                $("#redirectpath").attr('value', passpath);
            }
        });

        $(document).on('click', '.confirmdelete', function (e) {
            var redirectpasspath = $("#redirectpath").val();
            if (redirectpasspath != "")
            {
                window.location.href = redirectpasspath;
            } else
            {
                setTimeout(function () {
                    location.reload();
                }, 2000);
                toastr.error("<?= Config::get('constants.GENERAL_ERROR') ?>", 'Error');
            }
        });

        /*End: For Delete Record Show Modal and manage action*/

        //Begin :  Add Method For Check Father Email Address Exist Or Not
        $.validator.addMethod("checkfatheremailexistorno",
                function (value, element) {
                    var result;
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: '<?= action('AdminuserController@postValidatefatheremail') ?>', // script to validate in server side
                        data: {fatheremail: value},
                        success: function (data) {

                            result = (data != "false") ? true : false;

                        }
                    });
                    // return true if email is exist in database
                    return result;
                },
                "<?= Config::get('constants.FATHER_EMAIL_NOT_FOUND') ?>"
                );
        //End :  Add Method For Check Father Email Address Exist Or Not
        //Begin :  Add Method For Check Mother Email Address Exist Or Not
//                if($("#motheremail").val() != "")
//                {
        $.validator.addMethod("checkmotheremailexistorno",
                function (value, element) {
                    var result;
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: '<?= action('AdminuserController@postValidatemotheremail') ?>', // script to validate in server side
                        data: {motheremail: value},
                        success: function (data) {

                            result = (data != "false") ? true : false;

                        }
                    });
                    // return true if email is exist in database
                    return result;
                },
                "<?= Config::get('constants.FATHER_EMAIL_NOT_FOUND') ?>"
                );
        //End :  Add Method For Check Mother Email Address Exist Or Not

        //Begin : User My Event List
        var show_colslist = [0, 1, 2, 3, 4];
        var event_user_list = $('#event_user_list').dataTable({
            "processing": false,
//            "serverSide": true,
            "bFilter": false,
            "responsive": true,
//            "bSort": false,
            "bLengthChange": false,
            "serverSide": true, //for load back page where edit action was applied
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    title: 'User Events',
                    text: 'Export To Excel',
                    className: 'exportbutton',
                    exportOptions: {
                        columns: ':visible:not(.not-export-col)'
                    }
                },
                {
                    extend: 'columnsToggle',
                    columns: show_colslist,
                },
            ],
            "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Event(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Record found.", "sLengthMenu": "Show _MENU_"},
            "ajax": {
                'type': 'POST',
                'url': '<?= action('AdminuserController@postFetchusermyeventlist') ?>',
                'data': function (param) {
                    param.user_id = '{{$user->id}}';
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //    console.log('ERRORS: ' + textStatus);
                },
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex) {

                var oSettings = event_user_list.fnSettings();
                $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);

                return nRow;
            },
            "language": {
                "emptyTable": "No record found.",
                "search": "",
                "sProcessing": "<i class='fa fa-2x fa-spin fa-refresh'></i>"
            },
            "columns": [
                {"data": "id", name: "e.id", className: "table_titlecase_record", "searchable": false, "bsortable": false},
                {"data": "event_title", name: 'e.event_title', "class": "table_titlecase_record"},
                {"data": "event_dates", "class": "table_titlecase_record", "searchable": false},
                {"data": "event_location", name: 'e.event_location', "class": "table_titlecase_record"},
                {"data": "status", className: "table_titlecase_record", "searchable": false, "bsortable": false, "orderable": false}
            ],
            aoColumnDefs: [
                {targets: -1, visible: show_colslist}
            ]
        });
        //End : User My Event List
        
        //Begin : User Sabha Reporting List
        var show_colslist_sabha_reporting_list = [0, 1, 2, 3, 4, 5];
        var sabha_reporting_list = $('#sabha_reporting_list').dataTable({
            "processing": false,
            "bFilter": false,
            "responsive": true,
            "bLengthChange": false,
            "serverSide": true, //for load back page where edit action was applied
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    title: 'Sabha Reporting',
                    text: 'Export To Excel',
                    className: 'exportbutton',
                    exportOptions: {
                        columns: ':visible:not(.not-export-col)'
                    }
                },
                {
                    extend: 'columnsToggle',
                    columns: show_colslist_sabha_reporting_list,
                },
            ],
            "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Sabha(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Record found.", "sLengthMenu": "Show _MENU_"},
            "ajax": {
                'type': 'POST',
                'url': '<?= action('AdminuserController@postFetchusersabhareporting') ?>',
                'data': function (param) {
                    param.user_id = '{{$user->id}}';
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //    console.log('ERRORS: ' + textStatus);
                },
            },
//            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
//
//                var oSettings = sabha_reporting_list.fnSettings();
//                $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
//
//                return nRow;
//            },
            "language": {
                "emptyTable": "No record found.",
                "search": "",
                "sProcessing": "<i class='fa fa-2x fa-spin fa-refresh'></i>"
            },
            "columns": [
//                {"data": "id", name: "sb.id", className: "table_titlecase_record", "searchable": false, "bsortable": false},
                {"data": "sabha_date", name: 'sb.sabha_date', "class": "table_titlecase_record"},
                {"data": "sabha_label", name: 'sb.sabha_label', "class": "table_titlecase_record", "searchable": false},
                {"data": "wingName", name: 'w.wingName', "class": "table_titlecase_record"},
                {"data": "RegionName", name: 'r.RegionName', "class": "table_titlecase_record"},
                {"data": "CenterName", name: 'c.CenterName', "class": "table_titlecase_record"},
                {"data": "attendee_status", name: 'sa.attendee_status', "class": "table_titlecase_record", "bsortable": false, "orderable": false},
            ],
            aoColumnDefs: [
                {targets: -1, visible: show_colslist_sabha_reporting_list}
            ]
        });
        //End : User Sabha Reporting List
        
        
        
        //Begin : User Campus Sabha List
        var show_colslist_campus_sabha_list = [0, 1, 2, 3, 4];
        var campus_sabha_list = $('#campus_sabha_list').dataTable({
            "processing": false,
            "bFilter": false,
            "responsive": true,
            "bLengthChange": false,
            "serverSide": true, //for load back page where edit action was applied
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    title: 'Sabha Reporting',
                    text: 'Export To Excel',
                    className: 'exportbutton',
                    exportOptions: {
                        columns: ':visible:not(.not-export-col)'
                    }
                },
                {
                    extend: 'columnsToggle',
                    columns: show_colslist_campus_sabha_list,
                },
            ],
            "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Campus Sabha(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Record found.", "sLengthMenu": "Show _MENU_"},
            "ajax": {
                'type': 'POST',
                'url': '<?= action('AdminuserController@postFetchusercampussabha') ?>',
                'data': function (param) {
                    param.user_id = '{{$user->id}}';
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //    console.log('ERRORS: ' + textStatus);
                },
            },
//            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
//
//                var oSettings = campus_sabha_list.fnSettings();
//                $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
//
//                return nRow;
//            },
            "language": {
                "emptyTable": "No record found.",
                "search": "",
                "sProcessing": "<i class='fa fa-2x fa-spin fa-refresh'></i>"
            },
            "columns": [
//                {"data": "id", name: "cb.id", className: "table_titlecase_record", "searchable": false, "bsortable": false},
                {"data": "sabha_date", name: 'cb.sabha_date', "class": "table_titlecase_record"},
                {"data": "sabha_label", name: 'cb.sabha_label', "class": "table_titlecase_record", "searchable": false},
                {"data": "RegionName", name: 'r.RegionName', "class": "table_titlecase_record"},
                {"data": "CenterName", name: 'c.CenterName', "class": "table_titlecase_record"},
                {"data": "campus_name", name: 'cm.campus_name', "class": "table_titlecase_record"},
            ],
            aoColumnDefs: [
                {targets: -1, visible: show_colslist_campus_sabha_list}
            ],
            
        });
        //End : User Campus Sabha List
        
        
        /*Begin: Fetch Sub group*/
        $("#center_id").change(function () {
            $center_id = $('#center_id').val();
            $personal_wing_name = $('#personal_wing_name').val() == "" ? 0 : $('#personal_wing_name').val();
            $personal_group_name = $('#group_name').val() == "" ? 0 : $('#group_name').val();
            //if primarysevawing is blank then load all centers from region
            $("#subgroup").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchsubgroup') ?>',
                data: {"center_id": $center_id , "personal_wing_name": $personal_wing_name, "group_name": $personal_group_name},
                dataType: "json",
                success: function (result) {
                    $("#subgroup").prop("disabled", false);
                    html = "<option value=''>Select Sub Group</option>";
                    
                    for (i = 0; i < result.subgroups_count; i++) {
                            html += "<option value='" + result.subgroups[i] + "'>" + result.subgroups[i] + "</option>";
                        }
                    $("#subgroup").html(html);
                },
                error: function (result) {
                    //console.log(result);
                }
            });
            
            $("#networking_target").iCheck('uncheck');
        });

        //set location value selected if server side validation error occur
        var center_id = "{{old(@center_id)}}";
        var center_id_db = "<?php
if (count($user) > 0) {
    echo $user->center_id;
}
?>";

        var personal_wing_name = "{{old(@personal_wing_name)}}";
        var personal_wing_name_db = $("#personal_wing_name").val();
        var personal_group_name = "{{old(@group_name)}}";
        var personal_group_name_db = $("#group_name").val();
        if (center_id != "") {
            $center_id = center_id;
            $personal_wing_name = personal_wing_name;
            $personal_group_name = personal_group_name;
            var subgroup = "{{old(@subgroup)}}";
            $("#subgroup").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchsubgroup') ?>',
                data: {"center_id": $center_id , "personal_wing_name": $personal_wing_name, "group_name": $personal_group_name},
                dataType: "json",
                success: function (result) {
                    $("#subgroup").prop("disabled", false);
                    html = "<option value=''>Select Sub Group</option>";
                    
                    for (i = 0; i < result.subgroups_count; i++) {
                            html += "<option value='" + result.subgroups[i] + "'";
                                if (result.subgroups[i] == subgroup) {
                                    html += " selected";
                                }
                                html += ">" + result.subgroups[i] + "</option>";
                        }
                    $("#subgroup").html(html);
                },
                error: function (result) {
                    //console.log(result);
                }

            });
        } else if (center_id_db != "" && center_id_db != 0) {
            $center_id = center_id_db;
            $personal_wing_name = personal_wing_name_db;
            $personal_group_name = personal_group_name_db;

            var subgroup = "<?php
if (count($user) > 0) {
    echo $user->subgroup;
}
?>";
            $("#subgroup").prop("disabled", true);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                type: 'POST',
                url: '<?= action('AdminuserController@postFetchsubgroup') ?>',
                data: {"center_id": $center_id , "personal_wing_name": $personal_wing_name, "group_name": $personal_group_name},
                dataType: "json",
                success: function (result) {
                    $("#subgroup").prop("disabled", false);
                    html = "<option value=''>Select Sub Group</option>";
                    
                    for (i = 0; i < result.subgroups_count; i++) {
                            
                            html += "<option value='" + result.subgroups[i] + "'";
                                if (result.subgroups[i] == subgroup) {
                                    html += " selected";
                                }
                                html += ">" + result.subgroups[i] + "</option>";
                        }
                    $("#subgroup").html(html);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //    console.log('ERRORS: ' + textStatus);
                },
            });
        }
        /*End: Fetch Sub Group*/
        
        
        //Begin : User Campus Sabha List
        var show_colslist_karyakar_goshthi_list = [0, 1, 2, 3];
        var karyakar_goshthi_list = $('#karyakar_goshthi_list').dataTable({
            "processing": false,
            "bFilter": false,
            "responsive": true,
            "bLengthChange": false,
            "serverSide": true, //for load back page where edit action was applied
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excel',
                    title: 'Sabha Reporting',
                    text: 'Export To Excel',
                    className: 'exportbutton',
                    exportOptions: {
                        columns: ':visible:not(.not-export-col)'
                    }
                },
                {
                    extend: 'columnsToggle',
                    columns: show_colslist_karyakar_goshthi_list,
                },
            ],
            "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Campus Sabha(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Record found.", "sLengthMenu": "Show _MENU_"},
            "ajax": {
                'type': 'POST',
                'url': '<?= action('AdminuserController@postFetchuserkaryakargoshthi') ?>',
                'data': function (param) {
                    param.user_id = '{{$user->id}}';
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //    console.log('ERRORS: ' + textStatus);
                },
            },
//            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
//
//                var oSettings = karyakar_goshthi_list.fnSettings();
//                $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);
//
//                return nRow;
//            },
            "language": {
                "emptyTable": "No record found.",
                "search": "",
                "sProcessing": "<i class='fa fa-2x fa-spin fa-refresh'></i>"
            },
            "columns": [
    //                {"data": "id", name: "ks.id", className: "table_titlecase_record", "searchable": false, "bsortable": false},
                {"data": "sabha_date", name: 'ks.sabha_date', "class": "table_titlecase_record"},
                {"data": "sabha_label", name: 'ks.sabha_label', "class": "table_titlecase_record", "searchable": false},
                {"data": "RegionName", name: 'r.RegionName', "class": "table_titlecase_record"},
                {"data": "CenterName", name: 'c.CenterName', "class": "table_titlecase_record"},
            ],
            aoColumnDefs: [
                {targets: -1, visible: show_colslist_karyakar_goshthi_list}
            ],
            
        });
        //End : User Campus Sabha List
    });
})(jQuery);
/*Edit Chldren info view in modal*/
function editchildinfo(id) {
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
        },
        type: 'POST',
        url: '<?= action('AdminuserController@postGetuserchildid') ?>',
        data: {id: id},
        dataType: "json",
        success: function (result) {
            $("label.error").hide(); // this 2 lines are used for remove validation message initially.
            $(".error").removeClass("error");

            $('#edit_child_id').val(result.id);
            $('#child_id').val(result.id);
            $('#edit_first_name').val(result.first_name);
            $('#edit_last_name').val(result.last_name);
//            $('#edit_first_name').attr("value",result.first_name);
//            $('#edit_last_name').attr("value",result.last_name);
            $('#edit_gender_child').val(result.gender);
            //$('#edit_dob_child').attr("value",result.dob);
            $('#edit_dob_child').val(result.dob);

            $('#edit_first_name').attr("data-oldvalue", result.first_name);
            $('#edit_last_name').attr("data-oldvalue", result.last_name);
            $('#edit_allergies_medicalinfo').attr("data-oldvalue", result.allergies_medicalinfo);
            var disease = result.child_disease;
            $('input[name="child_disease[]"]').prop('checked', false);
            if (disease.trim() != '') {
                var dis = disease.split(",");
                for (var a = 0; a < dis.length; a++) {
                    $('#edit_child_disease' + dis[a]).iCheck('check');
                    //$('#child_disease' + dis[a]).prop('checked', true);
                    ///   $('#child_disease' + dis[a]).attr('checked');
                }
            }

            $('#edit_allergies_medicalinfo').val(result.allergies_medicalinfo);
            $('.childpopupnote').html('Edit Child');
            $('#editchild').modal('show');
        },
        error: function (result) {
            //console.log(result);
        }
    });
}

function cleardata() {

    $('.modal-body').find('input:text').val('');
    $('.modal-body').find('input:checkbox').prop('checked', false);
    $('.modal-body').find('textarea').val('');
    $('.modal-body').find('.icheckbox_square-blue').removeClass('checked');
    $('.modal-body').find('.icheckbox_square-blue').attr('aria-checked', 'false');
    $('#child_id').val('');
    $('#edit_child_id').val('');
    $('.childpopupnote').html('Add Child');
}
function getChildInfo(data) {
    var children = data.value;
    $('.child_info').html('');
    var mydiv = $('.child_information').html();
    for (var i = 0; i < children; i++) {
        $('.child_info').append(mydiv);
    }

    $('.child_number').each(function (index) {
        $(this).html('Child ' + index);
        $(this).parent('fieldset').find('.myfn').attr('id', 'fn' + index);
        $(this).parent('fieldset').find('.mydp').attr('id', 'dp' + index);
        var dis = index - 1;
        $(this).parent('fieldset').find('.per_disease').find('input').attr('name', 'child_disease[' + dis + '][]');
        $('#dp' + index).datepicker({
            format: "dd/mm/yyyy"
        });
    });
}
</script>
@endsection

@section('RenderBody')
<!-- Main content -->
<section class="content-header">
    <h1>
        Edit Profile
<!--        <small>@yield('contentheader_description')</small>-->
    </h1>

    <?php if (Auth::user()->id == $user->id) { ?>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/user/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit Profile</li>
        </ol>
    <?php } else { ?>
        <ol class="breadcrumb">
            <li><a href="{{ url('/admin/user/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="javascript:void(0);">Database</a></li>
            <li><a href="{{ url('/admin/user/userlist') }}">View Records</a></li>
            <li class="active">Edit Profile</li>
        </ol>
    <?php } ?>
</section>

@if (count($errors) > 0)
<?php if ($errors->first('firstname')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9" >{!! $errors->first('firstname') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('middlename')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('middlename') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('lastname')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('lastname') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('email')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('email') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('photo')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('photo') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('dob')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('dob') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('gender')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9" >{!! $errors->first('gender') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('region_id')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9" >{!! $errors->first('region_id') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('center_id')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9" >{!! $errors->first('center_id') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('tshirtsize')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('tshirtsize') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('yearenteredsatsang')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('yearenteredsatsang') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('address1')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('address1') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('address2')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('address2') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('apartment')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('apartment') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('city')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('city') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('state')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9" >{!! $errors->first('state') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('zip')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('zip') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('country')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9" >{!! $errors->first('country') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('phonehome')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('phonehome') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('phonecell')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('phonecell') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('fax')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('fax') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('primarysevawing')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('primarysevawing') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('primaryseva')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('primaryseva') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('secondarysevawing')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('secondarysevawing') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('secondaryseva')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('secondaryseva') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('schoolyear')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('schoolyear') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('schoolcollegename')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('schoolcollegename') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('majorexpected')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('majorexpected') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('careerinterest')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('careerinterest') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('fathername')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('fathername') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('fathercellphone')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('fathercellphone') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('fatheremail')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('fatheremail') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('mothername')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('mothername') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('mothercellphone')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('mothercellphone') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('motheremail')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('motheremail') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('emergencycontact1name')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('emergencycontact1name') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('emergencycontact1priphone')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('emergencycontact1priphone') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('emergencycontact1secphone')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('emergencycontact1secphone') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('emergencycontact1email')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('emergencycontact1email') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('emergencycontact2name')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('emergencycontact2name') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('emergencycontact2priphone')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('emergencycontact2priphone') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('emergencycontact2secphone')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('emergencycontact2secphone') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('emergencycontact2email')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('emergencycontact2email') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('allergies')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('allergies') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('medicationstaken')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('medicationstaken') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('medicalconditions')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('medicalconditions') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('spclmedicalinstructions')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('spclmedicalinstructions') !!} </label><br/>
<?php } ?>
<?php if ($errors->first('notes')) { ?>
    <label class="col-sm-3"></label>
    <label class="valerror col-sm-9">{!! $errors->first('notes') !!} </label><br/>
<?php } ?>
@endif
<section class="content">
    <div class="row">
        @if(session('message'))
        <div class="alert alert-success alert-dismissible margin-left-15 margin-right-15"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>{{session('message')}}</div>  
        @endif
        @if(session('error_message'))
        <div class="alert alert-danger alert-dismissible margin-left-15 margin-right-15"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>{{session('error_message')}}</div>  
        @endif
        <!--Notification Start-->
        @if(session('pending_count') > 0)
        <div class="alert alert-success alert-dismissible margin-left-15 margin-right-15"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>You have {{session('pending_count')}} event(s) registration pending.<a href="{!! URL::to('admin/event/myevents') !!}">Click here</a> to register</a></div>
        @endif
        @if(session('last_update_profile') >= 6 )
        <div class="alert alert-success alert-dismissible margin-left-15 margin-right-15"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>It has been {{session('last_update_profile')}} months since you last updated your information.<a href="{!! URL::to('admin/user/edituser/') !!}<?= "/" . base64_encode(Auth::id()); ?>">Click here</a> to make sure it's still up to date and update it if necessary.</div>
        @endif
        <!--Notification End-->
        {!! Form::open(array('url' => 'admin/user/updateuser' , 'class' => 'form-horizontal','id' => 'edituser_form', 'files' => true)) !!}

        <div class=" margin-left-15 margin-bottom-15">

            <input type="hidden" name="user_id" value="{{ $user->id or ''}}">
            <input type="hidden" name="selected_center" value="" id="selected_center">
            <input type="hidden" name="parent_password_class" value="{{ $parent_password_class }}" id="parent_password_class">
            <?php if ($user->id == Auth::id()) { ?>
                <input type="submit" class=" btn btn-success updateinfo" value="Save" />
            <?php } elseif (Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4) { ?>
                <input type="submit" class=" btn btn-success updateinfo" value="Save" />
            <?php } else { ?>
                @permission('update.user.database|assign.group.permission.database')
                <input type="submit" class=" btn btn-success updateinfo" value="Save" />
                @endpermission
            <?php } ?>

            <?php if (Auth::user()->id == $user->id) { ?>

                <a href="{!! URL::to('admin/user/myprofile') !!}" class=" cancel_btn btn btn-danger reset-btn text-right btn-info">Cancel</a>
            <?php } else { ?>
                <a href="{!! URL::to('admin/user/userlist') !!}" class=" cancel_btn btn btn-danger reset-btn text-right btn-info">Cancel</a>
            <?php } ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-lg-2 margin-bottom-15">

            <!-- Profile Image -->
            <div class="box box-primary">
                <input type="hidden" name="is_verified_address" id="is_verified_address" value="{{$user->is_verified_address or ''}}">   
                <input type="hidden" name="verified_city" id="verified_city" value="">
                <input type="hidden" name="verified_state" id="verified_state" value="">
                <input type="hidden" name="verified_country" id="verified_country" value="">
                <input type="hidden" name="verified_zip" id="verified_zip" value="">
                <input type="hidden" name="is_address_changed" id="is_address_changed" value="1"><!--To Identify its old address or new entered address-->
                <input type="hidden" name="is_mannual_click" id="is_mannual_click" value="1"><!--To Identify its clicked on mannual enter address button-->
                <div class="box-body box-profile">
                    <center><span class="profile-dp">
                            <?php if ($user->pictureUri != "") { ?>
                                <?php
                                $path = "uploads/users/" . $user->pictureUri;
                                if (file_exists($path)) {
                                    ?>
                                    <img class="profile-user-img img-responsive img-circle myimage" src="{{URL('uploads/users/')}}/{{$user->pictureUri}}" alt="User profile picture">
                                <?php } else { ?>
                                    <img class="profile-user-img img-responsive img-circle" src="{{ URL('uploads/users/default.png')}}" alt="User profile picture">
                                <?php } ?>
                            <?php } else { ?>
                                <img class="profile-user-img img-responsive img-circle" src="{{ URL('uploads/users/default.png')}}" alt="User profile picture">
                            <?php } ?>
                            <!--                        <h3 class="profile-username text-center">{{ $user->FirstName or ''}} {{ $user->LastName or '' }}</h3>-->
                        </span></center>
                    <?php if ($user->pictureUri != "") { ?>
                        <?php
                        $path = "uploads/users/" . $user->pictureUri;
                        if (file_exists($path)) {
                            ?>
                            <input type="hidden" id="original_profile_pic" name="original_profile_pic" value="{{URL('uploads/users/')}}/{{$user->pictureUri}}">
                        <?php } else { ?>
                            <input type="hidden" id="original_profile_pic" name="original_profile_pic" value="{{ URL('uploads/users/default.png')}}">
                        <?php } ?>
                    <?php } else { ?>
                        <input type="hidden" id="original_profile_pic" name="original_profile_pic" value="{{ URL('uploads/users/default.png')}}">
                    <?php } ?>


                    <?php if ($user->id == Auth::id()) { ?>
                        <a href="javascript:void(0);" class="btn-lg btn-primary text-center btn-block add-img margin-top-15">Change Image</a>
                    <?php } elseif (Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4) { ?>
                        <a href="javascript:void(0);" class="btn-lg btn-primary text-center btn-block add-img margin-top-15">Change Image</a>
                    <?php } else { ?>
                        @permission('update.user.database')
                        <a href="javascript:void(0);" class="btn-lg btn-primary text-center btn-block add-img margin-top-15">Change Image</a>
                        @endpermission
                    <?php } ?>
                    <input type="file" class="form-control uplod-buttons" id="profile_file" name="photo">
                    <input type="hidden" id="profile_x1" name="profile_x1" />
                    <input type="hidden" id="profile_y1" name="profile_y1" />
                    <input type="hidden" id="profile_w" name="profile_w" />
                    <input type="hidden" id="profile_h" name="profile_h" />
                    <input type="hidden" id="profilelg_x1" name="profilelg_x1" />
                    <input type="hidden" id="profilelg_y1" name="profilelg_y1" />
                    <input type="hidden" id="profilelg_w" name="profilelg_w" />
                    <input type="hidden" id="profilelg_h" name="profilelg_h" />
                </div>
                <!-- /.box-body -->
            </div>
            <?php if ($user->id == Auth::id()) { ?>
                <div class="info-row text-center">
                    <!-- If Account is activate thrn display deactvate account button -->
                    <?php if ($user->user_status == 1) { ?>
                        <a href="{!! URL::to('admin/user/makeuserdeactive') !!}<?= "/" . base64_encode($user->id); ?>" class="btn-lg btn-danger btn-block margin-top-15">Deactivate Account</a>
                        <!-- If Account is deactvate thrn display activate account button -->
                    <?php } else { ?>
                        <a href="{!! URL::to('admin/user/makeuseractive') !!}<?= "/" . base64_encode($user->id); ?>" class="btn-lg btn-success btn-block margin-top-15">Activate Account</a>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <!--                      r-admin,r-national,r-regional,r-eadmin,r-iadmin,r-sanyojak,r-local-->
                @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                <div class="info-row text-center">
                    <!-- If Account is activate thrn display deactvate account button -->
                    <?php if ($user->user_status == 1) { ?>
                        <a href="{!! URL::to('admin/user/makeuserdeactive') !!}<?= "/" . base64_encode($user->id); ?>" class="btn-lg btn-danger btn-block margin-top-15">Deactivate Account</a>
                        <!-- If Account is deactvate thrn display activate account button -->
                    <?php } else { ?>
                        <a href="{!! URL::to('admin/user/makeuseractive') !!}<?= "/" . base64_encode($user->id); ?>" class="btn-lg btn-success btn-block margin-top-15">Activate Account</a>
    <?php } ?>
                </div>
                @else
                @permission('deactivate.user.account.database|activate.user.account.database')
                <div class="info-row text-center">
                    <!-- If Account is activate thrn display deactvate account button -->
                    <?php if ($user->user_status == 1) { ?>
                        @permission('deactivate.user.account.database')
                        <a href="{!! URL::to('admin/user/makeuserdeactive') !!}<?= "/" . base64_encode($user->id); ?>" class="btn-lg btn-danger btn-block margin-top-15">Deactivate Account</a>
                        @endpermission
                        <!-- If Account is deactvate thrn display activate account button -->
    <?php } else { ?>
                        @permission('activate.user.account.database')
                        <a href="{!! URL::to('admin/user/makeuseractive') !!}<?= "/" . base64_encode($user->id); ?>" class="btn-lg btn-success btn-block margin-top-15">Activate Account</a>
                        @endpermission
                    <?php } ?>
                </div>
                @endpermission
                @endif
                <!--                        end-->
            <?php } ?>
            <?php if ($user->id != Auth::id()) { ?>    
                <!--                    'r-regional,r-national,r-admin'-->
                @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                <div class="info-row text-center">
                    <a href="javascript:void(0);" class="btn-lg btn-primary openrelocateuser btn-block margin-top-15">Relocate User</a>
                </div>
                @else
                @permission('relocate.user.database')
                <div class="info-row text-center">
                    <a href="javascript:void(0);" class="btn-lg btn-primary openrelocateuser btn-block margin-top-15">Relocate User</a>
                </div>
                @endpermission
                @endif

                <!--                    end-->
            <?php } ?>



            <!--            </div>
                         /.box-body 
                      </div>-->
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-xs-12 col-sm-12 col-lg-10">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#contact" data-toggle="tab">Contact Information</a></li>
                    <li><a href="#family" data-toggle="tab">Family Contact Information</a></li>
                    <li><a href="#emergency" data-toggle="tab">Emergency Contact Information</a></li>
                    <li><a href="#medical" data-toggle="tab">Medical Information</a></li>
                    <li><a href="#satsang" data-toggle="tab">Satsang Information</a></li>
                    <li><a href="#social" data-toggle="tab">Social Media Information</a></li>
                </ul>
                <div class="tab-content">
                    <!-- Begin :  Contact Information Section-->
                    <div class="active tab-pane" id="contact">
<!--                        <form class="form-horizontal">-->
                            <div class="form-group">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">User Id</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <input type="text" class="form-control" disabled="" value="{{ $user->id or ''}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">First Name<em class="mandatory-field">*</em></label>
                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <input type="text" class="form-control capitalizedata" placeholder="First Name" value="{{ $user->FirstName or ''}}" name="firstname" maxlength="30" data-oldvalue="{{ $user->FirstName or ''}}">
                                </div>
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Middle Name</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input type="text" class="form-control capitalizedata" placeholder="Middle Name" value="{{ $user->MiddleName or ''}}" name="middlename" maxlength="30" data-oldvalue="{{ $user->MiddleName or ''}}">
                                </div>
                            </div>
                            <!-- /.form group -->
                            <div class="form-group">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Last Name<em class="mandatory-field">*</em></label>
                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <input type="text" class="form-control capitalizedata" placeholder="Last Name" value="{{ $user->LastName or '' }}" name="lastname" maxlength="30" data-oldvalue="{{ $user->LastName or ''}}">
                                </div>
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Email<em class="mandatory-field">*</em></label>
                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input type="text" class="form-control useremailaddress" placeholder="Email" value="{{ $user->email or ''}}" name="email" maxlength="50" id="email">
                                    <label for="inputEmail3" class="control-label">Note : Not Required For Under 18</label>
                                    <input type="hidden" class="form-control" value="{{ $user->email or ''}}" name="dbemail" maxlength="50">
                                    <input type="hidden" class="form-control" value="1" name="is_admin" id="is_admin">
                                </div>
                            </div>
                            @if(($user->id == Auth::user()->id) && ($login_user_type == 1))
                            <div class="form-group">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Password</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <input class="form-control" type="password" placeholder="Password" name="password" id="pwd" value="">
                                </div>
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Confirm Password</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input class="form-control" type="password" placeholder="Confirm Password" name="cpassword" id="cpwd" value="">
                                </div>
                            </div>
                            @elseif($user->id != Auth::user()->id)
                            <div class="form-group">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Password</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <input class="form-control" type="password" placeholder="Password" name="password" id="pwd" value="">
                                </div>
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Confirm Password</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input class="form-control" type="password" placeholder="Confirm Password" name="cpassword" id="cpwd" value="">
                                </div>
                            </div>
                            @endif
                            
                            <!-- Begin: User Can SEe Option Of other user's profile or if parent can see won profile password field-->
                            @if(($user->id == Auth::user()->id) && ($login_user_type == 2))
                            <div class="form-group parents_password_div" id="parents_password_div">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Parent's Password</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <input class="form-control" type="password" placeholder="Parent's Password" name="parents_password" id="parents_password" value="">
                                </div>
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Parent's Confirm Password</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input class="form-control" type="password" placeholder="Parent's Confirm Password" name="cparents_password" id="cparents_password" value="">
                                </div>
                            </div>
                            @elseif($user->id != Auth::user()->id)
                            <div class="form-group parents_password_div" id="parents_password_div">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Parent's Password</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <input class="form-control" type="password" placeholder="Parent's Password" name="parents_password" id="parents_password" value="">
                                </div>
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Parent's Confirm Password</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input class="form-control" type="password" placeholder="Parent's Confirm Password" name="cparents_password" id="cparents_password" value="">
                                </div>
                            </div>
                            @endif
                            <!-- End: User Can SEe Option Of other user's profile or if parent can see won profile password field-->
                            <div class="form-group">
                                <label for="inputEmail" class="col-xs-12 col-sm-3 col-lg-2 control-label">Birth Date<em class="mandatory-field">*</em></label>
                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <?php
                                    $dob = $user->DOB;
                                    $dob = date("m/d/Y", strtotime($dob));
                                    ?>
                                    <input type="text" class="form-control" value="{{ $dob or ''}}" name="dob" id="dob" maxlength="50" data-inputmask="'alias': 'mm/dd/yyyy'"><label for="inputEmail3" class="control-label">Example : 01/23/2017</label>
                                </div>

                                <label for="inputEmail" class="col-xs-12 col-sm-3 col-lg-2 control-label">Apartment</label>

                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input type="text" class="form-control capitalizedata" placeholder="Apartment" value="{{ $user->Apartment or ''}}" id="apartment" name="apartment" maxlength="50" data-oldvalue="{{ $user->Apartment or ''}}">
                                </div>
                            </div>
                            <?php if ($user_role_type == 2 || $user_role_type == 3 || $user_role_type == 4) { ?>
                                <div class="form-group">
                                    <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Region<em class="mandatory-field">*</em></label>
                                    <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                        <select class="form-control" name="region_id" id="region_id">
                                            <option value="" >Select Region</option>
                                            <?php if (old(@region_id)) { ?>
                                                <?php
                                                if (isset($regions) && !empty($regions)) {
                                                    for ($i = 0; $i < count($regions); $i++) {
                                                        ?>
                                                        <option value="<?php echo $regions[$i]->id; ?>" <?php
                                                        if (old(@region_id) == $regions[$i]->id) {
                                                            echo " selected";
                                                        }
                                                        ?>><?php echo $regions[$i]->RegionName; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    <?php } else { ?>
                                                        <?php
                                                        if (isset($regions) && !empty($regions)) {
                                                            for ($i = 0; $i < count($regions); $i++) {
                                                                ?>
                                                        <option value="<?php echo $regions[$i]->id; ?>" <?php

                                                        if ($user->region_id == $regions[$i]->id) {
                                                            echo " selected";
                                                        }
                                                        ?>><?php echo $regions[$i]->RegionName; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                    <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Center<em class="mandatory-field">*</em></label>
                                    <div class="col-xs-12 col-sm-9 col-lg-4">
                                        <select class="form-control" name="center_id" id="center_id">
                                            <option value="" >Select Center</option>
                                            <?php if (old(@center_id)) { ?>
                                                <?php
                                                if (isset($centers) && !empty($centers)) {
                                                    for ($i = 0; $i < count($centers); $i++) {
                                                        ?>
                                                        <option value="<?php echo $centers[$i]->id; ?>" <?php

                                                        if (old(@center_id) == $centers[$i]->id) {
                                                            echo " selected";
                                                        }
                                                        ?>><?php echo $centers[$i]->CenterName; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    <?php } else { ?>
                                                        <?php
                                                        if (isset($centers) && !empty($centers)) {
                                                            for ($i = 0; $i < count($centers); $i++) {
                                                                ?>
                                                        <option value="<?php echo $centers[$i]->id; ?>" <?php
                                                        if ($user->center_id == $centers[$i]->id) {
                                                            echo " selected";
                                                        }
                                                        ?>><?php echo $centers[$i]->CenterName; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Address</label>

                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input type="text" class="form-control capitalizedata" placeholder="Address" value="{{ $user->Address1 or ''}}" id="address1" name="address1" maxlength="255" data-oldvalue="{{ $user->Address1 or ''}}">
                                    <span class="mannual_address_option"></span> </div>
                            </div>
                            <div class="form-group countrylist <?= $user->is_verified_address == 1 ? 'hide' : '' ?>"">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Country</label>
                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <select class="form-control" name="country" id="country">
                                        <option value="" >Select Country</option>

                                        <?php if (old(@country)) { ?>
                                            <?php
                                            if (isset($country) && !empty($country)) {
                                                for ($i = 0; $i < count($country); $i++) {
                                                    ?>
                                                    <option value="<?php echo $country[$i]->id; ?>" <?php
                                                    if (old(@country) == $country[$i]->id) {
                                                        echo " selected";
                                                    }
                                                    ?>><?php echo $country[$i]->country_name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                <?php } else { ?>
                                                    <?php
                                                    if (isset($country) && !empty($country)) {
                                                        for ($i = 0; $i < count($country); $i++) {
                                                            ?>
                                                    <option value="<?php echo $country[$i]->id; ?>" <?php
                                                    if ($user->Country == $country[$i]->id) {
                                                        echo " selected";
                                                    }
                                                    ?>><?php echo $country[$i]->country_name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                <?php } ?>
                                    </select>
                                </div>

                                <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">City</label>

                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input type="text" class="form-control capitalizedata" placeholder="City" value="{{ $user->City or ''}}" name="city" id="city" maxlength="50" data-oldvalue="{{ $user->City or ''}}">
                                </div>
                            </div>

                            <div class="form-group addressoption citystateziplist <?= $user->is_verified_address == 1 ? 'hide' : '' ?>"">
                                <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">State</label>

                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <select class="form-control" name="state" id="state">
                                        <option value="">Select State</option>
                                        <?php if (old(@state)) { ?>
                                            <?php
                                            if (isset($statelist) && !empty($statelist)) {
                                                for ($i = 0; $i < count($statelist); $i++) {
                                                    ?>
                                                    <option value="<?php echo $statelist[$i]->id; ?>" <?php
                                                    if (old(@state) == $statelist[$i]->id) {
                                                        echo "selected";
                                                    }
                                                    ?>><?php echo $statelist[$i]->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                <?php } else { ?>
                                                    <?php
                                                    if (isset($statelist) && !empty($statelist)) {
                                                        for ($i = 0; $i < count($statelist); $i++) {
                                                            ?>
                                                    <option value="<?php echo $statelist[$i]->id; ?>" <?php
                                                    if ($user->State == $statelist[$i]->id) {
                                                        echo "selected";
                                                    }
                                                    ?>><?php echo $statelist[$i]->name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                <?php } ?>
                                    </select>
                                </div>
                                <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Zip/Postal Code</label>


                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input type="text" class="form-control" placeholder="Zip/Postal Code" value="{{ $user->ZIP or ''}}" name="zip" maxlength="50" id="zip">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Home Phone</label>

                                <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                    <input type="text" class="form-control phone-group" placeholder="Home phone" value="{{ $user->PhoneHome or ''}}" name="phonehome" maxlength="50" id="phonehome" data-inputmask="'mask': '(999)999-9999'">
                                </div>

                                <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Cell Phone</label>

                                <div class="col-xs-12 col-sm-9 col-lg-4">
                                    <input type="text" class="form-control phone-group" placeholder="Cell phone" value="{{ $user->PhoneCell or ''}}" name="phonecell" maxlength="50" id="phonecell" data-inputmask="'mask': '(999)999-9999'">
                                </div>
                            </div>
                            <div class="form-group">
                                <?php if ($user->id != Auth::id()) { ?>
                                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                                    <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Role Type<em class="mandatory-field">*</em></label>

                                    <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                        <?php if (isset($role_list) && $role_list != "") { ?>

                                            <select class="form-control" name="role_type" id="role_type">
                                                <?php if (old(@role_type)) { ?>
                                                    <?php for ($i = 0; $i < count($role_list); $i++) { ?>
                                                        <option value="<?php echo $role_list[$i]->id; ?>" <?php
                                                        if (old(@role_type) == $user->user_role_type) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $role_list[$i]->name; ?></option>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <?php for ($i = 0; $i < count($role_list); $i++) {
                                                                ?>
                                                        <option value="<?php echo $role_list[$i]->id; ?>" <?php
                                                        if ($user->user_role_type == $role_list[$i]->id) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $role_list[$i]->name; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                            </select>
                                            <span></span>
                                        <?php } else { ?>
                                            <input type="text" class="form-control" value="--" name="role_type"  id="role_type" readonly="true" disabled="true">
                                        <?php } ?>
                                    </div>
                                    @else
                                    <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Role Type</label>

                                    <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                                        <?php if (isset($role_list) && $role_list != "") { ?>

                                        <select class="form-control" name="role_type" id="role_type" disabled="">
                                                <?php if (old(@role_type)) { ?>
                                                    <?php for ($i = 0; $i < count($role_list); $i++) { ?>
                                                        <option value="<?php echo $role_list[$i]->id; ?>" <?php
                                                        if (old(@role_type) == $user->user_role_type) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $role_list[$i]->name; ?></option>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <?php for ($i = 0; $i < count($role_list); $i++) {
                                                                ?>
                                                        <option value="<?php echo $role_list[$i]->id; ?>" <?php
                                                        if ($user->user_role_type == $role_list[$i]->id) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $role_list[$i]->name; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                            </select>
                                            <span></span>
                                        <?php } else { ?>
                                            <input type="text" class="form-control" value="--" name="role_type"  id="role_type" readonly="true" disabled="true">
                                        <?php } ?>
                                    </div>
                                    @endif
                                    
                                <?php } ?>
                                    
                                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                                    <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Permission Group</label>
                                    <?php if (isset($permission_group) && $permission_group != "") { ?>
                                        <div class="col-xs-12 col-sm-9 col-lg-4">
                                            <select class="form-control" name="permission_group" id="permission_group">
                                                <option value="">Select Permission Group</option>
                                                <?php if (old(@permission_group)) { ?>
                                                    <?php for ($i = 0; $i < count($permission_group); $i++) { ?>
                                                        <option value="<?php echo $permission_group[$i]->id; ?>" <?php
                                                        if (old(@permission_group) == $permission_group[$i]->id) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo ucwords($permission_group[$i]->permission_group); ?></option>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <?php for ($i = 0; $i < count($permission_group); $i++) {
                                                                ?>
                                                        <option value="<?php echo $permission_group[$i]->id; ?>" <?php
                                                        if ($permission_group[$i]->id == $assigned_permission_group) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo ucwords($permission_group[$i]->permission_group); ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-xs-12 col-sm-9 col-lg-4">
                                            <select class="form-control" name="permission_group" id="permission_group">
                                                <option value="">Select Permission Group</option>
                                            </select>
                                        </div>
                                    <?php } ?>
                                    @else
                                    @permission('assign.group.permission.database')
                                    <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Permission Group</label>
                                    <?php if (isset($permission_group) && $permission_group != "") { ?>
                                        <div class="col-xs-12 col-sm-9 col-lg-4">
                                            <select class="form-control" name="permission_group" id="permission_group" <?php if(Auth::user()->id == $user->id) { echo "disabled" ;}?>>
                                                <option value="">Select Permission Group</option>
                                                <?php if (old(@permission_group)) { ?>
                                                    <?php for ($i = 0; $i < count($permission_group); $i++) { ?>
                                                        <option value="<?php echo $permission_group[$i]->id; ?>" <?php
                                                        if (old(@permission_group) == $permission_group[$i]->id) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo ucwords($permission_group[$i]->permission_group); ?></option>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <?php for ($i = 0; $i < count($permission_group); $i++) {
                                                                ?>
                                                        <option value="<?php echo $permission_group[$i]->id; ?>" <?php
                                                        if ($permission_group[$i]->id == $assigned_permission_group) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo ucwords($permission_group[$i]->permission_group); ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-xs-12 col-sm-9 col-lg-4">
                                            <select class="form-control" name="permission_group" id="permission_group">
                                                <option value="">Select Permission Group</option>
                                            </select>
                                        </div>
                                    <?php } ?>
                                    @else
                                    <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Permission Group</label>
                                    <?php if (isset($permission_group) && $permission_group != "") { ?>
                                        <div class="col-xs-12 col-sm-9 col-lg-4">
                                            <select class="form-control" name="permission_group" id="permission_group" disabled>
                                                <option value="">Select Permission Group</option>
                                                <?php if (old(@permission_group)) { ?>
                                                    <?php for ($i = 0; $i < count($permission_group); $i++) { ?>
                                                        <option value="<?php echo $permission_group[$i]->id; ?>" <?php
                                                        if (old(@permission_group) == $permission_group[$i]->id) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo ucwords($permission_group[$i]->permission_group); ?></option>
                                                            <?php } ?>
                                                        <?php } else { ?>
                                                            <?php for ($i = 0; $i < count($permission_group); $i++) {
                                                                ?>
                                                        <option value="<?php echo $permission_group[$i]->id; ?>" <?php
                                                        if ($permission_group[$i]->id == $assigned_permission_group) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo ucwords($permission_group[$i]->permission_group); ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                            </select>
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-xs-12 col-sm-9 col-lg-4">
                                            <select class="form-control" name="permission_group" id="permission_group" disabled>
                                                <option value="">Select Permission Group</option>
                                            </select>
                                        </div>
                                    <?php } ?>
                                    @endpermission
                                    @endif
                            </div>
<!--                        </form>-->
                    </div>
                    <!-- End :  Contact Information Section-->

                    <!-- Begin :  Emergency Contact Information-->
                    <div class="tab-pane" id="emergency">
                        <div class="box box-default emergencyborder">
                            <div class="box-header with-border">
                                <h3 class="box-title">EMERGENCY CONTACT 1</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
<!--                                    <form class="form-horizontal">-->
                                        <div class="form-group subboxdiv">
                                            <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Name</label>
                                            <div class="col-xs-12 col-sm-9 col-lg-10">
                                                <input type="text" class="form-control capitalizedata" placeholder="Emergency Contact1 Name" value="{{ $usermeta_emergency->EmergencyContact1Name or ''}}" name="emergencycontact1name" maxlength="50" data-oldvalue="{{ $usermeta_emergency->EmergencyContact1Name or ''}}">
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group subboxdiv">
                                            <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Email</label>
                                            <div class="col-xs-12 col-sm-9 col-lg-10">
                                                <input type="text" class="form-control useremailaddress" placeholder="Emergency Contact1 Email" value="{{ $usermeta_emergency->EmergencyContact1Email or ''}}" name="emergencycontact1email" maxlength="50">
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                        <!-- /.col -->
                                        <div class="form-group subboxdiv">
                                            <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Primary Number</label>
                                            <div class="col-xs-12 col-sm-9 col-lg-10">
                                                <input type="text" maxlength="50" name="emergencycontact1priphone" value="{{ $usermeta_emergency->EmergencyContact1PriPhone or ''}}" placeholder="Emergency Contact1 Primary Number" class="form-control" id="emergencycontact1priphone" data-inputmask="'mask': '(999)999-9999'">
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                        <div class="form-group subboxdiv">
                                            <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Secondary Number</label>
                                            <div class="col-xs-12 col-sm-9 col-lg-10">
                                                <input type="text" maxlength="50" name="emergencycontact1secphone" value="{{ $usermeta_emergency->EmergencyContact1SecPhone or ''}}" placeholder="Emergency Contact1 Secondary Number" class="form-control" id="emergencycontact1secphone" data-inputmask="'mask': '(999)999-9999'">
                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                        <!-- /.col -->
<!--                                    </form>-->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>

                        <div class="box box-default emergencyborder">
                            <div class="box-header with-border">
                                <h3 class="box-title">EMERGENCY CONTACT 2</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <!--                          <form class="form-horizontal">-->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Name</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <input type="text" class="form-control capitalizedata" placeholder="Emergency Contact2 Name" value="{{ $usermeta_emergency->EmergencyContact2Name or ''}}" name="emergencycontact2name" maxlength="50" data-oldvalue="{{ $usermeta_emergency->EmergencyContact2Name or ''}}">
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Email</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <input type="text" class="form-control useremailaddress" placeholder="Emergency Contact2 Email" value="{{ $usermeta_emergency->EmergencyContact2Email or ''}}" name="emergencycontact2email" maxlength="50">
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                    <!-- /.col -->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Primary Number</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <input type="text" class="form-control" placeholder="Emergency Contact2 Primary Number" value="{{ $usermeta_emergency->EmergencyContact2PriPhone or ''}}" name="emergencycontact2priphone" maxlength="50" id="emergencycontact2priphone" data-inputmask="'mask': '(999)999-9999'">
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Secondary Number</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <div class="profile-input-rows"><input type="text" class="form-control" placeholder="Emergency Contact2 Secondary Number" value="{{ $usermeta_emergency->EmergencyContact2SecPhone or '' }}" name="emergencycontact2secphone" maxlength="50" id="emergencycontact2secphone" data-inputmask="'mask': '(999)999-9999'"></div>
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                    <!-- /.col -->
                                    <!--                          </form>-->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!-- End :  Emergency Contact Information-->

                    <!-- Begin :  Family Contact Information-->
                    <div class="tab-pane" id="family">
                        <div class="box box-default emergencyborder">
                            <div class="box-header with-border">
                                <h3 class="box-title">FATHER</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <!--                          <form class="form-horizontal">-->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Name</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <input type="text" class="form-control capitalizedata" placeholder="Father Name" id="fathername" value="{{ $usermeta_guardian->FatherName or ''}}" name="fathername" maxlength="50" data-oldvalue="{{ $usermeta_guardian->FatherName or ''}}">
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Email</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <input type="text" class="form-control useremailaddress" placeholder="Father's Email Address" id="fatheremail" value="{{ $usermeta_guardian->FatherEmail or ''}}" name="fatheremail" maxlength="50">
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                    <!-- /.col -->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Cell Phone</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <input type="text" class="form-control" placeholder="Father's Cell Phone" id="fathercellphone" value="{{ $usermeta_guardian->FatherCellPhone or '' }}" name="fathercellphone" maxlength="50" id="fathercellphone" data-oldvalue="{{ $usermeta_guardian->FatherCellPhone or ''}}" data-inputmask="'mask': '(999)999-9999'">
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                    <!-- /.col -->
                                    <!--                          </form>-->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.box-body -->
                        </div>

                        <div class="box box-default emergencyborder">
                            <div class="box-header with-border">
                                <h3 class="box-title">MOTHER</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="row">
                                    <!--                          <form class="form-horizontal">-->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Name</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <input type="text" class="form-control capitalizedata" placeholder="Mother Name" id="mothername" value="{{ $usermeta_guardian->MotherName or ''}}" name="mothername" maxlength="50" data-oldvalue="{{ $usermeta_guardian->MotherName or ''}}">
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Email</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <input type="text" class="form-control useremailaddress" placeholder="Mother's Email Address" id="motheremail" value="{{ $usermeta_guardian->MotherEmail or ''}}" name="motheremail" maxlength="50">
                                        </div>
                                    </div>
                                    <!-- /.form-group -->
                                    <!-- /.col -->
                                     <div class="form-group subboxdiv">
                                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Cell Phone</label>
                                        <div class="col-xs-12 col-sm-9 col-lg-10">
                                            <input type="text" class="form-control" placeholder="Mother's Cell Phone" id="mothercellphone" value="{{ $usermeta_guardian->MotherCellPhone or ''}}" name="mothercellphone" maxlength="50" id="mothercellphone" data-inputmask="'mask': '(999)999-9999'"></div>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                                <!-- /.col -->
                                <!--</form>-->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->

                    </div>
                    <!-- End :  Family Contact Information-->

                    <!-- Begin :  Medical Information-->
                    <div class="tab-pane" id="medical">
<!--                        <form class="form-horizontal">-->
                            <div class="form-group">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Allergies</label>
                                <div class="col-xs-12 col-sm-9 col-lg-10">
                                    <textarea class="form-control" name="allergies" rows="5" data-oldvalue="{{ $usermeta->Allergies or ''}}">{{ $usermeta->Allergies or ''}}</textarea>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Medications Taken</label>

                                <div class="col-xs-12 col-sm-9 col-lg-10">
                                    <textarea class="form-control" name="medicationstaken" rows="5" data-oldvalue="{{ $usermeta->MedicationsTaken or ''}}">{{ $usermeta->MedicationsTaken or ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Medical Conditions</label>
                                <div class="col-xs-12 col-sm-9 col-lg-10">
                                    <textarea class="form-control" name="medicalconditions" rows="5" data-oldvalue="{{ $usermeta->MedicalConditions or ''}}">{{ $usermeta->MedicalConditions or ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Special Medical Instructions</label>

                                <div class="col-xs-12 col-sm-9 col-lg-10">
                                    <textarea class="form-control capitalizedata" name="spclmedicalinstructions" rows="5" data-oldvalue="{{ $usermeta->SpclMedicalInstructions or ''}}">{{ $usermeta->SpclMedicalInstructions or ''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Notes</label>

                                <div class="col-xs-12 col-sm-9 col-lg-10">
                                    <textarea class="form-control capitalizedata" name="notes" rows="5" data-oldvalue="{{ $usermeta->notes or ''}}">{{ $usermeta->notes or ''}}</textarea>
                                </div>
                            </div>
<!--                        </form>-->
                    </div>
                    <!-- End :  Medical Information-->

                    <!-- Begin :  Satsang Information-->
                    <div class="tab-pane" id="satsang">
<!--                        <form class="form-horizontal">-->
                            <div class="form-group">
                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">T-Shirt Size</label>
                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <select class="form-control" name="tshirtsize">
                                <option value="">Select T-Shirt Size</option>
                                <?php
                                if (isset($tshirt_sizes) && !empty($tshirt_sizes)) {
                                    for ($i = 0; $i < count($tshirt_sizes); $i++) {
                                        ?>
                                        <option value="<?php echo $tshirt_sizes[$i]->id; ?>" <?php
                                        if ($user->tshirtsizeId == $tshirt_sizes[$i]->id) {
                                            echo " selected";
                                        }
                                        ?>><?php echo ucwords($tshirt_sizes[$i]->size); ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>

                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Year Entered Satsang</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $user->YearEnteredSatsang or ''}}" name="yearenteredsatsang" id="yearenteredsatsang" maxlength="50">
                        </div>
                    </div>

                    @if($user_role_type == 4)     
                    <div class="form-group">
                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">Gender<em class="mandatory-field">*</em></label>

                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <select class="form-control" name="gender" id="gender">
                                <option value="" <?php
                                if ($user->Gender == "") {
                                    echo "selected";
                                }
                                ?>>Select Gender</option>
                                        <?php if (isset($gender) && !empty($gender)) { ?>
                                            <?php for ($i = 0; $i < count($gender); $i++) { ?>
                                        <option value="<?php echo $gender[$i]; ?>" <?php
                                        if ($user->Gender == $gender[$i]) {
                                            echo " selected";
                                        }
                                        ?>><?php echo ucwords($gender[$i]); ?></option>
                                            <?php } ?>
                                        <?php } ?>
                            </select>
                        </div>
                    </div>
                    @else
                    <input type="hidden" value="{{ $user->Gender or ''}}" id="gender" name="gender">
                    @endif


                    <div class="form-group">
                        <label for="inputName" class="col-xs-12 col-sm-3 col-lg-2 control-label">School Year<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <select class="form-control" name="schoolyear" id="schoolyear">
                                <option value="">Select School Year</option>
                                <?php
                                if (isset($school_list) && !empty($school_list)) {
                                    for ($i = 0; $i < count($school_list); $i++) {
                                        ?>
                                        <option value="<?php echo $school_list[$i]->id; ?>"<?php
                                        if ($school_list[$i]->id == $user->SchoolYear) {
                                            echo " selected";
                                        }
                                        ?>><?php echo $school_list[$i]->school_display_name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Wing</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <input type="text" class="form-control"  value="{{$personal_wing_name}}" name="personal_wing_name" id="personal_wing_name" readonly="true">
                            <input type="hidden" class="form-control"  value="{{ $user->personalWing or 0}}" name="hidden_personal_wing_id" id="hidden_personal_wing_id">
                            
                        </div>

                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Group</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <input type="text" class="form-control"  value="{{$group_name}}" name="group_name" id="group_name" readonly="true">
                            <input type="hidden" class="form-control"  value="{{ $user->group_id or 0}}" name="group_id" id="group_id">
                        </div>
                    </div>
                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                    <div class="form-group subgroup_div">
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Sub Group</label>

                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <select class="form-control" name="subgroup" id="subgroup">
                                <option value="" >Select Sub Group</option>
                            </select>
                        </div>
                    </div>
                    @elseif(Auth::user()->id != $user->id)
                    <div class="form-group subgroup_div">
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Sub Group</label>

                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <select class="form-control" name="subgroup" id="subgroup">
                                <option value="" >Select Sub Group</option>
                            </select>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Primary Seva Wing</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <select class="form-control" name="primarysevawing" id="primarysevawing">
                                <option value="0" <?php
                                if ($user->PrimarySevaWing == "0") {
                                    echo "selected";
                                }
                                ?>>Select Wing</option>
                                        <?php
                                        if (isset($wings) && !empty($wings)) {
                                            for ($i = 0; $i < count($wings); $i++) {
                                                ?>
                                        <option value="<?php echo $wings[$i]->id; ?>" <?php
                                        if ($user->PrimarySevaWing == $wings[$i]->id) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $wings[$i]->wingName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>

                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Primary Seva</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <select class="form-control" name="primaryseva" id="primaryseva">
                                <option value="" >Select Primary Seva</option>
                                <?php if (old(@primaryseva)) { ?>
                                    <?php
                                    if (isset($primaryseva) && !empty($primaryseva)) {
                                        for ($i = 0; $i < count($primaryseva); $i++) {
                                            ?>
                                            <option value="<?php echo $primaryseva[$i]->id; ?>" <?php
                                            if (old(@primaryseva) == $primaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $primaryseva[$i]->sevaName; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } else { ?>
                                            <?php
                                            if (isset($primaryseva) && !empty($primaryseva)) {
                                                for ($i = 0; $i < count($primaryseva); $i++) {
                                                    ?>
                                            <option value="<?php echo $primaryseva[$i]->id; ?>" <?php
                                            if ($user->PrimarySeva == $primaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $primaryseva[$i]->sevaName; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } ?>
                            </select>
                        </div>
                        @else
                        @permission('update.user.database')
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Primary Seva Wing</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <select class="form-control" name="primarysevawing" id="primarysevawing" <?php if(Auth::user()->id == $user->id) { echo "disabled" ;}?>>
                                <option value="0" <?php
                                if ($user->PrimarySevaWing == "0") {
                                    echo "selected";
                                }
                                ?>>Select Wing</option>
                                        <?php
                                        if (isset($wings) && !empty($wings)) {
                                            for ($i = 0; $i < count($wings); $i++) {
                                                ?>
                                        <option value="<?php echo $wings[$i]->id; ?>" <?php
                                        if ($user->PrimarySevaWing == $wings[$i]->id) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $wings[$i]->wingName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>

                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Primary Seva</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <select class="form-control" name="primaryseva" id="primaryseva" <?php if(Auth::user()->id == $user->id) { echo "disabled" ;}?>>
                                <option value="" >Select Primary Seva</option>
                                <?php if (old(@primaryseva)) { ?>
                                    <?php
                                    if (isset($primaryseva) && !empty($primaryseva)) {
                                        for ($i = 0; $i < count($primaryseva); $i++) {
                                            ?>
                                            <option value="<?php echo $primaryseva[$i]->id; ?>" <?php
                                            if (old(@primaryseva) == $primaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $primaryseva[$i]->sevaName; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } else { ?>
                                            <?php
                                            if (isset($primaryseva) && !empty($primaryseva)) {
                                                for ($i = 0; $i < count($primaryseva); $i++) {
                                                    ?>
                                            <option value="<?php echo $primaryseva[$i]->id; ?>" <?php
                                            if ($user->PrimarySeva == $primaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $primaryseva[$i]->sevaName; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } ?>
                            </select>
                        </div>
                        @else
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Primary Seva Wing</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <select class="form-control" name="primarysevawing" id="primarysevawing" disabled>
                                <option value="0" <?php
                                if ($user->PrimarySevaWing == "0") {
                                    echo "selected";
                                }
                                ?>>Select Wing</option>
                                        <?php
                                        if (isset($wings) && !empty($wings)) {
                                            for ($i = 0; $i < count($wings); $i++) {
                                                ?>
                                        <option value="<?php echo $wings[$i]->id; ?>" <?php
                                        if ($user->PrimarySevaWing == $wings[$i]->id) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $wings[$i]->wingName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>

                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Primary Seva</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <select class="form-control" name="primaryseva" id="primaryseva" disabled>
                                <option value="" >Select Primary Seva</option>
                                <?php if (old(@primaryseva)) { ?>
                                    <?php
                                    if (isset($primaryseva) && !empty($primaryseva)) {
                                        for ($i = 0; $i < count($primaryseva); $i++) {
                                            ?>
                                            <option value="<?php echo $primaryseva[$i]->id; ?>" <?php
                                            if (old(@primaryseva) == $primaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $primaryseva[$i]->sevaName; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } else { ?>
                                            <?php
                                            if (isset($primaryseva) && !empty($primaryseva)) {
                                                for ($i = 0; $i < count($primaryseva); $i++) {
                                                    ?>
                                            <option value="<?php echo $primaryseva[$i]->id; ?>" <?php
                                            if ($user->PrimarySeva == $primaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $primaryseva[$i]->sevaName; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } ?>
                            </select>
                        </div>
                        @endpermission
                        
                        @endif
                    </div>



                    <div class="form-group">
                        @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Secondary Seva Wing</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <select class="form-control" name="secondarysevawing" id="secondarysevawing">
                                <!-- here secondary seva wing is not required field in validation so put default value 0 -->
                                <option value="0" <?php
                                if ($user->SecondarySevaWing == "balak") {
                                    echo "0";
                                }
                                ?>>Select Wing</option>
                                        <?php
                                        if (isset($wings) && !empty($wings)) {
                                            for ($i = 0; $i < count($wings); $i++) {
                                                ?>
                                        <option value="<?php echo $wings[$i]->id; ?>" <?php
                                        if ($user->SecondarySevaWing == $wings[$i]->id) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $wings[$i]->wingName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>

                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Secondary Seva</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <select class="form-control" name="secondaryseva" id="secondaryseva">
                                <option value="" >Select Secondary Seva</option>
                                <?php if (old(@secondaryseva)) { ?>
                                    <?php
                                    if (isset($secondaryseva) && !empty($secondaryseva)) {
                                        for ($i = 0; $i < count($secondaryseva); $i++) {
                                            ?>
                                            <option value="<?php echo $secondaryseva[$i]->id; ?>" <?php
                                            if (old(@secondaryseva) == $secondaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $secondaryseva[$i]->sevaName; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } else { ?>
                                            <?php
                                            if (isset($secondaryseva) && !empty($secondaryseva)) {
                                                for ($i = 0; $i < count($secondaryseva); $i++) {
                                                    ?>
                                            <option value="<?php echo $secondaryseva[$i]->id; ?>" <?php
                                            if ($user->SecondarySeva == $secondaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $secondaryseva[$i]->sevaName; ?></option>
                                                <?php } ?>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                        @else
                        @permission('update.user.database')
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Secondary Seva Wing</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <select class="form-control" name="secondarysevawing" id="secondarysevawing" <?php if(Auth::user()->id == $user->id) { echo "disabled" ;}?>>
                                <!-- here secondary seva wing is not required field in validation so put default value 0 -->
                                <option value="0" <?php
                                if ($user->SecondarySevaWing == "balak") {
                                    echo "0";
                                }
                                ?>>Select Wing</option>
                                        <?php
                                        if (isset($wings) && !empty($wings)) {
                                            for ($i = 0; $i < count($wings); $i++) {
                                                ?>
                                        <option value="<?php echo $wings[$i]->id; ?>" <?php
                                        if ($user->SecondarySevaWing == $wings[$i]->id) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $wings[$i]->wingName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>

                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Secondary Seva</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <select class="form-control" name="secondaryseva" id="secondaryseva" <?php if(Auth::user()->id == $user->id) { echo "disabled" ;}?>>
                                <option value="" >Select Secondary Seva</option>
                                <?php if (old(@secondaryseva)) { ?>
                                    <?php
                                    if (isset($secondaryseva) && !empty($secondaryseva)) {
                                        for ($i = 0; $i < count($secondaryseva); $i++) {
                                            ?>
                                            <option value="<?php echo $secondaryseva[$i]->id; ?>" <?php
                                            if (old(@secondaryseva) == $secondaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $secondaryseva[$i]->sevaName; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } else { ?>
                                            <?php
                                            if (isset($secondaryseva) && !empty($secondaryseva)) {
                                                for ($i = 0; $i < count($secondaryseva); $i++) {
                                                    ?>
                                            <option value="<?php echo $secondaryseva[$i]->id; ?>" <?php
                                            if ($user->SecondarySeva == $secondaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $secondaryseva[$i]->sevaName; ?></option>
                                                <?php } ?>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                        @else
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Secondary Seva Wing</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <select class="form-control" name="secondarysevawing" id="secondarysevawing" disabled>
                                <!-- here secondary seva wing is not required field in validation so put default value 0 -->
                                <option value="0" <?php
                                if ($user->SecondarySevaWing == "balak") {
                                    echo "0";
                                }
                                ?>>Select Wing</option>
                                        <?php
                                        if (isset($wings) && !empty($wings)) {
                                            for ($i = 0; $i < count($wings); $i++) {
                                                ?>
                                        <option value="<?php echo $wings[$i]->id; ?>" <?php
                                        if ($user->SecondarySevaWing == $wings[$i]->id) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $wings[$i]->wingName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>

                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Secondary Seva</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <select class="form-control" name="secondaryseva" id="secondaryseva" disabled>
                                <option value="" >Select Secondary Seva</option>
                                <?php if (old(@secondaryseva)) { ?>
                                    <?php
                                    if (isset($secondaryseva) && !empty($secondaryseva)) {
                                        for ($i = 0; $i < count($secondaryseva); $i++) {
                                            ?>
                                            <option value="<?php echo $secondaryseva[$i]->id; ?>" <?php
                                            if (old(@secondaryseva) == $secondaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $secondaryseva[$i]->sevaName; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } else { ?>
                                            <?php
                                            if (isset($secondaryseva) && !empty($secondaryseva)) {
                                                for ($i = 0; $i < count($secondaryseva); $i++) {
                                                    ?>
                                            <option value="<?php echo $secondaryseva[$i]->id; ?>" <?php
                                            if ($user->SecondarySeva == $secondaryseva[$i]->id) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $secondaryseva[$i]->sevaName; ?></option>
                                                <?php } ?>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                        @endpermission
                        @endif
                        
                    </div>
                    
                    <!--Begin : Multiple Region Center Module-->
                    @if($user_access_multiple_region == "yes" || $user_access_multiple_center == "yes")
                    <div class="form-group">
                        @if($user_access_multiple_region == "yes")
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Multiple Region</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <select class="form-control" name="multiple_region[]" id="multiple_region" multiple>
                                        <?php
                                        if (isset($multiple_region_list) && !empty($multiple_region_list)) {
                                            for ($i = 0; $i < count($multiple_region_list); $i++) {
                                                ?>
                                                <option value="<?php echo $multiple_region_list[$i]->id; ?>" <?php
                                                        if (in_array($multiple_region_list[$i]->id, explode(',',$multiple_region))) {
                                                            echo " selected";
                                                        }
                                                        ?>><?php echo $multiple_region_list[$i]->RegionName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                        @endif
                        @if($user_access_multiple_center == "yes")
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Multiple Center</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <select class="form-control" name="multiple_center[]" id="multiple_center" multiple>
                                        <?php
                                        if (isset($multiple_center_list) && !empty($multiple_center_list)) {
                                            for ($i = 0; $i < count($multiple_center_list); $i++) {
                                                ?>
                                                <option value="<?php echo $multiple_center_list[$i]->id; ?>" <?php
                                                        if (in_array($multiple_center_list[$i]->id, explode(',',$multiple_center))) {
                                                            echo " selected";
                                                        }
                                                        ?>><?php echo $multiple_center_list[$i]->CenterName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                        @endif
                    </div>
                    @endif
                    <!--End: Multiple Region Center Module-->

                    <div class="form-group">
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Undergraduate School</label>

                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <input list="schoolcollegename" name="schoolcollegename" class="form-control" value="<?= $userschool_name;?>">
                            <datalist id="schoolcollegename">
                            <?php
                            if (isset($campus_list) && !empty($campus_list)) {
                                for ($i = 0; $i < count($campus_list); $i++) {
                                    ?>
                                    <option value="<?= ucwords($campus_list[$i]->campus_name); ?>"></option>
                                    <?php
                                }
                            }
                            ?>
                            </datalist>
  
<!--                            <input type="text" class="form-control capitalizedata" placeholder="School Name" value="{{ $user->SchoolCollegeName or ''}}" name="schoolcollegename" id="schoolcollegename" maxlength="50" data-oldvalue="{{ $user->SchoolCollegeName or ''}}">-->
                                
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Major/Expected Major</label>

                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <input type="text" class="form-control capitalizedata" placeholder="Major/Expected Major" value="{{ $user->MajorExpected or ''}}" name="majorexpected" id="majorexpected" maxlength="50" data-oldvalue="{{ $user->MajorExpected or ''}}">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Career Interest</label>

                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <input type="text" class="form-control capitalizedata" placeholder="Career Interest" value="{{ $user->CareerInterest or ''}}" name="careerinterest" id="careerinterest" maxlength="50" data-oldvalue="{{ $user->CareerInterest or ''}}">
                        </div>
                    </div>
                    
                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                    <div class="form-group">
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Sabha Status Type</label>
                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <input type="text" class="form-control capitalizedata" value="{{ $sabha_user_status_type or ''}}" name="sabha_user_status_type" id="sabha_user_status_type" data-oldvalue="{{ $sabha_user_status_type or ''}}" disabled="">
                        </div>
                    </div>
                    @elseif(Auth::user()->id != $user->id)
                    <div class="form-group">
                        <label for="inputSkills" class="col-xs-12 col-sm-3 col-lg-2 control-label">Sabha Status Type</label>
                        <div class="col-xs-12 col-sm-9 col-lg-10">
                            <input type="text" class="form-control capitalizedata" value="{{ $sabha_user_status_type or ''}}" name="sabha_user_status_type" id="sabha_user_status_type" data-oldvalue="{{ $sabha_user_status_type or ''}}" disabled="">
                        </div>
                    </div>
                    @endif
                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-5 col-lg-3 control-label">
                            <input type="checkbox" class="minimal"  name="bst_member" value="bst_member" id="bst_member_checkbox" <?php
                            if ($user->BstKstMember == 1) {
                                echo " checked";
                            }
                            ?>>&nbsp;&nbsp;&nbsp;&nbsp;  BST/KST Member

                        </label>
                        
                        <label class="col-xs-12 col-sm-5 col-lg-3 control-label networking_individual_label <?php if(!in_array(ucwords($personal_wing_name),array("Kishore","Kishori","Bal","Balika"))) { echo "hide";}?>">
                            <input type="checkbox" class="minimal"  name="networking_target" value="networking_target" id="networking_target" <?php
                            if ($user->networking_target == 1) {
                                echo " checked";
                            }
                            ?>>&nbsp;&nbsp;&nbsp;&nbsp;  Networking Individual

                        </label>
                    </div>
                    @else
                    @permission('update.user.database')
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-5 col-lg-3 control-label">
                            <input type="checkbox" class="minimal"  name="bst_member" value="bst_member" id="bst_member_checkbox" <?php
                            if ($user->BstKstMember == 1) {
                                echo " checked";
                            }
                            ?> <?php if(Auth::user()->id == $user->id) { echo "disabled" ;}?>>&nbsp;&nbsp;&nbsp;&nbsp;  BST/KST Member

                        </label>
                        
                        @if(Auth::user()->id != $user->id)
                        <label class="col-xs-12 col-sm-5 col-lg-3 control-label networking_individual_label <?php if(!in_array(ucwords($personal_wing_name),array("Kishore","Kishori","Bal","Balika"))) { echo "hide";}?>">
                            <input type="checkbox" class="minimal"  name="networking_target" value="networking_target" id="networking_target" <?php
                            if ($user->networking_target == 1) {
                                echo " checked";
                            }
                            ?>>&nbsp;&nbsp;&nbsp;&nbsp;  Networking Individual
                        </label>
                        @endif
                    </div>
                    @else
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-5 col-lg-3 control-label">
                            <input type="checkbox" class="minimal"  name="bst_member" value="bst_member" id="bst_member_checkbox" <?php
                            if ($user->BstKstMember == 1) {
                                echo " checked";
                            }
                            ?> disabled>&nbsp;&nbsp;&nbsp;&nbsp;  BST/KST Member
                        </label>
                        
                        @if(Auth::user()->id != $user->id)
                        <label class="col-xs-12 col-sm-5 col-lg-3 control-label networking_individual_label <?php if(!in_array(ucwords($personal_wing_name),array("Kishore","Kishori"))) { echo "hide";}?>">
                            <input type="checkbox" class="minimal"  name="networking_target" value="networking_target" id="networking_target" <?php
                            if ($user->networking_target == 1) {
                                echo " checked";
                            }
                            ?>>&nbsp;&nbsp;&nbsp;&nbsp;  Networking Individual
                        </label>
                        @endif
                    </div>
                    @endpermission
                    @endif
                    
                    
                    
<!--                        </form>-->
                    </div>
                    <!-- End :  Satsang Information-->

                    <!-- Begin :  Social Media Information-->
                    <div class="tab-pane" id="social">
<!--                        <form class="form-horizontal">-->
                            <div class="form-group">
                        <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Facebook Account Url</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <input type="text" class="form-control" placeholder="Facebook Account Url" value="{{ $user->facebook_id or ''}}" name="facebook_id" maxlength="50" id="facebook_id">
                            <label for="inputEmail3" class="control-label socailinfotext">Example : https://www.facebook.com/name</label>
                        </div>

                        <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Twitter Account Url</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <input type="text" class="form-control" placeholder="Twitter Account Url" value="{{ $user->twitter_id or ''}}" name="twitter_id" maxlength="50" id="twitter_id">
                            <label for="inputEmail3" class="control-label socailinfotext">Example : https://www.twitter.com/name</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Snapchat Account Url</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4 userbottommargin">
                            <input type="text" class="form-control" placeholder="Snapchat Account Url" value="{{ $user->snapchat_id or ''}}" name="snapchat_id" maxlength="50" id="snapchat_id">
                            <label for="inputEmail3" class="control-label socailinfotext">Example : https://www.snapchat.com/add/name</label>
                        </div>

                        <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Instagram Account Url</label>

                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <input type="text" class="form-control" placeholder="Instagram Account Url" value="{{ $user->instagram_id or ''}}" name="instagram_id" maxlength="50" id="instagram_id">
                            <label for="inputEmail3" class="control-label socailinfotext">Example : https://www.instagram.com/name</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="text" class="col-xs-12 col-sm-3 col-lg-2 control-label">Linkedin Account Url</label>
                        <div class="col-xs-12 col-sm-9 col-lg-4">
                            <input type="text" class="form-control" placeholder="Linkedin Account Url" value="{{ $user->linkedin_id or ''}}" name="linkedin_id" maxlength="50" id="linkedin_id">
                            <label for="inputEmail3" class="control-label socailinfotext">Example : http://in.linkedin.com/in/name</label>
                        </div>
                    </div>
<!--                        </form>-->
                    </div>
                    <!-- End :  Social Media Information-->
                </div>
            </div>
            {!! Form::close() !!}
            @if($is_child_show == 2)
            <!-- Begin : Manage User Children -->
            <div class="box box box-info">
                <div class="box-header">
                    <h3 class="box-title">Children Information</h3>
                    <span class="adduser-icon pull-right"><a class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="cleardata();"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add</a></span>
      <!--              <img src="{{ URL::asset('img/add-ico.svg') }}" type="button" class="pull-right" data-toggle="modal" data-target="#myModal" onclick="cleardata();">-->
                </div>
                @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                <input type="hidden" name="is_action" id="is_action" value="1">
                <input type="hidden" name="show_action" id="show_action" value="true">
                @elseif(Auth::user()->id == $user->id)
                <input type="hidden" name="is_action" id="is_action" value="1">
                <input type="hidden" name="show_action" id="show_action" value="true">
                @else
                @permission('update.user.database')
                <input type="hidden" name="is_action" id="is_action" value="1">
                <input type="hidden" name="show_action" id="show_action" value="true">
                @else
                <input type="hidden" name="is_action" id="is_action" value="2">
                <input type="hidden" name="show_action" id="show_action" value="false">
                @endpermission
                @endif
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="children_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="usenum not-export-col">No</th>
                                <th class="uname">First Name</th>
                                <th class="uname">Last Name</th>
                                <th class="uname">Gender</th>
                                @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                                <th class="ucenter not-export-col actionfield">Action</th>
                                @elseif(Auth::user()->id == $user->id)
                                <th class="ucenter not-export-col actionfield">Action</th>
                                @else
                                @permission('update.user.database')
                                <th class="ucenter not-export-col actionfield">Action</th>
                                @endpermission
                                @endif
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            @endif
            <!-- End : Manage User Children -->
            @if(Auth::user()->id != $user->id)
            <!-- Begin : Manage User My Events -->
            <div class="box box box-info">
                <div class="box-header">
                    <h3 class="box-title">My Events</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="event_user_list" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="usenum not-export-col">No</th>
                                <th class="uname">Event Name</th>
                                <th class="uname">Dates</th>
                                <th class="uname">Location</th>
                                <th class="uname">Status</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            @endif
            <!-- End : Manage User My Events -->
            
            <!-- Begin : Manage User Sabha attendance of different sabhas -->
            <div class="box box box-info">
                <div class="box-header">
                    <h3 class="box-title">Sabha Attendance</h3>
                </div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#sabha_reporting" data-toggle="tab" class="sabha_reporting">Sabha Reporting</a></li>
                        <li><a href="#campus_sabha" data-toggle="tab" class="campus_sabha">Campus Sabha</a></li>
                        <li><a href="#karyakar_goshthi" data-toggle="tab" class="karyakar_goshthi">Karyakar Goshthi</a></li>
                    </ul>
                    <div class="tab-content">
                    <!-- Begin :  Sabha Reporting Section-->
                    <div class="active tab-pane" id="sabha_reporting">
                        <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <table id="sabha_reporting_list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
<!--                                            <th class="usenum not-export-col">No</th>-->
                                            <th class="uname">Sabha Date</th>
                                            <th class="uname">Sabha Label</th>
                                            <th class="uname">Wing</th>
                                            <th class="uname">Region</th>
                                            <th class="uname">Center</th>
                                            <th class="uname">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                    </div>
                    <!-- End :  Sabha Reporting Section-->
                    <!-- Begin :  Campus Sabha Section-->
                    <div class="tab-pane" id="campus_sabha">
                        <!-- /.box-header -->
                            <div class="box-body table-responsive">
                                <table id="campus_sabha_list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
<!--                                            <th class="usenum not-export-col">No</th>-->
                                            <th class="uname">Sabha Date</th>
                                            <th class="uname">Sabha Topic</th>
                                            <th class="uname">Sabha Region</th>
                                            <th class="uname">Sabha Center</th>
                                            <th class="uname">Sabha Campus</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                    </div>
                    <!-- End :  Campus Sabha Section-->
                    <!-- Begin :  Karyakar Goshthi Section-->
                    <div class="tab-pane" id="karyakar_goshthi">
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="karyakar_goshthi_list" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
<!--                                        <th class="usenum not-export-col">No</th>-->
                                        <th class="uname">Sabha Date</th>
                                        <th class="uname">Sabha Topic</th>
                                        <th class="uname">Sabha Region</th>
                                        <th class="uname">Sabha Center</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- End :  Karyakar Goshthi Section-->
                </div>
            </div>
            <!-- End : Manage User Sabha attendance of different sabhas -->
        </div>

        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title childpopupnote">Add Child</h4>
                </div>
                {!! Form::open(array('url' => 'admin/user/insertuserchild' , 'class' => 'form-horizontal','id' => 'addchild_form', 'files' => true)) !!}
                <div class="modal-body">
                    <div class="contact-other-info">
                        <div class="coulmn-my-info" style="padding:0px;">
                            <div class="info-row">
                                <label for="inputName" class="control-label">First Name<em class="mandatory-field">*</em></label>
                                <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                    <input type="text" class="form-control myfn capitalizedata" placeholder="First Name" value="" name="first_name" id="first_name" maxlength="50" data-oldvalue="" tabindex="1" required>
                                </div>
                            </div>

                            <div class="info-row">
                                <label for="inputName" class="control-label">Last Name<em class="mandatory-field">*</em></label>
                                <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                    <input type="text" class="form-control myfn capitalizedata" placeholder="Last Name" value="" name="last_name" id="last_name" maxlength="50" data-oldvalue="" tabindex="2" required>
                                </div>
                            </div>
                        </div>
                        <div class="coulmn-my-info">


                            <div class="info-row">
                                <label for="inputName" class="control-label">Gender<em class="mandatory-field">*</em></label>
                                <div class="profile-input-rows">
                                    <select class="form-control valid" aria-invalid="false" aria-required="true" name="gender_child" id="gender_child" tabindex="3" required>
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="info-row">
                                <label for="inputName" class="control-label">Date of Birth<em class="mandatory-field">*</em></label>
                                <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                    <input type="text" class="form-control date-picker mydp" placeholder="Date of Birth" value="" id="dob_child" name="dob_child" maxlength="50" tabindex="4" required>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="contact-other-info">
                        <label for="inputName" class="control-label">Diseases</label>
                        <div class="row margin-leftright-5">
                            @if(isset($diseases) && !empty($diseases))
                            @foreach($diseases as $disease)
                            <div class="col-xs-12 col-sm-6 col-lg-6">
                                <input id="child_disease{{ $disease->id }}" type="checkbox" tabindex="5" value="{{ $disease->id }}" name="child_disease[]">
                                <label class="control-label" for="child_disease{{ $disease->id }}"> {{ ucfirst($disease->name) }}</label>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="info-row">
                        <label for="inputName" class="control-label">Allergies & Medical Information</label>
                        <div class="profile-input-rows">
                            <textarea class="form-control valid" rows="3" name="allergies_medicalinfo" id="allergies_medicalinfo" aria-required="true" aria-invalid="false" data-oldvalue="" tabindex="6"></textarea>
                        </div>
                    </div>
                    <input type="hidden" id="child_id" name="child_id" />
                    <input type="hidden" name="user_id" value="{{ $user->id or ''}}">
                    <div class="contact-other-info"></div>
                </div>
                <div class="modal-footer buttons-rows">
                    <input type="submit" class="btn btn-primary removefloat" value="Save" />
                    <button type="button" class="btn btn-save removefloat" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>



    <div class="modal fade" id="editchild" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title childpopupnote">Add Child</h4>
                </div>
                {!! Form::open(array('url' => 'admin/user/insertuserchild' , 'class' => 'form-horizontal','id' => 'editchild_form', 'files' => true)) !!}
                <div class="modal-body">
                    <div class="contact-other-info">
                        <div class="coulmn-my-info" style="padding:0px;">
                            <div class="info-row">
                                <label for="inputName" class="control-label">First Name<em class="mandatory-field">*</em></label>
                                <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                    <input type="text" class="form-control myfn capitalizedata" placeholder="First Name" value="" name="first_name" id="edit_first_name" maxlength="50" data-oldvalue="" tabindex="1" required>
                                </div>
                            </div>
                            <div class="info-row">
                                <label for="inputName" class="control-label">Last Name<em class="mandatory-field">*</em></label>
                                <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                    <input type="text" class="form-control myfn capitalizedata" placeholder="Last Name" value="" name="last_name" id="edit_last_name" maxlength="50" data-oldvalue="" tabindex="2" required>
                                </div>
                            </div>
                        </div>
                        <div class="coulmn-my-info">

                            <div class="info-row">
                                <label for="inputName" class="control-label">Gender<em class="mandatory-field">*</em></label>
                                <div class="profile-input-rows">
                                    <select class="form-control valid" aria-invalid="false" aria-required="true" name="gender_child" id="edit_gender_child" tabindex="3" required>
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="info-row">
                                <label for="inputName" class="control-label">Date of Birth<em class="mandatory-field">*</em></label>
                                <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                    <input type="text" class="form-control date-picker mydp" placeholder="Date of Birth" value="" id="edit_dob_child" name="dob_child" maxlength="50" tabindex="4" required>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="contact-other-info">
                        <label for="inputName" class="control-label">Diseases</label>
                        <div class="row margin-leftright-5">
                            @if(isset($diseases) && !empty($diseases))
                            @foreach($diseases as $disease)
                            <div class="col-xs-6 col-sm-6 col-lg-6">
                                <input id="edit_child_disease{{ $disease->id }}" type="checkbox" tabindex="5" value="{{ $disease->id }}" name="child_disease[]">
                                <label class="control-label" for="child_disease{{ $disease->id }}"> {{ ucfirst($disease->name) }}</label>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="info-row">
                        <label for="inputName" class="control-label">Allergies & Medical Information</label>
                        <div class="profile-input-rows">
                            <textarea class="form-control valid" rows="3" name="allergies_medicalinfo" id="edit_allergies_medicalinfo" aria-required="true" aria-invalid="false" data-oldvalue="" tabindex="6"></textarea>
                        </div>
                    </div>
                    <input type="hidden" id="edit_child_id" name="child_id" />
                    <input type="hidden" name="user_id" value="{{ $user->id or ''}}">
                    <div class="contact-other-info"></div>
                </div>
                <div class="modal-footer buttons-rows">
                    <input type="submit" class="btn btn-primary removefloat" value="Save" />
                    <button type="button" class="btn btn-save removefloat" data-dismiss="modal">Close</button>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
    <!-- Begin: Modal For User Profile Pic -->
    <div class="modal" id="myModal_profile">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close closeModal" id="closeModal">&times;</button>
                    <h4 class="modal-title">Profile Picture</h4>
                </div>
                <div class="modal-body">
                    <div id="div_profile_pic"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="save_profile_pic" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default closeModal">Close</button>
                </div>
            </div>    
        </div>
    </div>
    <!-- End: Modal For User Profile Pic -->
    <!-- Begin: Modal For Relocate User -->
    <div class="modal fade" id="relocate_user_modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close closerelocatemodal" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Relocate User</h4>
                </div>
                <form id="relocate_user_form" role="form" method="post">

                    <div class="modal-body">
                        <!--                <div id="div_relocate_user">-->
                        <!--Begin : Current Data-->
                        <div class="row margin-bottom-20">
                            <div class="col-md-6">
                                <label class="control-label col-md-5 fontstylerelocate">Current Region : </label>
                                <div class="col-md-7">
                                    <?php if (isset($current_region) && ($current_region->region_name != "")) { ?>
                                        <label class="control-label fontstylerelocate" id="current_region">{{ $current_region->region_name or ''}}</label>
                                    <?php } else { ?>
                                        <label class="control-label fontstylerelocate" id="current_region">--</label>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-5 fontstylerelocate">Current Center : </label>
                                <div class="col-md-7">
                                    <?php if (isset($current_center) && ($current_center->center_name != "")) { ?>
                                        <label class="control-label fontstylerelocate" id="current_center">{{ $current_center->center_name or '' }}</label>
                                    <?php } else { ?>
                                        <label class="control-label fontstylerelocate" id="current_center">--</label>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--End : Current Data-->
                        <!--Begin : New Data-->
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label col-md-5 fontstylerelocate">New Region :<em class="mandatory-field">*</em></label>
                                <div class="col-md-7">
                                    <select class="form-control" name="new_region" id="new_region">
                                        <option value="" >Select Region</option>
                                        <?php
                                        if (isset($regions) && !empty($regions)) {
                                            for ($i = 0; $i < count($regions); $i++) {
                                                ?>
                                                <option value="<?php echo $regions[$i]->id; ?>"><?php echo $regions[$i]->RegionName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label col-md-5 fontstylerelocate">New Center :<em class="mandatory-field">*</em></label>
                                <div class="col-md-7">
                                    <select class="form-control" name="new_center" id="new_center">
                                        <option value="" >Select Center</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="user_id" value="{{ $user->id or ''}}">
                        <input type="hidden" name="current_first_name" value="{{ $user->FirstName or ''}}">
                        <input type="hidden" name="current_last_name" value="{{ $user->LastName or ''}}">
                        <input type="hidden" name="current_center_id" value="{{ $user->center_id or 0 }}" id="current_region_id">
                        <input type="hidden" name="current_region_name" value="{{ $current_region->region_name or '' }}">
                        <input type="hidden" name="current_center_name" value="{{ $current_center->center_name or '' }}">
                        <input type="hidden" value="{{$personal_wing_name}}" name="existing_personal_wing_name" id="existing_personal_wing_name">
                        <!--End : New Data-->
                        <!--                </div>-->
                    </div>

                    <div class="modal-footer buttons-rows">
                        <input type="submit" id="save_relocate_user" class="btn btn-primary removefloat">
                        <button type="button" class="btn btn-default closerelocatemodal removefloat" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>    
        </div>
    </div>
    <!-- End: Modal For Relocate User -->
    <!--Begin : Modal For Delete Record-->
    <div class="modal fade" id="modal_deleterecord">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Delete Record</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this record ?</p>
                </div>
                <input type="hidden" name="redirectpath" value="" id="redirectpath">
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary confirmdelete">Yes</button>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>

                </div>
            </div>
        </div>
    </div>
    <!--End : Modal For Delete Record-->
</section>
@endsection
