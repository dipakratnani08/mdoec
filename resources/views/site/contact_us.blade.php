@extends('theme.master')
@section('htmlheader_title')
	| Contact Us
@endsection
@section ('AdditionalVendorCssInclude')
<style type="text/css">
    #contactForm label.error {
    color: #ff0000;
}
textarea {
    resize: none
}
.alert.alert-info {
    background-color: #00cae3;
    color: #fff;
}
.alert.alert-success {
    background-color: #55b559;
    color: #fff;
}
.alert.alert-danger {
    background-color: #f55145;
    color: #fff;
}
.alert.alert-warning {
    background-color: #ff9e0f;
    color: #fff;
}
.alert {
    border: 0;
    border-radius: 0;
    padding: 20px 15px;
    line-height: 20px;
    position: relative;
}
.alert .alert-icon {
    display: block;
    float: left;
    margin-right: 1.071rem;
}
.close:not(:disabled):not(.disabled) {
    cursor: pointer;
}
.alert .close {
    color: #fff;
    text-shadow: none;
    opacity: .9;
}
button.close {
    padding: 0;
    background-color: transparent;
    border: 0;
    -webkit-appearance: none;
}
.alert b {
    font-weight: 500;
    text-transform: uppercase;
    font-size: 12px;
}
.successlist, .successlist th, .successlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }

    .successlist th, .successlist td {
        padding: 5px;
        text-align: left;
    }

    .btn-success{
        margin-right: 15px;
    }
    .mapBox iframe {
            width: 100% !important;
    }
    </style>
@endsection
@section('AdditionalVendorScriptsInclude')
<script>
$(document).ready(function() {
                /*Hide Success -error message after page load*/
                $('.alert-dismissible').delay(5000).fadeOut('slow');
});</script>
<!--    <script>

      function initMap() {
        var myLatLng = {lat: 20.218052, lng: 72.751274};

        var map = new google.maps.Map(document.getElementById('mapBox'), {
          zoom: 4,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Sahastrar Dham'
        });
      }
    </script>-->
<script src="{{ url (asset('/public/sitetheme/javascript/jquery.validate.min.js')) }}"></script>
<script type="text/javascript">
    $("#contactForm").validate({
        rules:{
            name: {
                required: true
            },
            email: {
                required: true
            },
            subject: {
                required: true
            },
            message: {
                required: true
            }
        },
        messages:{
            name: {
                required: 'Please enter your name'
            },
            email: {
                required: 'Please enter your email'
            },
            subject: {
                required: 'Please enter subject'
            },
            message: {
                required: 'Please enter message'
            }
        }
    });
</script>
@endsection
@section('main-content')
<!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area br_image">
            <div class="container">
                <div class="page-cover text-center">
                    <h2 class="page-cover-tittle">Contact Us</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/')}}">Home</a></li>
                        <li class="active">Contact Us</li>
                    </ol>
                </div>
            </div>
        </section>
        <!--================Breadcrumb Area =================-->
        <!--================Contact Area =================-->
        <section class="contact_area section_gap">
            <div class="container">
                <!-- Begin : Success Notification-->
<!--                <div class="alert alert-success">
                    <div class="col-md-12">
                        <div class="alert-icon">
                            <i class="fa fa-check">info_outline</i>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                        </button>

                        <b>Info alert:</b> {{session('success_message')}}
                    </div>
                </div>-->
                @if(session('success_message'))      
                <div class="alert alert-success alert-dismissible margin-left-15 margin-right-15"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>{{session('success_message')}}</div>
                @endif
                <!-- End : Success Notification-->
                <!-- Begin : Failure Notification-->
                @if(session('error_message'))
               <div class="alert alert-danger alert-dismissible margin-left-15 margin-right-15"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">�</button>{{session('error_message')}}</div>  
               @endif
<!--                <div class="alert alert-danger">
                    <div class="col-md-12">
                        <div class="alert-icon">
                            <i class="material-icons">info_outline</i>
                        </div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
                        </button>

                        <b class="fa fa-">Info alert:</b> {{session('success_message')}}
                    </div>
                </div>-->
                
                <!-- End : Failure Notification-->
                  
                <div id="mapBox456" class="mapBox" 
                    data-lat="20.218052" 
                    data-lon="72.751274" 
                    data-zoom="13" 
                    data-info="Sahastrar Dham, Nargol, Gujarat 396120."
                    data-mlat="20.218052"
                    data-mlon="72.751274">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3743.9822862954034!2d72.74908571533759!3d20.218057120332684!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be72ad400d8029d%3A0x583053b2e20951a3!2sSahastrar+Dham!5e0!3m2!1sen!2sin!4v1534849761117" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-4">
                        <div class="contact_info">
                            <div class="info_item">
                                <i class="lnr lnr-home"></i>
                                <h6>California, United States</h6>
                                <p>Santa monica bullevard</p>
                            </div>
                            <div class="info_item">
                                <i class="lnr lnr-phone-handset"></i>
                                <h6><a href="#">00 (440) 9865 562</a></h6>
                                <p>Mon to Fri 9am to 6 pm</p>
                            </div>
                            <div class="info_item">
                                <i class="lnr lnr-envelope"></i>
                                <h6>sahastrardhamnargol@gmail.com</h6>
                                <p>Send us your query anytime!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-lg-8">
<!--                        <form class="row contact_form" action="{{ url('/siteuser/savecontactus')}}" method="post" id="contactForm" novalidate="novalidate">-->
                            {!! Form::open(array('url' => '/savecontactus' , 'class' => 'row contact_form','id' => 'contactForm', 'files' => true)) !!}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email address">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" id="message" rows="1" placeholder="Enter Message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="submit" value="submit" class="btn btn_hover btn_hover_two">Send Message</button>
                            </div>
                            {!! Form::close() !!}
<!--                        </form>-->
                    </div>
                </div>
            </div>
        </section>
        <!--================Contact Area =================-->
        

<script src="{{ url (asset('/public/theme/js/jquery-3.2.1.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/popper.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/bootstrap.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/vendors/owl-carousel/owl.carousel.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/jquery.ajaxchimp.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/mail-script.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/stellar.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/vendors/imagesloaded/imagesloaded.pkgd.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/vendors/isotope/isotope-min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/stellar.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/vendors/lightbox/simpleLightbox.min.js')) }}"></script>
        <!--gmaps Js-->
<!--        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>-->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrRvcrZUEi46ckRhtlNJkTRmH2TVE4fGU&libraries=places"></script>
        <script src="{{ url (asset('/public/theme/js/gmaps.min.js')) }}"></script>
        <!-- contact js -->
        <script src="{{ url (asset('/public/theme/js/jquery.form.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/jquery.validate.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/vendors/nice-select/js/jquery.nice-select.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/contact.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/custom.js')) }}"></script>
@endsection
