@extends('theme.master')

@section('htmlheader_title')
@endsection
@section ('AdditionalVendorCssInclude')
<link rel="stylesheet" href="{{ url (asset('/public/theme/plugin/slickslider/slick/slick.css')) }}">
<link rel="stylesheet" href="{{ url (asset('/public/theme/plugin/slickslider/slick/slick-theme.css')) }}">
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.css">-->
<!--<link rel="stylesheet" href="http://kenwheeler.github.io/slick/slick/slick-theme.css">-->
<style type="text/css">
    #speech_container {
  max-width: 960px;
  margin: 0 auto;
  width: 90%;
}
    .slick {
  background: #EFEFEF;
  padding: 20px;
  .slick-slide {
    outline: none !important;
    -webkit-backface-visibility: hidden !important;
    * {
      -webkit-backface-visibility: hidden !important;
    }
  }
}

.caption {
  text-align: center;
  padding: 10px 0 0 0;
  color: #000;
  margin: 0;
  font-size: 24px;
}

.slick-prev, .slick-next {
  position: absolute;
  top: 50%;
  margin-top: -10px;
}
.slick-next, slick-next:hover, .slick-next:focus {
  
  background: -webkit-linear-gradient(0deg, #8490ff 0%, #62bdfc 100%) !important;
/*  left: 105%;*/
}
.slick-prev, .slick-prev:hover, .slick-prev:focus {
  right: 105%;
  background: -webkit-linear-gradient(0deg, #8490ff 0%, #62bdfc 100%) !important;
}

.slick-dots {
  text-align: center;
  margin: 10px 0 0 0;
  li {
    display: inline-block;
    vertical-align: top;
    margin: 0 8px;
  }
}
</style>
@endsection
@section('AdditionalVendorScriptsInclude')
<script src="{{ url (asset('/public/theme/plugin/slickslider/slick/slick.min.js')) }}"></script>
<script src="{{ url (asset('/public/theme/plugin/slickslider/slick/slick.js')) }}"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.1.0/jquery.fitvids.min.js"></script>
<script type="text/javascript">
    (function($) {
  jQuery(document).ready(function($) {
    //Refrence : https://codepen.io/michaelsmyth94/pen/EVQEQV
    /*
     * The below example uses Slick Carousel, however this can be
     * extended into any type of carousel, provided it lets you
     * bind events when the slide changes. This will only work
     * if all framed videos have the JS API parameters enabled.
     */
    
    //bind our event here, it gets the current slide and pauses the video before each slide changes.
    $(".slick").on("beforeChange", function(event, slick) {
      var currentSlide, slideType, player, command;
      
      //find the current slide element and decide which player API we need to use.
      currentSlide = $(slick.$slider).find(".slick-current");
      
      //determine which type of slide this, via a class on the slide container. This reads the second class, you could change this to get a data attribute or something similar if you don't want to use classes.
      slideType = currentSlide.attr("class").split(" ")[1];
      
      //get the iframe inside this slide.
      player = currentSlide.find("iframe").get(0);
      
      if (slideType == "vimeo") {
        command = {
          "method": "pause",
          "value": "true"
        };
      } else {
        command = {
          "event": "command",
          "func": "pauseVideo"
        };
      }
      
      //check if the player exists.
      if (player != undefined) {
        //post our command to the iframe.
        player.contentWindow.postMessage(JSON.stringify(command), "*");
      }
    });
    
    //start the slider
    $(".slick").slick({
//      infinite: false,
//      arrows: false,
//      dots: true
      dots: true,
  infinite: true,
  speed: 500,
  fade: true,
  cssEase: 'linear'
    });
    
    //run the fitVids jQuery plugin to ensure the iframes stay within the item.
    $('.item').fitVids();
    
  });
})(jQuery);
</script>
@endsection


@section('main-content')

<!--================banner Area =================-->
<section class="banner_area d-flex text-center">
    <div class="container align-self-center">
        <div class="row">
            <div class="col-md-12">
                <div class="banner_content">
                    <h6>Who Created us</h6>
                    <h1>Keep faith always</h1>
                    <p>If you are looking at blank cassettes on the web, you may be very confused at<br> the difference in price. You may see some for as low as $.17 each.</p>
                    <a href="#" class="btn_hover btn_hover_two">Get Started</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================banner Area =================-->

<!--================Event Date Area =================-->
<!--        <section class="event_date_area">
    <div class="container">
        <div class="row">
            <div class="col-md-6 d_flex">
                <div class="evet_location flex">
                    <h3>Spreading the faith to all</h3>
                        <p><span class="lnr lnr-calendar-full"></span>5th may, 2018</p>
                    <p><span class="lnr lnr-clock"></span>Saturday, 09.00 am to 05.00 pm</p>
                </div>
            </div>
            <div class="col-md-6 event_time">
                <h4>Our Next Event Starts in</h4>
                <div id="timer" class="timer">
                    <div class="timer__section days">
                        <div class="timer__number"></div>
                        <div class="timer__label">days</div>
                    </div>
                    <div class="timer__section hours">
                        <div class="timer__number"></div>
                        <div class="timer__label">hours</div>
                    </div>
                    <div class="timer__section minutes">
                        <div class="timer__number"></div>
                        <div class="timer__label">Minutes</div>
                    </div>
                    <div class="timer__section seconds">
                        <div class="timer__number"></div>
                        <div class="timer__label">seconds</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!--================Event Date Area =================-->

<!--================About Area =================-->
<section class="about_area section_gap">
    <div class="container">
        <div class="section_title text-center">
            <h2>Welcome to Sahastrar Dham</h2>
            <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from </p>
        </div>
        <div class="row">
            <div class="col-md-6 d_flex">
                <div class="about_content flex">
                    <h3 class="title_color">The standard Lorem Ipsum passage, used since the 1500s</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <!--                            <a href="#" class="about_btn btn_hover">Read Full Story</a>-->
                </div>
            </div>
            <div class="col-md-6">
                <img src="{{ url (asset('/public/theme/image/about.jpg')) }}" alt="abou_img">
            </div>
        </div>
        <div class="row">   
            <div class="col-md-12 d_flex">
                <div class="about_content flex" style="padding-right:0px;">
                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse painsOn the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================About Area =================-->

<!--================Features Area =================-->
<!--        <section class="features_area">
    <div class="row m0">
        <div class="col-md-3 features_item">
            <h3>Spreading Light to world</h3>
            <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from innocence and upturning of the natural.</p>
            <a href="#" class="btn_hover view_btn">View Details</a>
        </div>
        <div class="col-md-3 features_item">
            <h3>Spreading Light to world</h3>
            <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from innocence and upturning of the natural.</p>
            <a href="#" class="btn_hover view_btn">View Details</a>
        </div>
        <div class="col-md-3 features_item">
            <h3>Spreading Light to world</h3>
            <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from innocence and upturning of the natural.</p>
            <a href="#" class="btn_hover view_btn">View Details</a>
        </div>
        <div class="col-md-3 features_item">
            <h3>Spreading Light to world</h3>
            <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from innocence and upturning of the natural.</p>
            <a href="#" class="btn_hover view_btn">View Details</a>
        </div>
    </div>
</section>-->
<!--================Features Area =================-->

<!--================Sermons work Area =================-->
<section class="sermons_work_area section_gap">
    <div class="container">
        <div class="section_title text-center">
            <h2>History Of Nargol</h2>
            <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from</p>
        </div>
        <!--                <div class="row">
                            
                            <div class="col-md-12">
                                <img src="{{ url (asset('/public/theme/image/about.jpg')) }}" alt="abou_img">
                            </div>
                        </div>-->
        <div class="sermons_slider owl-carousel">
            <div class="item row">
                <div class="col-lg-12">
                    <div class="sermons_image">
                        <img src="{{ url (asset('/public/theme/image/sermns.jpg')) }}" alt="">
                        <p style="text-align: center; font-weight: bold; color: #00c8e6;">Image Caption 1</p>
                    </div>
                </div>
<!--                <div class="col-lg-4">
                    <div class="sermons_content">
                        <h3 class="title_color">Did not find your Package? Feel free to ask us. Weâ€˜ll make it for you</h3>
                        <ul class="list_style sermons_category">
                            <li><i class="lnr lnr-user"></i><span>Categories: </span><a href="#"> Travor James</a></li>
                            <li><i class="lnr lnr-database"></i><span>Sermon Speaker: </span> Prayer</li>
                            <li><i class="lnr lnr-calendar-full"></i><span>Date:</span> 5th may, 2018</li>
                        </ul>
                        <a href="#" class="btn_hover">View More Details</a>
                    </div>
                </div>-->
            </div>
            <div class="item row">
                <div class="col-lg-12">
                    <div class="sermons_image">
                        <img src="{{ url (asset('/public/theme/image/sermns.jpg')) }}" alt="">
                        <p style="text-align: center; font-weight: bold; color: #00c8e6;">Image Caption 2</p>
                    </div>
                </div>
<!--                <div class="col-lg-4">
                    <div class="sermons_content">
                        <h3 class="title_color">Did not find your Package? Feel free to ask us. Weâ€˜ll make it for you</h3>
                        <ul class="list_style sermons_category">
                            <li><i class="lnr lnr-user"></i><span>Categories: </span><a href="#"> Travor James</a></li>
                            <li><i class="lnr lnr-database"></i><span>Sermon Speaker: </span> Prayer</li>
                            <li><i class="lnr lnr-calendar-full"></i><span>Date:</span> 5th may, 2018</li>
                        </ul>
                        <a href="#" class="btn_hover">View More Details</a>
                    </div>
                </div>-->
            </div>
            <div class="item row">
                <div class="col-lg-12">
                    <div class="sermons_image">
                        <img src="{{ url (asset('/public/theme/image/sermns.jpg')) }}" alt="">
                        <p style="text-align: center; font-weight: bold; color: #00c8e6;">Image Caption 3</p>
                    </div>
                </div>
<!--                <div class="col-lg-4">
                    <div class="sermons_content">
                        <h3 class="title_color">Did not find your Package? Feel free to ask us. Weâ€˜ll make it for you</h3>
                        <ul class="list_style sermons_category">
                            <li><i class="lnr lnr-user"></i><span>Categories: </span><a href="#"> Travor James</a></li>
                            <li><i class="lnr lnr-database"></i><span>Sermon Speaker: </span> Prayer</li>
                            <li><i class="lnr lnr-calendar-full"></i><span>Date:</span> 5th may, 2018</li>
                        </ul>
                        <a href="#" class="btn_hover btn_hover_two">View More Details</a>
                    </div>
                </div>-->
            </div>
        </div>
        <div class="row">   
            <div class="col-md-12 d_flex">
                <div class="about_content flex" style="padding-right:0px;">
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.</p>
                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure.But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure.</p>
                    <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains..On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================Sermons work Area=================-->
<!--================Donate Area=================-->
<!--        <section class="donate_area">
    <div class="container">
        <div class="row">
            <div class="col-md-6 d-flex align-self-center">
                <div class="donate_content ">
                    <h2>Your donation can save <br>many lives</h2>
                    <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from innocence.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="donation_form">
                    <h3>How much would you like to donate?</h3>
                    <div class="form-group">
                        <input type='text' class="form-control" placeholder="$5"/>
                    </div>
                    <div class="form-group">
                        <input type='text' class="form-control" placeholder="$5"/>
                    </div>
                    <div class="form-group">
                        <input type='text' class="form-control" placeholder="$5"/>
                    </div>
                    <div class="form-group">
                        <input type='text' class="form-control" placeholder="Any"/>
                    </div>
                    <a href="#" class="btn_hover btn_hover_two">Donate Now</a>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!--================Donate Area=================-->

<!--================Event Blog Area=================-->
<section class="about_area section_gap">
    <div class="container">
        <div class="section_title text-center">
            <h2>Importance Of Nargol</h2>
            <p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast</p>
        </div>
        <div class="row">
            <div class="col-md-6 d_flex">
                <div class="about_content flex">
                    <h3 class="title_color">Did not find your Package Feel free to ask us. Weâ€˜ll make it for you</h3>
                    <p>inappropriate behavior is often laughed off as “boys will be boys”, women face higher conduct standards especially in the workplace. That's why it's crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.</p>
                    <!--                            <a href="#" class="about_btn btn_hover">Read Full Story</a>-->
                </div>
            </div>
            <div class="col-md-6">
                <img src="{{ url (asset('/public/theme/image/about.jpg')) }}" alt="abou_img">
            </div>
        </div>
        <div class="row">   
            <div class="col-md-12 d_flex">
                <div class="about_content flex" style="padding-right:0px;">
                    <p>inappropriate behavior is often laughed off as “boys will be boys”, women face higher conduct standards especially in the workplace. That's why it's crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.inappropriate behavior is often laughed off as “boys will be boys”, women face higher conduct standards especially in the workplace. That's why it's crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.inappropriate behavior is often laughed off as “boys will be boys”, women face higher conduct standards especially in the workplace. That's why it's crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.inappropriate behavior is often laughed off as “boys will be boys”, women face higher conduct standards especially in the workplace. That's why it's crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.inappropriate behavior is often laughed off as “boys will be boys”, women face higher conduct standards especially in the workplace. That's why it's crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.inappropriate behavior is often laughed off as “boys will be boys”, women face higher conduct standards especially in the workplace. That's why it's crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================Blog Area=================-->

<section class="sermons_work_area section_gap">
    <div class="container" id="speech_container">
        <div class="section_title text-center">
            <h2>Mother's Speech Of Nargol</h2>
            <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from</p>
        </div>
        <div class="slick">
    <div class="item youtube">
      <iframe id="youtube" width="920" height="518" src="https://www.youtube-nocookie.com/embed/jnvu1GpylP0?rel=0&amp;enablejsapi=1" frameborder="0" allowfullscreen></iframe> <!-- Make sure to enable the API by appending the "&enablejsapi=1" parameter onto the URL. -->
<!--      <p class="caption">YouTube</p>-->
    </div>
    <div class="item vimeo">
      <iframe id="vimeo" src="https://player.vimeo.com/video/100902001?byline=0&amp;portrait=0&amp;api=1" width="920" height="517" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <!-- Make sure to enable the API by appending the "&api=1" parameter onto the URL. -->
<!--      <p class="caption">Vimeo</p>-->
    </div>
  </div>
        
<!--        <div class="col-lg-12">
                    <<iframe height="250" src="https://www.youtube.com/embed/3395oxkxrxg" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen=""></iframe>
                </div>-->
<!--        <div class="sermons_slider owl-carousel">
            <div class="item row">
                <div class="col-lg-12">
                    <div class="sermons_image">
                        <img src="{{ url (asset('/public/theme/image/sermns.jpg')) }}" alt="">
                        <p style="text-align: center; font-weight: bold; color: #00c8e6;">Video Caption 1</p>
                    </div>
                </div>
            </div>
            <div class="item row">
                <div class="col-lg-12">
                    <div class="sermons_image">
                        <img src="{{ url (asset('/public/theme/image/sermns.jpg')) }}" alt="">
                        <p style="text-align: center; font-weight: bold; color: #00c8e6;">Video Caption 2</p>
                    </div>
                </div>
            </div>
            <div class="item row">
                <div class="col-lg-12">
                    <div class="sermons_image">
                        <img src="{{ url (asset('/public/theme/image/sermns.jpg')) }}" alt="">
                        <p style="text-align: center; font-weight: bold; color: #00c8e6;">Video Caption 3</p>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</section>
@endsection