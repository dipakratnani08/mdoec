@extends('theme.master')
@section('htmlheader_title')
| Health Centre
@endsection
@section ('AdditionalVendorCssInclude')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css">
<link rel="stylesheet" href="{{ url (asset('/public/theme/plugin/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.css')) }}">
<style type="text/css">
    a.signup-button,
input.signup-button{
    color: #202020;
    font-size: 14px;
    display: inline-block;
    line-height: 52px;
    width: 230px;
    text-align: center;
    border-radius: 26px;
    text-decoration: none;
    border: 1px solid #ddd;
}

a.signup-button::after,
input.signup-button::after{
    content: '\25B6';
    color: #202020;
    position: relative;
    left: 15px;
    transition: all 0.3s ease;
    text-decoration: none;
}

a.signup-button:hover,
input.signup-button:hover{
    transition: all 0.3s ease;
    color: #fff;
}

a.signup-button:hover::after,
input.signup-button:hover::after{
    left: 25px;
    color: #fff;
    transition: all 0.3s ease;
}
.pricings .signup-button,
.portfolio-single-aside .signup-button{
    line-height: 42px;
    width: 142px;
    margin: 45px 0 0;
}
    .pricings{
    min-height: 690px;
}

.pricings h3{
    margin-bottom: 50px;
}

.pricings .plan-name{
    font-family: 'Montserrat', sans-serif;
    font-size: 16px;
}

.pricings ul{
    padding-left: 0;
    margin-top: 40px;
}

.pricings .pricing-plan{
    border-top: 1px solid #e2ebf1;
    border-bottom: 1px solid #e2ebf1;
    padding-top: 35px;
    background-color: #fff;
    position: relative;
    z-index: 0;
    min-height: 545px;
}

.pricings-2 .pricing-plan{
    border: none;
    border-radius: 12px!important;
    box-shadow: 0 0 15px rgba(0,0,0,.25);
    border-top: 5px solid;
}

.pricings .pricing-plan:hover{
    box-shadow: 0 0 15px rgba(0,0,0,.25);
    border-radius: 6px;
    margin-top: -10px;
    border-top: 5px solid;
    transition: all 0.3s ease;
    min-height: 570px;
    z-index: 1;
}

.pricings-2 .pricing-plan:hover{
    margin-top: 0;
    min-height: 545px;
}

.pricings .plan-price{
    font-family: 'Montserrat', sans-serif;
    margin-top: 18px;
    font-size: 50px;
    font-weight: 500;
}

.pricings .plan-price-freq{
    font-family: 'Work Sans', sans-serif;
    margin: 20px 0 30px;
    font-style: italic;
    color: #87959c;
    font-weight: 500;
}

.pricings ul li,
.portfolio-single-aside ul li{
    font-family: 'Work Sans', sans-serif;
    font-size: 15px;
    color: #535353;
    list-style: none;
}

.pricings ul li:not(:last-child){
    margin-bottom: 20px;
}

.pricings ul li::before,
.portfolio-single-aside ul li::before{
    content: '\2713';
    display: inline-block;
    font-size: 9px;
    margin-right: 10px;
    font-weight: 900;
    position: relative;
    top: -1px;
}

.pricings .signup-button::after,
.portfolio-single-aside .signup-button::after{
    content: '';
}

.pricings .signup-button,
.portfolio-single-aside .signup-button{
    line-height: 42px;
    width: 142px;
    margin: 45px 0 0;
}

.portfolio-single-aside .signup-button:hover{
    background-color: #0bd7fc;
    box-shadow: 0 0 15px rgba(0,0,0,.25);
}

.pricings .col-lg-6:first-of-type .pricing-plan{
    border-left: 1px solid #e2ebf1;
    border-top-left-radius: 12px;
    border-bottom-left-radius: 12px;
}

.pricings .col-lg-3:last-child .pricing-plan{
    border-right: 1px solid #e2ebf1;
    border-top-right-radius: 12px;
    border-bottom-right-radius: 12px;
}

.pricings .pricing-plan:hover .signup-button{
    line-height: 47px;
    width: 155px;
    transition: all .3s ease;
}

.pricings-2 .pricing-plan:hover .signup-button,
.pricings-3 .pricing-plan:hover .signup-button{
    line-height: 42px;
    width: 142px;
}

.pricings-3 .pricing-plan{
    border: 1px solid #e2ebf1;	
    border-radius: 12px!important;
    box-shadow: 0 0 15px rgba(0,0,0,.25);
}

.pricings-3 .pricing-plan:hover{
    margin-top: 0;
    border: none!important;
    box-shadow: none;
    min-height: 545px;
    background: -webkit-linear-gradient(bottom, #6558e0, #26efd5);
    transition: all 0.3s ease;
}

.pricings-3 .pricing-plan:hover .signup-button,
.pricings-3 .pricing-plan:hover ul li,
.pricings-3 .pricing-plan:hover ul li::before,
.pricings-3 .pricing-plan:hover .plan-price-freq,
.pricings-3 .pricing-plan:hover .plan-price,
.pricings-3 .basic-plan:hover .plan-name,
.pricings-3 .premium-plan:hover .plan-name,
.pricings-3 .business-plan:hover .plan-name,
.pricings-3 .ultimate-plan:hover .plan-name{
    color: #fff;
    transition: all .3s ease;
}

.pricings-3 .signup-button:hover{
    background: transparent!important;
}
@media(max-width: 992px) {
    .pricings .col-lg-6:nth-child(2n+1) .pricing-plan{
        border-left: 1px solid #e2ebf1;
        border-top-left-radius: 12px;
        border-bottom-left-radius: 12px;
    }

    .pricings .col-lg-6:nth-child(2n) .pricing-plan{
        border-right: 1px solid #e2ebf1;
        border-top-right-radius: 12px;
        border-bottom-right-radius: 12px;
    }

    .pricing-plan{
        margin-bottom: 40px;
    }
}
@media(max-width: 576px) {	
    .pricings .col-lg-6 .pricing-plan{
        border-left: 1px solid #e2ebf1;
        border-top-left-radius: 12px;
        border-bottom-left-radius: 12px;
    }

    .pricings .col-lg-6 .pricing-plan{
        border-right: 1px solid #e2ebf1;
        border-top-right-radius: 12px;
        border-bottom-right-radius: 12px;
    }
}


.form-elegant .font-small {
    font-size: 0.8rem; }

.form-elegant .z-depth-1a {
    -webkit-box-shadow: 0 2px 5px 0 rgba(55, 161, 255, 0.26), 0 4px 12px 0 rgba(121, 155, 254, 0.25);
    box-shadow: 0 2px 5px 0 rgba(55, 161, 255, 0.26), 0 4px 12px 0 rgba(121, 155, 254, 0.25); }

.form-elegant .z-depth-1-half,
.form-elegant .btn:hover {
    -webkit-box-shadow: 0 5px 11px 0 rgba(85, 182, 255, 0.28), 0 4px 15px 0 rgba(36, 133, 255, 0.15);
    box-shadow: 0 5px 11px 0 rgba(85, 182, 255, 0.28), 0 4px 15px 0 rgba(36, 133, 255, 0.15); }

.form-elegant .modal-header {
    border-bottom: none; }

.modal-dialog .form-elegant .btn .fa {
    color: #2196f3!important; }

.form-elegant .modal-body, .form-elegant .modal-footer {
    font-weight: 400; 
}
.btn.blue-gradient {
	-webkit-transition: .5s ease;
	-o-transition: .5s ease;
	transition: .5s ease;
	color: #fff
}
.blue-gradient {
	background: -webkit-linear-gradient(50deg, #45cafc, #303f9f)!important;
	background: -o-linear-gradient(50deg, #45cafc, #303f9f)!important;
	background: linear-gradient(40deg, #45cafc, #303f9f)!important
}
.btn.blue-gradient:active,
.btn.blue-gradient:active:focus .btn.blue-gradient.active,
.btn.blue-gradient:focus,
.btn.blue-gradient:hover {
	background: -webkit-linear-gradient(50deg, #5ed1fc, #3647b3);
	background: -o-linear-gradient(50deg, #5ed1fc, #3647b3);
	background: linear-gradient(40deg, #5ed1fc, #3647b3)
}
.btn-block {
	display: block;
	width: 100%
}

.btn-block+.btn-block {
	margin-top: .5rem
}
.btn.btn-block {
	margin: inherit
}
.btn-rounded {
	-webkit-border-radius: 10em;
	border-radius: 10em
}

.input-group-icon .form-control {
    padding-left: 45px;
}



/** SEARCH AREA BOX 2 **/
.search-area-box-2 h3 {
    font-size: 21px;
    margin: 0 0 10px;
    font-weight: 600;
    color: #212121;
}

.search-area-box-2 h1 {
    font-size: 15px;
    font-weight: 600;
    letter-spacing: 2px;
    margin: 0 0 30px;
}

.search-area-box-2 .form-group {
    margin-bottom: 30px;
}

.search-area-box-2 .mrg-btm-10 {
    margin-bottom: 10px;
}


/** Search area box 6 **/
.search-area-box-6{
    background: #e6e6e6;
    padding: 30px 0 10px;
}

.search-area-box-6 .form-group {
    margin-bottom: 20px;
}

.events-secion {
    margin-bottom: 70px;
}

/** Booking System **/
.search-booking-box {
    padding: 30px 30px 0;
    width: 100%;
    margin-bottom: 50px;
    display: inline-block;
}

.search-booking-box h3 {
    text-align: left;
}

.wizard {
    margin: 20px auto;
    background: #fbfbfb;
}

.wizard .nav-tabs {
    position: relative;
    margin: 80px auto;
    margin-bottom: 0;
    border-bottom-color: #e0e0e0;
}

.wizard > div.wizard-inner {
    position: relative;
}

.connecting-line {
    height: 2px;
    background: #e0e0e0;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 36%;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border: 0;
    border-bottom-color: transparent;
    border-radius: 100px !important;
}

span.round-tab {
    width: 70px;
    height: 70px;
    line-height: 66px;
    display: inline-block;
    border-radius: 100px;
    background: #fff;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
}

.wizard li.active span.round-tab {
    color: #fff;
}

.wizard li.active span.round-tab i {
    color: #fff;
}

span.round-tab:hover {
    color: #333;
}

span.round-tab:hover i {
    color: #fff;
}

.wizard .nav-tabs > li {
    width: 25%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    transition: 0.1s ease-in-out;
}

.wizard li.active:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
}

.wizard .nav-tabs > li a {
    width: 70px;
    height: 70px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
}

.wizard .nav-tabs > li a:hover {
    background: transparent;
}

.wizard .tab-pane {
    position: relative;
    padding-top: 60px;
}

.wizard h3 {
    margin-top: 0;
}

.booking-flow .panel-box .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
    color: #fff;
    box-shadow: none !important;
    border-radius: 100px;
}

.booking-heading {
    margin-bottom: 30px;
    font-size: 20px;
    text-align: center;
}

.booking-heading-2 {
    margin-bottom: 30px;
    font-size: 25px;
    color: #fff;
    font-weight: 600;
}


.black-color{
    color: #515151;
}

.booling-details-box {
    background: #585858;
    padding: 30px;
    display: inline-block;
    color: #eaeaea;
}

.booling-details-box img {
    margin-bottom: 25px;
}

.booling-details-box h4 {
    font-size: 20px;
    font-weight: 600;
    margin: 0;
    padding-bottom: 15px;
    color: #fff;
    border-bottom: solid 1px #6f6767;
}

.booling-details-box ul {
    margin: 25px 0;
    padding: 0 0 25px;
    border-bottom: solid 1px #6f6767;
}

.booling-details-box ul li {
    list-style: none;
    line-height: 30px;
    font-size: 15px;
    color: #fff;
}

.booling-details-box ul li span {
    font-weight: 600;
    margin-right: 10px;
}

.booling-details-box .price {
    font-size: 20px;
    font-weight: 600;
}

.booling-details-box p {
    line-height: 30px;
    color: #fff;
}

.booling-details-box .slider-mover-left {
    position: absolute;
    top: 40%;
    width: 30px;
    height: 30px;
    line-height: 28px;
}

.booling-details-box .slider-mover-right {
    position: absolute;
    top: 40%;
    width: 30px;
    height: 30px;
    line-height: 28px;
}

.booling-details-box .slider-mover-left i {
    font-size: 20px;
}

.booling-details-box .slider-mover-right i {
    font-size: 20px;
}

.booking-flow .contact-form label {
    margin-bottom: 15px;
    font-size: 16px;
    color: #525252;
    font-weight: 400;
}

.booking-flow .contact-form .input-text {
    width: 100%;
    padding: 10px 17px;
    font-size: 13px;
    outline: none;
    color: #6c6c6c;
    height: 40px;
    border: 1px solid #efefef;
    background: #efefef;
    border-radius: 0;
}

.booking-flow .contact-form textarea {
    min-height: 210px;
}

.booking-flow .contact-form .button-theme {
    float: right;
}

.booking-flow .contact-form .form-group {
    margin-bottom: 40px;
}

.booking-flow .country {
    width: 100%;
}

.booking-flow .bootstrap-select {
    width: 100% !important;
    font-family: Roboto, sans-serif !important;
}

.booking-flow .bootstrap-select .btn {
    height: 40px;
    padding: 10px 17px;
    font-size: 14px;
    outline: none;
    border: 1px solid #efefef;
    background: #efefef;
    border-radius: 0;
    color: #6f6b6b;
}

.booking-flow .bootstrap-select .btn:hover {
    border: 1px solid #efefef;
    background: #efefef;
    color: #6f6b6b;
}

.bootstrap-select .dropdown-toggle:focus {
    outline: 0 auto -webkit-focus-ring-color !important;
}

.your-address {
    padding: 0 0 5px;
    margin-bottom: 25px;
    border-bottom: solid 1px #6f6767;
}

.your-address > strong {
    color: #fff;
    margin-bottom: 20px;
    display: block;
    float: left;
}

.your-address > address {
    margin-left: 120px;
}

.your-address > address strong {
    margin-bottom: 20px;
}

.default-plate {
    background: #fba466;
}

.green-plate {
    background: #95c41f;
}

.blue-plate {
    background: #5950f7;
}

.yellow-plate {
    background: #ffb400
}

.red-plate {
    background: #d20023;
}

.green-light-plate {
    background: #1abc9c
}

.peru-plate {
    background: #e2b78a;
}

.sandybrown-plate {
    background: #fba466;
}

.green-light-2-plate {
    background: #2adc71;
}

.blue-light-2-plate {
    background: #00c2f9;
}

.purple-plate {
    background: #8e44ad;
}

.blue-light-plate {
    background: #3c9dff;
}

.royalblue-plate {
    background: #4169E1;
}

.setting-button {
    background: #eee;
    position: absolute;
    font-size: 16px;
    text-align: center;
    width: 50px;
    height: 50px;
    line-height: 50px;
    left: -50px;
    color: #fff;
    top: 0;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    cursor: pointer;
}
.rooms-features ul li i {
    color: #3ac4fa;
}

.border-btn-theme {
    border: 2px solid #3ac4fa;
    color: #3ac4fa;
}

.search-contents h2 span{
    color: #3ac4fa;
}

.border-btn-theme:hover {
    background: #3ac4fa;
    color: #fff;
}
.mt-20 {
    margin-top: 20px;
}
.search-contents h3 {
/*    margin: 0 0 5px;*/
    text-transform: uppercase;
}
.search-contents h2 {
    margin: 0 0 15px;
    text-transform: uppercase;
    font-weight: 700;
    font-size: 28px;
}
.search-area-box-2 .search-contents .btn-default {
    background: #fff !important;
    border: solid 1px #fff !important;
    color: #565656;
}
.search-contents .btn-default {
    padding: 9px 15px;
    width: 100%;
    border-radius: 3px;
/*    color: #757575;*/
}
.datepicker, .datepicker:focus {
/*    border-radius: 0px;*/
    outline: none;
}
.form-control-2 {
    width: 100%;
}
.dropdown-menu {
    padding: 0;
    margin: 0;
    border-radius: 0;
}
.search-contents .search-button {
    padding: 9px 20px;
}
.btn-theme {
    background: #3ac4fa;
    border: 2px solid #3ac4fa;
}
.search-button {
    cursor: pointer;
    padding: 13px 20px;
    letter-spacing: 1px;
    font-size: 13px;
    font-weight: 600;
    text-transform: uppercase;
    color: #FFF;
    transition: .5s;
    border-radius: 2px;
    width: 100%;
    outline: none;
}
.btn-theme {
    color: #FFF !important;
}
@media (max-width:767px) {
	.hidden-xs {
		display: none!important
	}
}

@media (min-width:768px) and (max-width:991px) {
	.hidden-sm {
		display: none!important
	}
}

@media (min-width:992px) and (max-width:1199px) {
	.hidden-md {
		display: none!important
	}
}

@media (min-width:1200px) {
	.hidden-lg {
		display: none!important
	}
}
</style>
@endsection
@section('AdditionalVendorScriptsInclude')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
<script src="{{ url (asset('/public/theme/plugin/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js')) }}"></script>
<script type="text/javascript">
     
    
    
    $(document).ready(function () {
       // $("#book_reg_room").modal('show');
        $(document).on('click', ".bookroom", function () {
            //console.log("h"); return false;
            $("#modal_bookroom").modal('show');
            //Open Room Modal
        });
        $(document).on('click', "a.bookdormitory", function () {
            //Open Dormitory Modal
            $("#modal_bookdormitory").modal('show');
        });
        
        $(".datepicker").datepicker({
        ignoreReadonly: true,
        allowInputToggle: true,
        disableTouchKeyboard: true,
        Readonly: true,
        autoclose: true
    });
    
    $('.selectpicker').selectpicker();
    });
</script>
@endsection

@section('main-content')
<!--================Breadcrumb Area =================-->
<section class="breadcrumb_area br_image">
    <div class="container">
        <div class="page-cover text-center">
            <h2 class="page-cover-tittle">Health Centre</h2>
            <ol class="breadcrumb">
                <li><a href="{{ url('/')}}">Home</a></li>
                <li class="active">Health Centre</li>
            </ol>
        </div>
    </div>
</section>
<!--================Breadcrumb Area =================-->

<!--================About Area =================-->
<section class="about_area section_gap">
    <div class="container">
        <div class="section_title text-center">
            <h2>Overview</h2>
            <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from </p>
        </div>
        <div class="row">
            <div class="col-md-6 d_flex">
                <div class="about_content flex">
                    <h3 class="title_color">Did not find your Package Feel free to ask us. We‘ll make it for you</h3>
                    <p>inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.</p>
                    <a href="#" class="about_btn btn_hover">Read Full Story</a>
                </div>
            </div>
            <div class="col-md-6">
                <img src="{{ url (asset('/public/theme/image/about.jpg')) }}" alt="abou_img">
            </div>
        </div>
    </div>
</section>
<!--================About Area =================-->

<!--================facility Area =================-->
<!-- Start facility Area -->
<section class="services-area section_gap" id="facility">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <!--		                <div class="menu-content  col-lg-7">
                                                <div class="title text-center">
                                                    <h1 class="mb-10">My Offered Services</h1>
                                                    <p>At about this time of year, some months after New Year’s resolutions have been made and kept, or made and neglected.</p>
                                                </div>
                                            </div>-->
            <div class="section_title text-center">
                <h2>Facility</h2>
                <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from </p>
            </div>
        </div>						
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-pie-chart"></span>
                    <a href="#"><h4>Web Design</h4></a>
                    <p>
                        “It is not because things are difficult that we do not dare; it is because we do not dare that they are difficult.”
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-laptop-phone"></span>
                    <a href="#"><h4>Web Development</h4></a>
                    <p>
                        If you are an entrepreneur, you know that your success cannot depend on the opinions of others. Like the wind, opinions.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-camera"></span>
                    <a href="#"><h4>Photography</h4></a>
                    <p>
                        Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills.
                    </p>
                </div>	
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-picture"></span>
                    <a href="#"><h4>Clipping Path</h4></a>
                    <p>
                        Hypnosis quit smoking methods maintain caused quite a stir in the medical world over the last two decades. There is a lot of argument.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-tablet"></span>
                    <a href="#"><h4>Apps Interface</h4></a>
                    <p>
                        Do you sometimes have the feeling that you’re running into the same obstacles over and over again? Many of my conflicts.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-rocket"></span>
                    <a href="#"><h4>Graphic Design</h4></a>
                    <p>
                        You’ve heard the expression, “Just believe it and it will come.” Well, technically, that is true, however, ‘believing’ is not just thinking that.
                    </p>
                </div>				
            </div>														
        </div>
    </div>	
</section>
<!-- End facility Area -->
<!--================facility Area =================-->


<!--================Team Area=================-->
<section class="team_area section_gap" id="doctor_list">
    <div class="container">
        <div class="section_title text-center">
            <h2>Doctor List</h2>
            <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from</p>
        </div>
        <div class="row mb_30">
            <div class="col-md-3 col-sm-6">
                <div class="team_item">
                    <div class="team_img">
                        <img src="{{ url (asset('/public/theme/image/team1.jpg')) }}" alt="team">
                        <ul class="list_style">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="content">
                        <h3>Philip Goodwin</h3>
                        <p>Chief Pastor</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="team_item">
                    <div class="team_img">
                        <img src="{{ url (asset('/public/theme/image/team2.jpg')) }}" alt="team">
                        <ul class="list_style">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="content">
                        <h3>Duane Lewis</h3>
                        <p>Chief Pastor</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="team_item">
                    <div class="team_img">
                        <img src="{{ url (asset('/public/theme/image/team3.jpg')) }}" alt="team">
                        <ul class="list_style">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="content">
                        <h3>Jose Austin</h3>
                        <p>Chief Pastor</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="team_item">
                    <div class="team_img">
                        <img src="{{ url (asset('/public/theme/image/team5.jpg')) }}" alt="team">
                        <ul class="list_style">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <div class="content">
                        <h3>Leroy Lopez</h3>
                        <p>Chief Pastor</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================Team Area=================-->

<!--================Registration Area=================-->
<div class="demo price-area section_gap" id="registration">
    <div class="container">
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1517355312741"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="pricings  text-center drag-this-up drag-up"><p class="pretitle">PLANS &amp; PRICINGS</p><h3>Our special product offers</h3><div class="row no-gutters"><style>.pricings .basic-plan .plan-name, .pricings .basic-plan ul li::before{color: #20edd7;}.pricings .basic-plan:hover, .pricings-2 .basic-plan:hover{border-color: #20edd7;}.pricings .basic-plan .signup-button:hover{background-color: #20edd7;}</style><div class="col-lg-6 col-sm-6"><div class="pricing-plan basic-plan"><p class="plan-name">BASIC PLAN</p><p class="plan-price"><sup>$</sup>49</p><p class="plan-price-freq">per month</p> <hr><ul><li>unlimited traffic</li><li>2 GB disk space</li><li>100 GB monthly bandwidth</li><li>free security service</li><li>2 dashboard accounts</li></ul><a class="signup-button" href="#">Sign up now</a></div></div><style>.pricings .premium-plan .plan-name, .pricings .premium-plan ul li::before{color: #0bd7fc;}.pricings .premium-plan:hover, .pricings-2 .premium-plan:hover{border-color: #0bd7fc;}.pricings .premium-plan .signup-button:hover{background-color: #0bd7fc;}</style>
                                <div class="col-lg-6 col-sm-6"><div class="pricing-plan premium-plan"><p class="plan-name">PREMIUM PLAN</p><p class="plan-price"><sup>$</sup>69</p><p class="plan-price-freq">per month</p> <hr><ul><li>unlimited traffic</li><li>2 GB disk space</li><li>100 GB monthly bandwidth</li><li>free security service</li><li>2 dashboard accounts</li></ul><a class="signup-button" href="#">Sign up now</a></div></div><style>.pricings .business-plan .plan-name, .pricings .business-plan ul li::before{color: #1f90f9;}.pricings .business-plan:hover, .pricings-2 .business-plan:hover{border-color: #1f90f9;}.pricings .business-plan .signup-button:hover{background-color: #1f90f9;}</style>
<!--                                <div class="col-lg-3 col-sm-6"><div class="pricing-plan business-plan"><p class="plan-name">BUSINESS PLAN</p><p class="plan-price"><sup>$</sup>89</p><p class="plan-price-freq">per month</p> <hr><ul><li>unlimited traffic</li><li>2 GB disk space</li><li>100 GB monthly bandwidth</li><li>free security service</li><li>2 dashboard accounts</li></ul><a class="signup-button" href="#">Sign up now</a></div></div><style>.pricings .ultimate-plan .plan-name, .pricings .ultimate-plan ul li::before{color: #655ee0;}.pricings .ultimate-plan:hover, .pricings-2 .ultimate-plan:hover{border-color: #655ee0;}.pricings .ultimate-plan .signup-button:hover{background-color: #655ee0;}</style>
                                <div class="col-lg-3 col-sm-6"><div class="pricing-plan ultimate-plan"><p class="plan-name">ULTIMATE PLAN</p><p class="plan-price"><sup>$</sup>129</p><p class="plan-price-freq">per month</p> <hr><ul><li>unlimited traffic</li><li>2 GB disk space</li><li>100 GB monthly bandwidth</li><li>free security service</li><li>2 dashboard accounts</li></ul><a class="signup-button" href="#">Sign up now</a></div>
                                </div>-->
                            </div></div></div>
                                </div></div>
        </div>
        <!--        <div class="row d-flex justify-content-center">
                                                <div class="menu-content pb-70 col-lg-8">
                                                        <div class="title text-center">
                                                                <h1 class="mb-10">Registration</h1>
                                                                <p>Select Stayover over type form below given</p>
                                                        </div>
                                                </div>
                                        </div>	
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="pricingTable">
                            <h3 class="title">Standard</h3>
                            <div class="price-value">$10.00
                                <span class="month">/month</span>
                            </div>
                            <ul class="pricing-content">
                                <li>50GB Disk Space</li>
                                <li>50 Email Accounts</li>
                                <li>50GB Monthly Bandwidth</li>
                                <li class="disable"><i class="fa fa-times"></i></li>
                                <li class="disable"><i class="fa fa-times"></i></li>
                            </ul>
                            <a href="#" class="pricingTable-signup">Sign up</a>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="pricingTable blue">
                            <h3 class="title">Business</h3>
                            <div class="price-value">$20.00
                                <span class="month">/month</span>
                            </div>
                            <ul class="pricing-content">
                                <li>60GB Disk Space</li>
                                <li>60 Email Accounts</li>
                                <li>60GB Monthly Bandwidth</li>
                                <li>15 Subdomains</li>
                                <li class="disable"><i class="fa fa-times"></i></li>
                            </ul>
                            <a href="#" class="pricingTable-signup">Sign up</a>
                        </div>
                    </div>
                </div>-->
    </div>
</div>

<div class="search-area-box-2 search-area-box-6">
    <div class="container">
        <div class="search-contents">
            <form method="GET">
                <div class="row search-your-details">
                    <div class="col-lg-3 col-md-3">
                        <div class="search-your-rooms mt-20">
                            <h3 class="hidden-xs hidden-sm">Search</h3>
                            <h2 class="hidden-xs hidden-sm">Your <span>Rooms</span></h2>
                            <h2 class="hidden-lg hidden-md">Search Your <span>Rooms</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="form-group">
                                    <input type="text" class="btn-default datepicker" placeholder="Check In">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="form-group">
                                    <input type="text" class="btn-default datepicker" placeholder="Check Out">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="form-group">
                                    <div class="btn-group bootstrap-select search-fields form-control-2"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="Double Room" aria-expanded="false"><span class="filter-option pull-left">Double Room</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox" style="max-height: 221px; overflow: hidden; min-height: 125px;"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 219px; overflow-y: auto; min-height: 123px;"><li data-original-index="0" class=""><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Room</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1" class=""><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Single Room</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2" class="selected"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true"><span class="text">Double Room</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Deluxe Room</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="selectpicker search-fields form-control-2" name="room" tabindex="-98">
                                        <option>Room</option>
                                        <option>Single Room</option>
                                        <option>Double Room</option>
                                        <option>Deluxe Room</option>
                                    </select></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="form-group">
                                    <div class="btn-group bootstrap-select search-fields form-control-2"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="2" aria-expanded="false"><span class="filter-option pull-left">2</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox" style="max-height: 371px; overflow: hidden; min-height: 125px;"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 369px; overflow-y: auto; min-height: 123px;"><li data-original-index="0" class=""><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Adult</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2" class="selected"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true"><span class="text">2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">5</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="selectpicker search-fields form-control-2" name="adults" tabindex="-98">
                                        <option>Adult</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="form-group">
                                    <div class="btn-group bootstrap-select search-fields form-control-2"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" role="button" title="3" aria-expanded="false"><span class="filter-option pull-left">3</span>&nbsp;<span class="bs-caret"><span class="caret"></span></span></button><div class="dropdown-menu open" role="combobox" style="max-height: 371px; overflow: hidden; min-height: 125px;"><ul class="dropdown-menu inner" role="listbox" aria-expanded="false" style="max-height: 369px; overflow-y: auto; min-height: 123px;"><li data-original-index="0" class=""><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">Child</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">1</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="2"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">2</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="3" class="selected"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="true"><span class="text">3</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="4"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">4</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="5"><a tabindex="0" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">5</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div><select class="selectpicker search-fields form-control-2" name="children" tabindex="-98">
                                        <option>Child</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="form-group">
                                    <button class="search-button btn-theme">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--================Registration Area=================-->
<!--Begin : Modal For Book Room-->
<div class="modal fade" id="modal_bookroom">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Payment Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to pay for this event ?</p>
            </div>
            <input type="hidden" name="redirectpath" value="" id="redirectpath">
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary confirmdelete">Yes</button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>

            </div>
        </div>
    </div>
</div>
<!--End : Modal For Book Room-->

<div class="modal fade" id="book_reg_room" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content form-elegant">
            <!--Header-->
            <div class="modal-header text-center">
                <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalLabel"><strong>Book Room</strong></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--Body-->
            <div class="modal-body mx-4">
                
                            {!! Form::open(array('url' => '/savecontactus' , 'class' => 'row','id' => 'contactForm', 'files' => true)) !!}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email address">
                                </div>
                                <div class="form-group input-group-icon">
                                    <div class="icon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></div>
                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Enter Subject">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" id="message" rows="10" placeholder="Enter Message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="submit" value="submit" class="btn btn_hover btn_hover_two">Send Message</button>
                            </div>
                            {!! Form::close() !!}
                <!--Body-->
            </div>
            <!--Footer-->
            <div class="modal-footer mx-5 pt-3 mb-1">
                <p class="font-small grey-text d-flex justify-content-end">Not a member? <a href="#" class="blue-text ml-1"> Sign Up</a></p>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
@endsection