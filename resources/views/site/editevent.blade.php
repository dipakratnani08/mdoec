@extends('layouts/master')

@section ('AdditionalVendorCssInclude')
<style type="text/css">
    .errorlist, .errorlist th, .errorlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }

    .errorlist th, .errorlist td {
        padding: 5px;
        text-align: left;
    }
    .successlist, .successlist th, .successlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }

    .successlist th, .successlist td {
        padding: 5px;
        text-align: left;
    }
    #eligible_wings
    {
        background: none !important;
    }
    table_titlecase
    {
        text-transform: capitalize !important;
    }

    .table_titlecase_record
    {
        text-transform: capitalize !important;
    }
    .btn-primary{
        margin-right: 15px;
    }
    .margin-left-15
    {
        margin-left: 15px !important;
    }

    .margin-right-15
    {
        margin-right: 15px !important;
    }

    .margin-top-15
    {
        margin-top: 15px !important;
    }

    .padding-left-25
    {
        padding-left: 25px !important;
    }

    .padding-right-25
    {
        padding-right: 25px !important;
    }

    .margin-bottom-15
    {
        margin-bottom: 15px !important;
    }
    textarea {
        resize: none;
    }
    .wing-check-row {
        float: left;
        margin-right: 12px;
        width: auto;
        margin-top: 5px;
    }
    @media (max-width:992px)
    {
        .userbottommargin { margin-bottom: 15px !important;}
    }
    @media (min-width:993px) and (max-width:1199px)
    {
        .location_margin
        {
            margin-bottom: 15px;
        }
    }
    @media (min-width:768px) and (max-width:991px)
    {
        .form-horizontal .control-label
        {
            text-align: left;
            margin-bottom: 5px;
        }
    }
    @media (min-width:991px) and (max-width:1199px)
    {
        .form-horizontal .control-label
        {
            text-align: left;
            margin-bottom: 5px;
        }
        .userbottommargin { margin-bottom: 15px !important;}
    }
    .emergencyborder
    {
        border-top: none !important;
    }
    div.dt-buttons
    {
        width: 100% !important;
    }
    .action-buttons ,.actionfield
    {
        width:55px !important;
    }
    .pac-container {
        z-index: 10000 !important;
    }
</style>
@endsection

@section('PageTitle')
Edit Event
@endsection

@section('AdditionalVendorScriptsInclude')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.GOOGLE_MAP_API_KEY') ?>&libraries=places"></script>
<script type="text/javascript" src="{{ asset('global/js/jquery.geocomplete.js') }}"></script>
<script type="text/javascript">


/*Begin : Apply Icheck on checkbox*/
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        disabledClass: '',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
/*End : Apply Icheck on checkbox*/
/*Begin : Create Validation Method For ZIPCODE*/
jQuery.validator.addMethod("zipcode", function (value, element) {
    return this.optional(element) || /^\d{5}(?:-\d{4})?$/.test(value);
});
/*Begin : Create Validation Method For ZIPCODE*/

/*Begin: Create Function For Fetch Stayover Location List*/
var hotel_list = $('#hotel_list').dataTable({
    "bLengthChange": false,
    "processing": false,
    "serverSide": true,
    "bStateSave": false,
    "responsive": true,
//                "bStateSave": true, //for load back page where edit action was applied
    dom: 'Bfrtip',
    "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Stayover Location(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Record found."},
    "ajax": {
        'type': 'POST',
        'url': '<?= action('AdminmanageeventController@postStayoverlocationlist') ?>',
        'data': function (param) {
            param.event_id = $("#event_id").val();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //    console.log('ERRORS: ' + textStatus);
        },
    },
    "bFilter": false,
    "fnRowCallback": function (nRow, aData, iDisplayIndex) {

        var oSettings = hotel_list.fnSettings();
        $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);

        return nRow;
    },
    "language": {
        "emptyTable": "No Record found.",
        "search": ""
    },
    "columns": [
        {"data": "id", "searchable": false, "bsortable": false, "class": "table_titlecase_record"},
        {"data": "stayover_name", "class": "table_titlecase_record"},
        {"data": "address", "class": "table_titlecase_record"},
        {"data": "city", "class": "table_titlecase_record"},
        {"data": "state_name", "class": "table_titlecase_record"},
        {"data": "action", "class": "table_titlecase_record", "searchable": false, "bsortable": false, "orderable": false}
    ]
});
/*End: Create Function For Fetch Stayover Location List*/

/*Begin : Create Function For Fetch Outing Location List*/
var outing_list = $('#outing_list').dataTable({
    "bLengthChange": false,
    "processing": false,
    "responsive": true,
    "serverSide": true,
    "bStateSave": false,
//                "bStateSave": true, //for load back page where edit action was applied
    dom: 'Bfrtip',
    "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Outing Location(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Record found."},
    "ajax": {
        'type': 'POST',
        'url': '<?= action('AdminmanageeventController@postOutinglocationlist') ?>',
        'data': function (param) {
            param.event_id = $("#event_id").val();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //console.log('ERRORS: ' + textStatus);
        },
    },
    "bFilter": false,
    "fnRowCallback": function (nRow, aData, iDisplayIndex) {

        var oSettings = outing_list.fnSettings();
        $("td:first", nRow).html(oSettings._iDisplayStart + iDisplayIndex + 1);

        return nRow;
    },
    "language": {
        "emptyTable": "No Record found.",
        "search": ""
    },
    "columns": [
        {"data": "id", "searchable": false, "bsortable": false, "class": "table_titlecase_record"},
        {"data": "outing_name", "class": "table_titlecase_record"},
        {"data": "address", "class": "table_titlecase_record"},
        {"data": "city", "class": "table_titlecase_record"},
        {"data": "state_name", "class": "table_titlecase_record"},
        {"data": "action", "class": "table_titlecase_record", "searchable": false, "bsortable": false, "orderable": false}
    ]
});
/*End: Create Function For Fetch Outing Location List*/

// Begin : Get Zip code of event location
$('#event_location').keyup(function () {
    if ($("#event_location").val() != "")
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postZipcode') ?>',
            data: {"address": $("#event_location").val()},
            //dataType: "json",
            success: function (result) {

                if (result != "false")
                {
                    $("#event_zip").val(result);
                } else
                {
                    $("#event_zip").val("");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    }
});
//End : Get Zip code of event Loocation

//Begin : Get Zip code For Add Stayover
$('#address').keyup(function () {
    if ($("#address").val() != "")
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postZipcode') ?>',
            data: {"address": $("#address").val()},
            //dataType: "json",
            success: function (result) {

                if (result != "false")
                {
                    $("#pincode").val(result);
                } else
                {
                    $("#pincode").val("");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    }
});
//End : Get Zip code For Add Stayover
//Begin : Get Zip code For Edit Stayover
$('#edit_address').keyup(function () {
    if ($("#edit_address").val() != "")
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postZipcode') ?>',
            data: {"address": $("#edit_address").val()},
            //dataType: "json",
            success: function (result) {

                if (result != "false")
                {
                    $("#edit_pincode").val(result);
                } else
                {
                    $("#edit_pincode").val("");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    }
});
//End : Get Zip code For Edit Stayover


//Begin : Get Zip code For Add Outing
$('#add_outing_address').keyup(function () {
    if ($("#add_outing_address").val() != "")
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postZipcode') ?>',
            data: {"address": $("#add_outing_address").val()},
            //dataType: "json",
            success: function (result) {

                if (result != "false")
                {
                    $("#add_outing_pincode").val(result);
                } else
                {
                    $("#add_outing_pincode").val("");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    }
});
//End : Get Zip code For Add Outing
//Begin : Get Zip code For Edit Outing
$('#edit_outing_address').keyup(function () {
    if ($("#edit_outing_address").val() != "")
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postZipcode') ?>',
            data: {"address": $("#edit_outing_address").val()},
            //dataType: "json",
            success: function (result) {

                if (result != "false")
                {
                    $("#edit_outing_pincode").val(result);
                } else
                {
                    $("#edit_outing_pincode").val("");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    }
});
//End : Get Zip code For Edit Outing



(function ($) {
    $(function () {
        /*Begin : Method For Greter than*/
        jQuery.validator.addMethod("greaterThan",
                function (value, element, params) {
                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) >= new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val())
                            || (Number(value) >= Number($(params).val()));
                });
        /*End : Method For Greter than*/
        /*Begin : Form Validation*/
        $("#editevent_form").validate({
            rules: {
                event_type: {required: true},
                event_level: {required: true},
                event_title: {required: true},
                event_location: {required: true},
                event_zip: {required: true},
                eligible_regions: {required: true},
                event_center: {required: true},
                event_start_date: {required: true},
                event_end_date: {required: true},
                early_reg_start_date: {required: true},
                reg_start_date: {required: true},
                reg_end_date: {required: true},
                maximum_attendence: {required: true},
//                refund_amount: {required: true},
                transportation_deadline: {required: true},
                "eligible_wings[]": {required: true},
                event_description: {required: true},
                stay_start_date: {required: true},
                stay_start_date1: {required: true},
                stay_end_date: {required: true},
                payment_country: {required: true},
                host_region: {required: true},
//                payment_region: {required: true},
//                payment_center: {required: true},
                currency_type: {required: true},
                form_type: {required: true},
                stayover_info: {
                    required: {
                        depends: function (element) {
                            return $("#event_level").val() >= 3;//For Center only
                        }
                    },
                },
            },
            messages: {
                event_type: {required: "Please Select Event Type."},
                event_level: {required: "Please Select Event Level."},
                event_title: {required: "Please Enter Event Title."},
                event_location: {required: "Please Enter Event Location."},
                event_zip: {required: "Please Enter Zip/Postal Code."},
                eligible_regions: {required: "Please Select Event Region."},
                event_center: {required: "Please Select Event Center."},
                event_start_date: {required: "Please Select Event Start Date."},
                event_end_date: {required: "Please Select Event End Date."},
                early_reg_start_date: {required: "Please Select Early Registration Start Date."},
                reg_start_date: {required: "Please Select Registration Start Date."},
                reg_end_date: {required: "Please Select Registration End Date."},
                maximum_attendence: {required: "Please Enter Maximum Attendence."},
//                refund_amount: {required: "Please Enter Refund Amount."},
                transportation_deadline: {required: "Please Select Transportation Deadline."},
                event_description: {required: "Please Enter Event Description."},
                "eligible_wings[]": {required: "Please Select Eligible Wing."},
                stay_start_date: {required: "Please Select Stayover Start Date."},
                stay_start_date1: {required: "Please Select Stayover Start Date1."},
                stay_start_date2: {required: "Please Select Stayover Start Date2."},
                stay_end_date: {required: "Please Select Stayover End Date."},
                stayover_info: {required: "Please Select Stayover Type."},
                payment_country: {required: "Please Select Payment Country."},
                host_region: {required: "Please Select Host Region."},
//                payment_region: {required: "Please Select Payment Region."},
//                payment_center: {required: "Please Select Payment Center."},
                currency_type: {required: "Please Select Currency Type."},
                form_type: {required: "Please Select Form Type."},
            }
        });
        /*End : Form Validation*/


        /*Begin : Form Validation For Add Stayover*/
        $("#addhotel_form").validate({
            rules: {
                stayover_location_type: {required: true},
                stayover_name: {required: true},
                address: {required: true},
                state: {required: true},
                city: {required: true},
                pincode: {
                    required: true,
                    //zipcode: true
                },
                rooms: {
                    // required: true,
                    min: 1,
                    digits: true
                },
            },
            messages: {
                stayover_location_type: {required: "Please Select Location Type."},
                stayover_name: {required: "Please Enter Name."},
                address: {required: "Please Enter Address."},
                state: {required: "Please Select State/Province."},
                city: {required: "Please Enter City."},
                pincode: {
                    required: "Please Enter Zip/Postal Code",
                },
                rooms: {
                    digits: "Please Enter Numbers only.",
                },
            },
            submitHandler: function (form) {
                var event_id = $('#event_id').val();
                var data = $(form).serialize();
                var url = '<?= action('AdminmanageeventController@postSavestayoverlocation') ?>';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: "json",
                    success: function (result) {
                        $('#add_stayover_modal').modal('hide');
                        if (result.status == "false") {
                            toastr.error(result.message, 'Error');
                        } else
                        {
                            toastr.success(result.message, 'Success');
                            if($("#hotel_list tbody tr td:first").hasClass("dataTables_empty"))
                            {
                                $("#hotel_list tbody tr td:first").attr("colspan",6);
                            }
                            hotel_list.fnDraw(false); // CAll DAtatable of Stayover list
                        }
                        //window.location.reload();
                    },
                    error: function (result) {
                        //console.log(result);
                    }
                });
                return false;
            }
        });
        /*End : Form Validation For Add Stayover*/

        /*Begin : Form Validation For Edit Stayover*/
        $("#edithotel_form").validate({
            rules: {
                stayover_location_type: {required: true},
                stayover_name: {required: true},
                address: {required: true},
                state: {required: true},
                city: {required: true},
                pincode: {
                    required: true
                },
                rooms: {
                    //required: true,
                    min: 1,
                    digits: true
                },
            },
            messages: {
                stayover_location_type: {required: "Please Select Location Type."},
                stayover_name: {required: "Please Enter Name."},
                address: {required: "Please Enter Address."},
                state: {required: "Please Select State/Province."},
                city: {required: "Please Enter City."},
                pincode: {
                    required: "Please Enter Zip/Postal Code",
                },
                rooms: {
                    //required: "Please Enter Number of Rooms.",
                    digits: "Please Enter Numbers only.",
                },
            },
            submitHandler: function (form) {
                var event_id = $('#event_id').val();
                var data = $(form).serialize();
                var url = '<?= action('AdminmanageeventController@postUpdatestayoverlocation') ?>';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: "json",
                    success: function (result) {
                        $('#edit_stayover_modal').modal('hide');
                        if (result.status == "false") {
                            toastr.error(result.message, 'Error');
                        } else
                        {
                            toastr.success(result.message, 'Success');
                            if($("#hotel_list tbody tr td:first").hasClass("dataTables_empty"))
                            {
                                $("#hotel_list tbody tr td:first").attr("colspan",6);
                            }
                            hotel_list.fnDraw(false); // CAll DAtatable of Stayover list
                        }
                        //window.location.reload();
                    },
                    error: function (result) {
                        //console.log(result);
                    }
                });
                return false;
            }
        });
        /*End : Form Validation For Edit Stayover*/


        /*Begin : Form Validation For Add Location*/
        $("#addouting_form").validate({
            rules: {
                outing_name: {required: true},
                outing_description: {required: true},
                address: {required: true},
                state: {required: true},
                city: {required: true},
                pincode: {
                    required: true,
                    //zipcode: true
                },
                outing_start_date: {required: true},
                outing_end_date: {required: true},
            },
            messages: {
                outing_name: {required: "Please Enter Outing Name."},
                outing_description: {required: "Please Enter Outing Description."},
                address: {required: "Please Enter Outing Address."},
                state: {required: "Please Select State/Province."},
                city: {required: "Please Enter City."},
                pincode: {
                    required: "Please Enter Zip/Postal Code.",
                },
                outing_start_date: {required: "Please Enter Event Outing Start Date."},
                outing_end_date: {required: "Please Enter Event Outing End Date."},
            },
            submitHandler: function (form) {
                var event_id = $('#event_id').val();
                var data = $(form).serialize();
                var url = '<?= action('AdminmanageeventController@postSaveoutinglocation') ?>';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: "json",
                    success: function (result) {
                        $('#add_outing_modal').modal('hide');
                        if (result.status == "false") {
                            toastr.error(result.message, 'Error');
                        } else
                        {
                            toastr.success(result.message, 'Success');
                            if($("#outing_list tbody tr td:first").hasClass("dataTables_empty"))
                            {
                                $("#outing_list tbody tr td:first").attr("colspan",6);
                            }
                            outing_list.fnDraw(false); // CAll DAtatable of outing list
                        }
                        //window.location.reload();
                    },
                    error: function (result) {
                        //console.log(result);
                    }
                });
                return false;
            }
        });
        /*End : Form Validation For Add Outing Location*/

        /*Begin : Form Validation For Edit Location*/
        $("#editouting_form").validate({
            rules: {
                outing_name: {required: true},
                outing_description: {required: true},
                address: {required: true},
                state: {required: true},
                city: {required: true},
                pincode: {
                    required: true,
                    //zipcode: true
                },
                outing_start_date: {required: true},
                outing_end_date: {required: true},
            },
            messages: {
                outing_name: {required: "Please Enter Outing Name."},
                outing_description: {required: "Please Enter Outing Description."},
                address: {required: "Please Enter Outing Address."},
                state: {required: "Please Select State/Province."},
                city: {required: "Please Enter City."},
                pincode: {
                    required: "Please Enter Zip/Postal Code",
                },
                outing_start_date: {required: "Please Enter Event Outing Start Date."},
                outing_end_date: {required: "Please Enter Event Outing End Date."},
            },
            submitHandler: function (form) {
                var event_id = $('#event_id').val();
                var data = $(form).serialize();
                var url = '<?= action('AdminmanageeventController@postUpdateoutinglocation') ?>';
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: "json",
                    success: function (result) {
                        $('#edit_outing_modal').modal('hide');
                        if (result.status == "false") {
                            toastr.error(result.message, 'Error');
                        } else
                        {
                            toastr.success(result.message, 'Success');
                            if($("#outing_list tbody tr td:first").hasClass("dataTables_empty"))
                            {
                                $("#outing_list tbody tr td:first").attr("colspan",6);
                            }
                            outing_list.fnDraw(false); // CAll DAtatable of outing list
                        }
                        //window.location.reload();
                    },
                    error: function (result) {
                        //console.log(result);
                    }
                });
                return false;
            }
        });
        /*End : Form Validation For Edit Location*/
    });


    $(document).ready(function () {
        $("#address,#edit_address,#add_outing_address,#edit_outing_address").geocomplete();
        /*Begin : To select content on focus*/
        $("input").focus(function () {
            $(this).select();
        });
        $("textarea").focus(function () {
            $(this).select();
        });
        /*End : To select content on focus*/

        /*Begin : Datepicker Of Outing Date*/
        var FromEndDate = '';
        var startDate = new Date();
        $('#outing_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 1);
            $('#outing_end_date').datepicker('setStartDate', startDate);
        });

        $('#outing_end_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#outing_start_date').datepicker('setEndDate', FromEndDate);
        });

        $('#edit_outing_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 1);
            $('#edit_outing_end_date').datepicker('setStartDate', startDate);
        });

        $('#edit_outing_end_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#edit_outing_start_date').datepicker('setEndDate', FromEndDate);
        });
        /*End : Datepicker Of OUuting Date*/

        /*End : To select content on focus*/
        $('#address').keyup(function () {
            if ($("#address").val() != "")
            {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: '<?= action('AdminmanageeventController@postZipcode') ?>',
                    data: {"address": $("#address").val()},
                    //dataType: "json",
                    success: function (result) {

                        if (result != "false")
                        {
                            $("#pincode").val(result);
                        } else
                        {
                            $("#pincode").val("");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //    console.log('ERRORS: ' + textStatus);
                    },
                });
            }
        });
        //End : Get Zip code

        /*Begin : For Event location using google input*/
        var autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('event_location')), {types: ['geocode']}
        );
        /*End : For Event location using google input*/
        /*Begin : Datepicker*/
        var FromEndDate = '';
        var startDate = new Date();
        $('#transportation_deadline').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true});

        $('#early_reg_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#reg_start_date').datepicker('setStartDate', startDate);
        });

        $('#reg_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#early_reg_start_date').datepicker('setEndDate', FromEndDate);
            $('#reg_end_date').datepicker('setStartDate', FromEndDate);
        });
        $('#reg_end_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#reg_start_date').datepicker('setEndDate', FromEndDate);
            $('#transportation_deadline').datepicker('setStartDate', FromEndDate);
        });


        $('#event_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#event_end_date').datepicker('setStartDate', startDate);
            $('#stay_start_date').datepicker('setStartDate', startDate);
        });

        $('#event_end_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#event_start_date').datepicker('setEndDate', FromEndDate);
            $('#reg_end_date').datepicker('setEndDate', FromEndDate);
            $('#reg_start_date').datepicker('setEndDate', FromEndDate);
            $('#transportation_deadline').datepicker('setEndDate', FromEndDate);
            $('#stay_end_date').datepicker('setEndDate', FromEndDate);
            $('#early_reg_start_date').datepicker('setEndDate', FromEndDate);
            $('#stay_start_date2,#stay_start_date').datepicker('setEndDate', FromEndDate);
        });


        $('#event_start_date').datepicker('setEndDate', $("#event_end_date").val());
        $('#reg_end_date').datepicker('setEndDate', $("#event_end_date").val());
        $('#reg_start_date').datepicker('setEndDate', $("#event_end_date").val());
        $('#transportation_deadline').datepicker('setEndDate', $("#event_end_date").val());
        $('#stay_end_date').datepicker('setEndDate', $("#event_end_date").val());
        $('#early_reg_start_date').datepicker('setEndDate', $("#event_end_date").val());
        $('#stay_start_date2,#stay_end_date,#stay_start_date,#stay_start_date1').datepicker('setEndDate', $("#event_end_date").val());

        $('#reg_start_date').datepicker('setStartDate', $("#early_reg_start_date").val());
        $('#reg_end_date').datepicker('setEndDate', $("#event_end_date").val());
        $('#reg_end_date').datepicker('setStartDate', $("#reg_start_date").val());
        $('#transportation_deadline').datepicker('setEndDate', $("#reg_end_date").val());
        $('#event_end_date,#stay_start_date1,#stay_start_date2').datepicker('setStartDate', $("#event_start_date").val());
        $('#stay_start_date').datepicker('setStartDate', $("#event_start_date").val());

        $('#stay_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 1);
            $('#stay_end_date').datepicker('setStartDate', startDate);
        });

        $('#stay_end_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#stay_start_date').datepicker('setEndDate', FromEndDate);
            $('#stay_start_date1').datepicker('setStartDate', startDate);
            $('#stay_start_date2').datepicker('setStartDate', startDate);
        });
        $('#stay_start_date1').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 3);
            $('#stay_start_date2').datepicker('setStartDate', startDate);
        });
        $('#stay_start_date2').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#stay_start_date1').datepicker('setEndDate', FromEndDate);
        });
        /*End : Datepicker*/
    });
    /*Begin : Input Validation For Zipcode*/
    $("#event_zip").keypress(function (event) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (regex.test(str)) {
            return true;
        } else
        {
            if ($("event_zip").val() != "")
            {
                if (event.which == 13)
                {
                    return true;
                }
            }
        }
        event.preventDefault();
        return false;
    });
    /*End : Input Validation For Zipcode*/
    /*Begin : Display Stayoover Inofrmation as per Selected Option*/
    $("#stayover_info").change(function () {
        var selected_stayeover = $(this).val();

        if (selected_stayeover == 1)//If "No Stayover"option is selected
        {
            $(".stayoverstartdate,.stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        } else if (selected_stayeover == 2)
        {//if "One 1-night Stayover" selected
            $(".stayoverstartdate").removeClass('hide');
            $(".stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        } else if (selected_stayeover == 3)
        {//if "One 2-night Stayover" selcted
            $(".stayoverstartdate").removeClass('hide');
            $(".stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        } else if (selected_stayeover == 4)
        { // Two 1-night Stayover
            $(".stayoverstartdate,.stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").removeClass('hide');
        } else
        {
            $(".stayoverstartdate,.stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        }
    });
    /*End:Display Stayover Inofrmation AS per Selecrted Option*/
    $("#event_level,#eligible_wings").change(function () {
        $event_level = $("#event_level").val();
        //To Check stayover option checkbox is check
        if ($("#show_stayinfo").is(':checked'))
            var staycheck = 1;  // checked
        else
            var staycheck = 0;  // unchecked
        //If event level is cneter then filter as per wing
        $(".stayoverinfo").addClass('hide');


        if ($('#event_level').val() != "")
        {
            if ($("#event_level").val() == 3)
            {   //Display Stayover info
                if ($("#event_type").val() != "")
                {
                    if (staycheck == 1)
                    {
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                        $(".stayoverlisting").removeClass('hide');
                        if($("#hotel_list tbody tr td:first").hasClass("dataTables_empty"))
                        {
                            $("#hotel_list tbody tr td:first").attr("colspan",6);
                        }
                        hotel_list.fnDraw(false); //Call Stayover Datatables
                    }
                    else
                    {
                        $(".stayoverlisting").addClass('hide');
                    }
                } else
                {
                    $(".stayoverinfo").addClass('hide');
                    $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
//                    $("#show_stayinfo").prop('checked', false);
                    $("#show_stayinfo").iCheck('uncheck');
                    $("#show_stayinfo").attr("disabled", false);
                }
                //If event level is center,check wing is selected or not
                if (($('#eligible_wings').val()) == null)
                {
                    var selectedwings = 0;
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                    $('#event_type,#event_level').prop('selectedIndex', 0);
//                alert('Please Select Eligible Wing.');
                    toastr.info("Please Select Eligible Wing.", 'Info');
                    return false;
                } else
                {
                    var selectedwings = [];
                    $.each($("#eligible_wings option:selected"), function () {
                        selectedwings.push($(this).val());
                    });
                    //For Center Level Display Payemnt Options
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                }
            } else
            {
                if (staycheck == 1)
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                    $(".stayoverlisting").removeClass('hide');
                    if($("#hotel_list tbody tr td:first").hasClass("dataTables_empty"))
                    {
                        $("#hotel_list tbody tr td:first").attr("colspan",6);
                    }
                    hotel_list.fnDraw(false); //Call Stayover Datatables
                }
                else
                {
                    $(".stayoverlisting").addClass('hide');
                }

                if ($("#event_level").val() == 1)
                { // For Regional Event
                    $(".payment_region_outer").removeClass('hide');
                    $(".payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() == 2)
                { //For North American Level
                    $(".payment_country_outer").removeClass('hide');
                    $(".payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() > 3)
                { //For Other Newly Added Level
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                } else
                {
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                }

            }
        } else
        {
            $(".stayoverinfo").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            $(".payment_country_outer,.payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
            $(".outinglisting,.stayoverlisting").addClass('hide');
        }


        if ($("#event_level").val() >= 3)
        {
            /*Begin : Get Dynamic Center as per Region*/
            $("#eligible_regions").change(function () {
                $region_id = $(this).val();
                $("#event_center").prop("disabled", true);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: '<?= action('AdminuserController@postRegion_based_center_ajax') ?>',
                    data: {"region_id": $region_id},
                    dataType: "json",
                    success: function (result) {
                        $("#event_center").prop("disabled", false);
                        html = "<option value=''>Select Event Center</option>";
                        for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
                        }
                        $("#event_center").html(html);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //    console.log('ERRORS: ' + textStatus);
                    },
                });
            });
            /*End : Get Dynamic Center as per Region*/
        }

        $("#event_type").prop("disabled", true);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetcheventtypelevel') ?>',
            data: {"event_level": $event_level, 'wing_id': selectedwings},
            dataType: "json",
            success: function (result) {
                $("#event_type").prop("disabled", false);
                html = "<option value=''>Select Event Type</option>";
                for (i = 0; i < result.event_type.length; i++) {
                    //Get Child Array ,for nested event type
                    if (result.event_type[i].childs.length > 0)
                    {
                        html += "<optgroup label='" + result.event_type[i].event_type_name + "'>";
                        for (j = 0; j < result.event_type[i].childs.length; j++)
                        {
                            html += "<option value='" + result.event_type[i].childs[j].id + "' data-show_stayinfo='" + result.event_type[i].childs[j].show_stayinfo + "' data-show_outing='" + result.event_type[i].childs[j].show_outing + "' data-approval='" + result.event_type[i].childs[j].event_approval + "'>" + result.event_type[i].childs[j].event_type_name + "</option>";
                        }
                        html += "</option>";
                    } else
                    {
                        html += "<option value='" + result.event_type[i].id + "' data-show_stayinfo='" + result.event_type[i].show_stayinfo + "' data-show_outing='" + result.event_type[i].show_outing + "' data-approval='" + result.event_type[i].event_approval + "'>" + result.event_type[i].event_type_name + "</option>";
                    }
                }
                $("#event_type").html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    });
    /*End : Fetch Event Type as per event Level*/

    /*Begin : Assign Check box value for stayover and outing info on event type*/
    $("#event_type").change(function () {
        var stayoverinfo = $('#event_type option:selected').attr('data-show_stayinfo');
        var showoutinginfo = $('#event_type option:selected').attr('data-show_outing');
        //Aasign to check , event need to approve or not
        $("#hidden_aprroval_required").attr('value', $('#event_type option:selected').attr('data-approval'));


        if (stayoverinfo == 1)
        {
//        $("#show_stayinfo").prop('checked', true);
            $("#show_stayinfo").removeClass("disabled");
            $("#show_stayinfo").iCheck('check');
            $(".stayoverlisting").removeClass('hide');
            $("#show_stayinfo").attr("disabled", true);
            $("#hidden_is_stayover").attr('value', stayoverinfo);
            $(".nostay").addClass('hide');

            if ($("#event_level").val() != "")
            {
                if ($("#event_level").val() == 3)
                {   //Display Stayover info
                    {
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    }
                } else
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                }
            } else
            {
                $(".stayoverinfo").addClass('hide');
                $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
            }
        } else
        {
//        $("#show_stayinfo").prop('checked', false);
            $("#show_stayinfo").removeClass("disabled");
            $("#show_stayinfo").iCheck('uncheck');
            $(".stayoverlisting").addClass('hide');
            $("#show_stayinfo").attr("disabled", false);
            $("#hidden_is_stayover").attr('value', stayoverinfo);
            $(".nostay").removeClass('hide');

            $(".stayoverinfo").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        }
        if (showoutinginfo == 1)
        {
//        $("#show_outing").prop('checked', true);
            $("#show_outing").removeClass("disabled");
            $("#show_outing").iCheck('check');
            $(".outinglisting").removeClass('hide');
            $("#show_outing").attr("disabled", true);
            $("#hidden_is_outing").attr('value', showoutinginfo);
            if($("#outing_list tbody tr td:first").hasClass("dataTables_empty"))
            {
                $("#outing_list tbody tr td:first").attr("colspan",6);
            }
            outing_list.fnDraw(false); //Call outing_list Datatables
        } else
        {
//        $("#show_outing").prop('checked', false);
            $("#show_outing").removeClass("disabled");
            $("#show_outing").iCheck('uncheck');
            $(".outinglisting").addClass('hide');
            $("#show_outing").attr("disabled", false);
            $("#hidden_is_outing").attr('value', showoutinginfo);
        }
    });
    /*End : Assign Check box value for stayover and outing info on event type*/
    /*Begin : Change Status*/
//    $("#event_approve_status").change(function () {
//
//        var myval = $(this).val();
//        $("#event_approve_status_change").attr('value', myval);
//    });
    /*End Change Status*/
    /*Begin : Show Stayover inof as per selected option of database*/
    var eventlevel = "{{ $event->event_level or 0 }}";
    var showstayinfo = "{{ $event->show_stayinfo or 0}}";
    var stayover_option_type = "{{ $event->stayover_option_type or 0}}";
    if (showstayinfo == 1)
    {
        if (eventlevel == 1 || eventlevel == 2)
        {
            $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
        } else if (eventlevel == 3)
        {
            $(".stayoverinfo").removeClass('hide');
            if (stayover_option_type == 2 || stayover_option_type == 3)
            {
                $(".stayoverstartdate").removeClass('hide');
            } else if (stayover_option_type == 4)
            {
                $(".stayoverstartdate1,.stayoverstartdate2").removeClass('hide');
            }
        }
//        $(".stayoverlisting").removeClass('hide');
//        hotel_list.fnDraw(false); //Call Stayover Datatables
    } else
    {
        $(".stayoverinfo").addClass('hide');
        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
        $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        //$(".stayoverlisting").addClass('hide');
    }


    /*End:Stayover Info*/


    /*Begin :Display Transport Deadline Date*/
    var is_trans_deadline = "{{ $event->transportation_avail or 0}}";

    if (is_trans_deadline == 1)
    {
        $(".trans_deadline,.transportation_required_option").removeClass('hide');
    } else
    {
        $(".trans_deadline,.transportation_required_option").addClass('hide');
    }
    /*End :Display Transport Deadline Date*/


    /*BEgin : Display Stayover info*/
    var selected_stayeover = "{{ $event->stayover_option_type or 0}}";
    var show_stayinfo = "{{ $event->show_stayinfo or 0}}";
    var eventlevel = "{{ $event->event_level or 0 }}";
    if (show_stayinfo == 1)
    {
        if (selected_stayeover == 1 && eventlevel == 3)//If "No Stayover"option is selected
        {
            $(".stayoverstartdate,.stayoverenddate").addClass('hide');
        } else if (selected_stayeover == 2 && eventlevel == 3)
        {//if "One 1-night Stayover" selected
            $(".stayoverstartdate").removeClass('hide');
            $(".stayoverenddate").addClass('hide');
        } else if (selected_stayeover == 3 && eventlevel == 3)
        {//if "One 2-night Stayover" selcted
            $(".stayoverstartdate").removeClass('hide');
            $(".stayoverenddate").addClass('hide');
        } else if (selected_stayeover == 0 && eventlevel == 3)
        {
            $(".stayoverstartdate,.stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").removeClass('hide');
        }
    } else
    {
        $(".stayoverinfo").addClass('hide');
        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
        $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
    }
    /*Begin : Display Payement option as per event level*/
    if (eventlevel == 0)
    { //If event level not found
        $(".payment_country_outer,.payment_region_outer,.payment_center_outer").addClass('hide');
    } else if (eventlevel == 1)
    { // For Regional Event Level
        $(".payment_region_outer").removeClass('hide');
        $(".payment_country_outer,.payment_center_outer").addClass('hide');
    } else if (eventlevel == 2)
    { // For North American Event Level
        $(".payment_country_outer").removeClass('hide');
        $(".payment_region_outer,.payment_center_outer").addClass('hide');
    } else if (eventlevel == 3)
    { // For Center Event Level
        $(".payment_center_outer").removeClass('hide');
        $(".payment_region_outer,.payment_country_outer").addClass('hide');
    } else if (eventlevel > 3)
    { // For ONewlu Created Event Level
        $(".payment_center_outer").removeClass('hide');
        $(".payment_region_outer,.payment_country_outer").addClass('hide');
    } else
    {
        $(".payment_country_outer,.payment_region_outer,.payment_center_outer").addClass('hide');
    }
    /*Begin : Display Payement option as per event level*/
    /*End : Display Stayover info*/

    /*Begin: Disable Checkbox*/
    var stayoverinfo = $('#event_type option:selected').attr('data-show_stayinfo');
    var showoutinginfo = $('#event_type option:selected').attr('data-show_outing');
    var dbstayoverinfo = "{{ $event->show_stayinfo or 0 }}";
    var dbshowoutinginfo = "{{ $event->show_outing or 0 }}";

    if (stayoverinfo == 1)
    {
//    $("#show_stayinfo").prop('checked', true);
        $("#show_stayinfo").iCheck('check');
        $(".stayoverlisting").removeClass('hide');
        $("#show_stayinfo").attr("disabled", true);

        $("#hidden_is_stayover").attr('value', stayoverinfo);
        $(".nostay").addClass('hide');

    } else if (dbstayoverinfo == 1)
    {
//    $("#show_stayinfo").prop('checked', true);
        $("#show_stayinfo").iCheck('check');
        $(".stayoverlisting").removeClass('hide');
    } else
    {
//    $("#show_stayinfo").prop('checked', false);
        $("#show_stayinfo").iCheck('uncheck');
        $(".stayoverlisting").addClass('hide');
        $("#show_stayinfo").attr("disabled", false);
        $("#hidden_is_stayover").attr('value', stayoverinfo);
        $(".nostay").removeClass('hide');
    }
    if (showoutinginfo == 1)
    {
//    $("#show_outing").prop('checked', true);
        $("#show_outing").iCheck('check');
        $("#show_outing").attr("disabled", true);
        $(".outinglisting").removeClass('hide');
        if($("#outing_list tbody tr td:first").hasClass("dataTables_empty"))
            {
                $("#outing_list tbody tr td:first").attr("colspan",6);
            }
        outing_list.fnDraw(false); //Call outing_list Datatables

        $("#hidden_is_outing").attr('value', showoutinginfo);
    } else if (dbshowoutinginfo == 1)
    {
//    $("#show_outing").prop('checked', true);
        $("#show_outing").iCheck('check');
        $(".outinglisting").removeClass('hide');
        if($("#outing_list tbody tr td:first").hasClass("dataTables_empty"))
            {
                $("#outing_list tbody tr td:first").attr("colspan",6);
            }
        outing_list.fnDraw(false); //Call outing_list Datatables
    } else
    {
//    $("#show_outing").prop('checked', false);
        $("#show_outing").iCheck('uncheck');
        $(".outinglisting").addClass('hide');
        $("#show_outing").attr("disabled", false);
        $("#hidden_is_outing").attr('value', showoutinginfo);
    }
    /*End: Disable Checkbox*/

    /*Begin : Display / Hide Transportation Deadline Date Field*/
    $("#transportation_avail").change(function () {
        myvalue = $(this).val();
        if (myvalue == 1)
        {
            $(".trans_deadline,.transportation_required_option").removeClass('hide');
        } else
        {
            $(".trans_deadline,.transportation_required_option").addClass('hide');
        }
    });
    /*End : Display / Hide Transportation Deadline Date Field*/
    /*Begin : Display / Hide  Styover Date and Dropdown option if stayover option is checked*/

//To Check stayover option checkbox is check
    //$('#show_stayinfo').change(function () {
    $(document).on('ifChecked ifUnchecked', '#show_stayinfo', function (event) {
//        if ($(this).prop('checked'))
        if (event.type == 'ifChecked')
        {
            if ($("#event_level").val() != "")
            {
                if ($("#event_level").val() == 3)
                {   //Display Stayover info
                    {
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    }
                } else
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                }
            } else
            {
                $(".stayoverinfo").addClass('hide');
                $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
            }
            $(".stayoverlisting").removeClass('hide');
            if($("#hotel_list tbody tr td:first").hasClass("dataTables_empty"))
            {
                $("#hotel_list tbody tr td:first").attr("colspan",6);
            }
            hotel_list.fnDraw(false);

        } else
        {
            $(".stayoverinfo").addClass('hide');
            $(".stayoverlisting").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
            if($("#hotel_list tbody tr td:first").hasClass("dataTables_empty"))
            {
                $("#hotel_list tbody tr td:first").attr("colspan",6);
            }
        }
    });

    /*End : Display / Hide  Styover Date and Dropdown option if stayover option is checked*/
    
    /*Begin : Display / Hide  Outing  table  if Outing option is checked*/
    $(document).on('ifChecked ifUnchecked', '#show_outing', function (event) {
//        if ($(this).prop('checked'))
        if (event.type == 'ifChecked')
        {
            $(".outinglisting").removeClass('hide');
            if($("#outing_list tbody tr td:first").hasClass("dataTables_empty"))
            {
                $("#outing_list tbody tr td:first").attr("colspan",6);
            }
            outing_list.fnDraw(false);

        } else
        {
            $(".outinglisting").addClass('hide');
            if($("#outing_list tbody tr td:first").hasClass("dataTables_empty"))
            {
                $("#outing_list tbody tr td:first").attr("colspan",6);
            }
        }
    });
    
    /*End : Display / Hide  Outing  table  if Outing option is checked*/

    /*Begin : To hide Event Region Option If Event Level is North America*/
    $("#event_level").change(function () {
        var event_level = $(this).val();

        if (event_level == 2 || event_level == "")
        {
            $(".region_center_div").addClass('hide');
        } else
        {
            $(".region_center_div").removeClass('hide');
        }
    });
    /*End : To hide Event Region Option If Event Level is North America*/


    /*Begin : To Change REgistration form Type As Per Selection Of Registration Fee Option*/
    $("#registration_fee").change(function () {
        var registration_fee = $(this).val();

        if (registration_fee == "Yes")
        {
            html = "<option value=''>Select Form Type</option><option value='2'>Bal - Balika Form</option><option value='3'>Default Form</option><option value='1'>Kishore - Kishori Form</option><option value='4'>RAM Form</option>";
            $("#form_type").html(html);
        } else
        {
            html = "<option value='5'>No Form</option>";
            $("#form_type").html(html);
        }
    });
    /*End : To Change REgistration form Type As Per Selection Of Registration Fee Option*/


    /*Begin : To Fetch Wing As per Selection Of Multiple Wing checkbox selection*/
    $(document).on('ifChecked ifUnchecked', '#multi_wing_event', function (event) {

        if (event.type == 'ifChecked')
        {
            var is_multiwing_checked = "yes";
            $("#hidden_check_multi_wing").val(1);
        } else
        {
            var is_multiwing_checked = "no";
            $("#hidden_check_multi_wing").val(0);
        }
        var current_wing = $('#eligible_wings option').map(function () {
            return $(this).val();
        }).get();
        if (current_wing.length > 0)
        {
            current_wing = current_wing;
        } else
        {
            current_wing = 0;
        }

        var event_wings = "<?= implode(',', $event->eligible_wings); ?>";
        event_wings = event_wings.split(",");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetchmultiwing') ?>',
            data: {"hidden_check_multi_gender": $("#hidden_check_multi_gender").val(), "hidden_check_multi_wing": $("#hidden_check_multi_wing").val()},
            dataType: "json",
            success: function (result) {
                $("#eligible_wings").prop("disabled", false);
                html = "";
                for (i = 0; i < result.wings.length; i++) {
                    html += "<option value='" + result.wings[i].id + "'>" + result.wings[i].wingName + "</option>";
                }
                $("#eligible_wings").html(html);
                $("#eligible_wings").val(event_wings);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });


        $event_level = $("#event_level").val();
        //To Check stayover option checkbox is check
        if ($("#show_stayinfo").is(':checked'))
            var staycheck = 1;  // checked
        else
            var staycheck = 0;  // unchecked
        //If event level is cneter then filter as per wing
        $(".stayoverinfo").addClass('hide');


        if ($('#event_level').val() != "")
        {
            if ($("#event_level").val() == 3)
            {   //Display Stayover info
                if ($("#event_type").val() != "")
                {
                    if (staycheck == 1)
                    {
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                        $(".stayoverlisting").removeClass('hide');
                    }
                } else
                {
                    $(".stayoverinfo").addClass('hide');
                    $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
//                    $("#show_stayinfo").prop('checked', false);
                    $("#show_stayinfo").iCheck('uncheck');
                    $(".stayoverlisting").addClass('hide');
                    $("#show_stayinfo").attr("disabled", false);
                }
                //If event level is center,check wing is selected or not
                if (($('#eligible_wings').val()) == null)
                {
                    var selectedwings = 0;
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                    $('#event_type,#event_level').prop('selectedIndex', 0);
//                alert('Please Select Eligible Wing.');
                    toastr.info("Please Select Eligible Wing.", 'Info');
                    return false;
                } else
                {
                    var selectedwings = [];
                    $.each($("#eligible_wings option:selected"), function () {
                        selectedwings.push($(this).val());
                    });
                    //For Center Level Display Payemnt Options
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                }
            } else
            {
                if (staycheck == 1)
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                }

                if ($("#event_level").val() == 1)
                { // For Regional Event
                    $(".payment_region_outer").removeClass('hide');
                    $(".payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() == 2)
                { //For North American Level
                    $(".payment_country_outer").removeClass('hide');
                    $(".payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() > 3)
                { //For Other Newly Added Level
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                } else
                {
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                }

            }
        } else
        {
            $(".stayoverinfo").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            $(".payment_country_outer,.payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
        }


        if ($("#event_level").val() >= 3)
        {
            /*Begin : Get Dynamic Center as per Region*/
            $("#eligible_regions").change(function () {
                $region_id = $(this).val();
                $("#event_center").prop("disabled", true);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: '<?= action('AdminuserController@postRegion_based_center_ajax') ?>',
                    data: {"region_id": $region_id},
                    dataType: "json",
                    success: function (result) {
                        $("#event_center").prop("disabled", false);
                        html = "<option value=''>Select Event Center</option>";
                        for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
                        }
                        $("#event_center").html(html);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //    console.log('ERRORS: ' + textStatus);
                    },
                });
            });
            /*End : Get Dynamic Center as per Region*/
        }

        $("#event_type").prop("disabled", true);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetcheventtypelevel') ?>',
            data: {"event_level": $event_level, 'wing_id': selectedwings},
            dataType: "json",
            success: function (result) {
                $("#event_type").prop("disabled", false);
                html = "<option value=''>Select Event Type</option>";
                for (i = 0; i < result.event_type.length; i++) {
                    //Get Child Array ,for nested event type
                    if (result.event_type[i].childs.length > 0)
                    {
                        html += "<optgroup label='" + result.event_type[i].event_type_name + "'>";
                        for (j = 0; j < result.event_type[i].childs.length; j++)
                        {
                            html += "<option value='" + result.event_type[i].childs[j].id + "' data-show_stayinfo='" + result.event_type[i].childs[j].show_stayinfo + "' data-show_outing='" + result.event_type[i].childs[j].show_outing + "' data-approval='" + result.event_type[i].childs[j].event_approval + "'>" + result.event_type[i].childs[j].event_type_name + "</option>";
                        }
                        html += "</option>";
                    } else
                    {
                        html += "<option value='" + result.event_type[i].id + "' data-show_stayinfo='" + result.event_type[i].show_stayinfo + "' data-show_outing='" + result.event_type[i].show_outing + "' data-approval='" + result.event_type[i].event_approval + "'>" + result.event_type[i].event_type_name + "</option>";
                    }
                }
                $("#event_type").html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    });
    /*End : To Fetch Wing As per Selection Of Multiple Wing checkbox selection*/


    /*Begin : To Fetch Wing As per Selection Of Multiple Gender checkbox selection*/
    $(document).on('ifChecked ifUnchecked', '#multi_gender_event', function (event) {

        if (event.type == 'ifChecked')
        {
            var is_multigender_checked = "yes";
            $("#hidden_check_multi_gender").val(1);
        } else
        {
            var is_multigender_checked = "no";
            $("#hidden_check_multi_gender").val(0);
        }

        var current_wing = $('#eligible_wings option').map(function () {
            return $(this).val();
        }).get();
        if (current_wing.length > 0)
        {
            current_wing = current_wing;
        } else
        {
            current_wing = 0;
        }
        var event_wings = "<?= implode(',', $event->eligible_wings); ?>";
        event_wings = event_wings.split(",");
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetchmultiwing') ?>',
            data: {"hidden_check_multi_gender": $("#hidden_check_multi_gender").val(), "hidden_check_multi_wing": $("#hidden_check_multi_wing").val()},
            dataType: "json",
            success: function (result) {
                $("#eligible_wings").prop("disabled", false);
                html = "";
                for (i = 0; i < result.wings.length; i++) {
                    html += "<option value='" + result.wings[i].id + "'>" + result.wings[i].wingName + "</option>";
                }
                $("#eligible_wings").html(html);
                $("#eligible_wings").val(event_wings);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });

        
        $event_level = $("#event_level").val();
        //To Check stayover option checkbox is check
        if ($("#show_stayinfo").is(':checked'))
            var staycheck = 1;  // checked
        else
            var staycheck = 0;  // unchecked
        //If event level is cneter then filter as per wing
        $(".stayoverinfo").addClass('hide');


        if ($('#event_level').val() != "")
        {
            if ($("#event_level").val() == 3)
            {   //Display Stayover info
                if ($("#event_type").val() != "")
                {
                    if (staycheck == 1)
                    {
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                        $(".stayoverlisting").removeClass('hide');
                    }
                } else
                {
                    $(".stayoverinfo").addClass('hide');
                    $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
//                    $("#show_stayinfo").prop('checked', false);
                    $("#show_stayinfo").iCheck('uncheck');
                    $("#show_stayinfo").attr("disabled", false);
                    $(".stayoverlisting").addClass('hide');
                }
                //If event level is center,check wing is selected or not
                if (($('#eligible_wings').val()) == null)
                {
                    var selectedwings = 0;
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                    $('#event_type,#event_level').prop('selectedIndex', 0);
//                alert('Please Select Eligible Wing.');
                    toastr.info("Please Select Eligible Wing.", 'Info');
                    return false;
                } else
                {
                    var selectedwings = [];
                    $.each($("#eligible_wings option:selected"), function () {
                        selectedwings.push($(this).val());
                    });
                    //For Center Level Display Payemnt Options
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                }
            } else
            {
                if (staycheck == 1)
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                }

                if ($("#event_level").val() == 1)
                { // For Regional Event
                    $(".payment_region_outer").removeClass('hide');
                    $(".payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() == 2)
                { //For North American Level
                    $(".payment_country_outer").removeClass('hide');
                    $(".payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() > 3)
                { //For Other Newly Added Level
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                } else
                {
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                }

            }
        } else
        {
            $(".stayoverinfo").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            $(".payment_country_outer,.payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
        }


        if ($("#event_level").val() >= 3)
        {
            /*Begin : Get Dynamic Center as per Region*/
            $("#eligible_regions").change(function () {
                $region_id = $(this).val();
                $("#event_center").prop("disabled", true);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: '<?= action('AdminuserController@postRegion_based_center_ajax') ?>',
                    data: {"region_id": $region_id},
                    dataType: "json",
                    success: function (result) {
                        $("#event_center").prop("disabled", false);
                        html = "<option value=''>Select Event Center</option>";
                        for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
                        }
                        $("#event_center").html(html);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //    console.log('ERRORS: ' + textStatus);
                    },
                });
            });
            /*End : Get Dynamic Center as per Region*/
        }

        $("#event_type").prop("disabled", true);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetcheventtypelevel') ?>',
            data: {"event_level": $event_level, 'wing_id': selectedwings},
            dataType: "json",
            success: function (result) {
                $("#event_type").prop("disabled", false);
                html = "<option value=''>Select Event Type</option>";
                for (i = 0; i < result.event_type.length; i++) {
                    //Get Child Array ,for nested event type
                    if (result.event_type[i].childs.length > 0)
                    {
                        html += "<optgroup label='" + result.event_type[i].event_type_name + "'>";
                        for (j = 0; j < result.event_type[i].childs.length; j++)
                        {
                            html += "<option value='" + result.event_type[i].childs[j].id + "' data-show_stayinfo='" + result.event_type[i].childs[j].show_stayinfo + "' data-show_outing='" + result.event_type[i].childs[j].show_outing + "' data-approval='" + result.event_type[i].childs[j].event_approval + "'>" + result.event_type[i].childs[j].event_type_name + "</option>";
                        }
                        html += "</option>";
                    } else
                    {
                        html += "<option value='" + result.event_type[i].id + "' data-show_stayinfo='" + result.event_type[i].show_stayinfo + "' data-show_outing='" + result.event_type[i].show_outing + "' data-approval='" + result.event_type[i].event_approval + "'>" + result.event_type[i].event_type_name + "</option>";
                    }
                }
                $("#event_type").html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    });
    /*End : To Fetch Wing As per Selection Of Multiple Gender checkbox selection*/

    /*Begin : Load Wing On Page Load As per Multi Wing / Gender Selection*/
    var event_wings = "<?= implode(',', $event->eligible_wings); ?>";
    event_wings = event_wings.split(",");
    //console.log(event_wings);
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
        },
        type: 'POST',
        url: '<?= action('AdminmanageeventController@postFetchmultiwing') ?>',
        data: {"hidden_check_multi_gender": $("#hidden_check_multi_gender").val(), "hidden_check_multi_wing": $("#hidden_check_multi_wing").val()},
        dataType: "json",
        success: function (result) {
            $("#eligible_wings").prop("disabled", false);
            html = "";
            for (i = 0; i < result.wings.length; i++) {
                html += "<option value='" + result.wings[i].id + "'>" + result.wings[i].wingName + "</option>";
            }
            $("#eligible_wings").html(html);
            $("#eligible_wings").val(event_wings);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //    console.log('ERRORS: ' + textStatus);
        },
    });

    /*End : Load Wing On Page Load As per Multi Wing / Gender Selection*/


    /*Reset Form On Close Modal for stayover and outing module*/
    $(".closemodal_reset").click(function () {

        $("#add_stayover_modal,#edit_stayover_modal").hide();
        $("#addhotel_form,#edithotel_form").trigger('reset');
        
    });
    $('#add_stayover_modal').on('hidden.bs.modal', function () {
            $("#addhotel_form").validate().resetForm();
        });
        
        $('#add_outing_modal').on('hidden.bs.modal', function () {
            $("#addouting_form").validate().resetForm();
        });

    $('#add_stayover_modal').on('shown.bs.modal', function (e) {
        $("#addhotel_form").trigger('reset');
        
        // do something...
    });
    
    $('#add_outing_modal').on('shown.bs.modal', function (e) {
        $("#addouting_form").trigger('reset');
        
        // do something...
    });


    /*Begin : For Delete Record Show Modal and manage action*/
    $(document).on('click', '.deleterecord', function (e) {
        $("#modal_stayver_deleterecord").modal('show');
        var passpath = $(this).attr('data-pathaccess');
        var pathaccesstype = $(this).attr('data-pathaccesstype');
        if (passpath != "")
        {
            $("#redirectpath").attr('value', passpath);
            $("#pathaccesstype").attr('value', pathaccesstype);
        }
    });

    $(document).on('click', '.confirmdelete_statover', function (e) {
        var redirectpasspath = $("#redirectpath").val();
        var pathaccesstype = $("#pathaccesstype").val();
        if (redirectpasspath != "")
        {
            if (pathaccesstype == "1" || pathaccesstype == 1)
            {
                //delete Stayover
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'GET',
                    url: '<?= action('AdminmanageeventController@getDeletestayoverlocation') ?>',
                    data: {id: redirectpasspath},
                    dataType: "json",
                    success: function (result) {
                        if (result.status == 'false') {
                            toastr.error(result.message, 'Error');
                        } else
                        {
                            toastr.success(result.message, 'Success');
                            if($("#hotel_list tbody tr td:first").hasClass("dataTables_empty"))
                            {
                                $("#hotel_list tbody tr td:first").attr("colspan",6);
                            }
                            hotel_list.fnDraw(false); // CAll DAtatable of Stayover list
                        }
                    },
                    error: function (result) {
                        //console.log(result);
                    }
                });
            } else
            {
                //delete outing

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'GET',
                    url: '<?= action('AdminmanageeventController@getDeleteoutinglocation') ?>',
                    data: {id: redirectpasspath},
                    dataType: "json",
                    success: function (result) {
                        if (result.status == 'false') {
                            toastr.error(result.message, 'Error');
                        } else
                        {
                            toastr.success(result.message, 'Success');
                            if($("#outing_list tbody tr td:first").hasClass("dataTables_empty"))
                            {
                                $("#outing_list tbody tr td:first").attr("colspan",6);
                            }
                            outing_list.fnDraw(false); // CAll DAtatable of Stayover list
                        }
                    },
                    error: function (result) {
                        //console.log(result);
                    }
                });
            }

        } else
        {
            //setTimeout(function(){location.reload();},2000);
            toastr.error("<?= Config::get('constants.GENERAL_ERROR') ?>", 'Error');
        }
        $("#modal_stayver_deleterecord").modal('hide');
    });

    /*End: For Delete Record Show Modal and manage action*/

})(jQuery);

/*Begin : Function For Get STayover Info On Edit Action*/
function editstayoverinfo(id) {
    $("#address").geocomplete();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
        },
        type: 'GET',
        url: '<?= action('AdminmanageeventController@getEditstayoverlocation') ?>',
        data: {id: id},
        dataType: "json",
        success: function (result) {
            $("label.error").hide(); // this 2 lines are used for remove validation message initially.
            $(".error").removeClass("error");

            $('#stayoverlocation_id').val(result.id);
            $('#edit_stayover_location_type').val(result.stayover_location_type);
            $('#edit_stayover_name').val(result.stayover_name);
            $('#edit_address').val(result.address);
            $('#edit_city').val(result.city);
            $('#edit_state').val(result.state);
            $('#edit_pincode').val(result.pincode);
            $('#edit_rooms').val(result.rooms);
            $('#edit_dob_child').val(result.dob);
            $('#edit_stayover_modal').modal('show');
        },
        error: function (result) {
            //console.log(result);
        }
    });
}
/*End : Function For Get Stayover Info On Edit Action*/

/*Begin : Function Get Outing Info On Edit Action*/
function editoutinginfo(id) {
    $("#address").geocomplete();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
        },
        type: 'GET',
        url: '<?= action('AdminmanageeventController@getEditoutinglocation') ?>',
        data: {id: id},
        dataType: "json",
        success: function (result) {
            $("label.error").hide(); // this 2 lines are used for remove validation message initially.
            $(".error").removeClass("error");

            $('#outing_id').val(result.id);
            $('#edit_outing_name').val(result.outing_name);
            $('#edit_outing_description').val(result.outing_description);
            $('#edit_outing_address').val(result.address);
            $('#edit_outing_city').val(result.city);
            $('#edit_outing_state').val(result.state);
            $('#edit_outing_pincode').val(result.pincode);
            $('#edit_outing_start_date').val(result.outing_start_date);
            $('#edit_outing_end_date').val(result.outing_end_date);
            $('#edit_outing_modal').modal('show');
        },
        error: function (result) {
            //console.log(result);
        }
    });
}
/*End : Function For Outing Stayover Record*/



</script>
@endsection

@section('RenderBody')
<section class="content-header">
    <h1>Edit Event
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/admin/user/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="javascript:void(0);">Manage</a></li><li><a href="javascript:void(0);">Events</a></li>
        <li><a href="{{ url('/admin/manageevent/events') }}">View Events</a></li><li class="active">Edit Event</li>
    </ol>
</section>
<section class="content">
    {!! Form::open(array('url' => 'admin/manageevent/saveevent' , 'class' => 'form-horizontal','id' => 'editevent_form', 'files' => true)) !!}
    <!--Begin : Event Details Section-->
    <div class="box box-info">
        @if (count($errors) > 0)
        <?php if ($errors->first('event_level')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_level') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('event_type')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_type') !!} </label><br/>
        <?php } ?>    
        <?php if ($errors->first('event_title')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_title') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('event_location')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_location') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('event_zip')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_zip') !!} </label><br/>
        <?php } ?> 
        <?php if ($errors->first('event_start_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_start_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('event_end_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_end_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('early_reg_start_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('early_reg_start_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('reg_start_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('reg_start_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('reg_end_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('reg_end_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('maximum_attendence')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('maximum_attendence') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('currency_type')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('currency_type') !!} </label><br/>
        <?php } ?> 
        <?php if ($errors->first('transportation_deadline')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('transportation_deadline') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('form_type')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('form_type') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('payment_country')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('payment_country') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('host_region')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('host_region') !!} </label><br/>
        <?php } ?>
        @endif

        <!--Begin :Event Details Section-->
        <div class="box-header">
            <!--            <h3 class="box-title">Registration Information</h3>-->
            <div class="box-body padding-left-25 padding-right-25 form-horizontal">
                <div class="box box-default emergencyborder">
                    <div class="box-header with-border">
                        <h3 class="box-title">Event Details</h3>
                    </div>
                    <div class="box-body">
                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                    <div class="form-group">
                        <label for="multi_wing_event" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Multi Wing Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <input type="checkbox" name="multi_wing_event" id="multi_wing_event" value="1" <?php
                            if ($event->multi_wing_event == 1) {
                                echo "checked";
                            }
                            ?>>
                            <label for="multi_wing_event" class="control-label">This event includes multi wing</label>
                        </div>
                    </div>
                    @else
                    @permission('create.multi.wing.event.eventmanagement')
                    <div class="form-group">
                        <label for="multi_wing_event" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Multi Wing Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <input type="checkbox" name="multi_wing_event" id="multi_wing_event" value="1"  <?php
                            if ($event->multi_wing_event == 1) {
                                echo "checked";
                            }
                            ?>>
                            <label for="multi_wing_event" class="control-label">This event includes multi wing</label>
                        </div>
                    </div>
                    @endpermission
                    @endif

                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                    <div class="form-group">
                        <label for="multi_gender_event" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Multi Gender Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <input type="checkbox" name="multi_gender_event" id="multi_gender_event" value="1"  <?php
                            if ($event->multi_gender_event == 1) {
                                echo "checked";
                            }
                            ?>>
                            <label for="multi_gender_event" class="control-label">This event includes multi gender</label>
                        </div>
                    </div>
                    @else
                    @permission('edit.multi.gender.event.eventmanagement')
                    <div class="form-group">
                        <label for="multi_gender_event" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Multi Gender Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <input type="checkbox" name="multi_gender_event" id="multi_gender_event" value="1" <?php
                            if ($event->multi_gender_event == 1) {
                                echo "checked";
                            }
                            ?>>
                            <label for="multi_gender_event" class="control-label">This event includes multi gender</label>
                        </div>
                    </div>
                    @endpermission
                    @endif

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Eligible Wing<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <select class="form-control" name="eligible_wings[]" id="eligible_wings" multiple>
                                <?php
                                if (isset($wings) && !empty($wings)) {
                                    for ($i = 0; $i < count($wings); $i++) {
                                        ?>
                                        <option value="<?php echo $wings[$i]->id; ?>" <?php
                                        if (in_array($wings[$i]->id, $event->eligible_wings)) {
                                            echo " selected";
                                        }
                                        ?>><?php echo $wings[$i]->wingName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Level<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="event_level" id="event_level">
                                <option value="">Select Event Level</option>
                                <?php
                                if (isset($event_level) && !empty($event_level)) {
                                    for ($i = 0; $i < count($event_level); $i++) {
                                        ?>
                                        <option value="<?php echo $event_level[$i]->id; ?>" <?php
                                        if ($event_level[$i]->id == $event->event_level) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $event_level[$i]->event_level_name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Type<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select class="form-control" name="event_type" id="event_type">
                                <option value="">Select Event Type</option>
                                <?php
                                if (isset($event_type) && !empty($event_type)) {
                                    for ($i = 0; $i < count($event_type); $i++) {
                                        if (count($event_type[$i]->childs) > 0) {
                                            ?>
                                            <optgroup label="<?php echo $event_type[$i]->event_type_name; ?>">
                                                <?php for ($j = 0; $j < count($event_type[$i]->childs); $j++) { ?>
                                                    <option value="<?php echo $event_type[$i]->childs[$j]->id; ?>" data-show_stayinfo="<?php echo $event_type[$i]->childs[$j]->show_stayinfo; ?>" data-show_outing="<?php echo $event_type[$i]->childs[$j]->show_outing; ?>" data-approval="<?php echo $event_type[$i]->childs[$j]->event_approval; ?>" <?php
                                                    if ($event_type[$i]->childs[$j]->id == $event->event_type) {
                                                        echo "selected";
                                                    }
                                                    ?>><?php echo $event_type[$i]->childs[$j]->event_type_name; ?></option>
                                                        <?php } ?>
                                            </optgroup>
                                        <?php } else { ?>
                                            <option value="<?php echo $event_type[$i]->id; ?>" data-show_stayinfo="<?php echo $event_type[$i]->show_stayinfo; ?>" data-show_outing="<?php echo $event_type[$i]->show_outing; ?>" data-approval="<?php echo $event_type[$i]->event_approval; ?>" <?php
                                            if ($event_type[$i]->id == $event->event_type) {
                                                echo "selected";
                                            }
                                            ?>><?php echo $event_type[$i]->event_type_name; ?></option>     
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Title<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <input type="text" class="form-control"  value="{{ $event->event_title or ''}}" name="event_title" id="event_title" maxlength="50" placeholder="Event Title">
                            <input type="hidden"  value="{{ $event->event_title or ''}}" name="dbevent_title" id="dbevent_title" maxlength="50">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Description<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <textarea class="form-control" name="event_description" rows="5" id="event_description" placeholder="Event Description">{{ $event->event_description or ''}}</textarea>
                        </div>
                    </div>
                    <input type="hidden"  value="1" name="action_type" id="action_type" maxlength="50">
                    <input type="hidden"  value="0" name="hidden_is_stayover" id="hidden_is_stayover">
                    <input type="hidden"  value="0" name="hidden_is_outing" id="hidden_is_outing">
                    <input type="hidden"  value="{{ $event->event_approve_status or 0}}" name="hidden_aprroval_required" id="hidden_aprroval_required">
                    <input type="hidden"  value="<?= $event->created_by ?>" name="created_by" id="created_by">
                    <input type="hidden"  value="{{ $event->multi_wing_event or 0}}" name="hidden_check_multi_wing" id="hidden_check_multi_wing">
                    <input type="hidden"  value="{{ $event->multi_gender_event or 0}}" name="hidden_check_multi_gender" id="hidden_check_multi_gender">

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Location<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin location_margin">
                            <input type="text" class="form-control"  value="{{ $event->event_location or ''}}" name="event_location" id="event_location" placeholder="Event Location">
                        </div>

                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Zip/Postal Code<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $event->zipcode or ''}}" name="event_zip" id="event_zip" placeholder="Zip/Postal Code">
                        </div>
                    </div>

                    <div class="form-group region_center_div <?= $event->event_level == 2 ? 'hide' : ''; ?>">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Region<em class="mandatory-field">*</em></label>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="eligible_regions" id="eligible_regions">
                                <option value="">Select Event Region</option>
                                <?php
                                if (isset($regions) && !empty($regions)) {
                                    for ($i = 0; $i < count($regions); $i++) {
                                        ?>
                                        <option value="<?php echo $regions[$i]->id; ?>" <?php
                                        if (in_array($regions[$i]->id, ($event->eligible_regions))) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $regions[$i]->RegionName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>

                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label center_div <?= $event->event_level < 3 ? 'hide' : ''; ?>">Event Center<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin center_div <?= $event->event_level < 3 ? 'hide' : ''; ?>">
                            <select class="form-control" name="event_center" id="event_center">
                                <option value="">Select Event Center</option>
                                <?php
                                if (isset($centers) && !empty($centers)) {
                                    for ($i = 0; $i < count($centers); $i++) {
                                        ?>
                                        <option value="<?php echo $centers[$i]->id; ?>" <?php
                                        if ($centers[$i]->id == $event->event_center) {
                                            echo "selected";
                                        }
                                        ?>><?php echo $centers[$i]->CenterName; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Start Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <input type="text" class="form-control"  value="{{ $event->event_start_date or '' }}" name="event_start_date" id="event_start_date" maxlength="50" placeholder="Event Start Date">
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event End Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $event->event_end_date or '' }}" name="event_end_date" id="event_end_date" maxlength="50" placeholder="Event End Date">
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!--End : Event Details Section-->
    <!--Begin : Registration Details Section-->
    <div class="box box-info">
        <div class="box-header">
            <!--            <h3 class="box-title">Registration Information</h3>-->
            <div class="box-body padding-left-25 padding-right-25 form-horizontal">
                <div class="box box-default emergencyborder">
                    <div class="box-header with-border">
                        <h3 class="box-title">Registration Details</h3>
                    </div>
                    <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Early Registration Start Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <input type="text" class="form-control"  value="{{ $event->early_reg_start_date or '' }}" name="early_reg_start_date" id="early_reg_start_date" maxlength="50" placeholder="Early Registration Start Date">
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Registration Start Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $event->reg_start_date or '' }}" name="reg_start_date" id="reg_start_date" maxlength="50" placeholder="Registration Start Date">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Registration End Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <input type="text" class="form-control"  value="{{ $event->reg_end_date or '' }}" name="reg_end_date" id="reg_end_date" maxlength="50" placeholder="Registration End Date">
                        </div>

                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Maximum Attendance<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $event->maximum_attendence or '' }}" name="maximum_attendence" id="maximum_attendence" maxlength="50" placeholder="Maximum Attendance">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Registration Fee<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="registration_fee" id="registration_fee">
                                <option value="Yes" <?= $event->reg_fee == "Yes" ? 'selected' : '' ?>>Yes</option>
                                <option value="No" <?= $event->reg_fee == "No" ? 'selected' : '' ?>>No</option>
                            </select>
                        </div>


                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Registration Form<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select aria-required="true" aria-invalid="false" class="form-control valid" name="form_type" id="form_type">
                                <option value="">Select Form Type</option>
                                <?php if ($event->reg_fee == "Yes") { ?>
                                    <option value="2" <?php
                                    if ($event->form_type == 2) {
                                        echo " selected";
                                    }
                                    ?>>Bal - Balika Form</option>
                                    <option value="3" <?php
                                    if ($event->form_type == 3) {
                                        echo " selected";
                                    }
                                    ?>>Default Form</option>
                                    <option value="1" <?php
                                    if ($event->form_type == 1) {
                                        echo " selected";
                                    }
                                    ?>>Kishore - Kishori Form</option>
                                    <option value="4" <?php
                                    if ($event->form_type == 4) {
                                        echo " selected";
                                    }
                                    ?>>RAM Form</option>
                                        <?php } else { ?>
                                    <option value="5" <?php
                                    if ($event->form_type == 5) {
                                        echo " selected";
                                    }
                                    ?>>No Form</option>
                                        <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Currency Type<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="currency_type" id="currency_type">
                                <?php
                                if (isset($currency_type) && !empty($currency_type)) {
                                    for ($i = 0; $i < count($currency_type); $i++) {
                                        ?>
                                        <option value="<?php echo $currency_type[$i]; ?>" <?php
                                        if ($event->currency_type == $currency_type[$i]) {
                                            echo " selected";
                                        }
                                        ?>><?php echo ucwords($currency_type[$i]); ?></option>
                                            <?php } ?>
                                        <?php } ?>
                            </select>
                        </div>

                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Status<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select class="form-control" name="event_status">
                                <option value="1" <?php
                                if ($event->event_status == 1) {
                                    echo " selected";
                                }
                                ?>>Active</option>
                                <option value="0" <?php
                                if ($event->event_status == 0) {
                                    echo " selected";
                                }
                                ?>>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Is Transportation Available?<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="transportation_avail" id="transportation_avail">
                                <option value="1" <?php
                                if ($event->transportation_avail == 1) {
                                    echo " selected";
                                }
                                ?>>Yes</option>
                                <option value="0" <?php
                                if ($event->transportation_avail == 0) {
                                    echo " selected";
                                }
                                ?>>No</option>
                            </select>
                        </div>

                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label trans_deadline">Transportation Info Deadline<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control trans_deadline"  value="{{ $event->transportation_deadline or '' }}" name="transportation_deadline" id="transportation_deadline" maxlength="50" placeholder="Transportation Info Deadline">
                        </div>
                    </div>

                   @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                   <div class="form-group ">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label transportation_required_option">Is Transportation Required?<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin transportation_required_option">
                            <select class="form-control" name="transporation_require" id="transporation_require">
                                <option value="1" <?php
                                if ($event->transporation_require == 1) {
                                    echo " selected";
                                }
                                ?>>Yes</option>
                                <option value="0" <?php
                                if ($event->transporation_require == 0) {
                                    echo " selected";
                                }
                                ?>>No</option>
                            </select>
                        </div>
                        
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stripe Key Override</label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $event->client_secret_key or '' }}" name="stripe_client_secret" id="stripe_client_secret" maxlength="50" placeholder="Stripe Client Secret Key">
                        </div>
                    </div>
                   @else
                   <div class="form-group transportation_required_option">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Is Transportation Required?<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="transporation_require" id="transporation_require">
                                <option value="1" <?php
                                if ($event->transporation_require == 1) {
                                    echo " selected";
                                }
                                ?>>Yes</option>
                                <option value="0" <?php
                                if ($event->transporation_require == 0) {
                                    echo " selected";
                                }
                                ?>>No</option>
                            </select>
                        </div>
                    </div>
                   @endif
                    

                    <div class="form-group payment_country_outer hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Payment Country<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select class="form-control" name="payment_country" id="payment_country">
                                <option value="">Select Payment Country</option>
                                <?php
                                if (isset($country) && !empty($country)) {
                                    for ($i = 0; $i < count($country); $i++) {
                                        ?>
                                        <option value="<?php echo $country[$i]->id; ?>" <?php
                                        if ($country[$i]->id == $event->payment_country) {
                                            echo " selected";
                                        }
                                        ?>><?= ucwords($country[$i]->country_name); ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Host Region<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select class="form-control" name="host_region" id="host_region">
                                <option value="">Select Host Region</option>
                                <?php
                                if (isset($regions) && !empty($regions)) {
                                    for ($i = 0; $i < count($regions); $i++) {
                                        ?>
                                        <option value="<?php echo $regions[$i]->id; ?>" <?php
                                        if ($regions[$i]->id == $event->host_region && ($event->event_level == 2 || $event->event_level == "2")) {
                                            echo " selected";
                                        }
                                        ?>><?= ucwords($regions[$i]->RegionName); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="show_outing" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Outing Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <input type="checkbox" name="show_outing" id="show_outing" value="1" <?php
                            if ($event->show_outing == 1) {
                                echo "checked";
                            }
                            ?>>
                            <label for="show_outing" class="control-label">This event includes an outing</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="show_stayinfo" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <input type="checkbox" name="show_stayinfo" id="show_stayinfo" value="1" <?php
                            if ($event->show_stayinfo == 1) {
                                echo "checked";
                            }
                            ?>>
                            <label for="show_stayinfo" class="control-label">This event includes a stayover</label>
                        </div>
                    </div>
                    <div class="form-group stayoverinfo hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover Information<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select name="stayover_info" id="stayover_info" class="form-control">
                                <option value="">Select Stayover Type</option>
                                <option value="1" class="nostay" <?php
                                if ($event->stayover_option_type == 1) {
                                    echo "selected";
                                }
                                ?>>No Stayover</option>
                                <option value="2" <?php
                                if ($event->stayover_option_type == 2) {
                                    echo "selected";
                                }
                                ?>>One 1-night Stayover</option>
                                <option value="3" <?php
                                if ($event->stayover_option_type == 3) {
                                    echo "selected";
                                }
                                ?>>One 2-night Stayover</option>
                                <option value="4" <?php
                                if ($event->stayover_option_type == 4) {
                                    echo "selected";
                                }
                                ?>>Two 1-night Stayover</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group stayoverstartdate hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover Start Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $event->stay_start_date or '' }}" name="stay_start_date" id="stay_start_date" maxlength="50" placeholder="Stayover Start Date">
                        </div>
                    </div>
                    <div class="form-group stayoverenddate hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover End Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $event->stay_end_date or '' }}" name="stay_end_date" id="stay_end_date" maxlength="50" placeholder="Stayover End Date">
                        </div>
                    </div>
                    <div class="form-group stayoverstartdate1 hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover Start Date1<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $event->stay_start_date1 or '' }}" name="stay_start_date1" id="stay_start_date1" maxlength="50" placeholder="Stayover Start Date1">
                        </div>
                    </div>
                    <div class="form-group stayoverstartdate2 hide">
                        <label for="inputEmail3" class="col-md-12 control-label">Stayover Start Date2<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ $event->stay_start_date2 or '' }}" name="stay_start_date2" id="stay_start_date2" maxlength="50" placeholder="Stayover Start Date2">
                        </div>
                    </div>
                    <div class="form-group stayoverlisting">
                        <label for="inputEmail3" class="col-md-12 col-lg-2 control-label">Stayover List</label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title">Stayover Locations</h3>

                                    <span class="adduser-icon pull-right"><a data-toggle="modal" data-target="#add_stayover_modal" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add</a><button type="button" class="btn btn-box-tool" data-widget="collapse">&nbsp;&nbsp;&nbsp;<i class="fa fa-minus"></i>
                                        </button></span>

                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="hotel_list" class="table table-bordered table-striped">
                                            <thead>
                                            <th class="not-export-col">No</th>
                                            <th class="">Stayover Name</th>
                                            <th class="">Address</th>
                                            <th class="">City</th>
                                            <th class="">State/Province</th>
                                            <th class="ucenter not-export-col actionfield">Action</th>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>

                    <div class="form-group outinglisting">
                        <label for="inputEmail3" class="col-md-12 col-lg-2 control-label">Outing Location List</label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title">Outing Locations</h3>
                                    <span class="adduser-icon pull-right"><a data-toggle="modal" data-target="#add_outing_modal" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;Add</a><button type="button" class="btn btn-box-tool" data-widget="collapse">&nbsp;&nbsp;&nbsp;<i class="fa fa-minus"></i></button></span>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <!-- /.box-header -->
                                        <table id="outing_list" class="table table-bordered table-striped">
                                            <thead>
                                            <th class="usenum not-export-col">No</th>
                                            <th class="uname">Outing Name</th>
                                            <th class="uname">Address</th>
                                            <th class="uname">City</th>
                                            <th class="uname">State/Province</th>
                                            <th class="ucenter not-export-col actionfield">Action</th>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                            <input type="hidden" name="event_id" value="{{ $event->id or ''}}" id="event_id">
                            <input type="hidden" name="refund_compare" value="1" id="refund_compare">
                            <input type="hidden" name="event_approve_status_change" value="{{ $event->event_approve_status or 0 }}" id="event_approve_status_change">
                            <input type="submit" class="btn btn-primary" value="Submit" id="submit">
                            <a href="{!! URL::to('admin/manageevent/events') !!}" class="btn reset-btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!--End : Registration Details Section-->

        <!--        </div>-->
    </div>
    <!--End : Registration Details Section-->
    {!! Form::close() !!}
    <!--Begin : Modal For Add Stayover-->
<div class="modal fade" id="add_stayover_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closemodal_reset" data-dismiss="modal">&times;</button>
                <h4 class="modal-title childpopupnote">Add Stayover Location</h4>
            </div>
            {!! Form::open(array('url' => 'admin/user/insertuserchild' , 'class' => 'form-horizontal','id' => 'addhotel_form', 'files' => true)) !!}
            <div class="modal-body">
                <div class="contact-other-info">
                    <div class="coulmn-my-info" style="padding:0px;">
                        <div class="info-row">
                            <label for="inputName" class="control-label">Location Type<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <select id="stayover_location_type" name="stayover_location_type" class="form-control valid" tabindex="1">
                                    <option value="">Select Stayover Location Type</option>
                                    <option value="1">Mandir Campus</option>
                                    <option value="2">Personal Residence</option>
                                    <option value="3">Hotel/Motel</option>
                                    <option value="4">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="info-row">
                            <label for="inputName" class="control-label">Name<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <input type="text" class="form-control" placeholder="Name" value="{{ old('stayover_name') }}" name="stayover_name" maxlength="50" tabindex="2">
                            </div>
                        </div>
                    </div>

                    <div class="coulmn-my-info">
                        <div class="info-row">
                            <label for="inputName" class="control-label">Address<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows">
                                <input type="text" class="form-control" placeholder="Address" value="{{ old('address') }}" name="address" tabindex="3" id="address">
                            </div>
                        </div>
                        <div class="info-row">
                            <label for="inputName" class="control-label">City<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <input type="text" maxlength="50" id="city" name="city" value="{{ old('city') }}" placeholder="City" class="form-control myfn" aria-required="true" tabindex="4">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-other-info">
                    <label for="inputName" class="control-label">State/Province<em class="mandatory-field">*</em></label>
                    <div class="info-row">
                        <select id="state" name="state" aria-required="true" aria-invalid="false" class="form-control valid" tabindex="5">
                            <option value="">Select State/Province</option>
                            @if(isset($states) && !empty($states))
                            @foreach($states as $state)
                            <option value="{{ $state->id }}">{{ $state->state_code }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="info-row">
                        <label for="inputName" class="control-label">Zip/Postal Code<em class="mandatory-field">*</em></label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" id="pincode" name="pincode" value="{{ old('pincode') }}" placeholder="Zip/Postal Code" class="form-control myfn" aria-required="true" tabindex="6" >
                        </div>
                    </div>
                </div>
                <div class="contact-other-info">
                    <div class="info-row">
                        <label for="inputName" class="control-label">Number of Rooms</label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" maxlength="50" id="rooms" name="rooms" value="{{ old('rooms') }}" placeholder="Number of Rooms" class="form-control myfn" aria-required="true" tabindex="7">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="event_id" value="{{ $event->id or ''}}" id="event_id">
                <div class="contact-other-info"></div>
            </div>
            <div class="modal-footer buttons-rows">
                <input type="submit" class="btn btn-primary removefloat" value="Save" id="add_stayover" />
                <button type="button" class="btn btn-save removefloat closemodal_reset" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!-- End: Modal For Add Stayover-->



<!--Begin : Modal For Edit Stayover-->
<div class="modal fade" id="edit_stayover_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closemodal_reset" data-dismiss="modal">&times;</button>
                <h4 class="modal-title childpopupnote">Edit Stayover Location</h4>
            </div>
            {!! Form::open(array('url' => 'admin/user/insertuserchild' , 'class' => 'form-horizontal','id' => 'edithotel_form', 'files' => true)) !!}
            <div class="modal-body">
                <div class="contact-other-info">
                    <div class="coulmn-my-info" style="padding:0px;">
                        <div class="info-row">
                            <label for="inputName" class="control-label">Location Type<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <select id="edit_stayover_location_type" name="stayover_location_type" class="form-control valid" tabindex="1">
                                    <option value="">Select Stayover Location Type</option>
                                    <option value="1">Mandir Campus</option>
                                    <option value="2">Personal Residence</option>
                                    <option value="3">Hotel/Motel</option>
                                    <option value="4">Other</option>
                                </select>
                            </div>
                        </div>

                        <div class="info-row">
                            <label for="inputName" class="control-label">Name<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <input type="text" class="form-control" placeholder="Name" value="{{ old('stayover_name') }}" name="stayover_name" maxlength="50" tabindex="2" id="edit_stayover_name">
                            </div>
                        </div>
                    </div>

                    <div class="coulmn-my-info">
                        <div class="info-row">
                            <label for="inputName" class="control-label">Address<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows">
                                <input type="text" class="form-control" placeholder="Address" value="{{ old('address') }}" name="address" tabindex="3" id="edit_address">
                            </div>
                        </div>
                        <div class="info-row">
                            <label for="inputName" class="control-label">City<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <input type="text" maxlength="50" id="edit_city" name="city" value="{{ old('city') }}" placeholder="City" class="form-control myfn" aria-required="true" tabindex="4">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-other-info">
                    <label for="inputName" class="control-label">State/Province<em class="mandatory-field">*</em></label>
                    <div class="info-row">
                        <select id="edit_state" name="state" aria-required="true" aria-invalid="false" class="form-control valid" tabindex="5">
                            <option value="">Select State/Province</option>
                            @if(isset($states) && !empty($states))
                            @foreach($states as $state)
                            <option value="{{ $state->id }}">{{ $state->state_code }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="info-row">
                        <label for="inputName" class="control-label">Zip/Postal Code<em class="mandatory-field">*</em></label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" id="edit_pincode" name="pincode" value="{{ old('pincode') }}" placeholder="Zip/Postal Code" class="form-control myfn" aria-required="true" tabindex="6" >
                        </div>
                    </div>
                </div>
                <div class="contact-other-info">
                    <div class="info-row">
                        <label for="inputName" class="control-label">Number of Rooms</label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" maxlength="50" id="edit_rooms" name="rooms" value="{{ old('rooms') }}" placeholder="Number of Rooms" class="form-control myfn" aria-required="true" tabindex="7">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="event_id" value="{{ $event->id or ''}}" id="edit_event_id">
                <input type="hidden" name="stayoverlocation_id" value="" id="stayoverlocation_id">
                <div class="contact-other-info"></div>
            </div>
            <div class="modal-footer buttons-rows">
                <input type="submit" class="btn btn-primary removefloat" value="Save" id="update_stayover" />
                <button type="button" class="btn btn-save removefloat closemodal_reset" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!-- End: Modal For Edit Stayover-->
<!--Begin : Modal For Delete Stayover Record-->
<div class="modal fade" id="modal_stayver_deleterecord">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Record</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this record ?</p>
            </div>
            <input type="hidden" name="redirectpath" value="" id="redirectpath">
            <input type="hidden" name="pathaccesstype" value="" id="pathaccesstype">
            <div class="modal-footer">
                <button type="button" class="btn btn-primary confirmdelete_statover">Yes</button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!--End : Modal For Delete Stayover Record-->



<!--Begin : Modal For Add Outing-->
<div class="modal fade" id="add_outing_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closemodal_reset" data-dismiss="modal">&times;</button>
                <h4 class="modal-title childpopupnote">Add Outing Location</h4>
            </div>
            {!! Form::open(array('url' => 'admin/user/insertuserchild' , 'class' => 'form-horizontal','id' => 'addouting_form', 'files' => true)) !!}
            <div class="modal-body">
                <div class="contact-other-info">
                    <div class="coulmn-my-info" style="padding:0px;">
                        <div class="info-row">
                            <label for="inputName" class="control-label">Outing Name<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <input type="text" class="form-control" placeholder="Outing Name" value="{{ old('outing_name') }}" name="outing_name" maxlength="50" tabindex="1">
                            </div>
                        </div>

                        <div class="info-row">
                            <label for="inputName" class="control-label">Outing Description<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <textarea class="form-control" name="outing_description" rows="5" id="outing_description" tabindex="2">{{ old('outing_description') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="coulmn-my-info">
                        <div class="info-row">
                            <label for="inputName" class="control-label">Outing Address<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows">
                                <input type="text" class="form-control" placeholder="Outing Address" value="{{ old('address') }}" name="address" tabindex="3" id="add_outing_address">
                            </div>
                        </div>
                        <div class="info-row">
                            <label for="inputName" class="control-label">City<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <input type="text" maxlength="50" id="city" name="city" value="{{ old('city') }}" placeholder="City" class="form-control myfn" aria-required="true" tabindex="4">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-other-info">
                    <label for="inputName" class="control-label">State/Province<em class="mandatory-field">*</em></label>
                    <div class="info-row">
                        <select id="state" name="state" aria-required="true" aria-invalid="false" class="form-control valid" tabindex="5">
                            <option value="">Select State/Province</option>
                            @if(isset($states) && !empty($states))
                            @foreach($states as $state)
                            <option value="{{ $state->id }}">{{ $state->state_code }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="info-row">
                        <label for="inputName" class="control-label">Zip/Postal Code<em class="mandatory-field">*</em></label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" id="add_outing_pincode" name="pincode" value="{{ old('pincode') }}" placeholder="Zip/Postal Code" class="form-control myfn" aria-required="true" tabindex="6">
                        </div>
                    </div>
                </div>
                <div class="contact-other-info">
                    <div class="info-row">
                        <label for="inputName" class="control-label">Outing Start Date<em class="mandatory-field">*</em></label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" maxlength="50" id="outing_start_date" name="outing_start_date" value="{{ old('outing_start_date') }}" placeholder="Outing Start Date" class="form-control myfn" aria-required="true" tabindex="7">
                        </div>
                    </div>
                    <div class="info-row">
                        <label for="inputName" class="control-label">Outing End Date<em class="mandatory-field">*</em></label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" maxlength="50" id="outing_end_date" name="outing_end_date" value="{{ old('outing_end_date') }}" placeholder="Outing End Date" class="form-control myfn" aria-required="true" tabindex="8">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="event_id" value="{{ $event->id or ''}}" id="event_id">
                <div class="contact-other-info"></div>
            </div>
            <div class="modal-footer buttons-rows">
                <input type="submit" class="btn btn-primary removefloat" value="Save" id="add_stayover" />
                <button type="button" class="btn btn-save removefloat closemodal_reset" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!-- End: Modal For Add Outing-->


<!--Begin : Modal For Edit Outing-->
<div class="modal fade" id="edit_outing_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closemodal_reset" data-dismiss="modal">&times;</button>
                <h4 class="modal-title childpopupnote">Edit Outing Location</h4>
            </div>
            {!! Form::open(array('url' => 'admin/user/insertuserchild' , 'class' => 'form-horizontal','id' => 'editouting_form', 'files' => true)) !!}
            <div class="modal-body">
                <div class="contact-other-info">
                    <div class="coulmn-my-info" style="padding:0px;">
                        <div class="info-row">
                            <label for="inputName" class="control-label">Outing Name<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <input type="text" class="form-control" placeholder="Outing Name" value="{{ old('outing_name') }}" name="outing_name" maxlength="50" tabindex="1" id="edit_outing_name">
                            </div>
                        </div>

                        <div class="info-row">
                            <label for="inputName" class="control-label">Outing Description<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <textarea class="form-control" name="outing_description" rows="5" id="edit_outing_description" tabindex="2">{{ old('outing_description') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="coulmn-my-info">
                        <div class="info-row">
                            <label for="inputName" class="control-label">Outing Address<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows">
                                <input type="text" class="form-control" placeholder="Outing Address" value="{{ old('address') }}" name="address" tabindex="3" id="edit_outing_address">
                            </div>
                        </div>
                        <div class="info-row">
                            <label for="inputName" class="control-label">City<em class="mandatory-field">*</em></label>
                            <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                                <input type="text" maxlength="50" id="edit_outing_city" name="city" value="{{ old('city') }}" placeholder="City" class="form-control myfn" aria-required="true" tabindex="4">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="contact-other-info">
                    <label for="inputName" class="control-label">State/Province<em class="mandatory-field">*</em></label>
                    <div class="info-row">
                        <select id="edit_outing_state" name="state" aria-required="true" aria-invalid="false" class="form-control valid" tabindex="5">
                            <option value="">Select State/Province</option>
                            @if(isset($states) && !empty($states))
                            @foreach($states as $state)
                            <option value="{{ $state->id }}">{{ $state->state_code }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="info-row">
                        <label for="inputName" class="control-label">Zip/Postal Code<em class="mandatory-field">*</em></label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" id="edit_outing_pincode" name="pincode" value="{{ old('pincode') }}" placeholder="Zip/Postal Code" class="form-control myfn" aria-required="true" tabindex="6">
                        </div>
                    </div>
                </div>
                <div class="contact-other-info">
                    <div class="info-row">
                        <label for="inputName" class="control-label">Outing Start Date<em class="mandatory-field">*</em></label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" maxlength="50" id="edit_outing_start_date" name="outing_start_date" value="{{ old('outing_start_date') }}" placeholder="Outing Start Date" class="form-control myfn" aria-required="true" tabindex="7">
                        </div>
                    </div>
                    <div class="info-row">
                        <label for="inputName" class="control-label">Outing End Date<em class="mandatory-field">*</em></label>
                        <div class="profile-input-rows" style="margin: 0 5px 0 0;">
                            <input type="text" maxlength="50" id="edit_outing_end_date" name="outing_end_date" value="{{ old('outing_end_date') }}" placeholder="Outing End Date" class="form-control myfn" aria-required="true" tabindex="8">
                        </div>
                    </div>
                </div>
                <input type="hidden" name="event_id" value="{{ $event->id or ''}}" id="event_id">
                <input type="hidden" name="outing_id" value="" id="outing_id">
                <div class="contact-other-info"></div>
            </div>
            <div class="modal-footer buttons-rows">
                <input type="submit" class="btn btn-primary removefloat" value="Save" id="edit_outing" />
                <button type="button" class="btn btn-save removefloat closemodal_reset" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
<!-- End: Modal For Edit Outing-->
</section>
@endsection


