@extends('theme.master')
@section('htmlheader_title')
	| Meditation Hall
@endsection


@section('main-content')
<!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area br_image">
            <div class="container">
                <div class="page-cover text-center">
                    <h2 class="page-cover-tittle">Meditation Hall</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/')}}">Home</a></li>
                        <li class="active">Meditation Hall</li>
                    </ol>
                </div>
            </div>
        </section>
        <!--================Breadcrumb Area =================-->
        
        <!--================About Area =================-->
        <section class="about_area section_gap">
            <div class="container">
                <div class="section_title text-center">
                    <h2>Overview</h2>
                    <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from </p>
                </div>
                <div class="row">
                    <div class="col-md-6 d_flex">
                        <div class="about_content flex">
                            <h3 class="title_color">Did not find your Package Feel free to ask us. We‘ll make it for you</h3>
                            <p>inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.</p>
                            <a href="#" class="about_btn btn_hover">Read Full Story</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="{{ url (asset('/public/theme/image/about.jpg')) }}" alt="abou_img">
                    </div>
                </div>
            </div>
        </section>
        <!--================About Area =================-->
        
        <!--================Facility Area =================-->
        <!-- Start facility Area -->
<section class="services-area section_gap" id="facility">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <!--		                <div class="menu-content  col-lg-7">
                                                <div class="title text-center">
                                                    <h1 class="mb-10">My Offered Services</h1>
                                                    <p>At about this time of year, some months after New Year’s resolutions have been made and kept, or made and neglected.</p>
                                                </div>
                                            </div>-->
            <div class="section_title text-center">
                <h2>Facility</h2>
                <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from </p>
            </div>
        </div>						
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-pie-chart"></span>
                    <a href="#"><h4>Web Design</h4></a>
                    <p>
                        “It is not because things are difficult that we do not dare; it is because we do not dare that they are difficult.”
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-laptop-phone"></span>
                    <a href="#"><h4>Web Development</h4></a>
                    <p>
                        If you are an entrepreneur, you know that your success cannot depend on the opinions of others. Like the wind, opinions.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-camera"></span>
                    <a href="#"><h4>Photography</h4></a>
                    <p>
                        Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills.
                    </p>
                </div>	
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-picture"></span>
                    <a href="#"><h4>Clipping Path</h4></a>
                    <p>
                        Hypnosis quit smoking methods maintain caused quite a stir in the medical world over the last two decades. There is a lot of argument.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-tablet"></span>
                    <a href="#"><h4>Apps Interface</h4></a>
                    <p>
                        Do you sometimes have the feeling that you’re running into the same obstacles over and over again? Many of my conflicts.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-services">
                    <span class="lnr lnr-rocket"></span>
                    <a href="#"><h4>Graphic Design</h4></a>
                    <p>
                        You’ve heard the expression, “Just believe it and it will come.” Well, technically, that is true, however, ‘believing’ is not just thinking that.
                    </p>
                </div>				
            </div>														
        </div>
    </div>	
</section>
<!-- End facility Area -->
        <!--================Facility Area =================-->
        
        
@endsection