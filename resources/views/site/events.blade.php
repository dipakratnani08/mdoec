@extends('theme.master')
@section('htmlheader_title')
	| Events
@endsection


@section('main-content')
<!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area br_image">
            <div class="container">
                <div class="page-cover text-center">
                    <h2 class="page-cover-tittle">Events</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/')}}">Home</a></li>
                        <li class="active">Events</li>
                    </ol>
                </div>
            </div>
        </section>
        <!--================Breadcrumb Area =================-->
        
       
        
        <!--================Event Blog Area=================-->
        <section class="event_blog_area section_gap" id="past_events">
            <div class="container">
                <div class="section_title text-center">
                    <h2>Past Events</h2>
                    <p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast</p>
                </div>
                <h2 class="section_title text-center">No Events Available</h2>
<!--                <div class="row">
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="{{ url (asset('/public/theme/image/blog1.jpg')) }}" alt="">
                            <a href="#"><h2 class="event_title">Spreading Peace to world</h2></a>
                            <ul class="list_style sermons_category">
                                <li><i class="lnr lnr-user"></i>Saturday, 5th may, 2018</li>
                                <li><i class="lnr lnr-apartment"></i>Rocky beach Church</li>
                                <li><i class="lnr lnr-location"></i>Santa monica, Los Angeles, USA</li>
                            </ul>
                            <a href="#" class="btn_hover">View Details</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="{{ url (asset('/public/theme/image/blog2.jpg')) }}" alt="">
                            <a href="#"><h2 class="event_title">Spreading Peace to world</h2></a>
                            <ul class="list_style sermons_category">
                                <li><i class="lnr lnr-user"></i>Saturday, 5th may, 2018</li>
                                <li><i class="lnr lnr-apartment"></i>Rocky beach Church</li>
                                <li><i class="lnr lnr-location"></i>Santa monica, Los Angeles, USA</li>
                            </ul>
                            <a href="#" class="btn_hover">View Details</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="{{ url (asset('/public/theme/image/blog3.jpg')) }}" alt="">
                            <a href="#"><h2 class="event_title">Spreading Light to world</h2></a>
                            <ul class="list_style sermons_category">
                                <li><i class="lnr lnr-user"></i>Saturday, 5th may, 2018</li>
                                <li><i class="lnr lnr-apartment"></i>Rocky beach Church</li>
                                <li><i class="lnr lnr-location"></i>Santa monica, Los Angeles, USA</li>
                            </ul>
                            <a href="#" class="btn_hover">View Details</a>
                        </div>
                    </div>
                </div>-->
            </div>
        </section>
        <!--================Blog Area=================-->
        
        
        <!--================Event Blog Area=================-->
        <section class="event_blog_area section_gap" id="upcoming_events">
            <div class="container">
                <div class="section_title text-center">
                    <h2>Upcoming Events</h2>
                    <p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast</p>
                </div>
                <h2 class="section_title text-center">No Events Available</h2>
<!--                <div class="row">
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="{{ url (asset('/public/theme/image/blog1.jpg')) }}" alt="">
                            <a href="#"><h2 class="event_title">Spreading Peace to world</h2></a>
                            <ul class="list_style sermons_category">
                                <li><i class="lnr lnr-user"></i>Saturday, 5th may, 2018</li>
                                <li><i class="lnr lnr-apartment"></i>Rocky beach Church</li>
                                <li><i class="lnr lnr-location"></i>Santa monica, Los Angeles, USA</li>
                            </ul>
                            <a href="#" class="btn_hover">View Details</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="{{ url (asset('/public/theme/image/blog2.jpg')) }}" alt="">
                            <a href="#"><h2 class="event_title">Spreading Peace to world</h2></a>
                            <ul class="list_style sermons_category">
                                <li><i class="lnr lnr-user"></i>Saturday, 5th may, 2018</li>
                                <li><i class="lnr lnr-apartment"></i>Rocky beach Church</li>
                                <li><i class="lnr lnr-location"></i>Santa monica, Los Angeles, USA</li>
                            </ul>
                            <a href="#" class="btn_hover">View Details</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="event_post">
                            <img src="{{ url (asset('/public/theme/image/blog3.jpg')) }}" alt="">
                            <a href="#"><h2 class="event_title">Spreading Light to world</h2></a>
                            <ul class="list_style sermons_category">
                                <li><i class="lnr lnr-user"></i>Saturday, 5th may, 2018</li>
                                <li><i class="lnr lnr-apartment"></i>Rocky beach Church</li>
                                <li><i class="lnr lnr-location"></i>Santa monica, Los Angeles, USA</li>
                            </ul>
                            <a href="#" class="btn_hover">View Details</a>
                        </div>
                    </div>
                </div>-->
            </div>
        </section>
        <!--================Blog Area=================-->
@endsection