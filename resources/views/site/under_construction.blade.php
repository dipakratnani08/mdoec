@extends('theme.master')
@section('htmlheader_title')
	| School
@endsection


@section('main-content')
<!--================Breadcrumb Area =================-->
        <section class="breadcrumb_area br_image">
            <div class="container">
                <div class="page-cover text-center">
                    <h2 class="page-cover-tittle">School</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/')}}">Home</a></li>
                        <li class="active">School</li>
                    </ol>
                </div>
            </div>
        </section>
        <!--================Breadcrumb Area =================-->
        
        <!--================About Area =================-->
        <section class="about_area section_gap">
            <div class="container">
                <div class="section_title text-center">
                    <h2>Under Construction</h2>
                    <p>We are building something very cool. Stay tuned and be patient. Your patience will be well paid.</p>
                </div>
                <div class="row">
                    <div class=" place_balk_workers">
                        <div class="balk_workers"></div>
                        <div class="saw"></div>
                        <div class="balk"></div>
                    </div>
                </div>
            </div>
        </section>
        <!--================About Area =================-->
        
        
@endsection