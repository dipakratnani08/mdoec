@extends('layouts/master')

@section ('AdditionalVendorCssInclude')
<style type="text/css">
    .errorlist, .errorlist th, .errorlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }

    .errorlist th, .errorlist td {
        padding: 5px;
        text-align: left;
    }
    .successlist, .successlist th, .successlist td{
        border: 1px solid black;
        border-collapse: collapse;
    }

    .successlist th, .successlist td {
        padding: 5px;
        text-align: left;
    }
    #eligible_wings
    {
        background: none !important;
    }
    table_titlecase
    {
        text-transform: capitalize !important;
    }

    .table_titlecase_record
    {
        text-transform: capitalize !important;
    }
    .btn-primary{
        margin-right: 15px;
    }
    .margin-left-15
    {
        margin-left: 15px !important;
    }

    .margin-right-15
    {
        margin-right: 15px !important;
    }

    .margin-top-15
    {
        margin-top: 15px !important;
    }

    .padding-left-25
    {
        padding-left: 25px !important;
    }

    .padding-right-25
    {
        padding-right: 25px !important;
    }

    .margin-bottom-15
    {
        margin-bottom: 15px !important;
    }
    textarea {
        resize: none;
    }
    .wing-check-row {
        float: left;
        margin-right: 12px;
        width: auto;
        margin-top: 5px;
    }
    @media (max-width:992px)
    {
        .userbottommargin { margin-bottom: 15px !important;}
    }
    @media (min-width:993px) and (max-width:1199px)
    {
        .location_margin
        {
            margin-bottom: 15px;
        }
    }
    @media (min-width:768px) and (max-width:991px)
    {
        .form-horizontal .control-label
        {
            text-align: left;
            margin-bottom: 5px;
        }
    }
    @media (min-width:991px) and (max-width:1199px)
    {
        .form-horizontal .control-label
        {
            text-align: left;
            margin-bottom: 5px;
        }
        .userbottommargin { margin-bottom: 15px !important;}
    }
    .emergencyborder
    {
        border-top: none !important;
    }
</style>
@endsection

@section('PageTitle')
Add New Event
@endsection

@section('AdditionalVendorScriptsInclude')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?= Config::get('constants.GOOGLE_MAP_API_KEY') ?>&libraries=places"></script>
<script type="text/javascript" src="{{ asset('global/js/jquery.geocomplete.js') }}"></script>
<script type="text/javascript">

$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});
// Begin : Get Zip code of event location
$('#event_location').keyup(function () {
    if ($("#event_location").val() != "")
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postZipcode') ?>',
            data: {"address": $("#event_location").val()},
            //dataType: "json",
            success: function (result) {

                if (result != "false")
                {
                    $("#event_zip").val(result);
                } else
                {
                    $("#event_zip").val("");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    }
});
//End : Get Zip code of event Loocation
(function ($) {
    $(function () {
        /*Begin : Create Custom Method "Greate Than"*/
        jQuery.validator.addMethod("greaterThan",
                function (value, element, params) {
                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) >= new Date($(params).val());
                    }

                    return isNaN(value) && isNaN($(params).val())
                            || (Number(value) >= Number($(params).val()));
                });
        /*End : Create Custom Method "Greate Than"*/
        /*Begin : Form Validation*/
        $("#addevent_form").validate({
            rules: {
                event_type: {required: true},
                event_level: {required: true},
                event_title: {required: true},
                event_location: {required: true},
                event_zip: {required: true},
                eligible_regions: {required: true},
                event_center: {required: true},
                eligible_regions: {required: true},
                event_center: {required: true},
                event_start_date: {required: true},
                event_end_date: {required: true},
                early_reg_start_date: {required: true},
                reg_start_date: {required: true},
                reg_end_date: {required: true},
                maximum_attendence: {required: true},
                //refund_amount: {required: true},
                transportation_deadline: {required: true},
                "eligible_wings[]": {required: true},
                event_description: {required: true},
                stay_start_date: {required: true},
                stay_start_date1: {required: true},
                stay_start_date2: {required: true},
                stay_end_date: {required: true},
                payment_country: {required: true},
                host_region: {required: true},
//                payment_region: {required: true},
//                payment_center: {required: true},
                currency_type: {required: true},
                form_type: {required: true},
                stayover_info: {
                    required: {
                        depends: function (element) {
                            return $("#event_level").val() >= 3;//For Center only
                        }
                    },
                },
            },
            messages: {
                event_type: {required: "Please Select Event Type."},
                event_level: {required: "Please Select Event Level."},
                event_title: {required: "Please Enter Event Title."},
                event_location: {required: "Please Enter Event Location."},
                event_zip: {required: "Please Enter Zip/Postal Code."},
                eligible_regions: {required: "Please Select Event Region."},
                event_center: {required: "Please Select Event Center."},
                event_start_date: {required: "Please Select Event Start Date."},
                event_end_date: {required: "Please Select Event End Date."},
                early_reg_start_date: {required: "Please Select Early Registration Start Date."},
                reg_start_date: {required: "Please Select Registration Start Date."},
                reg_end_date: {required: "Please Select Registration End Date."},
                maximum_attendence: {required: "Please Enter Maximum Attendance."},
                //refund_amount: {required: "Please Enter Refund Amount."},
                transportation_deadline: {required: "Please Select Transportation Deadline."},
                event_description: {required: "Please Enter Event Description."},
                "eligible_wings[]": {required: "Please Select Eligible Wing."},
                stay_start_date: {required: "Please Select Stayover Start Date."},
                stay_start_date1: {required: "Please Select Stayover Start Date1."},
                stay_start_date2: {required: "Please Select Stayover Start Date2."},
                stay_end_date: {required: "Please Select Stayover End Date."},
                stayover_info: {required: "Please Select Stayover Type."},
                payment_country: {required: "Please Select Payment Country."},
                host_region: {required: "Please Select Host Region."},
//                payment_region: {required: "Please Select Payment Region."},
//                payment_center: {required: "Please Select Payment Center."},
                currency_type: {required: "Please Select Currency Type."},
                form_type: {required: "Please Select Form Type."},
            }
        });
        /*End : Form Validation*/
    });
    $(document).ready(function () {
        /*Begin : To select content on focus*/
        $("input").focus(function () {
            $(this).select();
        });
        $("textarea").focus(function () {
            $(this).select();
        });
        /*End : To select content on focus*/
        /*Begin : For Event location using google input*/
        //$("#event_location").geocomplete();
        var autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('event_location')), {types: ['geocode']}
        );
        /*End : For Event location using google input*/
        var FromEndDate = '';
        var startDate = new Date();
        $('#transportation_deadline').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true});

        $('#early_reg_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#reg_start_date').datepicker('setStartDate', startDate);
        });

        $('#reg_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#early_reg_start_date').datepicker('setEndDate', FromEndDate);
            $('#reg_end_date').datepicker('setStartDate', FromEndDate);
        });
        $('#reg_end_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#reg_start_date').datepicker('setEndDate', FromEndDate);
            $('#transportation_deadline').datepicker('setStartDate', FromEndDate);
        });


        $('#event_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#event_end_date').datepicker('setStartDate', startDate);
            $('#stay_start_date').datepicker('setStartDate', startDate);
            $('#stay_start_date1').datepicker('setStartDate', startDate);
            $('#stay_start_date2').datepicker('setStartDate', startDate);
        });

        $('#event_end_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#event_start_date').datepicker('setEndDate', FromEndDate);
            $('#reg_end_date').datepicker('setEndDate', FromEndDate);
            $('#reg_start_date').datepicker('setEndDate', FromEndDate);
            $('#transportation_deadline').datepicker('setEndDate', FromEndDate);
            $('#stay_end_date').datepicker('setEndDate', FromEndDate);
            $('#early_reg_start_date').datepicker('setEndDate', FromEndDate);
            $('#stay_start_date2,#stay_start_date').datepicker('setEndDate', FromEndDate);
        });


        $('#stay_start_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 1);
            $('#stay_end_date').datepicker('setStartDate', startDate);
        });

        $('#stay_end_date').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#stay_start_date').datepicker('setEndDate', FromEndDate);
        });


        $('#stay_start_date1').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())) + 3);
            $('#stay_start_date2').datepicker('setStartDate', startDate);
        });
        $('#stay_start_date2').datepicker({
            format: "mm/dd/yyyy",
            startDate: startDate,
            autoclose: true}).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#stay_start_date1').datepicker('setEndDate', FromEndDate);
        });



    });
    /*Begin : Input Validation For Zipcode*/
//$("#event_zip").keypress(function (event) {
//    var regex = new RegExp("^[a-zA-Z0-9]+$");
//                var str = String.fromCharCode(!event.charCode ? event.which : event.charCode);
//                if (regex.test(str)) {
//                    return true;
//                } else
//                {
//                    if ($("event_zip").val() != "")
//                    {
//                        if (event.which == 13)
//                        {
//                            return true;
//                        }
//                    }
//                }
//                event.preventDefault();
//                return false;
//});
    /*End : Input Validation For Zipcode*/
    /*Begin : Display Stayoover Inofrmation as per Selected Option*/
    $("#stayover_info").change(function () {
        var selected_stayeover = $(this).val();
        if (selected_stayeover == 1)//If "No Stayover"option is selected
        {
            $(".stayoverstartdate,.stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        } else if (selected_stayeover == 2)
        {//if "One 1-night Stayover" selected
            $(".stayoverstartdate").removeClass('hide');
            $(".stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        } else if (selected_stayeover == 3)
        {//if "One 2-night Stayover" selcted
            $(".stayoverstartdate").removeClass('hide');
            $(".stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        } else if (selected_stayeover == 4)
        { // Two 1-night Stayover
            $(".stayoverstartdate,.stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").removeClass('hide');
        } else
        {
            $(".stayoverstartdate,.stayoverenddate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        }
    });
    /*End:Display Stayover Inofrmation AS per Selecrted Option*/
    /*Begin : Fetch Event Type as per event Level*/
    $("#event_level,#eligible_wings").change(function () {
        $event_level = $("#event_level").val();
        //To Check stayover option checkbox is check
        if ($("#show_stayinfo").is(':checked'))
            var staycheck = 1;  // checked
        else
            var staycheck = 0;  // unchecked
        $(".stayoverinfo").addClass('hide');
        //If event level is center then filter as per wing

        if ($('#event_level').val() != "")
        {
            if ($("#event_level").val() == 3)
            {   //Display Stayover info
                if ($("#event_type").val() != "")
                {
                    if (staycheck == 1)
                    {
                        $("#show_stayinfo").iCheck('check');
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    }
                } else
                {
                    $("#show_stayinfo").iCheck('uncheck');
                    $(".stayoverinfo").addClass('hide');
                    $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
                    //$("#show_stayinfo").prop('checked', false);
                    $("#show_stayinfo").attr("disabled", false);

                }



                //If event level is center,check wing is selected or not
                if (($('#eligible_wings').val()) == null)
                {
                    var selectedwings = 0;
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                    $('#event_type,#event_level').prop('selectedIndex', 0);
                    toastr.info("Please Select Eligible Wing.", 'Info');
                    return false;
                } else
                {
                    var selectedwings = [];
                    $.each($("#eligible_wings option:selected"), function () {
                        selectedwings.push($(this).val());
                    });

                    //For Center Level Display Payemnt Options
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                }
            } else
            {
                if (staycheck == 1)
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                }

                if ($("#event_level").val() == 1)
                { // For Regional Event
//                    $(".payment_region_outer").removeClass('hide');
//                    $(".payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() == 2)
                { //For North American Level
                    $(".payment_country_outer").removeClass('hide');
                    $(".payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() >= 3)
                { //For Other Newly Added Level
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                } else
                {
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                }

            }
        } else
        {
            $(".stayoverinfo").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');

            $(".payment_country_outer,.payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
        }


        if ($("#event_level").val() >= 3)
        {
            /*Begin : Get Dynamic Center as per Region*/
            $("#eligible_regions").change(function () {
                $region_id = $(this).val();
                $("#event_center").prop("disabled", true);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: '<?= action('AdminuserController@postRegion_based_center_ajax') ?>',
                    data: {"region_id": $region_id},
                    dataType: "json",
                    success: function (result) {
                        $("#event_center").prop("disabled", false);
                        html = "<option value=''>Select Event Center</option>";
                        for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
                        }
                        $("#event_center").html(html);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //    console.log('ERRORS: ' + textStatus);
                    },
                });
            });
            /*End : Get Dynamic Center as per Region*/
        }


        $("#event_type").prop("disabled", true);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetcheventtypelevel') ?>',
            data: {"event_level": $event_level, 'wing_id': selectedwings},
            dataType: "json",
            success: function (result) {
                $("#event_type").prop("disabled", false);
                html = "<option value=''>Select Event Type</option>";
                for (i = 0; i < result.event_type.length; i++) {
                    //Get Child Array ,for nested event type
                    if (result.event_type[i].childs.length > 0)
                    {
                        html += "<optgroup label='" + result.event_type[i].event_type_name + "'>";
                        for (j = 0; j < result.event_type[i].childs.length; j++)
                        {
                            html += "<option value='" + result.event_type[i].childs[j].id + "' data-show_stayinfo='" + result.event_type[i].childs[j].show_stayinfo + "' data-show_outing='" + result.event_type[i].childs[j].show_outing + "' data-approval='" + result.event_type[i].childs[j].event_approval + "'>" + result.event_type[i].childs[j].event_type_name + "</option>";
                        }
                        html += "</optgroup>";
                    } else
                    {
                        html += "<option value='" + result.event_type[i].id + "' data-show_stayinfo='" + result.event_type[i].show_stayinfo + "' data-show_outing='" + result.event_type[i].show_outing + "' data-approval='" + result.event_type[i].event_approval + "'>" + result.event_type[i].event_type_name + "</option>";
                    }
                }
                $("#event_type").html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });

    });
    /*End : Fetch Event Type as per event Level*/
    /*Begin : Assign Check box value for stayover and outing info on event type*/
    $("#event_type").change(function () {
        var stayoverinfo = $('#event_type option:selected').attr('data-show_stayinfo');
        var showoutinginfo = $('#event_type option:selected').attr('data-show_outing');
        //Aasign to check , event need to approve or not
        $("#hidden_aprroval_required").attr('value', $('#event_type option:selected').attr('data-approval'));

        if (stayoverinfo == 1)
        {
//            $("#show_stayinfo").prop('checked', true);
            $("#show_stayinfo").iCheck('check');
            $("#show_stayinfo").attr("disabled", true);

            $("#hidden_is_stayover").attr('value', stayoverinfo);
            $(".nostay").addClass('hide');

            if ($("#event_level").val() != "")
            {
                if ($("#event_level").val() == 3)
                {   //Display Stayover info
                    {
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    }
                } else
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                }
            } else
            {
                $(".stayoverinfo").addClass('hide');
                $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            }
        } else
        {
//            $("#show_stayinfo").prop('checked', false);
            $("#show_stayinfo").iCheck('uncheck');
            $("#show_stayinfo").attr("disabled", false);

            $("#hidden_is_stayover").attr('value', stayoverinfo);
            $(".nostay").removeClass('hide');

            $(".stayoverinfo").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
        }
        if (showoutinginfo == 1)
        {
            //$("#show_outing").prop('checked', true);
            $("#show_outing").iCheck('check');
            $("#show_outing").attr("disabled", true);

            $("#hidden_is_outing").attr('value', showoutinginfo);
        } else
        {
//            $("#show_outing").prop('checked', false);
            $("#show_outing").iCheck('uncheck');
            $("#show_outing").attr("disabled", false);

            $("#hidden_is_outing").attr('value', showoutinginfo);
        }
    });


    $("#transportation_avail").change(function () {

        myvalue = $(this).val();
        if (myvalue == 1)
        {
            $(".trans_deadline,.transportation_required_option").removeClass('hide');
        } else
        {
            $(".trans_deadline,.transportation_required_option").addClass('hide');
        }
    });
    /*End : Assign Check box value for stayover and outing info on event type*/

    /*Begin : Display / Hide  Styover Date and Dropdown option if stayover option is checked*/

    //To Check stayover option checkbox is check

    $(document).on('ifChecked ifUnchecked', '#show_stayinfo', function (event) {

        if (event.type == 'ifChecked')
        {
            if ($("#event_level").val() != "")
            {
                if ($("#event_level").val() == 3)
                {   //Display Stayover info
                    {
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    }
                } else
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                }
            } else
            {
                $(".stayoverinfo").addClass('hide');
                $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
            }

        } else
        {

            $(".stayoverinfo").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
        }
    });


    /*Begin : To hide Event Region Option If Event Level is North America*/
    $("#event_level").change(function () {
        var event_level = $(this).val();

        if (event_level == 2 || event_level == "")
        {
            $(".region_center_div").addClass('hide');
        } else
        {
            $(".region_center_div").removeClass('hide');
        }
    });
    /*End : To hide Event Region Option If Event Level is North America*/


    /*Begin : To Change REgistration form Type As Per Selection Of Registration Fee Option*/
    $("#registration_fee").change(function () {
        var registration_fee = $(this).val();

        if (registration_fee == "Yes")
        {
            html = "<option value=''>Select Form Type</option><option value='2'>Bal - Balika Form</option><option value='3'>Default Form</option><option value='1'>Kishore - Kishori Form</option><option value='4'>RAM Form</option>";
            $("#form_type").html(html);
        } else
        {
            html = "<option value='5'>No Form</option>";
            $("#form_type").html(html);
        }
    });
    /*End : To Change REgistration form Type As Per Selection Of Registration Fee Option*/



    /*Begin : To Fetch Wing As per Selection Of Multiple Wing checkbox selection*/
    $(document).on('ifChecked ifUnchecked', '#multi_wing_event', function (event) {

        if (event.type == 'ifChecked')
        {
            var is_multiwing_checked = "yes";
            $("#hidden_check_multi_wing").val(1);
        } else
        {
            var is_multiwing_checked = "no";
            $("#hidden_check_multi_wing").val(0);
        }
        var current_wing = $('#eligible_wings option').map(function () {
            return $(this).val();
        }).get();
        if (current_wing.length > 0)
        {
            current_wing = current_wing;
        } else
        {
            current_wing = 0;
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetchmultiwing') ?>',
            data: {"hidden_check_multi_gender": $("#hidden_check_multi_gender").val(), "hidden_check_multi_wing": $("#hidden_check_multi_wing").val()},
            dataType: "json",
            success: function (result) {
                $("#eligible_wings").prop("disabled", false);
                html = "";
                for (i = 0; i < result.wings.length; i++) {
                    html += "<option value='" + result.wings[i].id + "'>" + result.wings[i].wingName + "</option>";
                }
                $("#eligible_wings").html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });


        $event_level = $("#event_level").val();
        //To Check stayover option checkbox is check
        if ($("#show_stayinfo").is(':checked'))
            var staycheck = 1;  // checked
        else
            var staycheck = 0;  // unchecked
        $(".stayoverinfo").addClass('hide');
        //If event level is center then filter as per wing

        if ($('#event_level').val() != "")
        {
            if ($("#event_level").val() == 3)
            {   //Display Stayover info
                if ($("#event_type").val() != "")
                {
                    if (staycheck == 1)
                    {
                        $("#show_stayinfo").iCheck('check');
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    }
                } else
                {
                    $("#show_stayinfo").iCheck('uncheck');
                    $(".stayoverinfo").addClass('hide');
                    $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
                    //$("#show_stayinfo").prop('checked', false);
                    $("#show_stayinfo").attr("disabled", false);

                }

                //If event level is center,check wing is selected or not
                if (($('#eligible_wings').val()) == null)
                {
                    var selectedwings = 0;
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                    $('#event_type,#event_level').prop('selectedIndex', 0);
                    toastr.info("Please Select Eligible Wing.", 'Info');
                    return false;
                } else
                {
                    var selectedwings = [];
                    $.each($("#eligible_wings option:selected"), function () {
                        selectedwings.push($(this).val());
                    });

                    //For Center Level Display Payemnt Options
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                }
            } else
            {
                if (staycheck == 1)
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                }

                if ($("#event_level").val() == 1)
                { // For Regional Event
//                    $(".payment_region_outer").removeClass('hide');
//                    $(".payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() == 2)
                { //For North American Level
                    $(".payment_country_outer").removeClass('hide');
                    $(".payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() >= 3)
                { //For Other Newly Added Level
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                } else
                {
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                }

            }
        } else
        {
            $(".stayoverinfo").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');

            $(".payment_country_outer,.payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
        }


        if ($("#event_level").val() >= 3)
        {
            /*Begin : Get Dynamic Center as per Region*/
            $("#eligible_regions").change(function () {
                $region_id = $(this).val();
                $("#event_center").prop("disabled", true);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: '<?= action('AdminuserController@postRegion_based_center_ajax') ?>',
                    data: {"region_id": $region_id},
                    dataType: "json",
                    success: function (result) {
                        $("#event_center").prop("disabled", false);
                        html = "<option value=''>Select Event Center</option>";
                        for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
                        }
                        $("#event_center").html(html);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //    console.log('ERRORS: ' + textStatus);
                    },
                });
            });
            /*End : Get Dynamic Center as per Region*/
        }


        $("#event_type").prop("disabled", true);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetcheventtypelevel') ?>',
            data: {"event_level": $event_level, 'wing_id': selectedwings},
            dataType: "json",
            success: function (result) {
                $("#event_type").prop("disabled", false);
                html = "<option value=''>Select Event Type</option>";
                for (i = 0; i < result.event_type.length; i++) {
                    //Get Child Array ,for nested event type
                    if (result.event_type[i].childs.length > 0)
                    {
                        html += "<optgroup label='" + result.event_type[i].event_type_name + "'>";
                        for (j = 0; j < result.event_type[i].childs.length; j++)
                        {
                            html += "<option value='" + result.event_type[i].childs[j].id + "' data-show_stayinfo='" + result.event_type[i].childs[j].show_stayinfo + "' data-show_outing='" + result.event_type[i].childs[j].show_outing + "' data-approval='" + result.event_type[i].childs[j].event_approval + "'>" + result.event_type[i].childs[j].event_type_name + "</option>";
                        }
                        html += "</optgroup>";
                    } else
                    {
                        html += "<option value='" + result.event_type[i].id + "' data-show_stayinfo='" + result.event_type[i].show_stayinfo + "' data-show_outing='" + result.event_type[i].show_outing + "' data-approval='" + result.event_type[i].event_approval + "'>" + result.event_type[i].event_type_name + "</option>";
                    }
                }
                $("#event_type").html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    });
    /*End : To Fetch Wing As per Selection Of Multiple Wing checkbox selection*/


    /*Begin : To Fetch Wing As per Selection Of Multiple Gender checkbox selection*/
    $(document).on('ifChecked ifUnchecked', '#multi_gender_event', function (event) {

        if (event.type == 'ifChecked')
        {
            var is_multigender_checked = "yes";
            $("#hidden_check_multi_gender").val(1);
        } else
        {
            var is_multigender_checked = "no";
            $("#hidden_check_multi_gender").val(0);
        }

        var current_wing = $('#eligible_wings option').map(function () {
            return $(this).val();
        }).get();
        if (current_wing.length > 0)
        {
            current_wing = current_wing;
        } else
        {
            current_wing = 0;
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetchmultiwing') ?>',
            data: {"hidden_check_multi_gender": $("#hidden_check_multi_gender").val(), "hidden_check_multi_wing": $("#hidden_check_multi_wing").val()},
            dataType: "json",
            success: function (result) {
                $("#eligible_wings").prop("disabled", false);
                html = "";
                for (i = 0; i < result.wings.length; i++) {
                    html += "<option value='" + result.wings[i].id + "'>" + result.wings[i].wingName + "</option>";
                }
                $("#eligible_wings").html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });


        $event_level = $("#event_level").val();
        //To Check stayover option checkbox is check
        if ($("#show_stayinfo").is(':checked'))
            var staycheck = 1;  // checked
        else
            var staycheck = 0;  // unchecked
        $(".stayoverinfo").addClass('hide');
        //If event level is center then filter as per wing

        if ($('#event_level').val() != "")
        {
            if ($("#event_level").val() == 3)
            {   //Display Stayover info
                if ($("#event_type").val() != "")
                {
                    if (staycheck == 1)
                    {
                        $("#show_stayinfo").iCheck('check');
                        $(".stayoverinfo").removeClass('hide');
                        $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    }
                } else
                {
                    $("#show_stayinfo").iCheck('uncheck');
                    $(".stayoverinfo").addClass('hide');
                    $(".stayoverenddate,.stayoverstartdate").addClass('hide');
                    $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');
                    //$("#show_stayinfo").prop('checked', false);
                    $("#show_stayinfo").attr("disabled", false);

                }



                //If event level is center,check wing is selected or not
                if (($('#eligible_wings').val()) == null)
                {
                    var selectedwings = 0;
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                    $('#event_type,#event_level').prop('selectedIndex', 0);
                    toastr.info("Please Select Eligible Wing.", 'Info');
                    return false;
                } else
                {
                    var selectedwings = [];
                    $.each($("#eligible_wings option:selected"), function () {
                        selectedwings.push($(this).val());
                    });

                    //For Center Level Display Payemnt Options
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                }
            } else
            {
                if (staycheck == 1)
                {
                    $(".stayoverenddate,.stayoverstartdate").removeClass('hide');
                }

                if ($("#event_level").val() == 1)
                { // For Regional Event
//                    $(".payment_region_outer").removeClass('hide');
//                    $(".payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() == 2)
                { //For North American Level
                    $(".payment_country_outer").removeClass('hide');
                    $(".payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
                } else if ($("#event_level").val() >= 3)
                { //For Other Newly Added Level
                    $(".payment_center_outer,.center_div").removeClass('hide');
                    $(".payment_region_outer,.payment_country_outer").addClass('hide');
                } else
                {
                    $(".payment_region_outer,.payment_country_outer,.payment_center_outer,.center_div").addClass('hide');
                }

            }
        } else
        {
            $(".stayoverinfo").addClass('hide');
            $(".stayoverenddate,.stayoverstartdate").addClass('hide');
            $(".stayoverstartdate1,.stayoverstartdate2").addClass('hide');

            $(".payment_country_outer,.payment_region_outer,.payment_center_outer,.center_div").addClass('hide');
        }


        if ($("#event_level").val() >= 3)
        {
            /*Begin : Get Dynamic Center as per Region*/
            $("#eligible_regions").change(function () {
                $region_id = $(this).val();
                $("#event_center").prop("disabled", true);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    },
                    type: 'POST',
                    url: '<?= action('AdminuserController@postRegion_based_center_ajax') ?>',
                    data: {"region_id": $region_id},
                    dataType: "json",
                    success: function (result) {
                        $("#event_center").prop("disabled", false);
                        html = "<option value=''>Select Event Center</option>";
                        for (i = 0; i < result.centers.length; i++) {
                            html += "<option value='" + result.centers[i].id + "'>" + result.centers[i].CenterName + "</option>";
                        }
                        $("#event_center").html(html);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //    console.log('ERRORS: ' + textStatus);
                    },
                });
            });
            /*End : Get Dynamic Center as per Region*/
        }


        $("#event_type").prop("disabled", true);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            },
            type: 'POST',
            url: '<?= action('AdminmanageeventController@postFetcheventtypelevel') ?>',
            data: {"event_level": $event_level, 'wing_id': selectedwings},
            dataType: "json",
            success: function (result) {
                $("#event_type").prop("disabled", false);
                html = "<option value=''>Select Event Type</option>";
                for (i = 0; i < result.event_type.length; i++) {
                    //Get Child Array ,for nested event type
                    if (result.event_type[i].childs.length > 0)
                    {
                        html += "<optgroup label='" + result.event_type[i].event_type_name + "'>";
                        for (j = 0; j < result.event_type[i].childs.length; j++)
                        {
                            html += "<option value='" + result.event_type[i].childs[j].id + "' data-show_stayinfo='" + result.event_type[i].childs[j].show_stayinfo + "' data-show_outing='" + result.event_type[i].childs[j].show_outing + "' data-approval='" + result.event_type[i].childs[j].event_approval + "'>" + result.event_type[i].childs[j].event_type_name + "</option>";
                        }
                        html += "</optgroup>";
                    } else
                    {
                        html += "<option value='" + result.event_type[i].id + "' data-show_stayinfo='" + result.event_type[i].show_stayinfo + "' data-show_outing='" + result.event_type[i].show_outing + "' data-approval='" + result.event_type[i].event_approval + "'>" + result.event_type[i].event_type_name + "</option>";
                    }
                }
                $("#event_type").html(html);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //    console.log('ERRORS: ' + textStatus);
            },
        });
    });
    /*End : To Fetch Wing As per Selection Of Multiple Gender checkbox selection*/
})(jQuery);
</script>
@endsection

@section('RenderBody')
<section class="content-header">
    <h1>Add Event
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url('/admin/user/dashboard') }}"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="javascript:void(0);">Manage</a></li><li><a href="javascript:void(0);">Events</a></li>
        <li><a href="{{ url('/admin/manageevent/events') }}">View Events</a></li><li class="active">Add Event</li>
    </ol>
</section>

<section class="content">
    {!! Form::open(array('url' => 'admin/manageevent/saveevent' , 'class' => 'form-horizontal','id' => 'addevent_form', 'files' => true)) !!}
    <!--Begin : Event Details Section-->
    <div class="box box-info">
        @if (count($errors) > 0)
        <?php if ($errors->first('event_level')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_level') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('event_type')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_type') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('event_title')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_title') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('event_location')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_location') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('event_zip')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_zip') !!} </label><br/>
        <?php } ?>    
        <?php if ($errors->first('event_start_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_start_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('event_end_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('event_end_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('early_reg_start_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('early_reg_start_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('reg_start_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('reg_start_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('reg_end_date')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('reg_end_date') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('maximum_attendence')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('maximum_attendence') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('currency_type')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('currency_type') !!} </label><br/>
        <?php } ?>    
        <?php if ($errors->first('transportation_deadline')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('transportation_deadline') !!} </label><br/>
        <?php } ?>

        <?php if ($errors->first('form_type')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('form_type') !!} </label><br/>
        <?php } ?>
        <?php if ($errors->first('payment_country')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('payment_country') !!} </label><br/>
        <?php } ?>
       <?php if ($errors->first('host_region')) { ?>
            <label class="col-sm-3"></label>
            <label class="valerror col-md-9" >{!! $errors->first('host_region') !!} </label><br/>
        <?php } ?>
        @endif

        <!--Begin :Event Details Section-->
        <div class="box-header">
            <!--            <h3 class="box-title">Registration Information</h3>-->
            <div class="box-body padding-left-25 padding-right-25 form-horizontal">
                <div class="box box-default emergencyborder">
                    <div class="box-header with-border">
                        <h3 class="box-title">Event Details</h3>
                    </div>
                    <div class="box-body">
                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                    <div class="form-group">
                        <label for="show_outing" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Multi Wing Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <input type="checkbox" name="multi_wing_event" id="multi_wing_event" value="1">
                            <label for="multi_wing_event" class="control-label">This event includes multi wing</label>
                        </div>
                    </div>
                    @else
                    @permission('create.multi.wing.event.eventmanagement')
                    <div class="form-group">
                        <label for="show_outing" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Multi Wing Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <input type="checkbox" name="multi_wing_event" id="multi_wing_event" value="1">
                            <label for="multi_wing_event" class="control-label">This event includes multi wing</label>
                        </div>
                    </div>
                    @endpermission
                    @endif

                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                    <div class="form-group">
                        <label for="show_stayinfo" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Multi Gender Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <input type="checkbox" name="multi_gender_event" id="multi_gender_event" value="1">
                            <label for="multi_gender_event" class="control-label">This event includes multi gender</label>
                        </div>
                    </div>
                    @else
                    @permission('create.multi.gender.event.eventmanagement')
                    <div class="form-group">
                        <label for="show_stayinfo" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Multi Gender Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <input type="checkbox" name="multi_gender_event" id="multi_gender_event" value="1">
                            <label for="multi_gender_event" class="control-label">This event includes multi gender</label>
                        </div>
                    </div>
                    @endpermission
                    @endif

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Eligible Wing<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <select class="form-control" name="eligible_wings[]" id="eligible_wings" multiple>
                                <?php
                                if (isset($wings) && !empty($wings)) {
                                    for ($i = 0; $i < count($wings); $i++) {
                                        ?>
                                        <option value="<?php echo $wings[$i]->id; ?>"><?php echo $wings[$i]->wingName; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Level<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="event_level" id="event_level">
                                <option value="">Select Event Level</option>
                                <?php
                                if (isset($event_level) && !empty($event_level)) {
                                    for ($i = 0; $i < count($event_level); $i++) {
                                        ?>
                                        <option value="<?php echo $event_level[$i]->id; ?>"><?php echo $event_level[$i]->event_level_name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Type<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select class="form-control" name="event_type" id="event_type">
                                <option value="">Select Event Type</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Title<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <input type="text" class="form-control"  value="{{ old('event_title') }}" name="event_title" id="event_title" maxlength="50" placeholder="Event Title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Description<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                            <textarea class="form-control" name="event_description" rows="5" id="event_description" placeholder="Event Description">{{ old('event_description') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Location<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin location_margin">
                            <input type="text" class="form-control"  value="{{ old('event_location') }}" name="event_location" id="event_location" placeholder="Event Location">
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Zip/Postal Code<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ old('event_zip') }}" name="event_zip" id="event_zip" placeholder="Zip/Postal Code">
                        </div>
                    </div>

                    <input type="hidden"  value="2" name="action_type" id="action_type" maxlength="50">
                    <input type="hidden"  value="0" name="hidden_is_stayover" id="hidden_is_stayover">
                    <input type="hidden"  value="0" name="hidden_is_outing" id="hidden_is_outing">
                    <input type="hidden"  value="0" name="hidden_aprroval_required" id="hidden_aprroval_required">
                    <input type="hidden"  value="0" name="hidden_check_multi_wing" id="hidden_check_multi_wing">
                    <input type="hidden"  value="0" name="hidden_check_multi_gender" id="hidden_check_multi_gender">

                    <div class="form-group region_center_div hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Region<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="eligible_regions" id="eligible_regions">
                                <option value="">Select Event Region</option>
                                <?php
                                if (isset($regions) && !empty($regions)) {
                                    for ($i = 0; $i < count($regions); $i++) {
                                        ?>
                                        <option value="<?php echo $regions[$i]->id; ?>"><?php echo $regions[$i]->RegionName; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label center_div hide">Event Center<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin center_div hide">
                            <select class="form-control" name="event_center" id="event_center">
                                <option value="">Select Event Center</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Start Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <input type="text" class="form-control"  value="{{ old('event_start_date') }}" name="event_start_date" id="event_start_date" maxlength="50" placeholder="Event Start Date">
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event End Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ old('event_end_date') }}" name="event_end_date" id="event_end_date" maxlength="50" placeholder="Event End Date">
                        </div>
                    </div>

                </div>
                </div>
            </div>
        </div>
    </div>
        <!--End : Event Details Section-->
        <!--Begin : Registration Details Section-->
        <div class="box box-info">
        <div class="box-header">
            <!--            <h3 class="box-title">Registration Information</h3>-->
            <div class="box-body padding-left-25 padding-right-25 form-horizontal">
                <div class="box box-default emergencyborder">
                    <div class="box-header with-border">
                        <h3 class="box-title">Registration Details</h3>
                    </div>
                    <div class="box-body">

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Early Registration Start Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <input type="text" class="form-control"  value="{{ old('early_reg_start_date') }}" name="early_reg_start_date" id="early_reg_start_date" maxlength="50" placeholder="Early Registration Start Date">
                        </div>

                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Registration Start Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ old('reg_start_date') }}" name="reg_start_date" id="reg_start_date" maxlength="50" placeholder="Registration Start Date">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Registration End Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <input type="text" class="form-control"  value="{{ old('reg_end_date') }}" name="reg_end_date" id="reg_end_date" maxlength="50" placeholder="Registration End Date">
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Maximum Attendance<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ old('maximum_attendence') }}" name="maximum_attendence" id="maximum_attendence" maxlength="50" placeholder="Maximum Attendance">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Registration Fee<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="registration_fee" id="registration_fee">
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Registration Form<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select aria-required="true" aria-invalid="false" class="form-control valid" name="form_type" id="form_type">
                                <option value="">Select Form Type</option>
                                <option value="2">Bal - Balika Form</option>
                                <option value="3">Default Form</option>
                                <option value="1">Kishore - Kishori Form</option>
                                <option value="4">RAM Form</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Currency Type<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select aria-required="true" aria-invalid="false" class="form-control valid" name="currency_type" id="currency_type">
                                <?php
                                if (isset($currency_type_list) && !empty($currency_type_list)) {
                                    for ($i = 0; $i < count($currency_type_list); $i++) {
                                        ?>
                                        <option value="<?php echo $currency_type_list[$i]; ?>"><?php echo ucwords($currency_type_list[$i]); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Event Status<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select class="form-control" name="event_status">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Is Transportation Available?<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="transportation_avail" id="transportation_avail">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label trans_deadline">Transportation Info Deadline<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control trans_deadline"  value="{{ old('transportation_deadline') }}" name="transportation_deadline" id="transportation_deadline" maxlength="50" placeholder="Transportation Info Deadline">
                        </div>

                    </div>
                        
                        
                    @if(Auth::user()->user_role_type != "" && Auth::user()->user_role_type == 4)
                    <div class="form-group">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label transportation_required_option">Is Transportation Required?<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin transportation_required_option">
                            <select class="form-control" name="transporation_require" id="transporation_require">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stripe Key Override</label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ old('stripe_client_secret') }}" name="stripe_client_secret" id="stripe_client_secret" maxlength="50" placeholder="Stripe Client Secret Key">
                        </div>
                    </div>
                    @else
                    <div class="form-group transportation_required_option">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Is Transportation Required?<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 userbottommargin">
                            <select class="form-control" name="transporation_require" id="transporation_require">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    @endif
                    
                    
                    <div class="form-group payment_country_outer hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Payment Country<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select class="form-control" name="payment_country" id="payment_country">
                                <option value="">Select Payment Country</option>
                                <?php
                                if (isset($country) && !empty($country)) {
                                    for ($i = 0; $i < count($country); $i++) {
                                        ?>
                                        <option value="<?php echo $country[$i]->id; ?>"><?= ucwords($country[$i]->country_name); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Host Region<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select class="form-control" name="host_region" id="host_region">
                                <option value="">Select Host Region</option>
                                <?php
                                if (isset($regions) && !empty($regions)) {
                                    for ($i = 0; $i < count($regions); $i++) {
                                        ?>
                                        <option value="<?php echo $regions[$i]->id; ?>"><?= ucwords($regions[$i]->RegionName); ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="show_outing" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Outing Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <!--                            <div style="width:225px;" class="checking">-->
                            <input type="checkbox" name="show_outing" id="show_outing" value="1">
                            <label for="show_outing" class="control-label">This event includes an outing</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="show_stayinfo" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover Option</label>
                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <input type="checkbox" name="show_stayinfo" id="show_stayinfo" value="1">
                            <label for="show_stayinfo" class="control-label">This event includes a stayover</label>
                        </div>
                    </div>
                    <div class="form-group stayoverinfo hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover Information<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <select name="stayover_info" id="stayover_info" class="form-control">
                                <option value="">Select Stayover Type</option>
                                <option value="1" class="nostay">No Stayover</option>
                                <option value="2">One 1-night Stayover</option>
                                <option value="3">One 2-night Stayover</option>
                                <option value="4">Two 1-night Stayover</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group stayoverstartdate hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover Start Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ old('stay_start_date') }}" name="stay_start_date" id="stay_start_date" maxlength="50" placeholder="Stayover Start Date">
                        </div>
                    </div>
                    <div class="form-group stayoverenddate hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover End Date<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ old('stay_end_date') }}" name="stay_end_date" id="stay_end_date" maxlength="50" placeholder="Stayover End Date">
                        </div>
                    </div>
                    <div class="form-group stayoverstartdate1 hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover Start Date1<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ old('stay_start_date1') }}" name="stay_start_date1" id="stay_start_date1" maxlength="50" placeholder="Stayover Start Date1">
                        </div>
                    </div>
                    <div class="form-group stayoverstartdate2 hide">
                        <label for="inputEmail3" class="col-xs-12 col-sm-12 col-md-12 col-lg-2 control-label">Stayover Start Date2<em class="mandatory-field">*</em></label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <input type="text" class="form-control"  value="{{ old('stay_start_date2') }}" name="stay_start_date2" id="stay_start_date2" maxlength="50" placeholder="Stayover Start Date2">
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                            <input type="submit" class="btn btn-primary" value="Submit" id="submit">
                            <a href="{!! URL::to('admin/manageevent/events') !!}" class="btn reset-btn btn-danger">Cancel</a>
                        </div>
                    </div>

                </div>
                </div>
            </div>
        </div>
        <!--End : Registration Details Section-->

        <!--        </div>-->
    </div>
        <!--End : Registration Details Section-->
    {!! Form::close() !!}

</section>
@endsection
