
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ url (asset('/public/theme/js/jquery-3.2.1.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/popper.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/bootstrap.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/vendors/owl-carousel/owl.carousel.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/vendors/lightbox/simpleLightbox.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/jquery.ajaxchimp.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/mail-script.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/vendors/nice-select/js/jquery.nice-select.min.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/stellar.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/custom.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/js/main.js')) }}"></script>
        <script src="{{ url (asset('/public/theme/plugin/easing/easing.min.js')) }}"></script>
        @yield('AdditionalVendorScriptsInclude')