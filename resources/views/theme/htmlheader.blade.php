<head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="{{ url (asset('/public//theme/image/favicon.png')) }}" type="image/png">
        <title>Sahastrar Dham @yield('htmlheader_title', 'Your title here')</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ url (asset('/public/theme/css/bootstrap.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/vendors/linericon/style.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/css/font-awesome.min.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/vendors/owl-carousel/owl.carousel.min.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/vendors/lightbox/simpleLightbox.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/vendors/nice-select/css/nice-select.css')) }}">
        <!-- main css -->
        <link rel="stylesheet" href="{{ url (asset('/public/theme/css/style.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/css/responsive.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/css/custom.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/css/bulk.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/css/respons.css')) }}">
        <link rel="stylesheet" href="{{ url (asset('/public/theme/css/under_construction.css')) }}">
        @yield('AdditionalVendorCssInclude')
    </head>