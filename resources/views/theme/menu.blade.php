<!--================Header Area =================-->
        <header class="header_area">
            <div class="header_top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-5">
                            <ul class="nav social_icon">
                                <li><a href="https://www.facebook.com/sahasrardhamnargol/" target="blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-7">
                            <div class="top_btn d-flex justify-content-end">
<!--                                <a href="#">My Account</a>-->
                                <a href="#">Donate Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
<!--                    <a class="navbar-brand logo_h" href="index.html"><img src="{{ url (asset('/public/theme/image/Logo.png')) }}" alt=""></a>-->
                    <a class="navbar-brand logo_h" href="index.html"><img src="{{ url (asset('/public/logo/l2.png')) }}" alt="" style="width:86px; height: 30px;"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="{{ url('/')}}">Home</a></li> 
                            <li class="nav-item"><a class="nav-link" href="{{ url('/projects')}}">About Project</a></li>
                            <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Health Centre</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="nav-link" href="{{ url('/healthcentre')}}">Overview</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ url('/healthcentre')}}#facility">Facility</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ url('/healthcentre')}}#doctor_list">Doctor List</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ url('/healthcentre')}}#registration">Registration</a></li>
                                </ul>
                            </li>
                            <li class="nav-item submenu dropdown">
                                <a class="nav-link" href="{{ url('/school')}}">School</a>
<!--                                <a href="{{ url('/school')}}" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">School</a>-->
<!--                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="nav-link" href="{{ url('/school')}}">Overview</a></li>
                                    <li class="nav-item"><a class="nav-link" href="event-details.html">Facility</a></li>
                                    <li class="nav-item"><a class="nav-link" href="event-details.html">Staff List</a></li>
                                </ul>-->
                            </li>
                            <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Event</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="nav-link" href="{{ url('/events')}}">Past Events</a></li>
                                    <li class="nav-item"><a class="nav-link" href="{{ url('/events')}}#upcoming_events">Upcoming Events</a></li>
                                </ul>
                            </li>
                            <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Meditation Hall</a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a class="nav-link" href="{{ url('/meditationhall')}}">Overview</a></li>
                                    <li class="nav-item"><a class="nav-link" href="event-details.html">Facility</a></li>
                                </ul>
                            </li>
                              
                            
                            <li class="nav-item"><a class="nav-link" href="{{ url('/contact')}}">Contact</a></li>
                        </ul>
                    </div> 
                </div>
            </nav>
        </header>
        <!--================Header Area =================-->