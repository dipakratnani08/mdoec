<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@include('theme.htmlheader')

<body>
<!-- /.content-wrapper -->
    @include('theme.menu')
    @yield('main-content')

    @include('theme.footer')

@include('theme.scripts')

</body>
</html>