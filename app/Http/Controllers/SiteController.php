<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use json;
use Config;
use Redirect;
use Session;
use Illuminate\Support\Facades\DB;
use Mail;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\ServerError;
use Razorpay\Api\Errors\GatewayError;
use Razorpay\Api\Errors\SignatureVerificationError;
use Razorpay\Api\Errors\BadRequestError;
use Barryvdh\DomPDF\Facade as PDF;
use App;

class SiteController extends Controller {
    /* Use : Load Index Page
      Name: getIndex
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getIndex(Request $request) {
        return view('sitetheme/pages/home');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getAboutus(Request $request) {
        return view('sitetheme/pages/about_us');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getStudyInAustralia(Request $request) {
        return view('sitetheme/pages/study_in_australia');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getStudyInCanada(Request $request) {
        return view('sitetheme/pages/study_in_canada');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getStudyInUk(Request $request) {
        return view('sitetheme/pages/study_in_uk');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getStudyInIreland(Request $request) {
        return view('sitetheme/pages/study_in_ireland');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getStudyInNewzealand(Request $request) {
        return view('sitetheme/pages/study_in_newzealand');
    }


    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getCareerCounselling(Request $request) {
        return view('sitetheme/pages/career_counselling');
    }


    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getApplicationAssistance(Request $request) {
        return view('sitetheme/pages/application_assistance');
    }


    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getVisaApplicationAssistance(Request $request) {
        return view('sitetheme/pages/visa_application_assistance');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getAccommodation(Request $request) {
        return view('sitetheme/pages/accommodation');
    }


    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getPreDepartureSupport(Request $request) {
        return view('sitetheme/pages/pre_departure_support');
    }


    /* Use : Load About Project Page
      Name: getTravel
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getTravel(Request $request) {
        return view('sitetheme/pages/travel');
    }


    /* Use : Load About Project Page
      Name: getTravel
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getForex(Request $request) {
        return view('sitetheme/pages/forex');
    }


    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getArt(Request $request) {
        return view('sitetheme/pages/art');
    }


    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getArchitecture(Request $request) {
        return view('sitetheme/pages/architecture');
    }


    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getBusinessCommerce(Request $request) {
        return view('sitetheme/pages/business');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getComputing(Request $request) {
        return view('sitetheme/pages/computing');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getEngineering(Request $request) {
        return view('sitetheme/pages/engineering');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getHospitality(Request $request) {
        return view('sitetheme/pages/hospitality');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getHealth(Request $request) {
        return view('sitetheme/pages/health');
    }

    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getLaw(Request $request) {
        return view('sitetheme/pages/law');
    }


    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getContact(Request $request) {
        return view('sitetheme/pages/contactus');
    }



    /* Use : Load About Project Page
      Name: getProjects
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getAppointment(Request $request) {
        return view('sitetheme/pages/appointment');
    }


    
    /* Use : For About Project
      Name: getProjects
      Date: 23rd Jun 2018
      Created By: Dipak Ratnani */

    public function postSavecontactus(Request $request) {
        //        //Define Rules
        $rules = array('first_name' => 'required',
            'mobile_no' => 'required|numeric',
            'email' => 'required|email',
            'destination' => 'required',
            'intake' => 'required',
            'city' => 'required',
        );
        $messages = [
            'first_name.required' => 'Please enter your first name',
            'mobile_no.required' => 'Please enter your mobile number',
            'email.required' => 'Please enter your email',
            'destination.required' => 'Please select preferred destination',
            'intake.required' => 'Please select preferred intake',
            'city.required' => 'Please enter your city',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('/contact')->withErrors($validator)->withInput();
        } else {
            $savearray = array('first_name' => trim($request->input('first_name')),
              'last_name' => trim($request->input('last_name')),
              'mobile_no' => trim($request->input('mobile_no')),
              'email' => trim($request->input('email')),
              'destination' => trim($request->input('destination')),
              'intake' => trim($request->input('intake')),
              'city' => trim($request->input('city')),
              "created_date" => date('Y-m-d H:i:s'));

            $save_record = DB::table('contact_us')->insert($savearray);
            
            $admin_email_list_record = explode(',', env('EVENT_ADMIN_EMAILS'));
            if (isset($save_record) && !empty($save_record)) {

                try {
                    $sendown_mail = Mail::send('emails.contact_us', ['user_name' => ucwords(trim($request->input("first_name"))), 'user_email' => trim($request->input("email")), 'subject' => 'Enquiry', 'destination' => ucwords(trim($request->input('destination'))), 'intake' => ucwords(trim($request->input('intake')))], function($message) use($admin_email_list_record){
                                $message->from('noreply@mdoec.in', 'MDOEC');
                                $message->to($admin_email_list_record);
                                $message->subject('MDOEC : New Enquiry');
                            });
                } catch (\Exception $e) {
                    $sendown_mail = $e->getMessage();
                }
                return redirect('/contact')->with('success_message', Config::get('constants.CONTACT_US_SAVE_SUCCESS'));
            } else {
                //fail
                return redirect('/contact')->with('error_message', Config::get('constants.CONTACT_US_SAVE_FAIL'));
            }
        }
    }



    /* Use : For About Project
      Name: getProjects
      Date: 23rd Jun 2018
      Created By: Dipak Ratnani */

    public function postSaveappintment(Request $request) {
        //        //Define Rules
        $rules = array('first_name' => 'required',
            'mobile_no' => 'required|numeric',
            'email' => 'required|email',
            'destination' => 'required',
            'intake' => 'required',
            'city' => 'required',
            'appointment_datetime' => 'required',
        );
        $messages = [
            'first_name.required' => 'Please enter your first name',
            'mobile_no.required' => 'Please enter your mobile number',
            'email.required' => 'Please enter your email',
            'destination.required' => 'Please select preferred destination',
            'intake.required' => 'Please select preferred intake',
            'city.required' => 'Please enter your city',
            'appointment_datetime.required' => 'Please enter your Appointment Date-Time',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('/appointment')->withErrors($validator)->withInput();
        } else {
            $savearray = array('first_name' => trim($request->input('first_name')),
              'last_name' => trim($request->input('last_name')),
              'mobile_no' => trim($request->input('mobile_no')),
              'email' => trim($request->input('email')),
              'destination' => trim($request->input('destination')),
              'intake' => trim($request->input('intake')),
              'city' => trim($request->input('city')),
              'appointment_datetime' => trim($request->input('appointment_datetime')),
              'status' => 'Not Attempted',
              "created_date" => date('Y-m-d H:i:s'));

            $save_record = DB::table('appointment')->insert($savearray);
            
            $admin_email_list_record = explode(',', env('EVENT_ADMIN_EMAILS'));
            if (isset($save_record) && !empty($save_record)) {

                try {
                    $sendown_mail = Mail::send('emails.appointment', ['user_name' => ucwords(trim($request->input("first_name"))), 'user_email' => trim($request->input("email")), 'subject' => 'Enquiry', 'destination' => ucwords(trim($request->input('destination'))), 'intake' => ucwords(trim($request->input('intake'))), 'appointment_datetime' => ucwords(trim($request->input('appointment_datetime')))], function($message) use($admin_email_list_record){
                                $message->from('noreply@mdoec.in', 'MDOEC');
                                $message->to($admin_email_list_record);
                                $message->subject('MDOEC : New Appointment');
                            });
                } catch (\Exception $e) {
                    $sendown_mail = $e->getMessage();
                }
                
                return redirect('/appointment')->with('success_message', Config::get('constants.CONTACT_US_SAVE_SUCCESS'));
            } else {
                //fail
                return redirect('/appointment')->with('error_message', Config::get('constants.CONTACT_US_SAVE_FAIL'));
            }
        }
    }

}
