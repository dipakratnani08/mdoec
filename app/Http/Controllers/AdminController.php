<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use json;
use Config;
use Redirect;
use Session;
use Illuminate\Support\Facades\DB;
use Mail;
use Razorpay\Api\Api;
use Razorpay\Api\Errors\ServerError;
use Razorpay\Api\Errors\GatewayError;
use Razorpay\Api\Errors\SignatureVerificationError;
use Razorpay\Api\Errors\BadRequestError;
use App;
use Datatables;
use Auth;
use Illuminate\Support\Facades\Input;
use File;
use Exception;
use Illuminate\Database\QueryException;
use App\Http\Controllers\Controller;
use Hash;


class AdminController extends Controller {
    
    public function __construct() {
        //to check user login  system or not
        $this->middleware('auth', ['except' => 'getIndex']);
    }
    
    /* Use : Load Index Page
      Name: getIndex
      Date: 30th Jan 2019
      Created By: Dipak Ratnani */

    public function getIndex(Request $request) {
        if(Auth::user())
        {
            return redirect("admin/home");
        }
        else
        {
            return view('admin/auth/login');
        }
        
    }

    /* Use : Load Home Page
      Name: getHome
      Date: 4th Aug 2018
      Created By: Dipak Ratnani */

    public function getHome(Request $request) {
        return view('admin/pages/dashboard');
    }
    
    
    
    /* Use : Load Edit profile page
      Name: getEdituser
      Date: 13th May 2019
      Created By: Dipak Ratnani */

    public function getEdituser(Request $request) {
        
        $data['user'] = DB::table('users')->where('id',Auth::user()->id)->first();
        return view('admin/pages/edituser',$data);
    }
    
    
    
    /**
     * Function : postUpdateuser()
     * Purpose  : To save changes of user profile
     * @author  : Agile Dev
     * @date-Created :13th May 2019
     */
    public function postUpdateuser(Request $request) {
        //Validation define
        $rules = array(
            'name' => 'required',
            'email' => 'required|email',
        );
        $messages = [
            'firstname.required' => 'Please Enter Name',
            'email.required' => 'Please Enter Email',
            'email.email' => 'Please Enter Valid Email Address',
        ];
        //Assign rule and data to validator
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            //If validation become fail ,then redirect to back
            return redirect('admin/edituser/')
                            ->withErrors($validator) // send back all errors to the addfaq form
                            ->withInput();
        } else {
                $user_array = array(
                    'email' => strtolower(trim($request->input("email"))),
                    'name' => ucwords(trim($request->input("name"))),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                
                 if (trim($request->input('password')) != '') {
                    $user_array['password'] = Hash::make(trim($request->input("password")));
                }
                
                $update_user = DB::table('users')->where('id', Auth::user()->id)->update($user_array);
                return redirect('admin/edituser')->with('message', "Profile Updated");
            }
        }



    /**
     * Function : postFetchactivityloglist()
     * Purpose  : To fetch activity log list on ajax call
     * @author  : Agile Dev
     * @date-Created :7th Jun 2018
     */
    public function postFetchactivityloglist(Request $request) {
        //To check region Id and gnder
        //Get Input For Filter Record
        $year = $request->input("year");
        $month = trim($request->input("month"));
        $activity_fromdate = trim($request->input("activity_fromdate"));
        $activity_todate = trim($request->input("activity_todate"));
        $activity_type = trim($request->input("activity_type"));
        $search_id = trim($request->input("search_id"));
        
        
        //If User Has Permission then Display data
        $data = DB::table('appointment as al')
                ->select('al.*')
                ->orderby('al.created_date', 'desc');
        /* Begin : Filter */
        if ($year != "") {
            $data->where(DB::raw('YEAR(al.created_date)'), $year);
        }
        if ($month != "") {
            $data->where(DB::raw('MONTH(al.created_date)'), $month);
        }
        if ($activity_fromdate != "") {
            $activity_fromdate = date("Y-m-d", strtotime($activity_fromdate));
            $data->whereRaw("DATE(al.created_date) >= '" . $activity_fromdate . "'");
        }
        if ($activity_todate != "") {
            $activity_todate = date("Y-m-d", strtotime($activity_todate));
            $data->whereRaw("DATE(al.created_date) <= '" . $activity_todate . "'");
        }
        if ($activity_type != "") {
            $data->where('al.activity_type', $activity_type);
        }
        if ($search_id != "") {
            $querydata = "(al.id LIKE '%$search_id%' OR al.name LIKE '%$search_id%' OR al.email LIKE '%$search_id%' OR al.  subject LIKE '%$search_id%' OR al.message LIKE '%$search_id%')";
            $data->whereRaw($querydata);
        }
        /* End: Filter */

        return Datatables::of($data)
                  ->edit_column('status', '<select class="action form-control" name="action" id="action" appointment_id={{$id}}><option value="">Select Status</option><option value="Not Attempted" @if($status == "Not Attempted") selected @endif>Not Attempted</option><option value="Attempted" @if($status == "Attempted") selected @endif>Attempted</option><option value="Contacted" @if($status == "Contacted") selected @endif>Contacted</option><option value="New Opportunity" @if($status == "New Opportunity") selected @endif>New Opportunity</option><option value="Additional Contact" @if($status == "Additional Contact") selected @endif>Additional Contact</option><option value="Disqualified" @if($status == "Disqualified") selected @endif>Disqualified</option>/select>')
                  ->make(true);
    }





    /**
     * Function : getUpdateappointment()
     * Purpose  : To register for an event for my event page
     * @author  : Agile Dev
     * @date-Created :19th Dec 2016
     */
    public function getUpdateappointmentattend($appointment_id, Request $request) 
    {
        $update_record = DB::table('appointment')
                        ->where('id',base64_decode($appointment_id))
                        ->update(array('status'=> 'Attempted', 'updated_date' => date('Y-m-d H:i:s')));
        if($update_record)
        {
          return redirect('admin/home')->with('message', "Record Updated");  
        }
        else
        {
          return redirect('admin/home')->with('error_message', "something went wrong");  
        }
    }



    /**
     * Function : getUpdateappointment()
     * Purpose  : To register for an event for my event page
     * @author  : Agile Dev
     * @date-Created :19th Dec 2016
     */
    public function getUpdateappointmentcontact($appointment_id, Request $request) 
    {
        $update_record = DB::table('appointment')
                        ->where('id',base64_decode($appointment_id))
                        ->update(array('status'=> 'Contacted', 'updated_date' => date('Y-m-d H:i:s')));
        if($update_record)
        {
          return redirect('admin/home')->with('message', "Record Updated");  
        }
        else
        {
          return redirect('admin/home')->with('error_message', "something went wrong");  
        }
    }

    /**
     * Function : getUpdateappointment()
     * Purpose  : To register for an event for my event page
     * @author  : Agile Dev
     * @date-Created :19th Dec 2016
     */
    public function getUpdateappointmentnew($appointment_id, Request $request) 
    {
        $update_record = DB::table('appointment')
                        ->where('id',base64_decode($appointment_id))
                        ->update(array('status'=> 'New Opportunity', 'updated_date' => date('Y-m-d H:i:s')));
        if($update_record)
        {
          return redirect('admin/home')->with('message', "Record Updated");  
        }
        else
        {
          return redirect('admin/home')->with('error_message', "something went wrong");  
        }
    }

    /**
     * Function : getUpdateappointment()
     * Purpose  : To register for an event for my event page
     * @author  : Agile Dev
     * @date-Created :19th Dec 2016
     */
    public function getUpdateappointmentadditional($appointment_id, Request $request) 
    {
        $update_record = DB::table('appointment')
                        ->where('id',base64_decode($appointment_id))
                        ->update(array('status'=> 'Additional Contact', 'updated_date' => date('Y-m-d H:i:s')));
        if($update_record)
        {
          return redirect('admin/home')->with('message', "Record Updated");  
        }
        else
        {
          return redirect('admin/home')->with('error_message', "something went wrong");  
        }
    }


    /**
     * Function : getUpdateappointment()
     * Purpose  : To register for an event for my event page
     * @author  : Agile Dev
     * @date-Created :19th Dec 2016
     */
    public function getUpdateappointmentdis($appointment_id, Request $request) 
    {
        $update_record = DB::table('appointment')
                        ->where('id',base64_decode($appointment_id))
                        ->update(array('status'=> 'Disqualified', 'updated_date' => date('Y-m-d H:i:s')));
        if($update_record)
        {
          return redirect('admin/home')->with('message', "Record Updated");  
        }
        else
        {
          return redirect('admin/home')->with('error_message', "something went wrong");  
        }
    }


    /**
     * Function : getUpdateappointment()
     * Purpose  : To register for an event for my event page
     * @author  : Agile Dev
     * @date-Created :19th Dec 2016
     */
    public function getUpdateappointmentnotattempted($appointment_id, Request $request) 
    {
        $update_record = DB::table('appointment')
                        ->where('id',base64_decode($appointment_id))
                        ->update(array('status'=> 'Not Attempted', 'updated_date' => date('Y-m-d H:i:s')));
        if($update_record)
        {
          return redirect('admin/home')->with('message', "Record Updated");  
        }
        else
        {
          return redirect('admin/home')->with('error_message', "something went wrong");  
        }
    }
    
}
