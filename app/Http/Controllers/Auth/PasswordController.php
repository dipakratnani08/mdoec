<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Validator;
use Input;
use Hash;
use DB;
use Mail;
use Auth;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    //###########################################################
    //Function : postForgotpassword
    //purpose : To send mail for forgot password link and procced
    //input : email
    //outpur : success/error
    //###########################################################
    public function postForgotpassword($data = 'null') {

        $validator = Validator::make(array(
                    'email' => trim(Input::get('email'))
                        ), array(
                    'email' => 'required|email'
        ));

        if ($validator->fails()) {
            $message = $validator->messages();
            return redirect("password/email")->withErrors($validator);
        } else {
            $remember_token = Hash::make(date('Y-m-d H:i:s'));

            /* Get the email addresss. */
            $emailid = trim(Input::get('email'));

            /* Update the user with forgot token. */
            $data_temp = array('remember_token' => $remember_token);
            $userinfo = DB::table('users')->select('name', 'id')->whereRaw("email = '$emailid'")->first();
            
            if ($userinfo) 
            {
                // For users
                $result = DB::table('users')->where('email', $emailid)->update($data_temp);
                if ($result) 
                {
                    $path = url('/');
                    $username = ucwords($userinfo->name);

                    try {
                            $test = Mail::send('emails.forgotpassword', ['key' => $remember_token, 'email' => $emailid, 'id' => $userinfo->id, 'url' => $path, 'user' => $username != "" && $username != NULL ? $username : 'User'], function($message) 
                            {
                                $message->from('noreply@mdoec.in', 'MDOEC');
                                $message->to(trim(Input::get('email')));
                                $message->subject('MDOEC : Forgot Password!');
                            });

                        $message = "If the email provided exists in our system, then you should get an email";
                    } catch (\Exception $e) {
                        $message = "Something went wrong. Please try again later";
                    }
                    return redirect("password/email")->with('message', $message);

                }
                else
                {
                    $message = "There is some error.Try again.!";
                    return redirect("password/email")->with('message', $message);
                }
            } 
            else
            {
                    $message = "There is some error.Try again.!";
                    return redirect("password/email")->with('message', $message);
            }
        }
    }

    //###########################################################
    //Function : getChangepass
    //purpose : To load change password form from email link
    //input : $remember_token,$email
    //outpur : success/error
    //###########################################################
    public function getChangepass() {

        $remember_token = trim(Input::get('key'));
        $email = trim(Input::get('email'));

        if (Auth::check()) {
            return redirect("/admin");
        } else {
            //Check User is exsit or not
            $chekcuser = DB::table('users')
                    ->where('remember_token', $remember_token)
                    ->where('email', $email)
                    ->first();


            if ($chekcuser) {
                $data['email'] = $email;
                return view('sso/changepassword', $data);
            } else {

                return view('sso/urlexpired');
            }
        }
    }




    //###########################################################
    //Function : getChangepass
    //purpose : To load change password form from email link
    //input : $forgot_token,$email
    //outpur : success/error
    //###########################################################
    public function postChangepassword() {

        $validator = Validator::make(array(
                    'email' => Input::get('email'),
                    'New Password' => Input::get('newpassword'),
                    'Confirm New Password' => Input::get('confirmedpassword'),), array('email' => 'required',
                    'New Password' => 'required|min:6',
                    'Confirm New Password' => 'required|min:6',));

        if ($validator->fails()) {
            $validator = $validator->messages()->first();
            return back()->withErrors($validator);
        } else {
            //To Check Token is avaialble or blank
            $usertoken = DB::table('users')->select('name', 'remember_token')->where('email', trim(Input::get('email')))->first();
            if (isset($usertoken) && ($usertoken->remember_token != "" || $usertoken->remember_token != NULL)) {
                $updateuserpass = DB::table('users')
                        ->where('email', trim(Input::get('email')))
                        ->update(array('remember_token' => '', 'password' => Hash::make(trim(Input::get('newpassword')))));

                if ($updateuserpass) {
                    $message = 'Your password has been updated! You can now login with your new password.';
                    return redirect("/admin")->with('message', $message);
                } else {
                    $data['error_message'] = 'Something went wrong .Please try again later';
                    return view('sso/passwordchanged', $data);
                }
            } else {
                return view('sso/urlexpired');
            }
        }
    }
}
