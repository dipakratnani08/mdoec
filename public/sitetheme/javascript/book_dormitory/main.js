$(function () {
    var form = $("#add_dormitory_booking").show();
    $("#wizard").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        transitionEffectSpeed: 500,
        onStepChanging: function (event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
//             return form.valid();
//            if (form.valid() == true)
//            {

                if (newIndex === 1) {
                    return form.valid();
                    $('.steps ul').addClass('step-2');
                } else {
                    $('.steps ul').removeClass('step-2');
                }
                if (newIndex === 2) {
                    return form.valid();
                    $('.steps ul').addClass('step-3');
                } else {
                    $('.steps ul').removeClass('step-3');
                }

                if (newIndex === 3) {
                    return form.valid();
                    $('.steps ul').addClass('step-4');
                    //$('.actions ul').addClass('step-last');
                } else {
                    $('.steps ul').removeClass('step-4');
                    //$('.actions ul').removeClass('step-last');
                }

                if (newIndex === 4) {
                    return form.valid();
                    $('.steps ul').addClass('step-5');
                    //$('.actions ul').addClass('step-last');
                } else {
                    $('.steps ul').removeClass('step-5');
                    $('.actions ul').removeClass('step-last');
                }
                return true;
            //}


        },
        labels: {
            finish: "Submit",
            next: "Next",
            previous: "Previous"
        },

        onFinished: function (event, currentIndex) {
            if (form.valid() == true)
            {
                var data = $("#add_dormitory_booking").serialize();
                var url = "savebookdormitory";
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}',
                            },
                            type: 'POST',
                            url: url,
                            data: data,
                            dataType: "json",
                            success: function (result) {
                                if (result.status == "false") {
                                    toastr.error(result.message, 'Error');
                                } else {
                                    var options = result.data;
                                    options.handler = function (response) {
                                        document.getElementById('razorpay_payment_id').value = response.razorpay_payment_id;
                                        document.getElementById('razorpay_signature').value = response.razorpay_signature;
                                        document.getElementById("add_dormitory_booking").submit();
                                        
                                        $.blockUI({ css: { 
                                            border: 'none', 
                                            padding: '15px', 
                                            backgroundColor: '#000', 
                                            '-webkit-border-radius': '10px', 
                                            '-moz-border-radius': '10px', 
                                            opacity: .5, 
                                            color: '#fff' 
                                        } }); 
                                    };

                                    // Boolean whether to show image inside a white frame. (default: true)
                                    //options.theme.image_padding = false;

                                    options.modal = {
                                        ondismiss: function () {
                                            console.log("This code runs when the popup is closed");
                                        },
                                        // Boolean indicating whether pressing escape key 
                                        // should close the checkout form. (default: true)
                                        escape: true,
                                        // Boolean indicating whether clicking translucent blank
                                        // space outside checkout form should close the form. (default: false)
                                        backdropclose: false
                                    };
                                    /*Begin : Block Form*/
                                    //$("#add_room_booking").css({pointerEvents: "none"});
                                    document.getElementById("add_dormitory_booking").style.pointerEvents = "none";
                                    /*Begin : Block Form*/
                                    var rzp = new Razorpay(options);
                                    rzp.open();
                                }
                                return false;
                                //window.location.reload();
                            },
                            error: function (result) {
                            }
                        });
                return false;
            }
            //$("#form").submit();
        }
    });
    // Custom Steps Jquery Steps
    $('.wizard > .steps li a').click(function () {
        $(this).parent().addClass('checked');
        $(this).parent().prevAll().addClass('checked');
        $(this).parent().nextAll().removeClass('checked');
    });
    // Custom Button Jquery Steps
    $('.forward').click(function () {
        $("#wizard").steps('next');
    })
    $('.backward').click(function () {
        $("#wizard").steps('previous');
    })
    // Checkbox
    $('.checkbox-circle label').click(function () {
        $('.checkbox-circle label').removeClass('active');
        $(this).addClass('active');
    })
})
